/*
 * Decompiled with CFR 0_118.
 */
package annotador.interview;

import annotador.interview.Conversor;
import annotador.interview.ConversorExcepion;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.Internationalization;
import annotador.utils.TEIConstants;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class XMLTEIConversor
implements Conversor {
    @Override
    public Node getInterview(String path, String codification, int numberOfInterviews) throws ConversorExcepion {
        try {
            FileInputStream fin = new FileInputStream(path);
            InputStreamReader isr = new InputStreamReader((InputStream)fin, codification);
            BufferedReader br = new BufferedReader(isr);
            String aux = null;
            String total = "";
            while ((aux = br.readLine()) != null) {
                total = total + aux + "\n";
            }
            Document doc = DOMUtils.StringToDOM(total, codification, Constants.TEI_NAMESPACE);
            if (doc.getDocumentElement().getNodeName().equals(TEIConstants.TEI_TAG_TEI)) {
                return doc.getDocumentElement();
            }
            throw new ConversorExcepion(Internationalization.CONVERSION_XML_ERROR);
        }
        catch (IOException e) {
            throw new ConversorExcepion(e.getMessage());
        }
        catch (Exception e) {
            throw new ConversorExcepion(e.getMessage());
        }
    }
}

