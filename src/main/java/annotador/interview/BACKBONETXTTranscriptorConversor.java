/*
 * Decompiled with CFR 0_118.
 */
package annotador.interview;

import annotador.interview.Conversor;
import annotador.interview.ConversorExcepion;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.Internationalization;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jdom.Attribute;
import org.jdom.Content;
import org.jdom.DocType;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.ProcessingInstruction;
import org.jdom.output.EscapeStrategy;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.w3c.dom.Node;

public class BACKBONETXTTranscriptorConversor
implements Conversor {
    @Override
    public Node getInterview(String path, String codification, int numberOsInterviews) throws ConversorExcepion {
        Logger logger = Logger.getLogger(BACKBONETXTTranscriptorConversor.class.getName());
        try {
            FileInputStream fr = new FileInputStream(path);
            InputStreamReader isr = new InputStreamReader((InputStream)fr, codification);
            BufferedReader br = new BufferedReader(isr);
            String temp = null;
            String interview = "";
            while ((temp = br.readLine()) != null) {
                interview = interview + temp + "\n";
            }
            Pattern metadataP = Pattern.compile("\\[METADATA\\].*\\[/METADATA\\]", 42);
            Matcher match = metadataP.matcher(interview);
            String metadata = null;
            boolean isMeta = false;
            if (match.find()) {
                isMeta = true;
                metadata = interview.substring(match.start(), match.end());
            }
            String body = null;
            Pattern bodyP = Pattern.compile("\\[BODY\\].*\\[/BODY]", 42);
            match = bodyP.matcher(interview);
            if (match.find()) {
                body = interview.substring(match.start(), match.end());
            } else {
                if (isMeta) {
                    throw new ConversorExcepion(Internationalization.TXT_CONVERSION_ERROR_2);
                }
                body = interview;
            }
            String teiNS = "http://www.tei-c.org/ns/1.0";
            Element tei = new Element("TEI", teiNS);
            Document document = new Document(tei, null, teiNS);
            document.addContent(new ProcessingInstruction("oxygen", "RNGSchema=\"http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng\" type=\"xml\""));
            document.detachRootElement();
            document.addContent(tei);
            Element teiHeader = new Element("teiHeader", teiNS);
            document.getRootElement().addContent(teiHeader);
            Element fileDesc = new Element("fileDesc", teiNS);
            teiHeader.addContent(fileDesc);
            Element titleStmt = new Element("titleStmt", teiNS);
            fileDesc.addContent(titleStmt);
            Element title = new Element("title", teiNS);
            titleStmt.addContent(title);
            Element editor = new Element("editor", teiNS);
            titleStmt.addContent(editor);
            Element respStmt = new Element("respStmt", teiNS);
            titleStmt.addContent(respStmt);
            Element name = new Element("name", teiNS);
            respStmt.addContent(name);
            Element resp = new Element("resp", teiNS);
            resp.setText("Transcriber");
            respStmt.addContent(resp);
            Element principal = new Element("principal", teiNS);
            titleStmt.addContent(principal);
            Element author = new Element("author", teiNS);
            titleStmt.addContent(author);
            Element funder = new Element("funder", teiNS);
            titleStmt.addContent(funder);
            Element sponsor = new Element("sponsor", teiNS);
            titleStmt.addContent(sponsor);
            Element publicationStmt = new Element("publicationStmt", teiNS);
            fileDesc.addContent(publicationStmt);
            Element idno = new Element("idno", teiNS);
            publicationStmt.addContent(idno);
            Element authority = new Element("authority", teiNS);
            publicationStmt.addContent(authority);
            Element address = new Element("address", teiNS);
            publicationStmt.addContent(address);
            Element addName = new Element("addName", teiNS);
            address.addContent(addName);
            addName.setText(Constants.WEB_PAGE_DIRECTION);
            Element sourceDesc = new Element("sourceDesc", teiNS);
            fileDesc.addContent(sourceDesc);
            Element recordingStmt = new Element("recordingStmt", teiNS);
            sourceDesc.addContent(recordingStmt);
            Element recording = new Element("recording", teiNS);
            recordingStmt.addContent(recording);
            recording.setAttribute("type", "video");
            Element profileDesc = new Element("profileDesc", teiNS);
            teiHeader.addContent(profileDesc);
            Element creation = new Element("creation", teiNS);
            profileDesc.addContent(creation);
            Element creationDate = new Element("date", teiNS);
            creation.addContent(creationDate);
            Element creationTime = new Element("time", teiNS);
            creation.addContent(creationTime);
            Element particDesc = new Element("particDesc", teiNS);
            Element person = null;
            Element settingDesc = new Element("settingDesc", teiNS);
            profileDesc.addContent(settingDesc);
            Element setting = new Element("setting", teiNS);
            settingDesc.addContent(setting);
            Element dateSetting = new Element("date", teiNS);
            setting.addContent(dateSetting);
            Element timeSetting = new Element("time", teiNS);
            setting.addContent(timeSetting);
            Element activity = new Element("activity", teiNS);
            setting.addContent(activity);
            Element locale = new Element("locale", teiNS);
            setting.addContent(locale);
            String mediaFile = "";
            String mediaServer = "";
            if (metadata != null) {
                Pattern attributes = Pattern.compile("[\\p{Alpha}| ]+:.*\n", 64);
                match = attributes.matcher(metadata);
            }
            Element language = new Element("language", teiNS);
            String variety = "";
            while (match.find()) {
                String atribute = metadata.substring(match.start(), match.end());
                String attName = atribute.substring(0, atribute.indexOf(58)).trim();
                String attValue = atribute.substring(atribute.indexOf(58) + 1, atribute.length() - 1).trim();
                if (attName.equalsIgnoreCase("Title")) {
                    title.setText(attValue);
                }
                if (attName.equalsIgnoreCase("Date Recording") && !attValue.equals("")) {
                    Element recordingDate = new Element("date", teiNS);
                    recording.addContent(recordingDate);
                    recordingDate.setAttribute("when", attValue);
                }
                if (attName.equalsIgnoreCase("Date Transcription")) {
                    creationDate.setText(attValue);
                    dateSetting.setText(attValue);
                }
                if (attName.equalsIgnoreCase("Interview Description")) {
                    Element notesStmt = new Element("notesStmt", teiNS);
                    if (fileDesc.getContentSize() > 1) {
                        fileDesc.addContent(fileDesc.getContentSize() - 2, notesStmt);
                    } else {
                        fileDesc.addContent(fileDesc.getContentSize() - 1, notesStmt);
                    }
                    Element note = new Element("note", teiNS);
                    notesStmt.addContent(note);
                    note.addContent(attValue);
                }
                if (attName.equalsIgnoreCase("Time")) {
                    creationTime.setText(attValue);
                    timeSetting.setText(attValue);
                }
                if (attName.equalsIgnoreCase("Locale")) {
                    locale.setText(attValue);
                }
                if (attName.equalsIgnoreCase("Activity")) {
                    activity.setText(attValue);
                }
                if (attName.equalsIgnoreCase("Principal Investigator")) {
                    principal.setText(attValue);
                }
                if (attName.equalsIgnoreCase("Researcher")) {
                    author.setText(attValue);
                }
                if (attName.equalsIgnoreCase("Transcriber")) {
                    name.setText(attValue);
                }
                if (attName.equalsIgnoreCase("Editor")) {
                    editor.setText(attValue);
                }
                if (attName.equalsIgnoreCase("Authority")) {
                    funder.setText(attValue);
                    authority.setText(attValue);
                    sponsor.setText(attValue);
                }
                if (attName.equalsIgnoreCase("ID")) {
                    idno.setText(attValue);
                }
                if (attName.equalsIgnoreCase("Language") && !attValue.equals("")) {
                    Element langUsage = new Element("langUsage", teiNS);
                    profileDesc.addContent(langUsage);
                    langUsage.addContent(language);
                    language.setAttribute("ident", attValue);
                    if (variety.equals("")) {
                        language.setText(attValue);
                    }
                }
                if (attName.equalsIgnoreCase("LanguageVariery")) {
                    language.setText(attValue);
                    variety = attValue;
                }
                if (attName.equalsIgnoreCase("MediaFileName") && !(mediaFile = attValue).equals("")) {
                    recording.setAttribute("rend", mediaServer + mediaFile);
                }
                // Adding duration from metadata
                if (attName.equalsIgnoreCase("Duration") && !attValue.equals("")) {
                    recording.setAttribute("dur", attValue);
                }

                if (attName.equalsIgnoreCase("MediaServerPath") && !(mediaServer = attValue).equals("") && !mediaFile.equals("")) {
                    recording.setAttribute("rend", mediaServer + mediaFile);
                }
                // Adding speaker from metadata
                if (attName.equalsIgnoreCase("Speaker")) {
                    person = new Element("person", teiNS);
                    particDesc.addContent(person);
                    if (!attValue.equals("")) {
                        person.setAttribute("rend", "speaker");
                    }
                }
                // Adding speaker name from metadata
                if (attName.equalsIgnoreCase("Speaker")) {
                    Element persName = new Element("persName", teiNS);
                    persName.setText(attValue);
                    person.addContent(persName);
                }
                if (attName.equalsIgnoreCase("role") && !attValue.equals("")) {
                    person.setAttribute("role", attValue);
                }
                if (attName.equalsIgnoreCase("sex")) {
                    if (attValue.equalsIgnoreCase("M")) {
                        person.setAttribute("sex", "1");
                    }
                    if (attValue.equalsIgnoreCase("F")) {
                        person.setAttribute("sex", "2");
                    }
                }
                if (attName.equalsIgnoreCase("age") && !attValue.equals("")) {
                    person.setAttribute("age", attValue);
                }
                if (!attName.equalsIgnoreCase("description")) continue;
                Element note = new Element("note", teiNS);
                note.setAttribute("type", "description");
                note.setText(attValue);
                person.addContent(note);
            }
            if (particDesc.getChildren("person", Namespace.getNamespace(teiNS)).size() != 0) {
                profileDesc.addContent(particDesc);
            }
            Element text = new Element("text", teiNS);
            document.getRootElement().addContent(text);
            Element bodyE = new Element("body", teiNS);
            text.addContent(bodyE);
            int start = -1;
            ArrayList<String> sections = new ArrayList<String>();
            for (int i = 0; i < body.length(); ++i) {
                if (body.charAt(i) != '#') continue;
                if (start != -1) {
                    sections.add(body.substring(start + 1, i));
                }
                start = i;
            }
            if (start == -1) {
                if (body.toUpperCase().lastIndexOf("[/BODY]") != -1) {
                    body = body.substring(0, body.toUpperCase().lastIndexOf("[/BODY]"));
                }
                sections.add(body);
            }
            HashMap uterreances = new HashMap();
            int j = 1;
            Iterator i$ = sections.iterator();
            while (i$.hasNext()) {
                int i2;
                String section = (String)i$.next();
                Element div = new Element("div", teiNS);
                div.setAttribute("type", "event");
                div.setAttribute(new Attribute("id", "R" + this.random() + "C" + numberOsInterviews + "D" + String.valueOf(j), Namespace.XML_NAMESPACE));
                bodyE.addContent(div);
                try {
                    if (section.contains("{")) {
                        String comment = section.substring(section.indexOf("{") + 1, section.indexOf("}"));
                        section = section.substring(0, section.indexOf("{")) + section.substring(section.indexOf("}") + 1);
                        Element note = new Element("note", teiNS);
                        note.setAttribute("type", Constants.RESERVED_WORD_NOTE_TYPE_TRANSCRIPTOR);
                        note.setText(comment);
                        div.addContent(note);
                    }
                }
                catch (IndexOutOfBoundsException i) {
                    throw new ConversorExcepion(Internationalization.ADD_INTERVIEW_ERROR_MESSAGE_2);
                }
                int numberOfTurn = 0;
                ArrayList<Integer> indexs = new ArrayList<Integer>();
                Matcher matcher = Pattern.compile("\\[\\d*\\...\\]\\s\\[\\d*\\...\\]").matcher(section);
                while(matcher.find()) {
                    int index = matcher.start();
                    indexs.add(index);
                    ++numberOfTurn;
                }
//                for (i2 = 0; i2 < section.length(); ++i2) {
//                    if (section.charAt(i2) != ':') continue;
//                    ++numberOfTurn;
//                    indexs.add(i2);
//                }
                start = -1;
                for (i2 = 0; i2 < numberOfTurn; ++i2) {
                    int aux = this.getIndexOfBeforeReturn(section, (Integer)indexs.get(i2));
                    if (start != -1 && aux > start) {
                        Element u = new Element("u", teiNS);
                        div.addContent(u);
                        u.setText(section.substring(start + 1, aux));
                    }
                    start = aux;
                }
                Element u = new Element("u", teiNS);
                div.addContent(u);
                u.setText(section.substring(start + 1));
                ++j;
            }
            List<Element> divs = bodyE.getChildren();
            for (Element div : divs) {
                List<Element> us = div.getChildren();
                ArrayList<String> globaltimes = new ArrayList<String>();
                for (Element u : us) {
                    String content = u.getText();
                    u.removeContent();
                    Collection<String> times = this.parseTurn(content, u);
                    globaltimes.addAll(times);
                }
                if (globaltimes.size() < 2) continue;
                String starttime = (String)globaltimes.toArray()[0];
                String endtime = (String)globaltimes.toArray()[globaltimes.size() - 1];
                if (us.size() > 1) {
                    Element timeStart = new Element("time", teiNS);
                    timeStart.setAttribute("from", starttime);
                    Element timeEnd = new Element("time", teiNS);
                    timeEnd.setAttribute("to", endtime);
                    ((Element)us.get(0)).addContent(0, timeStart);
                    ((Element)us.get(us.size() - 1)).addContent(0, timeEnd);
                    continue;
                }
                Element timeRange = new Element("time", teiNS);
                timeRange.setAttribute("from", starttime);
                timeRange.setAttribute("to", endtime);
                ((Element)us.get(0)).addContent(0, timeRange);
            }
            XMLOutputter xout = new XMLOutputter();
            ByteArrayOutputStream sw = new ByteArrayOutputStream();
            Format format = Format.getPrettyFormat();
            format.setEncoding(Constants.ENCODING);
            format.setIndent("   ");
            format.setEscapeStrategy(new EscapeStrategy(){

                @Override
                public boolean shouldEscape(char c) {
                    return false;
                }
            });
            format.setExpandEmptyElements(false);
            format.setTextMode(Format.TextMode.NORMALIZE);
            format.setLineSeparator("\n");
            xout.setFormat(format);
            sw.flush();
            xout.output(document, (OutputStream)sw);
            String out = sw.toString(Constants.ENCODING);
            out = out.replaceAll("xmlns:=", "xmlns=");
            out = out.replaceAll("\u201d", "\"");
            out = out.replaceAll("\u201c", "\"");
            out = out.replaceAll("&lt;", "<");
            out = out.replaceAll("&gt;", ">");
            FileOutputStream fw = new FileOutputStream(Constants.WORKING_PATH + "\\last_convert.xml", false);
            OutputStreamWriter osw = new OutputStreamWriter((OutputStream)fw, Constants.ENCODING);
            osw.write(out);
            osw.flush();
            osw.close();
            fw.close();
            org.w3c.dom.Document doc = null;
            try {
                doc = DOMUtils.StringToDOM(out, Constants.ENCODING, Constants.TEI_NAMESPACE);
            }
            catch (Exception e) {
                throw new ConversorExcepion(e.getMessage());
            }
            return doc.getDocumentElement();
        }
        catch (FileNotFoundException e1) {
            throw new ConversorExcepion(e1.getMessage());
        }
        catch (IOException e1) {
            throw new ConversorExcepion(e1.getMessage());
        }
    }

    public int random() {
        double rnd = Math.random();
        rnd *= 8999.0;
        return (int)Math.round(rnd += 1000.0);
    }

    public int getIndexOfBeforeReturn(String section, int colon) {
        boolean found = false;
        for (int i = colon; i >= 0 && !found; --i) {
            if (section.charAt(i) != '\n') continue;
            return i;
        }
        return 0;
    }

    public Collection<String> parseTurn(String turn, Element u) {
        String teiNS = "http://www.tei-c.org/ns/1.0";
        // Adding who attribute for correct work of search tool
        int index = -1;
        if (turn.indexOf(58) != -1 && turn.indexOf(58) < turn.indexOf('[')) {
            index = turn.indexOf(58);
        } else {
            u.setAttribute("who", "");
        }
        String first = turn.substring(0, index + 1);
        String text = turn.substring(index + 1);
        Pattern timeStamping = Pattern.compile("\\[[\\p{Digit}|,|\\.]*\\]");
        Matcher m = timeStamping.matcher(first);
        ArrayList<String> times = new ArrayList<String>();
        while (m.find()) {
            String time = first.substring(m.start(), m.end());
            time = time.substring(1, time.length() - 1);
            time = time.replaceAll(",", "").replaceAll("\\.", "");
            while (time.length() < 4) {
                time = "0" + time;
            }
            if (time.equals("0000")) {
                time = "0001";
            }
            times.add(time);
        }
        Pattern whoP = Pattern.compile("\\p{L}*:", 64);
        m = whoP.matcher(first);
        if (m.find()) {
            String who = first.substring(m.start(), m.end());
            who = who.substring(0, who.length() - 1);
            u.setAttribute("who", "#" + who);
        }
        text = Pattern.compile("<break/>", 2).matcher(text).replaceAll("<gap reason='break'/>");
        text = Pattern.compile("<cut/>", 2).matcher(text).replaceAll("<gap reason='cut'/>");
        text = Pattern.compile("<trunc>", 2).matcher(text).replaceAll("<abbr>");
        text = Pattern.compile("</trunc>", 2).matcher(text).replaceAll("</abbr>");
        text = Pattern.compile("\\(", 2).matcher(text).replaceAll("<add>");
        text = Pattern.compile("\\)", 2).matcher(text).replaceAll("</add>");
        StringBuilder sb = new StringBuilder();
        boolean open = false;
        for (int i = 0; i < text.length(); ++i) {
            if (text.charAt(i) == '\"') {
                open = !open;
                if (open) {
                    sb.append("<hi>");
                    continue;
                }
                sb.append("</hi>");
                continue;
            }
            sb.append(text.charAt(i));
        }
        text = sb.toString();
        boolean cent = true;
        while (cent) {
            Matcher altMatch = Pattern.compile("<alternative>", 2).matcher(text);
            cent = altMatch.find();
            if (!cent) continue;
            sb = new StringBuilder();
            int start = altMatch.start();
            sb.append(text.substring(0, start));
            int end = altMatch.end();
            Matcher altEndMatch = Pattern.compile("</alternative>", 2).matcher(text);
            if (altEndMatch.find()) {
                String inside = text.substring(end, altEndMatch.start());
                m = Pattern.compile("([\\p{L}\\p{M}\\p{Z}\\p{S}\\p{N}\\p{P}\\p{C}&&[^/\\\\]])+", 64).matcher(inside);
                int i2 = 0;
                while (m.find()) {
                    if (i2 == 0) {
                        sb.append("\n<choice>\n<orig>\n");
                        sb.append(inside.substring(m.start(), m.end()));
                        sb.append("\n</orig>");
                    } else {
                        sb.append("\n<corr>\n");
                        sb.append(inside.substring(m.start(), m.end()));
                        sb.append("\n</corr>");
                    }
                    ++i2;
                }
                if (i2 != 0) {
                    sb.append("\n</choice>\n");
                }
                sb.append(text.substring(altEndMatch.end()));
                text = sb.toString();
                continue;
            }
            cent = false;
        }
        String endEnd = null;
        if (times.size() >= 2) {
            endEnd = (String)times.remove(1);
        }
        m = Pattern.compile("\\[[\\p{Digit}|,|\\.]*\\]").matcher(text);
        while (m.find()) {
            String time = text.substring(m.start(), m.end());
            time = time.substring(1, time.length() - 1);
            time = time.replaceAll(",", "").replaceAll("\\.", "");
            while (time.length() < 4) {
                time = "0" + time;
            }
            if (time.equals("0000")) {
                time = "0001";
            }
            times.add(time);
        }
        text = m.replaceAll("");
        if (endEnd != null) {
            times.add(endEnd);
        }
        u.addContent(text);
        // Adding start and end time to every part which have timestamp
        u.setAttribute("start", times.get(0));
        u.setAttribute("end", times.get(1));
        return times;
    }

}

