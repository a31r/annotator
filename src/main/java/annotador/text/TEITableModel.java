/*
 * Decompiled with CFR 0_118.
 */
package annotador.text;

import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.TEIConstants;
import annotador.utils.TEINamespaceContext;
import java.awt.Dimension;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.swing.event.TableModelListener;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import table.model.AttributiveCellTableModel;
import table.model.CellAttribute;
import table.model.CellSpan;
import table.model.DefaultCellAttribute;

public class TEITableModel
extends AttributiveCellTableModel {
    Document document;
    int indexOfInterview;
    int cachedRowCount = -1;
    XPath xpath;
    HashMap sectionCache;
    HashMap speakerCache;
    HashMap appliedTagsCache;
    HashMap sectionTextCache;
    ArrayList<TableModelListener> listeners;

    public Document getDocument() {
        return this.document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public int getIndexOfInterview() {
        return this.indexOfInterview;
    }

    public void updateTable(int indexOfInterview) {
        this.indexOfInterview = indexOfInterview;
        this.caching();
    }

    private void caching() {
        Date start = new Date();
        this.sectionCache = new HashMap();
        this.cachedRowCount = -1;
        this.speakerCache = new HashMap();
        this.appliedTagsCache = new HashMap();
        this.sectionTextCache = new HashMap();
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new TEINamespaceContext());
        this.setCellAttribute(new DefaultCellAttribute());
        DefaultCellAttribute d = (DefaultCellAttribute)this.getCellAttribute();
        try {
            NodeList allUs = (NodeList)xpath.evaluate("//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:u|//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:p", this.document, XPathConstants.NODESET);
            int rows = allUs.getLength();
            d.setSize(new Dimension(this.getColumnCount(), rows));
            for (int i = 0; i < rows; ++i) {
                Node actualU = allUs.item(i);
                Node div = actualU.getParentNode();
                NodeList divChild = div.getChildNodes();
                int indexActualU = 0;
                int prevDiv = 0;
                int nextDiv = divChild.getLength();
                boolean cent = false;
                for (int j = 0; j < divChild.getLength(); ++j) {
                    if (divChild.item(j).getNodeName().equals(TEIConstants.TEI_TAG_DIV) && !cent) {
                        prevDiv = j;
                    }
                    if (divChild.item(j) == actualU) {
                        indexActualU = j;
                        cent = true;
                    }
                    if (!divChild.item(j).getNodeName().equals(TEIConstants.TEI_TAG_DIV) || !cent) continue;
                    nextDiv = j;
                }
                Node firstU = null;
                for (int j2 = prevDiv; j2 < nextDiv; ++j2) {
                    Node actual = divChild.item(j2);
                    if (!actual.getNodeName().equals(TEIConstants.TEI_TAG_U) && !actual.getNodeName().equals(TEIConstants.TEI_TAG_P)) continue;
                    firstU = actual;
                    break;
                }
                int totalChild = 0;
                for (int j3 = prevDiv; j3 < nextDiv; ++j3) {
                    Node actual = divChild.item(j3);
                    if (!actual.getNodeName().equals(TEIConstants.TEI_TAG_U) && !actual.getNodeName().equals(TEIConstants.TEI_TAG_P)) continue;
                    ++totalChild;
                }
                if (actualU != firstU) continue;
                int rowSpan = totalChild;
                int[] rowsCombine = new int[rowSpan];
                int[] colsCombine = new int[]{1};
                int[] colsCombine2 = new int[]{0};
                int index = 0;
                int j4 = i;
                while (j4 < i + rowSpan) {
                    rowsCombine[index++] = j4++;
                }
                d.combine(rowsCombine, colsCombine);
                d.combine(rowsCombine, colsCombine2);
            }
            if (Constants.DEBUG) {
                Date end = new Date();
                long gap = end.getTime() - start.getTime();
            }
        }
        catch (XPathExpressionException e) {
            // empty catch block
        }
    }

    public TEITableModel(Document document, int indexOfInterview) {
        super(0, 0);
        this.document = document;
        this.listeners = new ArrayList();
        this.indexOfInterview = indexOfInterview;
        this.xpath = XPathFactory.newInstance().newXPath();
        this.xpath.setNamespaceContext(new TEINamespaceContext());
        this.caching();
    }

    @Override
    public int getRowCount() {
        Date start = new Date();
        int rowCount = 0;
        if (this.cachedRowCount != -1) {
            rowCount = this.cachedRowCount;
        } else {
            try {
                NodeList allUs = (NodeList)this.xpath.evaluate("//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:u|//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:p", this.document, XPathConstants.NODESET);
                this.cachedRowCount = rowCount = allUs.getLength();
            }
            catch (XPathExpressionException e) {
                rowCount = 0;
            }
        }
        if (Constants.DEBUG) {
            Date end = new Date();
            long gap = end.getTime() - start.getTime();
            System.out.println("TEITableModel.getRowCount() -> " + gap + " ms");
        }
        return rowCount;
    }

    @Override
    public int getColumnCount() {
        if (this.document == null) {
            return 0;
        }
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            Date start = new Date();
            CellSpan span = (CellSpan)((Object)this.getCellAttribute());
            if (span.isVisible(rowIndex, columnIndex)) {
                Node node;
                String xpathExpression;
                Date end;
                if (columnIndex == 0) {
                    if (this.sectionCache.containsKey(rowIndex)) {
                        return this.sectionCache.get(rowIndex);
                    }
                    xpathExpression = "//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:u|//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:p";
                    NodeList u = (NodeList)this.xpath.evaluate(xpathExpression, this.document, XPathConstants.NODESET);
                    Node divSelected = u.item(rowIndex).getParentNode();
                    xpathExpression = "(//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:div)";
                    NodeList divs = (NodeList)this.xpath.evaluate(xpathExpression, this.document, XPathConstants.NODESET);
                    for (int i = 0; i < divs.getLength(); ++i) {
                        if (divs.item(i) != divSelected) continue;
                        if (Constants.DEBUG) {
                            Date end2 = new Date();
                            long gap = end2.getTime() - start.getTime();
                        }
                        this.sectionCache.put(rowIndex, i + 1);
                        return i + 1;
                    }
                }
                if (columnIndex == 1 && span.isVisible(rowIndex, columnIndex)) {
                    if (this.appliedTagsCache.containsKey(rowIndex)) {
                        return this.appliedTagsCache.get(rowIndex);
                    }
                    xpathExpression = "//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:u|//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:p";
                    NodeList nodeSet = (NodeList)this.xpath.evaluate(xpathExpression, this.document, XPathConstants.NODESET);
                    node = nodeSet.item(rowIndex);
                    if (node != null) {
                        if (Constants.DEBUG) {
                            end = new Date();
                            long gap = end.getTime() - start.getTime();
                            System.out.println("TEITableModel.getValueAt() -> " + gap + " ms");
                        }
                        this.appliedTagsCache.put(rowIndex, node);
                        return node;
                    }
                }
                if (columnIndex == 2) {
                    if (this.speakerCache.containsKey(rowIndex)) {
                        return this.speakerCache.get(rowIndex);
                    }
                    xpathExpression = "//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:u|//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:p";
                    NodeList list = (NodeList)this.xpath.evaluate(xpathExpression, this.document, XPathConstants.NODESET);
                    node = list.item(rowIndex);
                    String who = "";
                    if (node instanceof Element) {
                        Element e = (Element)node;
                        who = e.getAttribute(TEIConstants.TEI_ATTRIBUTE_WHO);
                    }
                    if (Constants.DEBUG) {
                        Date end3 = new Date();
                        long gap = end3.getTime() - start.getTime();
                        System.out.println("TEITableModel.getValueAt() -> " + gap + " ms");
                    }
                    this.speakerCache.put(rowIndex, who);
                    return who;
                }
                if (columnIndex == 3) {
                    if (this.sectionTextCache.containsKey(rowIndex)) {
                        return this.sectionTextCache.get(rowIndex);
                    }
                    xpathExpression = "//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:u|//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:p";
                    NodeList ns = (NodeList)this.xpath.evaluate(xpathExpression, this.document, XPathConstants.NODESET);
                    Node o = ns.item(rowIndex);
                    if (Constants.DEBUG) {
                        end = new Date();
                        long gap = end.getTime() - start.getTime();
                        System.out.println("TEITableModel.getValueAt() -> " + gap + " ms");
                    }
                    this.sectionTextCache.put(rowIndex, o);
                    return o;
                }
            }
        }
        catch (XPathExpressionException e) {
            System.out.println("TEITableModel.getValueAt() -> XPathExpressionException column(2)");
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0: {
                return Internationalization.COLUMN_NAME_NUM;
            }
            case 1: {
                return Internationalization.COLUMN_NAME_APPLIED_TAX;
            }
            case 2: {
                return Internationalization.COLUMN_NAME_SPEAKER;
            }
            case 3: {
                return Internationalization.COLUMN_NAME_TURN;
            }
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return Integer.class;
            }
            case 1: {
                return Node.class;
            }
            case 2: {
                return String.class;
            }
            case 3: {
                return Node.class;
            }
        }
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 2) {
            return false;
        }
        if (columnIndex == 0 || columnIndex == 1 || columnIndex == 3) {
            return true;
        }
        return true;
    }

    public Node getSection(int sectionNumber) throws XPathExpressionException {
        String xpathExpression = "//tei:TEI[" + this.indexOfInterview + "]/tei:text/tei:body//tei:div";
        NodeList divs = (NodeList)this.xpath.evaluate(xpathExpression, this.document, XPathConstants.NODESET);
        return divs.item(sectionNumber - 1);
    }
}

