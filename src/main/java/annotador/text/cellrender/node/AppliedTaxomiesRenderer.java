/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.node;

import annotador.gui.MainForm;
import annotador.text.cellrender.node.appliedtags.TaxonomiesListMouseListener;
import annotador.text.cellrender.node.appliedtags.TaxonomiesListRenderer;
import annotador.utils.Constants;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventObject;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.plaf.ListUI;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class AppliedTaxomiesRenderer
implements TableCellRenderer,
TableCellEditor {
    MainForm mainForm;
    JList list;
    JTable table;
    int row;
    String lastData;
    int startRow;
    int endRow;
    CellEditorListener cellEditorListener;

    public JList getList() {
        return this.list;
    }

    public void invalidateCache() {
        this.lastData = null;
    }

    public AppliedTaxomiesRenderer(JTable table, int row, MainForm mainForm) {
        this.mainForm = mainForm;
        this.list = new JList();
        this.list.setOpaque(true);
        this.list.setFont(table.getFont());
        this.list.setEnabled(true);
        this.list.addMouseListener(new TaxonomiesListMouseListener(this.list));
        this.table = table;
        this.row = row;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        String content;
        Node node;
        Element div;
        if (value instanceof Node && (node = (Node)value) != null && !(content = (div = (Element)node.getParentNode()).getAttribute(TEIConstants.TEI_ATTRIBUTE_DECLS)).equals(this.lastData)) {
            Pattern pattern = Pattern.compile("#\\p{Alnum}+", 64);
            Matcher matcher = pattern.matcher(content);
            Vector<String> datas = new Vector<String>();
            while (matcher.find()) {
                int start = matcher.start();
                int end = matcher.end();
                String token = content.substring(start + 1, end);
                datas.add(token);
            }
            this.list.setCellRenderer(new TaxonomiesListRenderer(table, row, div, this.mainForm));
            this.list.setListData(datas);
            this.lastData = content;
            if (table.getShowHorizontalLines() || table.getShowVerticalLines()) {
                this.list.setBorder(BorderFactory.createLineBorder(Color.decode(Constants.COLOR_BORDER)));
            }
            this.startRow = Utils.calculateStartSectionRow(row, table);
            this.endRow = Utils.calculateEndSectionRow(row, table);
            Dimension d = this.list.getUI().getPreferredSize(this.list);
            int sectionHeight = 0;
            for (int i = this.startRow; i <= this.endRow; ++i) {
                sectionHeight += table.getRowHeight(i);
            }
            if (d.height > sectionHeight) {
                int diff = d.height - sectionHeight;
                table.setRowHeight(this.endRow, table.getRowHeight(this.endRow) + diff);
            }
            if (d.width > table.getColumnModel().getColumn(1).getWidth()) {
                table.getColumnModel().getColumn(1).setWidth(d.width);
                table.getColumnModel().getColumn(1).setPreferredWidth(d.width);
                table.getColumnModel().getColumn(1).setMaxWidth(d.width);
                table.getColumnModel().getColumn(1).setMinWidth(d.width);
            }
        }
        return this.list;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        return this.list;
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        if (anEvent instanceof MouseEvent) {
            return true;
        }
        return false;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }

    @Override
    public boolean stopCellEditing() {
        if (this.cellEditorListener != null) {
            this.cellEditorListener.editingCanceled(new ChangeEvent(this));
        }
        return true;
    }

    @Override
    public void cancelCellEditing() {
        if (this.cellEditorListener != null) {
            this.cellEditorListener.editingCanceled(new ChangeEvent(this));
        }
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
        this.cellEditorListener = l;
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        this.cellEditorListener = null;
    }
}

