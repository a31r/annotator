/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.node.transcriptor;

import annotador.action.text.DeleteTitleAction;
import annotador.action.text.InsertKeywordActionListener;
import annotador.gui.MainForm;
import annotador.text.cellrender.node.transcriptor.TranscriptionRendererManager;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.TEIConstants;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextPane;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TranscriptionPopUp
extends JPopupMenu {
    public TranscriptionPopUp(Node utterance, JTextPane pane, MainForm mainForm, TranscriptionRendererManager manager, int row) {
        this.setDoubleBuffered(false);
        Element div = (Element)utterance.getParentNode();
        String decls = div.getAttribute(TEIConstants.TEI_ATTRIBUTE_DECLS);
        Pattern pattern = Pattern.compile("#\\p{Alnum}+", 64);
        Matcher matcher = pattern.matcher(decls);
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            String id = decls.substring(start + 1, end);
            String xpathExpression = "//tei:category[@xml:id=\"" + id + "\"]/tei:catDesc/text()";
            XPath xpath = XPathFactory.newInstance().newXPath();
            xpath.setNamespaceContext(new TEINamespaceContext());
            Document document = DocumentSingleton.getInstance();
            String description = null;
            try {
                description = xpath.evaluate(xpathExpression, document);
                if (description.equals("")) {
                    xpathExpression = "//tei:taxonomy[@xml:id=\"" + id + "\"]/@n";
                    description = xpath.evaluate(xpathExpression, document);
                }
            }
            catch (XPathExpressionException e) {
                e.printStackTrace();
            }
            JMenuItem labelActual = new JMenuItem(description, new ImageIcon(Utils.getReourceImage(mainForm, Constants.ICON_POPUP_CATEGORY)));
            labelActual.addActionListener(new InsertKeywordActionListener(id, utterance, mainForm, pane, manager, row));
            labelActual.setName(id);
            this.add(id, labelActual);
        }
        NodeList list = div.getChildNodes();
        Element titleElement = null;
        for (int i = 0; i < list.getLength(); ++i) {
            Element e;
            Node actual = list.item(i);
            if (!(actual instanceof Element) || !(e = (Element)actual).getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_HEAD)) continue;
            titleElement = e;
        }
        if (titleElement != null) {
            this.addSeparator();
            JMenuItem labelActual = new JMenuItem(Internationalization.DELETE_TITLE_POP_UP, new ImageIcon(Utils.getReourceImage(mainForm, Constants.TABLE_ICON_DELETE)));
            labelActual.addActionListener(new DeleteTitleAction(div, mainForm));
            this.add(labelActual);
        }
        this.addSeparator();
    }
}

