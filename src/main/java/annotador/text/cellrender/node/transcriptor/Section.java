/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.node.transcriptor;

import org.w3c.dom.Node;

public class Section {
    Node node;
    int visualStartIndex;
    int visualEndIndex;

    public Node getNode() {
        return this.node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public int getVisualStartIndex() {
        return this.visualStartIndex;
    }

    public void setVisualStartIndex(int visualStartIndex) {
        this.visualStartIndex = visualStartIndex;
    }

    public int getVisualEndIndex() {
        return this.visualEndIndex;
    }

    public void setVisualEndIndex(int visualEndIndex) {
        this.visualEndIndex = visualEndIndex;
    }
}

