/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.node.transcriptor;

import annotador.action.text.DeleteKeywordActionListener;
import annotador.gui.MainForm;
import annotador.text.cellrender.node.transcriptor.Section;
import annotador.text.cellrender.node.transcriptor.TranscriptionRendererManager;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.TEIConstants;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public class TranscriptionCaretListener
implements CaretListener {
    JTextPane pane;
    TranscriptionRendererManager manager;
    ArrayList<Component> buttons = new ArrayList();
    MainForm mainForm;
    int row;

    public TranscriptionCaretListener(JTextPane pane, TranscriptionRendererManager manager, MainForm mainForm, int row) {
        this.pane = pane;
        this.manager = manager;
        this.mainForm = mainForm;
        this.row = row;
    }

    @Override
    public void caretUpdate(CaretEvent e) {
        JPopupMenu menu = this.pane.getComponentPopupMenu();
        for (Component c : this.buttons) {
            menu.remove(c);
        }
        if (e.getDot() != 0 && e.getDot() != this.pane.getText().length()) {
            ArrayList<Section> keys = this.manager.getSections();
            for (Section s : keys) {
                Node actualNode;
                if (s.getVisualStartIndex() > e.getDot() || s.getVisualEndIndex() < e.getDot()) continue;
                ArrayList<Element> applied = new ArrayList<Element>();
                ArrayList listNodes = new ArrayList();
                listNodes = Utils.getInOrderRecursivo(1, this.manager.getNode(), listNodes);
                Iterator i$ = listNodes.iterator();
                while (i$.hasNext() && (actualNode = (Node)i$.next()) != s.getNode()) {
                    Element actualElement;
                    if (!(actualNode instanceof Element) || !(actualElement = (Element)actualNode).getNodeName().equals(TEIConstants.TEI_TAG_ANCHOR)) continue;
                    if (!actualElement.getAttribute(TEIConstants.TEI_ATTRIBUTE_NEXT).equals("")) {
                        applied.add(actualElement);
                        continue;
                    }
                    Element select = null;
                    for (Element actual : applied) {
                        String prev = actualElement.getAttribute(TEIConstants.TEI_ATTRIBUTE_PREV);
                        if (!actual.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID).equalsIgnoreCase(prev)) continue;
                        select = actual;
                    }
                    if (select == null) continue;
                    applied.remove(select);
                }
                for (Element appliedElement : applied) {
                    String atribId = appliedElement.getAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE);
                    String xpathExpression = "//tei:category[@xml:id=\"" + atribId + "\"]/tei:catDesc/text()";
                    XPath xpath = XPathFactory.newInstance().newXPath();
                    xpath.setNamespaceContext(new TEINamespaceContext());
                    Document document = DocumentSingleton.getInstance();
                    String description = null;
                    try {
                        description = xpath.evaluate(xpathExpression, document);
                        if (description.equals("")) {
                            xpathExpression = "//tei:taxonomy[@xml:id=\"" + atribId + "\"]/@n";
                            description = xpath.evaluate(xpathExpression, document);
                        }
                        Node node = null;
                        node = appliedElement.getNextSibling();
                        if (node instanceof Text && node != null) {
                            description = description + " - " + node.getTextContent().substring(0, Math.min(Integer.parseInt(Constants.NUMBER_CHAR_IN_DELETE_INDIVIDUAL_TAXONOMY), node.getTextContent().length()));
                            description = description + "...";
                        }
                        JMenuItem labelActual = new JMenuItem(description, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.TABLE_ICON_DELETE)));
                        labelActual.addActionListener(new DeleteKeywordActionListener(this.mainForm.getDocumentTable(), appliedElement, this.manager.getNode(), this.mainForm, this.row));
                        this.buttons.add(labelActual);
                        menu.add(labelActual);
                        menu.updateUI();
                    }
                    catch (XPathExpressionException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
    }

    public int getIndex(Node node, ArrayList list) {
        Iterator it = list.iterator();
        int i = 0;
        while (it.hasNext()) {
            if (it.next() == node) {
                return i;
            }
            ++i;
        }
        return -1;
    }
}

