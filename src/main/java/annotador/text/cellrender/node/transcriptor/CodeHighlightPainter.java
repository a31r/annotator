/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.node.transcriptor;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Shape;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;

public class CodeHighlightPainter
extends DefaultHighlighter.DefaultHighlightPainter {
    public CodeHighlightPainter(Color color) {
        super(color);
    }

    @Override
    public Shape paintLayer(Graphics g, int offs0, int offs1, Shape bounds, JTextComponent docPane, View view) {
        Rectangle r;
        if (offs0 == view.getStartOffset() && offs1 == view.getEndOffset()) {
            r = bounds instanceof Rectangle ? (Rectangle)bounds : bounds.getBounds();
        } else {
            Shape shape;
            try {
                shape = view.modelToView(offs0, Position.Bias.Forward, offs1, Position.Bias.Backward, bounds);
            }
            catch (BadLocationException e) {
                return null;
            }
            r = shape instanceof Rectangle ? (Rectangle)shape : shape.getBounds();
        }
        Color color = this.getColor();
        g.setColor(color);
        g.fillRoundRect(r.x, r.y + 1, r.width - 1, r.height - 1, 6, 6);
        g.fillRoundRect(r.x, r.y + 1, r.width - 1, r.height - 2, 6, 6);
        g.setColor(new Color(color.getRed() / 2, color.getGreen() / 2, color.getBlue() / 2));
        g.drawRoundRect(r.x, r.y + 1, r.width - 1, r.height - 1, 6, 6);
        g.drawRoundRect(r.x, r.y + 1, r.width - 1, r.height - 2, 6, 6);
        g.setColor(color);
        return r;
    }
}

