/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.node.transcriptor;

import annotador.gui.MainForm;
import annotador.text.TEITableModel;
import annotador.text.cellrender.node.transcriptor.CodeHighlightPainter;
import annotador.text.cellrender.node.transcriptor.Section;
import annotador.utils.ColorMap;
import annotador.utils.ShowedCategory;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.Color;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.table.TableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Highlighter;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import table.model.CellAttribute;
import table.model.CellSpan;

public class TranscriptionRendererManager {
    Node node;
    ArrayList<Section> sectionMap;
    String textoTotal;
    int row;
    MainForm mainForm;
    NodeList childrens;

    public TranscriptionRendererManager(MainForm mainForm, int row) {
        this.mainForm = mainForm;
        this.row = this.calculateDivRow(row);
    }

    public TranscriptionRendererManager(MainForm mainForm, Node node, int row) {
        this.mainForm = mainForm;
        this.node = node;
        this.row = this.calculateDivRow(row);
    }

    private int calculateDivRow(int uRow) {
        if (this.mainForm.getDocumentTable().getModel() instanceof TEITableModel) {
            TEITableModel model = (TEITableModel)this.mainForm.getDocumentTable().getModel();
            CellSpan span = (CellSpan)((Object)model.getCellAttribute());
            for (int i = uRow; i >= 0; --i) {
                if (!span.isVisible(i, 0)) continue;
                return i;
            }
        }
        return -1;
    }

    public ArrayList<Section> getSections() {
        return this.sectionMap;
    }

    public String getText() {
        return this.textoTotal;
    }

    public NodeList getChildrens() {
        return this.childrens;
    }

    public void initSections(Node node) {
        this.node = node;
        this.sectionMap = new ArrayList();
        this.textoTotal = "";
        this.childrens = node.getChildNodes();
        this.textoTotal = this.updateSections(node, this.textoTotal);
        for (Section s : this.sectionMap) {
        }
    }

    public String updateSections(Node node, String textoTotal) {
        NodeList childrens = node.getChildNodes();
        for (int index = 0; index < childrens.getLength(); ++index) {
            Node actual = childrens.item(index);
            Section map = new Section();
            int start = textoTotal.length();
            map.setVisualStartIndex(start);
            map.setNode(actual);
            if (actual instanceof Text) {
                Text textNode = (Text)actual;
                String texto = textNode.getNodeValue();
                String textoLimpio = texto.replaceAll("\n|\t", " ");
                if (!textoLimpio.trim().equals("")) {
                    textoTotal = textoLimpio.charAt(0) == ' ' && textoLimpio.charAt(textoLimpio.length() - 1) == ' ' ? textoTotal + " " + textoLimpio.trim() + " " : (textoLimpio.charAt(0) == ' ' ? textoTotal + " " + textoLimpio.trim() : (textoLimpio.charAt(textoLimpio.length() - 1) == ' ' ? textoTotal + textoLimpio.trim() + " " : textoTotal + textoLimpio.trim()));
                    map.setVisualEndIndex(textoTotal.length());
                    this.sectionMap.add(map);
                    continue;
                }
                if (!texto.equals(" ")) continue;
                textoTotal = textoTotal + " ";
                continue;
            }
            if (!(actual instanceof Element)) continue;
            Element element = (Element)actual;
            if (element.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_GAP)) {
                if (element.getAttribute(TEIConstants.TEI_ATTRIBUTE_REASON).equalsIgnoreCase(TEIConstants.TEI_ATTRIBUTE_REASON_BREAK)) {
                    textoTotal = textoTotal + "...";
                    map.setVisualEndIndex(textoTotal.length());
                    this.sectionMap.add(map);
                }
                if (!element.getAttribute(TEIConstants.TEI_ATTRIBUTE_REASON).equalsIgnoreCase(TEIConstants.TEI_ATTRIBUTE_REASON_CUT)) continue;
                textoTotal = textoTotal + "/\\";
                map.setVisualEndIndex(textoTotal.length());
                this.sectionMap.add(map);
                continue;
            }
            if (element.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_UNCLEAR)) {
                if (element.getChildNodes().getLength() == 0) {
                    textoTotal = textoTotal + "\\^";
                    map.setVisualEndIndex(textoTotal.length());
                    this.sectionMap.add(map);
                    continue;
                }
                textoTotal = textoTotal + "^";
                map.setVisualEndIndex(textoTotal.length());
                this.sectionMap.add(map);
                textoTotal = this.updateSections(element, textoTotal);
                map = new Section();
                start = textoTotal.length();
                map.setVisualStartIndex(start);
                map.setNode(actual);
                textoTotal = textoTotal + "^";
                map.setVisualEndIndex(textoTotal.length());
                this.sectionMap.add(map);
                continue;
            }
            if (element.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_ABBR)) {
                textoTotal = textoTotal + "{";
                map.setVisualEndIndex(textoTotal.length());
                this.sectionMap.add(map);
                textoTotal = this.updateSections(element, textoTotal);
                map = new Section();
                start = textoTotal.length();
                map.setVisualStartIndex(start);
                map.setNode(actual);
                textoTotal = textoTotal + "}";
                map.setVisualEndIndex(textoTotal.length());
                this.sectionMap.add(map);
                continue;
            }
            if (element.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_FOREIGN)) {
                textoTotal = textoTotal + "\u00b7";
                map.setVisualEndIndex(textoTotal.length());
                this.sectionMap.add(map);
                textoTotal = this.updateSections(element, textoTotal);
                map = new Section();
                start = textoTotal.length();
                map.setVisualStartIndex(start);
                map.setNode(actual);
                textoTotal = textoTotal + "\u00b7";
                map.setVisualEndIndex(textoTotal.length());
                this.sectionMap.add(map);
                continue;
            }
            if (element.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_HI)) {
                textoTotal = textoTotal + "\"";
                map.setVisualEndIndex(textoTotal.length());
                this.sectionMap.add(map);
                textoTotal = this.updateSections(element, textoTotal);
                map = new Section();
                start = textoTotal.length();
                map.setVisualStartIndex(start);
                map.setNode(actual);
                textoTotal = textoTotal + "\"";
                map.setVisualEndIndex(textoTotal.length());
                this.sectionMap.add(map);
                continue;
            }
            if (element.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_ADD)) {
                textoTotal = textoTotal + "(";
                map.setVisualEndIndex(textoTotal.length());
                this.sectionMap.add(map);
                textoTotal = this.updateSections(element, textoTotal);
                map = new Section();
                start = textoTotal.length();
                map.setVisualStartIndex(start);
                map.setNode(actual);
                textoTotal = textoTotal + ")";
                map.setVisualEndIndex(textoTotal.length());
                this.sectionMap.add(map);
                continue;
            }
            if (element.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CORR)) {
                textoTotal = textoTotal + "[";
                map.setVisualEndIndex(textoTotal.length());
                this.sectionMap.add(map);
                textoTotal = this.updateSections(element, textoTotal);
                map = new Section();
                start = textoTotal.length();
                map.setVisualStartIndex(start);
                map.setNode(actual);
                textoTotal = textoTotal + "]";
                map.setVisualEndIndex(textoTotal.length());
                this.sectionMap.add(map);
                continue;
            }
            textoTotal = this.updateSections(element, textoTotal);
        }
        return textoTotal;
    }

    public void insertHighLight(JTextPane pane) {
        try {
            Highlighter highlighter = pane.getHighlighter();
            ArrayList<Node> listNodes = new ArrayList();
            listNodes = Utils.getInOrderRecursivo(1, this.node, listNodes);
            ArrayList<String> appliedIds = new ArrayList<String>();
            HashMap<String, Integer> startIndexs = new HashMap<String, Integer>();
            for (Node actualNode : listNodes) {
                Element actualElement;
                if (!(actualNode instanceof Element) || !(actualElement = (Element)actualNode).getNodeName().equals(TEIConstants.TEI_TAG_ANCHOR)) continue;
                boolean abre = false;
                if (!actualElement.getAttribute(TEIConstants.TEI_ATTRIBUTE_NEXT).equals("")) {
                    Node nextSibling;
                    appliedIds.add(actualElement.getAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE));
                    abre = true;
                    for (nextSibling = actualElement.getNextSibling(); nextSibling != null && (nextSibling.getNodeName().equals(TEIConstants.TEI_TAG_ANCHOR) || nextSibling instanceof Text && nextSibling.getTextContent().trim().equals("")); nextSibling = nextSibling.getNextSibling()) {
                    }
                    try {
                        if (pane.getText().charAt(this.getFirstSection(nextSibling).getVisualStartIndex()) == ' ') {
                            startIndexs.put(actualElement.getAttribute(TEIConstants.TEI_ATTRIBUTE_NEXT), this.getFirstSection(nextSibling).getVisualStartIndex() + 1);
                        } else {
                            startIndexs.put(actualElement.getAttribute(TEIConstants.TEI_ATTRIBUTE_NEXT), this.getFirstSection(nextSibling).getVisualStartIndex());
                        }
                    }
                    catch (NullPointerException npe) {
                        System.out.println("TranscriptionRendererManager.insertHighLight() -> NullPoint en Start");
                    }
                }
                if (!abre && !appliedIds.isEmpty()) {
                    Node previousSibling;
                    for (previousSibling = actualElement.getPreviousSibling(); previousSibling != null && (previousSibling.getNodeName().equals(TEIConstants.TEI_TAG_ANCHOR) || previousSibling instanceof Text && previousSibling.getTextContent().trim().equals("")); previousSibling = previousSibling.getPreviousSibling()) {
                    }
                    try {
                        int endIndex = pane.getText().charAt(this.getLastSection(previousSibling).getVisualEndIndex() - 1) == ' ' ? this.getLastSection(previousSibling).getVisualEndIndex() - 1 : this.getLastSection(previousSibling).getVisualEndIndex();
                        String id = "";
                        id = actualElement.getAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE);
                        int indexOfInterview = 0;
                        TableModel tm = this.mainForm.getDocumentTable().getModel();
                        if (tm instanceof TEITableModel) {
                            TEITableModel teitm = (TEITableModel)tm;
                            indexOfInterview = teitm.getIndexOfInterview();
                        }
                        if (id.length() > 0 && ShowedCategory.isShowedAppliedCategory("I" + indexOfInterview + "R" + this.row + actualElement.getAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE))) {
                            Color color = ColorMap.getColor(id);
                            CodeHighlightPainter highlightPainter = new CodeHighlightPainter(color);
                            highlighter.addHighlight((Integer)startIndexs.get(actualElement.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID)), endIndex, highlightPainter);
                        }
                    }
                    catch (NullPointerException npe) {
                        System.out.println("TranscriptionRendererManager.insertHighLight() -> NullPointer en End");
                    }
                }
                if (actualElement.getAttribute(TEIConstants.TEI_ATTRIBUTE_PREV).equals("")) continue;
                appliedIds.remove(actualElement.getAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE));
                abre = false;
            }
        }
        catch (BadLocationException e) {
            e.printStackTrace();
        }
    }

    private Section getFirstSection(Node node) {
        for (Section s : this.sectionMap) {
            if (s.getNode() != node) continue;
            return s;
        }
        int i = 0;
        NodeList list = node.getChildNodes();
        if (i < list.getLength()) {
            return this.getFirstSection(list.item(i));
        }
        Node next = node.getNextSibling();
        if (next != null) {
            return this.getFirstSection(next);
        }
        return null;
    }

    private Section getLastSection(Node node) {
        Section select = null;
        for (Section s : this.sectionMap) {
            if (s.getNode() != node) continue;
            select = s;
        }
        if (select == null) {
            Node prev;
            NodeList list = node.getChildNodes();
            for (int i = list.getLength() - 1; i > -1 && (select = this.getLastSection(list.item(i))) == null; --i) {
            }
            if (select == null && (prev = node.getPreviousSibling()) != null) {
                return this.getLastSection(prev);
            }
        }
        return select;
    }

    public Node getNode() {
        return this.node;
    }
}

