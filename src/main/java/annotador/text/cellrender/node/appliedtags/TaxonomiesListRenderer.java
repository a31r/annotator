/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.node.appliedtags;

import annotador.gui.MainForm;
import annotador.text.TEITableModel;
import annotador.text.cellrender.node.appliedtags.DeleteAppliedCetegoryAction;
import annotador.text.cellrender.node.appliedtags.ShowAppliedCategoryAction;
import annotador.utils.ColorMap;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.ShowedCategory;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class TaxonomiesListRenderer
extends DefaultListCellRenderer {
    MainForm mainForm;
    JTable table;
    int maxWidth;
    int row;
    Node div;
    ImageIcon show;
    ImageIcon noShow;
    HashMap<String, Component> cache;

    public void invalidateCache() {
        this.cache = new HashMap();
    }

    public int getMaxWidth() {
        return this.maxWidth;
    }

    public void setMaxWidth(int maxWidth) {
        this.maxWidth = maxWidth;
    }

    public TaxonomiesListRenderer(JTable table, int row, Node div, MainForm mainForm) {
        this.mainForm = mainForm;
        this.table = table;
        this.row = row;
        this.div = div;
        this.cache = new HashMap();
        this.show = new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_SHOW_KEYWORD));
        this.noShow = new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_NOSHOW_KEYWORD));
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        JPanel panel;
        int width = 0;
        String token = (String)value;
        if (!this.cache.containsKey(token)) {
            String xpathExpression = "//tei:category[@xml:id=\"" + token + "\"]/tei:catDesc/text()";
            XPath xpath = XPathFactory.newInstance().newXPath();
            xpath.setNamespaceContext(new TEINamespaceContext());
            Document document = DocumentSingleton.getInstance();
            String description = null;
            try {
                description = xpath.evaluate(xpathExpression, document);
                if (description.equals("")) {
                    xpathExpression = "//tei:taxonomy[@xml:id=\"" + token + "\"]/@n";
                    description = xpath.evaluate(xpathExpression, document);
                }
            }
            catch (XPathExpressionException e) {
                e.printStackTrace();
            }
            final JLabel label = new JLabel(description, 2);
            label.setFont(list.getFont());
            label.setOpaque(true);
            label.addMouseListener(new DeleteAppliedCetegoryAction(this.table, list, this.div, this.mainForm, this.cache, this.row));
            if (isSelected) {
                label.setBackground(this.table.getBackground());
                label.setBorder(BorderFactory.createLineBorder(Color.MAGENTA));
            }
            Color color = ColorMap.getColor(token);
            label.setBackground(color);
            FlowLayout f = new FlowLayout();
            f.setAlignment(0);
            f.setHgap(0);
            f.setVgap(0);
            JPanel panel2 = new JPanel(f);
            panel2.setBackground(this.table.getBackground());
            ImageIcon icon = null;
            int indexOfInterview = 0;
            TableModel tm = this.mainForm.getDocumentTable().getModel();
            if (tm instanceof TEITableModel) {
                TEITableModel teitm = (TEITableModel)tm;
                indexOfInterview = teitm.getIndexOfInterview();
            }
            icon = ShowedCategory.isShowedAppliedCategory("I" + indexOfInterview + "R" + this.row + token) ? this.show : this.noShow;
            final JLabel box = new JLabel(icon);
            box.addMouseListener(new ShowAppliedCategoryAction(this.table, list, this.mainForm.getTaxonomyTree(), this.row));
            Rectangle2D bound = label.getFontMetrics(label.getFont()).getStringBounds(description, label.getGraphics());
            box.setOpaque(false);
            panel2.add(box);
            panel2.add(label);
            panel2.setOpaque(true);
            panel2.setEnabled(true);
            final ImageIcon icon1 = icon;
            panel2.addMouseListener(new MouseAdapter(){

                @Override
                public void mouseClicked(MouseEvent e) {
                    if (e.getX() <= icon1.getIconWidth()) {
                        for (MouseListener m : box.getMouseListeners()) {
                            m.mouseClicked(e);
                        }
                    } else {
                        for (MouseListener m : label.getMouseListeners()) {
                            m.mouseClicked(e);
                        }
                    }
                }
            });
            width = (int)(bound.getWidth() + (double)f.getHgap() + (double)box.getIcon().getIconWidth());
            panel2.setSize(width, panel2.getHeight());
            this.cache.put(token, panel2);
        }
        if ((width = (panel = (JPanel)this.cache.get(token)).getWidth()) > this.maxWidth) {
            this.maxWidth = width;
            if (this.table.getColumnModel().getColumn(1).getWidth() < this.maxWidth) {
                this.table.getColumnModel().getColumn(1).setWidth(this.maxWidth);
            }
            if (this.table.getColumnModel().getColumn(1).getMaxWidth() < this.maxWidth) {
                this.table.getColumnModel().getColumn(1).setMaxWidth(this.maxWidth);
            }
            if (this.table.getColumnModel().getColumn(1).getMinWidth() < this.maxWidth) {
                this.table.getColumnModel().getColumn(1).setMinWidth(this.maxWidth);
            }
            if (this.table.getColumnModel().getColumn(1).getPreferredWidth() < this.maxWidth) {
                this.table.getColumnModel().getColumn(1).setPreferredWidth(this.maxWidth);
            }
        }
        return panel;
    }

}

