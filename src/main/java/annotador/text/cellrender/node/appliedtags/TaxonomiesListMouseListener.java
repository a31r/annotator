/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.node.appliedtags;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class TaxonomiesListMouseListener
extends MouseAdapter {
    JList list;

    public TaxonomiesListMouseListener(JList list) {
        this.list = list;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Component c;
        super.mouseClicked(e);
        if (!this.list.isSelectionEmpty() && (c = this.list.getCellRenderer().getListCellRendererComponent(this.list, this.list.getSelectedValue(), this.list.getSelectedIndex(), true, true)).getMouseListeners() != null) {
            for (MouseListener m : c.getMouseListeners()) {
                m.mouseClicked(e);
            }
        }
    }
}

