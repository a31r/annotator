/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.node.appliedtags;

import annotador.gui.MainForm;
import annotador.text.cellrender.node.NodeTableRendererEditor;
import annotador.text.cellrender.node.TranscriptionRenderer;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellRenderer;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class DeleteAppliedCetegoryAction
extends MouseAdapter {
    JList list;
    JTable table;
    Node div;
    boolean carrera = false;
    MainForm mainForm;
    HashMap<String, Component> cache;
    int row;

    public DeleteAppliedCetegoryAction(JTable table, JList list, Node div, MainForm mainForm, HashMap<String, Component> cache, int row) {
        this.row = row;
        this.table = table;
        this.list = list;
        this.div = div;
        this.mainForm = mainForm;
        this.cache = cache;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        String id;
        super.mouseClicked(e);
        if (e.getClickCount() >= 2 && !this.carrera && (id = (String)this.list.getSelectedValue()) != null) {
            this.carrera = true;
            if (0 == JOptionPane.showConfirmDialog(this.mainForm.getTabbedPane(), Internationalization.DELETE_APPLIED_TAXONOMY_MESSAGE_1, Internationalization.WARNING_WORD, 2, 2, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)))) {
                ArrayList<Node> nodes = new ArrayList();
                nodes.add((Node)this.div);
                nodes = Utils.getInOrderRecursivo(1, this.div, nodes);
                for (Node node : nodes) {
                    String decls;
                    String attr;
                    if (!(node instanceof Element)) continue;
                    Element element = (Element)node;
                    if (element.getNodeName().equals(TEIConstants.TEI_TAG_ANCHOR) && (attr = element.getAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE)).intern().equals(id)) {
                        element.getParentNode().removeChild(element);
                    }
                    if (!element.getNodeName().equals(TEIConstants.TEI_TAG_U) && !element.getNodeName().equals(TEIConstants.TEI_TAG_DIV) || (decls = element.getAttribute(TEIConstants.TEI_ATTRIBUTE_DECLS)).equals("")) continue;
                    decls = decls.replaceAll("#" + id, "");
                    if (!(decls = decls.trim()).equals("")) {
                        element.setAttribute(TEIConstants.TEI_ATTRIBUTE_DECLS, decls);
                        continue;
                    }
                    element.removeAttribute(TEIConstants.TEI_ATTRIBUTE_DECLS);
                }
                DocumentSingleton.getInstance().normalizeDocument();
                Utils.setChangedCorpus(true);
                this.list.clearSelection();
                this.table.editingCanceled(new ChangeEvent(this));
                this.cache.remove(id);
                TableCellRenderer tc = this.table.getCellRenderer(this.row, 3);
                if (tc instanceof NodeTableRendererEditor) {
                    NodeTableRendererEditor tr = (NodeTableRendererEditor)tc;
                    int sectionRow = Utils.calculateStartSectionRow(this.row, this.table);
                    int end = Utils.calculateEndSectionRow(this.row, this.table);
                    for (int i = sectionRow; i <= end; ++i) {
                        if (tr.getUterranceRenderer().get(i) != null) {
                            tr.getUterranceRenderer().get(i).invalidateField();
                        }
                        this.table.prepareRenderer(this.table.getDefaultRenderer(Node.class), i, 3);
                    }
                }
                this.list.repaint();
                this.table.repaint();
            } else {
                this.list.clearSelection();
            }
            this.carrera = false;
        }
    }
}

