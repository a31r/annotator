/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.node.appliedtags;

import annotador.text.TEITableModel;
import annotador.text.cellrender.node.NodeTableRendererEditor;
import annotador.text.cellrender.node.TranscriptionRenderer;
import annotador.text.cellrender.node.appliedtags.TaxonomiesListRenderer;
import annotador.utils.ShowedCategory;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import org.w3c.dom.Node;

public class ShowAppliedCategoryAction
extends MouseAdapter {
    JList list;
    JTable table;
    JTree tree;
    int row;

    public ShowAppliedCategoryAction(JTable table, JList list, JTree tree, int row) {
        this.table = table;
        this.list = list;
        this.tree = tree;
        this.row = row;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        String id;
        super.mouseClicked(e);
        if (e.getClickCount() >= 2 && (id = (String)this.list.getSelectedValue()) != null) {
            boolean isShowed;
            int sectionRow = Utils.calculateStartSectionRow(this.row, this.table);
            int indexOfInterview = 0;
            TableModel tm = this.table.getModel();
            if (tm instanceof TEITableModel) {
                TEITableModel teitm = (TEITableModel)tm;
                indexOfInterview = teitm.getIndexOfInterview();
            }
            isShowed = !(isShowed = ShowedCategory.isShowedAppliedCategory("I" + indexOfInterview + "R" + sectionRow + id));
            ShowedCategory.setShowAppliedCategory("I" + indexOfInterview + "R" + sectionRow + id, isShowed);
            TaxonomiesListRenderer tlr = (TaxonomiesListRenderer)this.list.getCellRenderer();
            tlr.invalidateCache();
            TableCellRenderer tc = this.table.getCellRenderer(this.row, 3);
            if (tc instanceof NodeTableRendererEditor) {
                NodeTableRendererEditor tr = (NodeTableRendererEditor)tc;
                int end = Utils.calculateEndSectionRow(this.row, this.table);
                for (int i = sectionRow; i <= end; ++i) {
                    if (tr.getUterranceRenderer().get(i) != null) {
                        tr.getUterranceRenderer().get(i).invalidateField();
                    }
                    this.table.prepareRenderer(this.table.getDefaultRenderer(Node.class), i, 3);
                }
            }
            this.tree.invalidate();
            this.tree.repaint();
            this.table.invalidate();
            this.table.repaint();
            this.list.clearSelection();
            this.list.repaint();
        }
    }
}

