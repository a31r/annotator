/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.node;

import annotador.gui.MainForm;
import annotador.text.cellrender.node.AppliedTaxomiesRenderer;
import annotador.text.cellrender.node.TranscriptionRenderer;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.HashMap;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

public class NodeTableRendererEditor
implements TableCellRenderer,
TableCellEditor {
    HashMap<Integer, AppliedTaxomiesRenderer> appliedTaxomiesRenderer = new HashMap();
    HashMap<Integer, TranscriptionRenderer> uterranceRenderer = new HashMap();
    MainForm mainForm;
    int column;
    int row;
    JTable table;

    public HashMap<Integer, TranscriptionRenderer> getUterranceRenderer() {
        return this.uterranceRenderer;
    }

    public HashMap<Integer, AppliedTaxomiesRenderer> getAppliedTaxomiesRenderer() {
        return this.appliedTaxomiesRenderer;
    }

    public NodeTableRendererEditor(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (column == 1) {
            if (this.appliedTaxomiesRenderer.get(row) == null) {
                this.appliedTaxomiesRenderer.put(row, new AppliedTaxomiesRenderer(table, row, this.mainForm));
            }
            return this.appliedTaxomiesRenderer.get(row).getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
        if (column == 3) {
            if (this.uterranceRenderer.get(row) == null) {
                this.uterranceRenderer.put(row, new TranscriptionRenderer(this.mainForm, row));
            }
            return this.uterranceRenderer.get(row).getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
        return new JLabel("");
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.table = table;
        this.column = column;
        this.row = row;
        if (column == 1) {
            if (this.appliedTaxomiesRenderer.get(row) == null) {
                this.appliedTaxomiesRenderer.put(row, new AppliedTaxomiesRenderer(table, row, this.mainForm));
            }
            return this.appliedTaxomiesRenderer.get(row).getTableCellEditorComponent(table, value, isSelected, row, column);
        }
        if (column == 3) {
            if (this.uterranceRenderer.get(row) == null) {
                this.uterranceRenderer.put(row, new TranscriptionRenderer(this.mainForm, row));
            }
            return this.uterranceRenderer.get(row).getTableCellEditorComponent(table, value, isSelected, row, column);
        }
        return new JLabel("");
    }

    @Override
    public Object getCellEditorValue() {
        if (this.column == 1) {
            if (this.appliedTaxomiesRenderer.get(this.row) == null) {
                this.appliedTaxomiesRenderer.put(this.row, new AppliedTaxomiesRenderer(this.table, this.row, this.mainForm));
            }
            return this.appliedTaxomiesRenderer.get(this.row).getCellEditorValue();
        }
        if (this.column == 3) {
            if (this.uterranceRenderer.get(this.row) == null) {
                this.uterranceRenderer.put(this.row, new TranscriptionRenderer(this.mainForm, this.row));
            }
            return this.uterranceRenderer.get(this.row).getCellEditorValue();
        }
        return null;
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        if (anEvent instanceof MouseEvent) {
            MouseEvent mEvent = (MouseEvent)anEvent;
            JTable table = (JTable)anEvent.getSource();
            int column = table.getColumnModel().getColumnIndexAtX(mEvent.getX());
            int row = table.getSelectedRow();
            if (column == 1) {
                if (this.appliedTaxomiesRenderer.get(row) == null) {
                    this.appliedTaxomiesRenderer.put(row, new AppliedTaxomiesRenderer(table, row, this.mainForm));
                }
                return this.appliedTaxomiesRenderer.get(row).isCellEditable(anEvent);
            }
            if (column == 3) {
                if (this.uterranceRenderer.get(row) == null) {
                    this.uterranceRenderer.put(row, new TranscriptionRenderer(this.mainForm, row));
                }
                return this.uterranceRenderer.get(row).isCellEditable(anEvent);
            }
        }
        return false;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        if (anEvent instanceof MouseEvent) {
            MouseEvent mEvent = (MouseEvent)anEvent;
            JTable table = (JTable)anEvent.getSource();
            int column = table.getColumnModel().getColumnIndexAtX(mEvent.getX());
            int row = table.getSelectedRow();
            if (column == 1) {
                if (this.appliedTaxomiesRenderer.get(row) == null) {
                    this.appliedTaxomiesRenderer.put(row, new AppliedTaxomiesRenderer(table, row, this.mainForm));
                }
                return this.appliedTaxomiesRenderer.get(row).shouldSelectCell(anEvent);
            }
            if (column == 3) {
                if (this.uterranceRenderer.get(row) == null) {
                    this.uterranceRenderer.put(row, new TranscriptionRenderer(this.mainForm, row));
                }
                return this.uterranceRenderer.get(row).shouldSelectCell(anEvent);
            }
        }
        return false;
    }

    @Override
    public boolean stopCellEditing() {
        if (this.column == 1) {
            if (this.appliedTaxomiesRenderer.get(this.row) == null) {
                this.appliedTaxomiesRenderer.put(this.row, new AppliedTaxomiesRenderer(this.table, this.row, this.mainForm));
            }
            return this.appliedTaxomiesRenderer.get(this.row).stopCellEditing();
        }
        if (this.column == 3) {
            if (this.uterranceRenderer.get(this.row) == null) {
                this.uterranceRenderer.put(this.row, new TranscriptionRenderer(this.mainForm, this.row));
            }
            return this.uterranceRenderer.get(this.row).stopCellEditing();
        }
        return false;
    }

    @Override
    public void cancelCellEditing() {
        if (this.column == 1) {
            if (this.appliedTaxomiesRenderer.get(this.row) == null) {
                this.appliedTaxomiesRenderer.put(this.row, new AppliedTaxomiesRenderer(this.table, this.row, this.mainForm));
            }
            this.appliedTaxomiesRenderer.get(this.row).cancelCellEditing();
        }
        if (this.column == 3) {
            if (this.uterranceRenderer.get(this.row) == null) {
                this.uterranceRenderer.put(this.row, new TranscriptionRenderer(this.mainForm, this.row));
            }
            this.uterranceRenderer.get(this.row).cancelCellEditing();
        }
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
        if (this.column == 1) {
            if (this.appliedTaxomiesRenderer.get(this.row) == null) {
                this.appliedTaxomiesRenderer.put(this.row, new AppliedTaxomiesRenderer(this.table, this.row, this.mainForm));
            }
            this.appliedTaxomiesRenderer.get(this.row).addCellEditorListener(l);
        }
        if (this.column == 3) {
            if (this.uterranceRenderer.get(this.row) == null) {
                this.uterranceRenderer.put(this.row, new TranscriptionRenderer(this.mainForm, this.row));
            }
            this.uterranceRenderer.get(this.row).addCellEditorListener(l);
        }
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        if (this.column == 1) {
            if (this.appliedTaxomiesRenderer.get(this.row) == null) {
                this.appliedTaxomiesRenderer.put(this.row, new AppliedTaxomiesRenderer(this.table, this.row, this.mainForm));
            }
            this.appliedTaxomiesRenderer.get(this.row).removeCellEditorListener(l);
        }
        if (this.column == 3) {
            if (this.uterranceRenderer.get(this.row) == null) {
                this.uterranceRenderer.put(this.row, new TranscriptionRenderer(this.mainForm, this.row));
            }
            this.uterranceRenderer.get(this.row).removeCellEditorListener(l);
        }
    }

    public void removeCellRendererCache(int row, int colum) {
        if (colum == 1) {
            this.appliedTaxomiesRenderer.remove(row);
        }
        if (colum == 3) {
            this.uterranceRenderer.remove(row);
        }
    }

    public void invalidateAllCache() {
        this.appliedTaxomiesRenderer = new HashMap();
        this.uterranceRenderer = new HashMap();
    }
}

