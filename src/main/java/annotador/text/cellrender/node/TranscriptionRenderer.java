/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.node;

import annotador.gui.MainForm;
import annotador.text.cellrender.node.transcriptor.TranscriptionCaretListener;
import annotador.text.cellrender.node.transcriptor.TranscriptionPopUp;
import annotador.text.cellrender.node.transcriptor.TranscriptionRendererManager;
import annotador.utils.Constants;
import annotador.utils.TEIConstants;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.JViewport;
import javax.swing.border.Border;
import javax.swing.event.CaretListener;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.plaf.TextUI;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.Caret;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.View;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TranscriptionRenderer
implements TableCellRenderer,
TableCellEditor {
    TranscriptionRendererManager renderer;
    MainForm mainForm;
    int row;
    JTextPane jTextPane;
    CellEditorListener cellEditorListener;

    public TranscriptionRenderer(MainForm mainForm, int row) {
        this.renderer = new TranscriptionRendererManager(mainForm, row);
        this.mainForm = mainForm;
        this.row = row;
        this.cellEditorListener = null;
        this.jTextPane = null;
    }

    public void invalidateField() {
        this.jTextPane = null;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (value != null) {
            if (this.jTextPane == null) {
                this.jTextPane = new JTextPane();
                this.jTextPane.setEditable(false);
                this.jTextPane.setOpaque(false);
                this.jTextPane.setFocusable(true);
                this.jTextPane.setSelectionColor(Color.decode(Constants.SELECTION_COLOR));
                if (table.getShowHorizontalLines() || table.getShowVerticalLines()) {
                    this.jTextPane.setBorder(BorderFactory.createLineBorder(Color.decode(Constants.COLOR_BORDER)));
                } else {
                    this.jTextPane.setBorder(BorderFactory.createLineBorder(this.jTextPane.getBackground()));
                }
                Node utterance = (Node)value;
                this.renderer.initSections(utterance);
                StyledEditorKit kit = new StyledEditorKit();
                this.jTextPane.setEditorKit(kit);
                Document doc = kit.createDefaultDocument();
                DefaultHighlighter defaultHighlighter = new DefaultHighlighter();
                this.jTextPane.setHighlighter(defaultHighlighter);
                this.jTextPane.setBackground(table.getBackground());
                this.jTextPane.setComponentPopupMenu(new TranscriptionPopUp(utterance, this.jTextPane, this.mainForm, this.renderer, row));
                this.jTextPane.setDocument(doc);
                this.jTextPane.setText(this.renderer.getText());
                this.jTextPane.selectAll();
                SimpleAttributeSet attributeSetParagraph = new SimpleAttributeSet();
                StyleConstants.setFirstLineIndent(attributeSetParagraph, 0.0f);
                StyleConstants.setLeftIndent(attributeSetParagraph, 0.0f);
                StyleConstants.setRightIndent(attributeSetParagraph, 0.0f);
                StyleConstants.setAlignment(attributeSetParagraph, 3);
                StyleConstants.setSpaceAbove(attributeSetParagraph, 0.0f);
                StyleConstants.setSpaceBelow(attributeSetParagraph, 0.0f);
                this.jTextPane.setParagraphAttributes(attributeSetParagraph, true);
                SimpleAttributeSet attributeSetCharacter = new SimpleAttributeSet();
                StyleConstants.setFontFamily(attributeSetCharacter, table.getFont().getFamily());
                StyleConstants.setFontSize(attributeSetCharacter, table.getFont().getSize());
                this.jTextPane.setCharacterAttributes(attributeSetCharacter, true);
                if (Constants.RENDER_DOUBLE_COMMAS.equalsIgnoreCase("true")) {
                    Matcher m = Pattern.compile("[\u201c|\"](.+?)[\u201d|\"]").matcher(this.renderer.getText());
                    while (m.find()) {
                        this.jTextPane.select(m.start(), m.end());
                        attributeSetCharacter = new SimpleAttributeSet();
                        StyleConstants.setFontFamily(attributeSetCharacter, table.getFont().getFamily());
                        StyleConstants.setFontSize(attributeSetCharacter, table.getFont().getSize());
                        StyleConstants.setBold(attributeSetCharacter, true);
                        this.jTextPane.setCharacterAttributes(attributeSetCharacter, true);
                    }
                }
                this.renderer.insertHighLight(this.jTextPane);
                NodeList brothers = utterance.getParentNode().getChildNodes();
                for (int i = 0; i < brothers.getLength(); ++i) {
                    if (!brothers.item(i).getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_HEAD)) continue;
                    this.jTextPane.setToolTipText(brothers.item(i).getTextContent());
                }
            }
            JViewport pane = (JViewport)table.getParent();
            int columnWitdh = pane.getWidth();
            columnWitdh -= table.getColumnModel().getColumn(0).getWidth();
            columnWitdh -= table.getColumnModel().getColumn(1).getWidth();
            if (table.getColumnModel().getColumn(3).getWidth() != (columnWitdh -= table.getColumnModel().getColumn(2).getWidth())) {
                table.getColumnModel().getColumn(3).setWidth(columnWitdh);
                table.getColumnModel().getColumn(3).setPreferredWidth(columnWitdh);
                table.getColumnModel().getColumn(3).setMaxWidth(columnWitdh);
                table.getColumnModel().getColumn(3).setMinWidth(columnWitdh);
            }
            View v = this.jTextPane.getUI().getRootView(this.jTextPane);
            v.setSize(columnWitdh, 2.14748365E9f);
            int preferredHeight = (int)v.getPreferredSpan(1);
            this.jTextPane.setSize(columnWitdh, preferredHeight += Constants.ROW_INTERCELL_SPACING);
            this.jTextPane.setPreferredSize(new Dimension(columnWitdh, preferredHeight));
            this.jTextPane.setMaximumSize(new Dimension(columnWitdh, preferredHeight));
            this.jTextPane.setMinimumSize(new Dimension(columnWitdh, preferredHeight));
            if (table.getRowHeight(row) < preferredHeight) {
                table.setRowHeight(row, preferredHeight);
            }
            return this.jTextPane;
        }
        return new JLabel("");
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.jTextPane = null;
        JTextPane editor = (JTextPane)this.getTableCellRendererComponent(table, value, isSelected, true, row, column);
        this.jTextPane = null;
        editor.getCaret().setVisible(true);
        editor.getCaret().setBlinkRate(200);
        editor.addCaretListener(new TranscriptionCaretListener(editor, this.renderer, this.mainForm, row));
        return editor;
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        if (anEvent instanceof MouseEvent) {
            MouseEvent mouseEvent = (MouseEvent)anEvent;
            return true;
        }
        return false;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }

    @Override
    public boolean stopCellEditing() {
        this.jTextPane = null;
        if (this.cellEditorListener != null) {
            this.cellEditorListener.editingCanceled(new ChangeEvent(this));
        }
        return true;
    }

    @Override
    public void cancelCellEditing() {
        this.jTextPane = null;
        if (this.cellEditorListener != null) {
            this.cellEditorListener.editingCanceled(new ChangeEvent(this));
        }
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
        this.cellEditorListener = l;
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        this.cellEditorListener = null;
    }
}

