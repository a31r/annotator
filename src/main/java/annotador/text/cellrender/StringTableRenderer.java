/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender;

import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.Utils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class StringTableRenderer
extends DefaultTableCellRenderer {
    MainForm mainForm;
    int maxWidth;
    ImageIcon icon;
    HashMap<Integer, Component> cache;

    public int getMaxWidth() {
        return this.maxWidth;
    }

    public void setMaxWidth(int maxWidth) {
        this.maxWidth = maxWidth;
    }

    public StringTableRenderer(MainForm mainForm) {
        this.mainForm = mainForm;
        this.maxWidth = 0;
        this.icon = new ImageIcon(Utils.getReourceImage(mainForm, Constants.TABLE_ICON_USER));
        this.cache = new HashMap();
    }

    public void invalidateCache() {
        this.cache = new HashMap();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        int height;
        if (this.cache.get(row) == null) {
            if (value != null) {
                String speaker = (String)value;
                if (speaker.length() > 0) {
                    speaker = speaker.substring(1) + ":";
                }
                JLabel label = new JLabel(speaker, this.icon, 2);
                label.setHorizontalTextPosition(4);
                label.setVerticalTextPosition(0);
                label.setHorizontalAlignment(2);
                label.setVerticalAlignment(1);
                label.setOpaque(false);
                label.setBackground(table.getBackground());
                label.setFont(table.getFont());
                label.setIconTextGap(0);
                this.cache.put(row, label);
            } else {
                this.cache.put(row, new JLabel(""));
            }
        }
        JLabel label = (JLabel)this.cache.get(row);
        Rectangle2D bound = table.getFontMetrics(label.getFont()).getStringBounds(label.getText(), label.getGraphics());
        int width = (int)bound.getWidth();
        width = width + label.getIconTextGap() + this.icon.getIconWidth();
        if (label.getBorder() != null) {
            width = width + label.getBorder().getBorderInsets((Component)label).left + label.getBorder().getBorderInsets((Component)label).right;
        }
        if (width > this.maxWidth) {
            this.maxWidth = width;
            table.getColumnModel().getColumn(column).setWidth(this.maxWidth);
            table.getColumnModel().getColumn(column).setMaxWidth(this.maxWidth);
            table.getColumnModel().getColumn(column).setMinWidth(this.maxWidth);
            table.getColumnModel().getColumn(column).setPreferredWidth(this.maxWidth);
        }
        if ((height = (int)bound.getHeight()) < this.icon.getIconHeight()) {
            height = this.icon.getIconHeight();
        }
        if (height > table.getRowHeight(row)) {
            table.setRowHeight(row, height);
        }
        return label;
    }
}

