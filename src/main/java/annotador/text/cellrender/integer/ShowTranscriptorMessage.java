/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.integer;

import annotador.gui.MainForm;
import annotador.text.TEITableModel;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.PrintStream;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableModel;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ShowTranscriptorMessage
extends MouseAdapter {
    MainForm mainForm;
    int sectionNumber;

    public ShowTranscriptorMessage(MainForm mainForm, int sectionNumber) {
        this.mainForm = mainForm;
        this.sectionNumber = sectionNumber;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        TableModel tm;
        super.mouseClicked(e);
        if (e.getClickCount() >= 2 && (tm = this.mainForm.getDocumentTable().getModel()) instanceof TEITableModel) {
            TEITableModel ttm = (TEITableModel)tm;
            try {
                Node div = ttm.getSection(this.sectionNumber);
                Element divE = (Element)div;
                NodeList list = divE.getElementsByTagName(TEIConstants.TEI_TAG_NOTE);
                String nota = null;
                for (int i = 0; i < list.getLength(); ++i) {
                    Node n = list.item(i);
                    Element note = (Element)n;
                    String type = note.getAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE);
                    if (!type.equals(Constants.RESERVED_WORD_NOTE_TYPE_TRANSCRIPTOR)) continue;
                    nota = note.getTextContent();
                }
                if (nota == null) {
                    JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.TRANSCRIPTOR_MESSAGE_1, Internationalization.INFORMATION_WORD, 1, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_INFORM)));
                } else {
                    JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), new JScrollPane(new JTextArea(Internationalization.TRANSCRIPTOR_MESSAGE_2 + nota, 10, 50)), Internationalization.INFORMATION_WORD, 1, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_INFORM)));
                }
            }
            catch (XPathExpressionException e1) {
                System.out.println("ShowRecourceForm.mouseClicked -> EXCEPTIION XPATH");
            }
        }
    }
}

