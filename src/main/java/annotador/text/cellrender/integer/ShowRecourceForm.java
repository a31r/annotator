/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.integer;

import annotador.gui.MainForm;
import annotador.gui.SectionResourceManagementForm;
import annotador.text.TEITableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.PrintStream;
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Node;

public class ShowRecourceForm
extends MouseAdapter {
    MainForm mainForm;
    int sectionNumber;

    public ShowRecourceForm(MainForm mainForm, int sectionNumber) {
        this.mainForm = mainForm;
        this.sectionNumber = sectionNumber;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        TableModel tm;
        super.mouseClicked(e);
        if (e.getClickCount() >= 2 && (tm = this.mainForm.getDocumentTable().getModel()) instanceof TEITableModel) {
            TEITableModel ttm = (TEITableModel)tm;
            try {
                Node div = ttm.getSection(this.sectionNumber);
                SectionResourceManagementForm sectionResourceManagementForm = new SectionResourceManagementForm(this.mainForm, div, this.sectionNumber);
                sectionResourceManagementForm.mainDialog.setVisible(true);
            }
            catch (XPathExpressionException e1) {
                System.out.println("ShowRecourceForm.mouseClicked -> EXCEPTIION XPATH");
            }
        }
    }
}

