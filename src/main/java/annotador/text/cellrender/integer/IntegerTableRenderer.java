/*
 * Decompiled with CFR 0_118.
 */
package annotador.text.cellrender.integer;

import annotador.gui.MainForm;
import annotador.text.cellrender.integer.ShowRecourceForm;
import annotador.text.cellrender.integer.ShowTranscriptorMessage;
import annotador.utils.Constants;
import annotador.utils.Utils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.util.EventObject;
import java.util.HashMap;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class IntegerTableRenderer
extends DefaultTableCellRenderer
implements TableCellEditor {
    int maxWidth;
    HashMap<Integer, Component> cache;
    MainForm mainForm;
    ImageIcon pin;
    ImageIcon transcritor;
    CellEditorListener cellEditorListener;

    public int getMaxWidth() {
        return this.maxWidth;
    }

    public void setMaxWidth(int maxWidth) {
        this.maxWidth = maxWidth;
    }

    public IntegerTableRenderer(MainForm mainForm) {
        this.mainForm = mainForm;
        this.maxWidth = 0;
        this.cache = new HashMap();
        this.pin = new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_POPUP_CATEGORY));
        this.transcritor = new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_TRANSCRIPTOR));
    }

    public void invalidateCache() {
        this.cache = new HashMap();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        int height;
        if (this.cache.get(row) == null) {
            if (value != null) {
                Integer integer = (Integer)value;
                JLabel label = new JLabel(integer.toString(), this.pin, 2);
                label.setHorizontalAlignment(0);
                label.setHorizontalTextPosition(0);
                label.setVerticalTextPosition(1);
                label.setVerticalAlignment(1);
                label.setFont(table.getFont());
                label.setOpaque(false);
                label.setBackground(table.getBackground());
                label.addMouseListener(new ShowRecourceForm(this.mainForm, integer));
                JPanel p = new JPanel();
                p.setOpaque(false);
                p.setLayout(new BoxLayout(p, 1));
                p.add(label);
                JLabel l2 = new JLabel(this.transcritor);
                l2.addMouseListener(new ShowTranscriptorMessage(this.mainForm, integer));
                p.add(l2);
                this.cache.put(row, p);
            } else {
                JPanel p = new JPanel();
                p.setLayout(new BoxLayout(p, 1));
                p.setOpaque(false);
                p.add(new JLabel(" "));
                p.add(new JLabel(" "));
                this.cache.put(row, p);
            }
        }
        JPanel panel = (JPanel)this.cache.get(row);
        BoxLayout b = (BoxLayout)panel.getLayout();
        JLabel label = (JLabel)panel.getComponent(0);
        FontMetrics f = table.getFontMetrics(label.getFont());
        Rectangle2D bound = f.getStringBounds(label.getText(), label.getGraphics());
        int width = (int)bound.getWidth();
        width = width + label.getIconTextGap() + this.pin.getIconWidth();
        if (label.getBorder() != null) {
            width = width + label.getBorder().getBorderInsets((Component)label).left + label.getBorder().getBorderInsets((Component)label).right;
        }
        if (width > this.maxWidth) {
            this.maxWidth = width;
            table.getColumnModel().getColumn(column).setWidth(this.maxWidth);
            table.getColumnModel().getColumn(column).setMaxWidth(this.maxWidth);
            table.getColumnModel().getColumn(column).setMinWidth(this.maxWidth);
            table.getColumnModel().getColumn(column).setPreferredWidth(this.maxWidth);
        }
        if ((height = (int)b.preferredLayoutSize(panel).getHeight()) > table.getRowHeight(row)) {
            table.setRowHeight(row, height);
        }
        if (!Constants.ENABLE_INTEGER_CACHE.equalsIgnoreCase("TRUE")) {
            this.cache.remove(row);
        }
        return panel;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        return this.getTableCellRendererComponent(table, value, isSelected, true, row, column);
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        if (anEvent instanceof MouseEvent) {
            MouseEvent mouseEvent = (MouseEvent)anEvent;
            return true;
        }
        return false;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }

    @Override
    public boolean stopCellEditing() {
        if (this.cellEditorListener != null) {
            this.cellEditorListener.editingCanceled(new ChangeEvent(this));
        }
        return true;
    }

    @Override
    public void cancelCellEditing() {
        if (this.cellEditorListener != null) {
            this.cellEditorListener.editingCanceled(new ChangeEvent(this));
        }
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
        this.cellEditorListener = l;
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        this.cellEditorListener = null;
    }
}

