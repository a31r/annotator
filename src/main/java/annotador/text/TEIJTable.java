/*
 * Decompiled with CFR 0_118.
 */
package annotador.text;

import annotador.gui.MainForm;
import annotador.text.TEIDropTargetListener;
import annotador.text.TEITableModel;
import annotador.text.cellrender.StringTableRenderer;
import annotador.text.cellrender.integer.IntegerTableRenderer;
import annotador.text.cellrender.node.NodeTableRendererEditor;
import java.awt.Component;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetListener;
import javax.swing.JScrollPane;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import table.model.MultiSpanCellTable;

public class TEIJTable
extends MultiSpanCellTable {
    public TEIJTable(MainForm mainForm) {
        super(new TEITableModel(null, -1), mainForm.getDocumentScrollTable());
        NodeTableRendererEditor rendererAndEditor = new NodeTableRendererEditor(mainForm);
        this.setDefaultRenderer(Node.class, rendererAndEditor);
        this.setDefaultEditor(Node.class, rendererAndEditor);
        this.setDefaultRenderer(String.class, new StringTableRenderer(mainForm));
        this.setDefaultRenderer(Integer.class, new IntegerTableRenderer(mainForm));
        this.setDefaultEditor(Integer.class, new IntegerTableRenderer(mainForm));
        this.setOpaque(true);
        this.setAutoscrolls(false);
        this.setCellSelectionEnabled(true);
        this.setRowSelectionAllowed(false);
        this.setColumnSelectionAllowed(false);
        this.setAutoResizeMode(0);
        this.setDropTarget(new DropTarget(this, 1, new TEIDropTargetListener(mainForm), true));
        this.setDoubleBuffered(false);
    }

    public void invalidateGraphicsCache() {
        TableCellRenderer tc = this.getDefaultRenderer(Node.class);
        if (tc instanceof NodeTableRendererEditor) {
            NodeTableRendererEditor nt = (NodeTableRendererEditor)tc;
            nt.invalidateAllCache();
        }
        if ((tc = this.getDefaultRenderer(Integer.class)) instanceof IntegerTableRenderer) {
            IntegerTableRenderer nt = (IntegerTableRenderer)tc;
            nt.invalidateCache();
        }
        if ((tc = this.getDefaultRenderer(String.class)) instanceof StringTableRenderer) {
            StringTableRenderer nt = (StringTableRenderer)tc;
            nt.invalidateCache();
        }
    }
}

