/*
 * Decompiled with CFR 0_118.
 */
package annotador.text;

import annotador.text.TEIJTable;
import annotador.text.TEITableModel;
import annotador.text.cellrender.StringTableRenderer;
import annotador.text.cellrender.integer.IntegerTableRenderer;
import annotador.text.cellrender.node.AppliedTaxomiesRenderer;
import annotador.text.cellrender.node.NodeTableRendererEditor;
import annotador.text.cellrender.node.appliedtags.TaxonomiesListRenderer;
import annotador.utils.Constants;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import org.w3c.dom.Node;

public class TEITableComponentListener
extends ComponentAdapter {
    JTable table;

    public TEITableComponentListener(JTable documentTable) {
        this.table = documentTable;
    }

    @Override
    public void componentResized(ComponentEvent e) {
        Date start = new Date();
        super.componentResized(e);
        if (this.table.getModel() instanceof TEITableModel) {
            int rowCount = this.table.getModel().getRowCount();
            for (int i = 0; i < rowCount; ++i) {
                this.table.setRowHeight(i, 1);
            }
        }
        this.componentShown(e);
        if (Constants.DEBUG) {
            Date end = new Date();
            long gap = end.getTime() - start.getTime();
            System.out.println("TEITableComponentListener.componentResized() -> " + gap + " ms");
        }
    }

    @Override
    public void componentShown(ComponentEvent e) {
        Date start = new Date();
        super.componentShown(e);
        if (this.table instanceof TEIJTable) {
            TEIJTable teit = (TEIJTable)this.table;
            teit.invalidateGraphicsCache();
        }
        IntegerTableRenderer itr = (IntegerTableRenderer)this.table.getDefaultRenderer(Integer.class);
        itr.setMaxWidth(0);
        StringTableRenderer str = (StringTableRenderer)this.table.getDefaultRenderer(String.class);
        str.setMaxWidth(0);
        NodeTableRendererEditor ntr = (NodeTableRendererEditor)this.table.getDefaultRenderer(Node.class);
        HashMap<Integer, AppliedTaxomiesRenderer> hashMapAppliedTaxonomiesRenderers = ntr.getAppliedTaxomiesRenderer();
        for (Integer actualKey : hashMapAppliedTaxonomiesRenderers.keySet()) {
            AppliedTaxomiesRenderer appliedTaxomiesRenderer = hashMapAppliedTaxonomiesRenderers.get(actualKey);
            JList list = appliedTaxomiesRenderer.getList();
            if (!(list.getCellRenderer() instanceof TaxonomiesListRenderer)) continue;
            TaxonomiesListRenderer taxonomiesListRenderer = (TaxonomiesListRenderer)list.getCellRenderer();
            taxonomiesListRenderer.setMaxWidth(0);
        }
        this.table.revalidate();
        this.table.repaint();
        if (Constants.DEBUG) {
            Date end = new Date();
            long gap = end.getTime() - start.getTime();
            System.out.println("TEITableComponentListener.componentShown() -> " + gap + " ms");
        }
    }
}

