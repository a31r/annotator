/*
 * Decompiled with CFR 0_118.
 */
package annotador.text;

import annotador.gui.MainForm;
import annotador.text.TEIJTable;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Image;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetContext;
import java.awt.dnd.DropTargetDropEvent;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TEIDropTargetListener
extends DropTargetAdapter {
    MainForm mainForm;

    public TEIDropTargetListener(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void drop(DropTargetDropEvent dtde) {
        try {
            JTable jTable = (JTable)dtde.getDropTargetContext().getDropTarget().getComponent();
            Point point = dtde.getLocation();
            int accum = 0;
            int selectedRow = -1;
            boolean cent = true;
            for (int i = 0; i < jTable.getRowCount(); ++i) {
                if ((double)(accum += jTable.getRowHeight(i)) <= point.getY() || !cent) continue;
                cent = false;
                selectedRow = i;
            }
            Transferable transferable = dtde.getTransferable();
            boolean centi = false;
            for (DataFlavor dataFlavor : dtde.getCurrentDataFlavorsAsList()) {
                centi = true;
                if (dataFlavor.getMimeType().contains(Constants.MIME_TYPE_JAVA_OBJECT)) {
                    Node nodeTable;
                    Node nodeCategory = (Node)transferable.getTransferData(dataFlavor);
                    if (nodeCategory.getNodeName().equals(TEIConstants.TEI_TAG_TAXONOMY)) {
                        Element taxonomy = (Element)nodeCategory;
                        String taxonomyName = taxonomy.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID);
                        if (taxonomyName.equalsIgnoreCase(Constants.RESERVED_NAME_TO_TITLE_TAXONOMY)) {
                            nodeTable = ((Node)jTable.getModel().getValueAt(selectedRow, 3)).getParentNode();
                            NodeList list = nodeTable.getChildNodes();
                            Node titleElement = null;
                            for (int i2 = 0; i2 < list.getLength(); ++i2) {
                                Element e;
                                Node actual = list.item(i2);
                                if (!(actual instanceof Element) || !(e = (Element)actual).getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_HEAD)) continue;
                                titleElement = e;
                            }
                            String title = "";
                            title = titleElement != null ? JOptionPane.showInputDialog(this.mainForm.getTabbedPane(), Internationalization.INSERT_TITLE_MESSAGE_1, titleElement.getTextContent()) : JOptionPane.showInputDialog(this.mainForm.getTabbedPane(), (Object)Internationalization.INSERT_TITLE_MESSAGE_1);
                            if (!title.equals("")) {
                                dtde.acceptDrop(dtde.getSourceActions());
                                if (titleElement == null) {
                                    Element newTitle = nodeTable.getOwnerDocument().createElement(TEIConstants.TEI_TAG_HEAD);
                                    newTitle.setTextContent(title);
                                    if (list.getLength() >= 1) {
                                        nodeTable.insertBefore(newTitle, list.item(0));
                                    } else {
                                        nodeTable.appendChild(newTitle);
                                    }
                                } else {
                                    titleElement.setTextContent(title);
                                }
                                if (jTable instanceof TEIJTable) {
                                    TEIJTable table = (TEIJTable)jTable;
                                    table.invalidateGraphicsCache();
                                }
                                jTable.repaint();
                            } else {
                                dtde.rejectDrop();
                            }
                        } else if (selectedRow != -1) {
                            dtde.acceptDrop(dtde.getSourceActions());
                            Element elementCategory = (Element)nodeCategory;
                            String categoryId = elementCategory.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID);
                            Node nodeTable2 = (Node)jTable.getModel().getValueAt(selectedRow, 3);
                            Element divNode = (Element)nodeTable2.getParentNode();
                            String appliedId = divNode.getAttribute(TEIConstants.TEI_ATTRIBUTE_DECLS);
                            Matcher m = Pattern.compile("#" + categoryId + " |#" + categoryId + "$", 64).matcher(appliedId);
                            if (m.find()) {
                                JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.INSERT_TITLE_ERROR_2, Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE)));
                            } else {
                                appliedId = appliedId + " #" + categoryId;
                                appliedId = appliedId.trim();
                                divNode.setAttribute(TEIConstants.TEI_ATTRIBUTE_DECLS, appliedId);
                                jTable.repaint();
                            }
                        } else {
                            dtde.rejectDrop();
                        }
                    } else if (selectedRow != -1) {
                        dtde.acceptDrop(dtde.getSourceActions());
                        Element elementCategory = (Element)nodeCategory;
                        String categoryId = elementCategory.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID);
                        nodeTable = (Node)jTable.getModel().getValueAt(selectedRow, 3);
                        Element divNode = (Element)nodeTable.getParentNode();
                        String appliedId = divNode.getAttribute(TEIConstants.TEI_ATTRIBUTE_DECLS);
                        if (appliedId.contains(categoryId)) {
                            JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.INSERT_TITLE_ERROR_2, Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE)));
                        } else {
                            appliedId = appliedId + " #" + categoryId;
                            appliedId = appliedId.trim();
                            divNode.setAttribute(TEIConstants.TEI_ATTRIBUTE_DECLS, appliedId);
                            jTable.repaint();
                        }
                    } else {
                        dtde.rejectDrop();
                    }
                    Utils.setChangedCorpus(true);
                    continue;
                }
                dtde.rejectDrop();
            }
            if (!centi) {
                dtde.rejectDrop();
            }
        }
        catch (UnsupportedFlavorException e1) {
            e1.printStackTrace();
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}

