/*
 * Decompiled with CFR 0_118.
 */
package annotador.text;

import annotador.gui.MainForm;
import annotador.utils.Constants;
import java.awt.Cursor;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class TableMouseWhell
implements MouseWheelListener {
    MainForm mainForm;

    public TableMouseWhell(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        this.mainForm.getDocumentTable().setCursor(Cursor.getPredefinedCursor(12));
        this.mainForm.getDocumentScrollTable().getVerticalScrollBar().setValue(this.mainForm.getDocumentScrollTable().getVerticalScrollBar().getValue() + Integer.valueOf(Constants.BLOCK_INCREMENT_SCROLL) * e.getScrollAmount() * e.getWheelRotation());
        this.mainForm.getDocumentTable().setCursor(Cursor.getPredefinedCursor(0));
    }
}

