/*
 * Decompiled with CFR 0_118.
 */
package annotador.taxonomy.drag;

import annotador.utils.Constants;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import org.w3c.dom.Node;

public class NodeTransferable
implements Transferable {
    Node node;

    public NodeTransferable(Node node) {
        this.node = node;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        DataFlavor[] dataFlavors = new DataFlavor[]{new DataFlavor(Constants.MIME_TYPE_JAVA_OBJECT, "W3C DOM Node")};
        return dataFlavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        if (flavor.getMimeType().contains(Constants.MIME_TYPE_JAVA_OBJECT)) {
            return true;
        }
        return false;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        if (flavor.getMimeType().contains(Constants.MIME_TYPE_JAVA_OBJECT)) {
            return this.node;
        }
        throw new UnsupportedFlavorException(flavor);
    }
}

