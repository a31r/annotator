/*
 * Decompiled with CFR 0_118.
 */
package annotador.taxonomy.drag;

import annotador.taxonomy.drag.NodeTransferable;
import java.awt.datatransfer.Transferable;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import org.w3c.dom.Node;

public class NodeTransferHandler
extends TransferHandler {
    @Override
    public Transferable createTransferable(JComponent c) {
        JTree jTree = (JTree)c;
        DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode)jTree.getSelectionPath().getLastPathComponent();
        NodeTransferable transferable = new NodeTransferable((Node)defaultMutableTreeNode.getUserObject());
        return transferable;
    }

    @Override
    public int getSourceActions(JComponent c) {
        return 3;
    }
}

