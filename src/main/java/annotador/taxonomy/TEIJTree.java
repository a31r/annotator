/*
 * Decompiled with CFR 0_118.
 */
package annotador.taxonomy;

import annotador.gui.MainForm;
import annotador.taxonomy.cellrender.NodeTreeRenderer;
import annotador.taxonomy.drag.NodeTransferHandler;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.TreeCellRenderer;

public class TEIJTree
extends JTree {
    public TEIJTree(MainForm mainForm) {
        this.setCellRenderer(new NodeTreeRenderer(mainForm));
        this.setDragEnabled(true);
        this.setTransferHandler(new NodeTransferHandler());
    }
}

