/*
 * Decompiled with CFR 0_118.
 */
package annotador.taxonomy.cellrender;

import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class NodeTreeRendererBK
extends DefaultTreeCellRenderer {
    MainForm mainForm;

    public NodeTreeRendererBK(MainForm mainForm) {
        this.mainForm = mainForm;
        this.setOpenIcon(new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_TREE_OPEN)));
        this.setClosedIcon(new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_TREE_CLOSE)));
        this.setLeafIcon(new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_TREE_LEAF)));
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        DefaultMutableTreeNode graphicNode = (DefaultMutableTreeNode)value;
        if (graphicNode.getUserObject() instanceof Node) {
            Node node = (Node)graphicNode.getUserObject();
            if (node.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_TAXONOMY)) {
                Element taxonomy = (Element)node;
                String attribute = taxonomy.getAttribute(TEIConstants.TEI_ATTRIBUTE_N);
                return super.getTreeCellRendererComponent(tree, attribute, sel, expanded, leaf, row, hasFocus);
            }
            if (node.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CATEGORY)) {
                Element category = (Element)node;
                NodeList list = category.getElementsByTagName(TEIConstants.TEI_TAG_CATDESC);
                return super.getTreeCellRendererComponent(tree, list.item(0).getTextContent(), sel, expanded, leaf, row, hasFocus);
            }
            if (node.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CLASSDECL)) {
                String attribute = Internationalization.TAXONOMIES_WORD;
                return super.getTreeCellRendererComponent(tree, attribute, sel, expanded, leaf, row, hasFocus);
            }
        }
        return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
    }
}

