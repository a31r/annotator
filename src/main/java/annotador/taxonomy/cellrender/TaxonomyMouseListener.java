/*
 * Decompiled with CFR 0_118.
 */
package annotador.taxonomy.cellrender;

import annotador.gui.MainForm;
import annotador.gui.ModifyTaxonomyDialog;
import annotador.text.cellrender.node.AppliedTaxomiesRenderer;
import annotador.text.cellrender.node.NodeTableRendererEditor;
import annotador.text.cellrender.node.TranscriptionRenderer;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.ShowedCategory;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TaxonomyMouseListener
extends MouseAdapter {
    MainForm mainForm;

    public TaxonomyMouseListener(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        TreePath path = this.mainForm.getTaxonomyTree().getSelectionPath();
        if (path != null && path.getPathCount() != 1) {
            this.mainForm.getUpTaxonomy().setEnabled(true);
            this.mainForm.getDownTaxonomy().setEnabled(true);
        } else {
            this.mainForm.getUpTaxonomy().setEnabled(false);
            this.mainForm.getDownTaxonomy().setEnabled(false);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        TableCellEditor tableCellEditor = this.mainForm.getDocumentTable().getDefaultEditor(Node.class);
        tableCellEditor.stopCellEditing();
        if (e.getClickCount() == 2) {
            this.mainForm.getTaxonomyTree().setSelectionPath(this.mainForm.getTaxonomyTree().getClosestPathForLocation(e.getX(), e.getY()));
            TreePath path = this.mainForm.getTaxonomyTree().getSelectionPath();
            if (path != null) {
                if (e.getButton() == 3) {
                    if (Constants.IS_COLLABORATIVE_CORPUS != null && !Constants.IS_TAXONOMY_LOCKED) {
                        JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.TAXONOMIES_ADD_ERROR_MESSAGE_6, Internationalization.WARNING_WORD, 2, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)));
                        return;
                    }
                    DefaultMutableTreeNode treeNodeGraphic = (DefaultMutableTreeNode)path.getLastPathComponent();
                    Node node = (Node)treeNodeGraphic.getUserObject();
                    ModifyTaxonomyDialog modifyTaxonomyDialog = new ModifyTaxonomyDialog(this.mainForm, (Element)node);
                    modifyTaxonomyDialog.setVisible(true);
                }
                if (e.getButton() == 1) {
                    DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode)path.getLastPathComponent();
                    Node select = (Node)treeNode.getUserObject();
                    ArrayList<Node> nodes = new ArrayList<Node>();
                    nodes = this.processPreOrder(nodes, select);
                    for (Node node : nodes) {
                        String catID = this.processTreeNode(node);
                        if (catID == null) continue;
                        if (ShowedCategory.isShowedAnyInstanceOf(catID)) {
                            ShowedCategory.setShowOfCategory(catID, false);
                            continue;
                        }
                        ShowedCategory.setShowOfCategory(catID, true);
                    }
                }
                this.mainForm.getTaxonomyTree().invalidate();
                this.mainForm.getTaxonomyTree().repaint();
                for (int i = 0; i < this.mainForm.getDocumentTable().getModel().getRowCount(); ++i) {
                    TableCellRenderer tc = this.mainForm.getDocumentTable().getCellRenderer(i, 3);
                    if (!(tc instanceof NodeTableRendererEditor)) continue;
                    NodeTableRendererEditor tr = (NodeTableRendererEditor)tc;
                    if (tr.getUterranceRenderer().get(i) != null) {
                        tr.getUterranceRenderer().get(i).invalidateField();
                    }
                    if (tr.getAppliedTaxomiesRenderer().get(i) != null) {
                        tr.getAppliedTaxomiesRenderer().get(i).invalidateCache();
                    }
                    this.mainForm.getDocumentTable().prepareRenderer(this.mainForm.getDocumentTable().getDefaultRenderer(Node.class), i, 3);
                }
                this.mainForm.getDocumentTable().invalidate();
                this.mainForm.getDocumentTable().repaint();
            }
        }
    }

    private ArrayList<Node> processPreOrder(ArrayList<Node> list, Node current) {
        list.add(current);
        NodeList children = current.getChildNodes();
        for (int i = 0; i < children.getLength(); ++i) {
            this.processPreOrder(list, children.item(i));
        }
        return list;
    }

    private String processTreeNode(Node node) {
        String catID = null;
        if (node.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_TAXONOMY)) {
            String attribute;
            Element taxonomy = (Element)node;
            catID = attribute = taxonomy.getAttribute(TEIConstants.TEI_ATTRIBUTE_N);
        }
        if (node.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CATEGORY)) {
            Element category = (Element)node;
            catID = category.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID);
        }
        if (node.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CLASSDECL)) {
            String attribute;
            catID = attribute = Internationalization.TAXONOMIES_WORD;
        }
        return catID;
    }
}

