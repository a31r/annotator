/*
 * Decompiled with CFR 0_118.
 */
package annotador.taxonomy.cellrender;

import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.ShowedCategory;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class NodeTreeRenderer
extends DefaultTreeCellRenderer {
    ImageIcon show;
    ImageIcon noShow;
    MainForm mainForm;

    public NodeTreeRenderer(MainForm mainForm) {
        this.mainForm = mainForm;
        this.setOpenIcon(new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_TREE_OPEN)));
        this.setClosedIcon(new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_TREE_CLOSE)));
        this.setLeafIcon(new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_TREE_LEAF)));
        this.show = new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_SHOW_KEYWORD));
        this.noShow = new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_NOSHOW_KEYWORD));
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        DefaultMutableTreeNode graphicNode = (DefaultMutableTreeNode)value;
        Component c = null;
        String catID = null;
        if (graphicNode.getUserObject() instanceof Node) {
            Node node = (Node)graphicNode.getUserObject();
            if (node.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_TAXONOMY)) {
                String attribute;
                Element taxonomy = (Element)node;
                catID = attribute = taxonomy.getAttribute(TEIConstants.TEI_ATTRIBUTE_N);
                c = super.getTreeCellRendererComponent(tree, attribute, sel, expanded, leaf, row, hasFocus);
            }
            if (node.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CATEGORY)) {
                Element category = (Element)node;
                NodeList list = category.getElementsByTagName(TEIConstants.TEI_TAG_CATDESC);
                catID = category.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID);
                c = super.getTreeCellRendererComponent(tree, list.item(0).getTextContent(), sel, expanded, leaf, row, hasFocus);
            }
            if (node.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CLASSDECL)) {
                String attribute;
                catID = attribute = Internationalization.TAXONOMIES_WORD;
                c = super.getTreeCellRendererComponent(tree, attribute, sel, expanded, leaf, row, hasFocus);
            }
        } else {
            c = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        }
        JPanel p = new JPanel();
        JLabel layer = new JLabel();
        if (catID != null && ShowedCategory.isShowedAnyInstanceOf(catID)) {
            layer.setIcon(this.show);
        } else {
            layer.setIcon(this.noShow);
        }
        p.setAutoscrolls(false);
        p.setOpaque(false);
        p.setBorder(BorderFactory.createEmptyBorder());
        p.setDoubleBuffered(true);
        p.setLayout(new FlowLayout(0, 0, 0));
        p.add(layer);
        p.add(c);
        return p;
    }
}

