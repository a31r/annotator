/*
 * Decompiled with CFR 0_118.
 */
package annotador.taxonomy.cellrender;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.PrintStream;

public class SetLayerAction
implements ActionListener,
MouseListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("SHOW/HIDE LAYER");
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            this.actionPerformed(new ActionEvent(e.getSource(), e.getID(), "doubleClick"));
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}

