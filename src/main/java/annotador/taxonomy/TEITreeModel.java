/*
 * Decompiled with CFR 0_118.
 */
package annotador.taxonomy;

import annotador.utils.Constants;
import annotador.utils.TEIConstants;
import annotador.utils.TEINamespaceContext;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TEITreeModel
implements TreeModel {
    Node node;
    ArrayList<TreeModelListener> listeners = new ArrayList();

    public void setDocument(Document document) {
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();
            xpath.setNamespaceContext(new TEINamespaceContext());
            String xpathExpression = "//tei:classDecl";
            NodeList taxonomies = (NodeList)xpath.evaluate(xpathExpression, document, XPathConstants.NODESET);
            this.node = taxonomies.item(0);
        }
        catch (XPathExpressionException e) {
            try {
                Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
                Element classDel = doc.createElement(TEIConstants.TEI_TAG_CLASSDECL);
                doc.appendChild(classDel);
                this.node = doc.getDocumentElement();
            }
            catch (ParserConfigurationException e1) {
                e1.printStackTrace();
            }
        }
    }

    public TEITreeModel(Document document) {
        this.setDocument(document);
    }

    @Override
    public Object getRoot() {
        DefaultMutableTreeNode d = new DefaultMutableTreeNode(this.node);
        return d;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Date start = new Date();
        DefaultMutableTreeNode graphicNode = (DefaultMutableTreeNode)parent;
        Element parentElement = (Element)graphicNode.getUserObject();
        NodeList childs = parentElement.getChildNodes();
        Node seleccionado = null;
        int aux = 0;
        for (int i = 0; i < childs.getLength(); ++i) {
            Node element;
            if (!(childs.item(i) instanceof Element) || (element = childs.item(i)).getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CATDESC)) continue;
            if (aux == index) {
                seleccionado = childs.item(i);
            }
            ++aux;
        }
        DefaultMutableTreeNode newGraphicNode = new DefaultMutableTreeNode();
        newGraphicNode.setUserObject(seleccionado);
        if (Constants.DEBUG) {
            Date end = new Date();
            long gap = end.getTime() - start.getTime();
        }
        return newGraphicNode;
    }

    @Override
    public int getChildCount(Object parent) {
        Date start = new Date();
        DefaultMutableTreeNode graphicNode = (DefaultMutableTreeNode)parent;
        Element parentElement = (Element)graphicNode.getUserObject();
        NodeList childs = parentElement.getChildNodes();
        int aux = 0;
        for (int i = 0; i < childs.getLength(); ++i) {
            Element element;
            if (!(childs.item(i) instanceof Element) || (element = (Element)childs.item(i)).getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CATDESC)) continue;
            ++aux;
        }
        if (Constants.DEBUG) {
            Date end = new Date();
            long gap = end.getTime() - start.getTime();
        }
        return aux;
    }

    @Override
    public boolean isLeaf(Object node) {
        Date start = new Date();
        DefaultMutableTreeNode graphicNode = (DefaultMutableTreeNode)node;
        Node n = (Node)graphicNode.getUserObject();
        boolean cent = true;
        if (n != null) {
            NodeList childs = n.getChildNodes();
            for (int i = 0; i < childs.getLength(); ++i) {
                Element element;
                if (!(childs.item(i) instanceof Element) || (element = (Element)childs.item(i)).getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CATDESC)) continue;
                cent = false;
            }
        }
        if (Constants.DEBUG) {
            Date end = new Date();
            long gap = end.getTime() - start.getTime();
        }
        return cent;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        for (int i = 0; i < this.listeners.size(); ++i) {
            TreeModelListener listener = this.listeners.get(i);
            listener.treeStructureChanged(new TreeModelEvent((Object)this, path));
        }
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        Date start = new Date();
        DefaultMutableTreeNode graphicNode = (DefaultMutableTreeNode)parent;
        Element parentElement = (Element)graphicNode.getUserObject();
        NodeList childs = parentElement.getChildNodes();
        int aux = 0;
        for (int i = 0; i < childs.getLength(); ++i) {
            Node actual = childs.item(i);
            if (!(actual instanceof Element)) continue;
            if (actual == child) {
                if (Constants.DEBUG) {
                    Date end = new Date();
                    long gap = end.getTime() - start.getTime();
                }
                return aux;
            }
            ++aux;
        }
        if (Constants.DEBUG) {
            Date end = new Date();
            long gap = end.getTime() - start.getTime();
        }
        return -1;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        this.listeners.add(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        this.listeners.remove(l);
    }
}

