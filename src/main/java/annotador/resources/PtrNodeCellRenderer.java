/*
 * Decompiled with CFR 0_118.
 */
package annotador.resources;

import annotador.action.resources.DeleteResourceAction;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.util.EventObject;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import org.w3c.dom.Node;

public class PtrNodeCellRenderer
implements TableCellRenderer,
TableCellEditor {
    MainForm mainForm;
    JTable t;

    public PtrNodeCellRenderer(MainForm mainForm, JTable t) {
        this.mainForm = mainForm;
        this.t = t;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JButton button = new JButton(Internationalization.DELETE_WORD, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE_MINI)));
        button.addActionListener(new DeleteResourceAction((Node)value, this.t));
        return button;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.t = table;
        return this.getTableCellRendererComponent(table, value, isSelected, true, row, column);
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return true;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }

    @Override
    public boolean stopCellEditing() {
        this.t.editingStopped(new ChangeEvent(this));
        return true;
    }

    @Override
    public void cancelCellEditing() {
        this.t.editingCanceled(new ChangeEvent(this));
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
    }
}

