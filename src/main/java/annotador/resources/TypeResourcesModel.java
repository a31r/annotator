/*
 * Decompiled with CFR 0_118.
 */
package annotador.resources;

import annotador.utils.Constants;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

public class TypeResourcesModel
implements ComboBoxModel {
    Object selected;
    String[] types = new String[]{"weblink", "audio", "video", "transcription", Constants.VRP_TYPE, "picture", "exercise", "meta-picture"};

    @Override
    public void setSelectedItem(Object anItem) {
        this.selected = anItem;
    }

    @Override
    public Object getSelectedItem() {
        return this.selected;
    }

    @Override
    public int getSize() {
        return this.types.length;
    }

    @Override
    public Object getElementAt(int index) {
        return this.types[index];
    }

    @Override
    public void addListDataListener(ListDataListener l) {
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
    }
}

