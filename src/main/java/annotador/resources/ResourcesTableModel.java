/*
 * Decompiled with CFR 0_118.
 */
package annotador.resources;

import annotador.utils.Internationalization;
import annotador.utils.TEIConstants;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ResourcesTableModel
implements TableModel {
    Node linkGroup = null;
    TableModelListener tml;

    public Node getLinkGroup() {
        return this.linkGroup;
    }

    public void setLinkGroup(Node linkGroup) {
        this.linkGroup = linkGroup;
    }

    public ResourcesTableModel(Node div) {
        Element divE = (Element)div;
        NodeList list = divE.getChildNodes();
        for (int i = 0; i < list.getLength(); ++i) {
            Node n = list.item(i);
            if (!n.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_LINKGRP)) continue;
            this.linkGroup = n;
        }
    }

    @Override
    public int getRowCount() {
        if (this.linkGroup == null) {
            return 0;
        }
        int i = 0;
        for (int j = 0; j < this.linkGroup.getChildNodes().getLength(); ++j) {
            if (!this.linkGroup.getChildNodes().item(j).getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_PTR)) continue;
            ++i;
        }
        return i;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return Internationalization.COLUMN_NAME_NUMBER;
            }
            case 1: {
                return Internationalization.COLUMN_NAME_NAME;
            }
            case 2: {
                return Internationalization.COLUMN_NAME_TYPE;
            }
            case 3: {
                return Internationalization.COLUMN_NAME_DESCRIPTION;
            }
            case 4: {
                return Internationalization.COLUMN_NAME_ADDRESS;
            }
            case 5: {
                return Internationalization.COLUMN_NAME_ACTION;
            }
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return Integer.class;
            }
            case 1: {
                return String.class;
            }
            case 2: {
                return String.class;
            }
            case 3: {
                return String.class;
            }
            case 4: {
                return String.class;
            }
            case 5: {
                return Node.class;
            }
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 5) {
            return true;
        }
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Element ptr = (Element)this.linkGroup.getChildNodes().item(rowIndex);
        switch (columnIndex) {
            case 0: {
                return rowIndex;
            }
            case 1: {
                return ptr.getAttribute(TEIConstants.TEI_ATTRIBUTE_ANA);
            }
            case 2: {
                return ptr.getAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE);
            }
            case 3: {
                return ptr.getAttribute(TEIConstants.TEI_ATTRIBUTE_REND);
            }
            case 4: {
                return ptr.getAttribute(TEIConstants.TEI_ATTRIBUTE_TARGET);
            }
            case 5: {
                return ptr;
            }
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Element ptr = (Element)this.linkGroup.getChildNodes().item(rowIndex);
        if (ptr != null) {
            switch (columnIndex) {
                case 1: {
                    ptr.setAttribute(TEIConstants.TEI_ATTRIBUTE_ANA, (String)aValue);
                }
                case 2: {
                    ptr.setAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE, (String)aValue);
                }
                case 3: {
                    ptr.setAttribute(TEIConstants.TEI_ATTRIBUTE_REND, (String)aValue);
                }
                case 4: {
                    ptr.setAttribute(TEIConstants.TEI_ATTRIBUTE_TARGET, (String)aValue);
                }
            }
            ptr.getOwnerDocument().normalizeDocument();
        }
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        this.tml = l;
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        this.tml = null;
    }
}

