/*
 * Decompiled with CFR 0_118.
 */
package annotador.metadata;

import annotador.utils.TEIConstants;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PersonRenderer
implements ListCellRenderer {
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if (value != null) {
            Element person = (Element)value;
            NodeList l = person.getElementsByTagName(TEIConstants.TEI_TAG_PERSNAME);
            String name = "";
            if (l.getLength() > 0) {
                name = l.item(0).getTextContent();
            }
            JLabel label = new JLabel(name);
            label.setFont(list.getFont());
            return label;
        }
        return new JLabel("");
    }
}

