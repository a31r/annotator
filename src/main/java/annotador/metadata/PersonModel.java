/*
 * Decompiled with CFR 0_118.
 */
package annotador.metadata;

import annotador.utils.TEIConstants;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PersonModel
implements ComboBoxModel {
    Element partDesc;
    Node selected;
    ListDataListener l;

    public PersonModel(Node partDesc) {
        this.partDesc = (Element)partDesc;
    }

    @Override
    public void setSelectedItem(Object anItem) {
        this.selected = (Node)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return this.selected;
    }

    @Override
    public int getSize() {
        if (this.partDesc != null && this.partDesc.getElementsByTagName(TEIConstants.TEI_TAG_PERSON) != null) {
            return this.partDesc.getElementsByTagName(TEIConstants.TEI_TAG_PERSON).getLength();
        }
        return 0;
    }

    @Override
    public Object getElementAt(int index) {
        return this.partDesc.getElementsByTagName(TEIConstants.TEI_TAG_PERSON).item(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        this.l = l;
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        this.l = null;
    }
}

