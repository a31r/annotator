/*
 * Decompiled with CFR 0_118.
 */
package annotador.metadata;

import annotador.utils.TEIConstants;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class KeyWordModel
implements ComboBoxModel {
    Object selected;
    Node list;

    @Override
    public void setSelectedItem(Object anItem) {
        this.selected = anItem;
    }

    @Override
    public Object getSelectedItem() {
        return this.selected;
    }

    public KeyWordModel(Node list) {
        this.list = list;
    }

    @Override
    public int getSize() {
        int size = 0;
        if (this.list != null) {
            NodeList childs = this.list.getChildNodes();
            for (int i = 0; i < childs.getLength(); ++i) {
                if (!childs.item(i).getNodeName().equals(TEIConstants.TEI_TAG_ITEM)) continue;
                ++size;
            }
        }
        return size;
    }

    @Override
    public Object getElementAt(int index) {
        int size = 0;
        if (this.list != null) {
            NodeList childs = this.list.getChildNodes();
            for (int i = 0; i < childs.getLength(); ++i) {
                if (!childs.item(i).getNodeName().equals(TEIConstants.TEI_TAG_ITEM)) continue;
                if (size == index) {
                    return childs.item(i);
                }
                ++size;
            }
        }
        return null;
    }

    @Override
    public void addListDataListener(ListDataListener l) {
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
    }
}

