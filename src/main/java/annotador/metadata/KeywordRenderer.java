/*
 * Decompiled with CFR 0_118.
 */
package annotador.metadata;

import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import org.w3c.dom.Element;

public class KeywordRenderer
implements ListCellRenderer {
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if (value != null) {
            Element item = (Element)value;
            String name = item.getTextContent();
            JLabel label = new JLabel(name);
            label.setFont(list.getFont());
            return label;
        }
        return new JLabel("");
    }
}

