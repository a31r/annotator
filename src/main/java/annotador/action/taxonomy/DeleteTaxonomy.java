/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.taxonomy;

import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.text.TEIJTable;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.TEIConstants;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DeleteTaxonomy
implements ActionListener {
    MainForm mainForm;

    public DeleteTaxonomy(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    public MainForm getMainForm() {
        return this.mainForm;
    }

    public void setMainForm(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        TreePath path = this.mainForm.getTaxonomyTree().getSelectionPath();
        if (path == null) {
            JOptionPane.showMessageDialog(this.getMainForm().getTabbedPane(), Internationalization.TAXONOMIES_DELETE_ERROR_MESSAGE_1, Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE)));
        } else if (path.getPathCount() > 2 || path.getPathCount() == 2 && Constants.ALLOW_DELETE_TAXONOMY.equalsIgnoreCase("true")) {
            if (Constants.IS_COLLABORATIVE_CORPUS != null && !Constants.IS_TAXONOMY_LOCKED) {
                JOptionPane.showMessageDialog(this.getMainForm().getTabbedPane(), Internationalization.TAXONOMIES_ADD_ERROR_MESSAGE_6, Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_ADD)));
            } else if (0 == JOptionPane.showConfirmDialog(this.mainForm.getTabbedPane(), Internationalization.TAXONOMIES_DELETE_ERROR_MESSAGE_2, Internationalization.WARNING_WORD, 0, 2, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)))) {
                DefaultMutableTreeNode graphicNodeSelected = (DefaultMutableTreeNode)path.getLastPathComponent();
                ArrayList<Node> hijos = new ArrayList();
                hijos = Utils.getInOrderRecursivo(1, (Node)graphicNodeSelected.getUserObject(), hijos);
                for (Node hijo : hijos) {
                    this.realDeletion(hijo);
                }
                this.realDeletion((Node)graphicNodeSelected.getUserObject());
                graphicNodeSelected.setUserObject(null);
                graphicNodeSelected.removeFromParent();
                Utils.setChangedCorpus(true);
                this.mainForm.getTaxonomyTree().collapsePath(path.getParentPath());
                this.mainForm.getTaxonomyTree().getModel().valueForPathChanged(path.getParentPath(), graphicNodeSelected);
                this.mainForm.getTaxonomyTree().clearSelection();
                TEIJTable table = (TEIJTable)this.mainForm.getDocumentTable();
                table.invalidateGraphicsCache();
                table.repaint();
            }
        } else {
            JOptionPane.showMessageDialog(this.getMainForm().getTabbedPane(), Internationalization.TAXONOMIES_DELETE_ERROR_MESSAGE_3, Internationalization.RECTRICTION_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE)));
        }
    }

    private void realDeletion(Node nodeSelected) {
        if (nodeSelected instanceof Element) {
            Element element = (Element)nodeSelected;
            nodeSelected.getParentNode().removeChild(nodeSelected);
            if (Constants.IS_COLLABORATIVE_CORPUS != null) {
                String id = element.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID);
                try {
                    if (id != null && !id.equals("")) {
                        CMTManager.getCMT().deleteCategory(Constants.CMT_USER, id, Constants.IS_COLLABORATIVE_CORPUS);
                    }
                }
                catch (Exception e) {
                    JOptionPane.showMessageDialog(this.getMainForm().getTabbedPane(), e.getMessage(), Internationalization.ERROR_WORD, 0);
                }
            } else if (element.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CATEGORY) || element.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_TAXONOMY)) {
                String id;
                if (element.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CATEGORY)) {
                    id = element.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID);
                }
                if (element.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_TAXONOMY)) {
                    id = element.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID);
                }
                XPath xpath = XPathFactory.newInstance().newXPath();
                xpath.setNamespaceContext(new TEINamespaceContext());
                Document document = DocumentSingleton.getInstance();
                try {
                    Node actualE;
                    NodeList list = (NodeList)xpath.evaluate("//tei:anchor", document, XPathConstants.NODESET);
                    ArrayList<Element> deleted = new ArrayList<Element>();
                    for (int i = 0; i < list.getLength(); ++i) {
                        Node actualN = list.item(i);
                        if (!(actualN instanceof Element) || !(actualE = (Element)actualN).getAttributes().getNamedItem(TEIConstants.TEI_ATTRIBUTE_TYPE).equals(element.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID))) continue;
                        deleted.add((Element)actualE);
                    }
                    for (Node n : deleted) {
                        n.getParentNode().removeChild(n);
                    }
                    list = (NodeList)xpath.evaluate("//tei:div/@decls", document, XPathConstants.NODESET);
                    for (int i2 = 0; i2 < list.getLength(); ++i2) {
                        String value;
                        Node actualN = list.item(i2);
                        if (!(actualN instanceof Attr) || !(value = (actualE = (Attr)actualN).getNodeValue()).contains("#" + element.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID))) continue;
                        int start = value.indexOf("#" + element.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID));
                        int end = start + 1 + element.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID).length();
                        String newValue = "";
                        if (start > 0) {
                            newValue = value.substring(0, start);
                        }
                        newValue = newValue + value.substring(end);
                        if ((newValue = newValue.trim()).equals("")) {
                            actualE.getParentNode().getAttributes().removeNamedItem(actualE.getNodeName());
                            continue;
                        }
                        actualE.setNodeValue(newValue);
                    }
                    DocumentSingleton.getInstance().normalizeDocument();
                }
                catch (XPathExpressionException e1) {
                    JOptionPane.showMessageDialog(this.getMainForm().getTabbedPane(), Internationalization.TAXONOMIES_DELETE_ERROR_MESSAGE_3, Internationalization.RECTRICTION_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE)));
                }
            }
        }
    }
}

