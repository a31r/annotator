/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.taxonomy;

import annotador.gui.AddTaxonomyDialog;
import annotador.gui.MainForm;
import annotador.utils.ColorMap;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class AddTaxonomy
implements ActionListener {
    MainForm mainForm;

    public MainForm getMainForm() {
        return this.mainForm;
    }

    public void setMainForm(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    public AddTaxonomy(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        TreePath path = this.mainForm.getTaxonomyTree().getSelectionPath();
        if (path == null) {
            JOptionPane.showMessageDialog(this.getMainForm().getTabbedPane(), Internationalization.TAXONOMIES_ADD_ERROR_MESSAGE_1, Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE)));
        } else if (path.getPathCount() != 1 || path.getPathCount() == 1 && Constants.ALLOW_ADD_TAXONOMY.equalsIgnoreCase("true")) {
            if (Constants.IS_COLLABORATIVE_CORPUS != null && !Constants.IS_TAXONOMY_LOCKED) {
                JOptionPane.showMessageDialog(this.getMainForm().getTabbedPane(), Internationalization.TAXONOMIES_ADD_ERROR_MESSAGE_6, Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_ADD)));
            } else {
                AddTaxonomyDialog addTaxonomyDialog = new AddTaxonomyDialog(this.mainForm.getMainPanel(), this.mainForm);
                if (addTaxonomyDialog.isAcepted() && addTaxonomyDialog.getCategoryId() != null && !addTaxonomyDialog.getCategoryId().equals("") && addTaxonomyDialog.getCategoryName() != null && !addTaxonomyDialog.getCategoryName().equals("")) {
                    DefaultMutableTreeNode rootG = (DefaultMutableTreeNode)this.mainForm.getTaxonomyTree().getModel().getRoot();
                    Node root = (Node)rootG.getUserObject();
                    ArrayList<Node> list = new ArrayList();
                    list = Utils.getInOrderRecursivo(1, root, list);
                    boolean exist = false;
                    for (Node n : list) {
                        String id;
                        Element el;
                        if (!(n instanceof Element) || !(id = (el = (Element)n).getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID)).equalsIgnoreCase(addTaxonomyDialog.getCategoryId())) continue;
                        exist = true;
                    }
                    if (!exist) {
                        this.mainForm.getTaxonomyTree().expandPath(path);
                        DefaultMutableTreeNode graphicNodeSelected = (DefaultMutableTreeNode)path.getLastPathComponent();
                        Node nodeSelected = (Node)graphicNodeSelected.getUserObject();
                        if (path.getPathCount() != 1) {
                            Element newCategory = nodeSelected.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, TEIConstants.TEI_TAG_CATEGORY);
                            newCategory.setAttributeNS(Constants.XML_NAMESPACE, TEIConstants.TEI_ATTRIBUTE_XML_ID, addTaxonomyDialog.getCategoryId());
                            Element newCatDesc = nodeSelected.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, TEIConstants.TEI_TAG_CATDESC);
                            newCatDesc.setTextContent(addTaxonomyDialog.getCategoryName());
                            newCategory.appendChild(newCatDesc);
                            nodeSelected.appendChild(newCategory);
                        } else {
                            Element newTaxonomy = nodeSelected.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, TEIConstants.TEI_TAG_TAXONOMY);
                            newTaxonomy.setAttribute(TEIConstants.TEI_ATTRIBUTE_N, addTaxonomyDialog.getCategoryName());
                            newTaxonomy.setAttributeNS(Constants.XML_NAMESPACE, TEIConstants.TEI_ATTRIBUTE_XML_ID, addTaxonomyDialog.getCategoryId());
                            nodeSelected.appendChild(newTaxonomy);
                        }
                        Utils.setChangedCorpus(true);
                        ColorMap.addColor(addTaxonomyDialog.getCategoryId(), addTaxonomyDialog.getCategoryColor());
                        this.mainForm.getTaxonomyTree().getModel().valueForPathChanged(path, graphicNodeSelected);
                        this.mainForm.getTaxonomyTree().clearSelection();
                        this.mainForm.getDocumentTable().repaint();
                    } else {
                        JOptionPane.showMessageDialog(this.getMainForm().getTabbedPane(), Internationalization.TAXONOMIES_ADD_ERROR_MESSAGE_3, Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE)));
                    }
                } else if (addTaxonomyDialog.isAcepted()) {
                    JOptionPane.showMessageDialog(this.getMainForm().getTabbedPane(), Internationalization.TAXONOMIES_ADD_ERROR_MESSAGE_2, Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE)));
                }
            }
        } else {
            JOptionPane.showMessageDialog(this.getMainForm().getTabbedPane(), Internationalization.TAXONOMIES_ADD_ERROR_MESSAGE_4, Internationalization.RECTRICTION_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE)));
        }
    }
}

