/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.taxonomy;

import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.w3c.dom.Node;

public class DownTaxonomy
implements ActionListener {
    MainForm mainForm;

    public DownTaxonomy(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        TreePath path = this.mainForm.getTaxonomyTree().getSelectionPath();
        if (path != null) {
            Node parent;
            Node next;
            if (Constants.IS_COLLABORATIVE_CORPUS != null && !Constants.IS_TAXONOMY_LOCKED) {
                JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.TAXONOMIES_ADD_ERROR_MESSAGE_6, Internationalization.WARNING_WORD, 2, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)));
                return;
            }
            DefaultMutableTreeNode graphicsNode = (DefaultMutableTreeNode)path.getLastPathComponent();
            Node actual = (Node)graphicsNode.getUserObject();
            if (actual.getNodeName().equals(TEIConstants.TEI_TAG_CATEGORY)) {
                for (next = actual.getNextSibling(); next != null && !next.getNodeName().equals(TEIConstants.TEI_TAG_CATEGORY); next = next.getNextSibling()) {
                }
                if (next != null) {
                    parent = actual.getParentNode();
                    parent.removeChild(next);
                    parent.insertBefore(next, actual);
                    this.mainForm.getTaxonomyTree().getModel().valueForPathChanged(path.getParentPath(), graphicsNode);
                    Utils.setChangedCorpus(true);
                }
            }
            if (actual.getNodeName().equals(TEIConstants.TEI_TAG_TAXONOMY)) {
                for (next = actual.getNextSibling(); next != null && !next.getNodeName().equals(TEIConstants.TEI_TAG_TAXONOMY); next = next.getNextSibling()) {
                }
                if (next != null) {
                    parent = actual.getParentNode();
                    parent.removeChild(next);
                    parent.insertBefore(next, actual);
                    this.mainForm.getTaxonomyTree().getModel().valueForPathChanged(path.getParentPath(), graphicsNode);
                    Utils.setChangedCorpus(true);
                }
            }
        }
    }
}

