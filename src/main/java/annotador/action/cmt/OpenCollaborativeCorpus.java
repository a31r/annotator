/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.cmt;

import annotador.action.corpus.CloseCorpusFile;
import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class OpenCollaborativeCorpus
implements ActionListener {
    MainForm mainForm;

    public OpenCollaborativeCorpus(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Collection<String> corpusIDs = CMTManager.getCMT().getAllRegisteredCorpus();
            if (corpusIDs.size() > 0) {
                Object index = JOptionPane.showInputDialog(this.mainForm.getMainPanel(), Internationalization.CMT_RETRIEVE, Internationalization.CMT_RETRIEVE_TITLE, 1, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.MENU_ICON_CMT_RETRIEVE)), corpusIDs.toArray(), -1);
                if (index != null) {
                    CloseCorpusFile ccf = new CloseCorpusFile(this.mainForm);
                    ccf.actionPerformed(new ActionEvent(this, 1, "close"));
                    String corpusID = (String)index;
                    String taxonomyTree = CMTManager.getCMT().getTaxonomyTree(Constants.CMT_USER, corpusID);
                    DocumentSingleton.loadFromDOM(DOMUtils.StringToDOM(taxonomyTree, Constants.ENCODING, Constants.TEI_NAMESPACE));
                    Constants.IS_COLLABORATIVE_CORPUS = corpusID;
                    Constants.LAST_OPENED_CORPUS_FILE_PATH = null;
                    Utils.changeCorpusEvent(this.mainForm);
                }
            } else {
                JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.CMT_RETRIEVE_NOCORPUS);
            }
        }
        catch (Exception e1) {
            JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), e1.getMessage());
        }
    }
}

