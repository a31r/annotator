/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.cmt;

import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class PublishCollaborativeCorpus
implements ActionListener {
    MainForm mainForm;

    public PublishCollaborativeCorpus(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (DocumentSingleton.getInstance() != null) {
            if (Constants.IS_COLLABORATIVE_CORPUS != null) {
                JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.CMT_REPUBLISH, Internationalization.CMT_REPUBLISH_TITLE, 2);
                return;
            }
            String corpusID = (String)JOptionPane.showInputDialog(this.mainForm.getMainPanel(), Internationalization.CMT_PUBLISH_SURE, Internationalization.CMT_PUBLISH, 1, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.MENU_ICON_CMT_PUBLISH)), null, null);
            if (corpusID != null) {
                if (!corpusID.contains("_")) {
                    try {
                        String corpus = DOMUtils.DOMToString(DocumentSingleton.getInstance(), Constants.ENCODING);
                        CMTManager.getCMT().registerCorpus(Constants.CMT_USER, corpusID, corpus);
                        Constants.IS_COLLABORATIVE_CORPUS = corpusID;
                        JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.CMT_PUBLISH_CONFIR);
                    }
                    catch (Exception e1) {
                        JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), e1.getMessage());
                    }
                } else {
                    JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.CMT_PUBLISH_CHAR, Internationalization.CMT_PUBLISH_CHAR_TITLE, 2);
                }
            }
        } else {
            JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.CMT_PUBLISH_NOCORPUS);
        }
    }
}

