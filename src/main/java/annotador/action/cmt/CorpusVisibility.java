/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.cmt;

import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class CorpusVisibility
implements ActionListener {
    MainForm mainForm;

    public CorpusVisibility(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Collection<String> corpusIDs = CMTManager.getCMT().getAllRegisteredCorpus();
            ArrayList<Boolean> visibility = new ArrayList<Boolean>();
            for (String corpusID : corpusIDs) {
                visibility.add(false);
            }
            Collection<String> publics = CMTManager.getCMT().getAllPublicCorpus();
            block3 : for (String pub : publics) {
                int i = 0;
                for (String corpusID2 : corpusIDs) {
                    if (corpusID2.equals(pub)) {
                        visibility.set(i, true);
                        continue block3;
                    }
                    ++i;
                }
            }
            ArrayList<String> rendering = new ArrayList<String>();
            int i = 0;
            for (String corpusID3 : corpusIDs) {
                if (((Boolean)visibility.get(i)).booleanValue()) {
                    rendering.add("Public - " + corpusID3);
                } else {
                    rendering.add("Private - " + corpusID3);
                }
                ++i;
            }
            if (corpusIDs.size() > 0) {
                Object index = JOptionPane.showInputDialog(this.mainForm.getMainPanel(), Internationalization.CMT_VISIBILITY, Internationalization.CMT_VISIBILITY_TITLE, 1, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.MENU_ICON_CMT_VISIBILITY)), rendering.toArray(), -1);
                if (index != null) {
                    String text = (String)index;
                    if (text.startsWith("Public - ")) {
                        text = text.substring("Public - ".length());
                        CMTManager.getCMT().setCorpusVisibility(Constants.CMT_USER, text, false);
                        JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.CMT_VISIBILITY_PRIV);
                    } else {
                        text = text.substring("Private - ".length());
                        CMTManager.getCMT().setCorpusVisibility(Constants.CMT_USER, text, true);
                        JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.CMT_VISIBILITY_PUB);
                    }
                }
            } else {
                JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.CMT_RETRIEVE_NOCORPUS);
            }
        }
        catch (Exception e1) {
            JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), e1.getMessage());
        }
    }
}

