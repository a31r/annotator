/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.cmt;

import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class RemoveCollaborativeCorpus
implements ActionListener {
    MainForm mainForm;

    public RemoveCollaborativeCorpus(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Collection<String> corpusIDs = CMTManager.getCMT().getAllRegisteredCorpus();
            if (corpusIDs.size() > 0) {
                Object index = JOptionPane.showInputDialog(this.mainForm.getMainPanel(), Internationalization.CMT_REMOVE, Internationalization.CMT_REMOVE_TITLE, 1, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)), corpusIDs.toArray(), -1);
                if (index != null && 0 == JOptionPane.showConfirmDialog(this.mainForm.getMainPanel(), Internationalization.CMT_REMOVE_SURE, Internationalization.CMT_REMOVE_TITLE, 0, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)))) {
                    if (Constants.IS_COLLABORATIVE_CORPUS.equals(index)) {
                        DocumentSingleton.loadFromFile("");
                        Constants.IS_COLLABORATIVE_CORPUS = null;
                        Constants.DOCUMENT_OPENED = null;
                        Utils.setChangedCorpus(false);
                        Utils.changeCorpusEvent(this.mainForm);
                    }
                    CMTManager.getCMT().unregisterCorpus(Constants.CMT_USER, (String)index);
                }
            } else {
                JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.CMT_RETRIEVE_NODOCS);
            }
        }
        catch (Exception e1) {
            JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), e1.getMessage());
        }
    }
}

