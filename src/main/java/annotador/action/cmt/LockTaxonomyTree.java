/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.cmt;

import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTree;
import org.w3c.dom.Document;

public class LockTaxonomyTree
implements ActionListener {
    MainForm mainForm;

    public LockTaxonomyTree(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (Constants.IS_COLLABORATIVE_CORPUS == null) {
            JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.ONLY_COLLABAROTIVE);
        } else if (Constants.DOCUMENT_OPENED != null) {
            JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.TAXNOMY_NOT_OPEN_DOC);
        } else {
            try {
                String taxonomy = CMTManager.getCMT().lockTaxonomyTree(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS);
                Constants.IS_TAXONOMY_LOCKED = true;
                Document doc = DOMUtils.StringToDOM(taxonomy, Constants.ENCODING, Constants.TEI_NAMESPACE);
                DocumentSingleton.loadFromDOM(doc);
                Utils.changeCorpusEvent(this.mainForm);
                this.mainForm.getTaxonomyTree().clearSelection();
                JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.TAXNOMY_LOCKED);
            }
            catch (Exception e1) {
                JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), e1.getMessage());
            }
        }
    }
}

