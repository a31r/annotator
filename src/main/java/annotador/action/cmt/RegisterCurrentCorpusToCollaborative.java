/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.cmt;

import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.gui.NewCorpusDialog;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class RegisterCurrentCorpusToCollaborative
implements ActionListener {
    MainForm mainForm;

    public RegisterCurrentCorpusToCollaborative(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (Constants.IS_COLLABORATIVE_CORPUS != null) {
            JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.CMT_REPUBLISH, Internationalization.CMT_REPUBLISH_TITLE, 2);
            return;
        }
        String corpusID = (String)JOptionPane.showInputDialog(this.mainForm.getMainPanel(), Internationalization.CMT_PUBLISH_SURE, Internationalization.CMT_PUBLISH, 1, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.MENU_ICON_CMT_PUBLISH)), null, null);
        if (corpusID != null) {
            if (!corpusID.contains("_")) {
                try {
                    if (DocumentSingleton.getInstance() == null) {
                        NewCorpusDialog ncd = new NewCorpusDialog(this.mainForm, "New Collaborative Corpus");
                        ncd.getCorpusName().setText(corpusID);
                        ncd.setPath(".");
                        ncd.onOK();
                    }
                    String corpus = DOMUtils.DOMToString(DocumentSingleton.getInstance(), Constants.ENCODING);
                    CMTManager.getCMT().registerCorpus(Constants.CMT_USER, corpusID, corpus);
                    Constants.IS_COLLABORATIVE_CORPUS = corpusID;
                    JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.CMT_PUBLISH_CONFIR);
                }
                catch (Exception e1) {
                    JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), e1.getMessage());
                }
            } else {
                JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.CMT_PUBLISH_CHAR, Internationalization.CMT_PUBLISH_CHAR_TITLE, 2);
            }
        }
    }
}

