/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.cmt;

import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class UnlockTaxonomyTree
implements ActionListener {
    MainForm mainForm;

    public UnlockTaxonomyTree(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (Constants.IS_COLLABORATIVE_CORPUS == null) {
            JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.ONLY_COLLABAROTIVE);
        } else if (Constants.DOCUMENT_OPENED != null) {
            JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.TAXNOMY_NOT_OPEN_DOC);
        } else if (Constants.IS_TAXONOMY_LOCKED) {
            try {
                int option = JOptionPane.showConfirmDialog(this.mainForm.getMainPanel(), Internationalization.TAXNOMY_SAVE, Internationalization.TAXNOMY_SAVE_TITLE, 0);
                if (option == 1) {
                    CMTManager.getCMT().unlockDiscartingTaxonomyTree(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS);
                }
                if (option == 0) {
                    String doc = DOMUtils.DOMToString(DocumentSingleton.getInstance(), Constants.ENCODING);
                    CMTManager.getCMT().unlockUpdatingTaxonomyTree(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, doc);
                }
                Constants.IS_TAXONOMY_LOCKED = false;
                Utils.changeCorpusEvent(this.mainForm);
                JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.TAXNOMY_RELEASED);
            }
            catch (Exception e1) {
                JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), e1.getMessage());
            }
        }
    }
}

