/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.interview;

import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.gui.SelectDocument;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.TEINamespaceContext;
import java.awt.Component;
import java.awt.ItemSelectable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collection;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.NodeList;

public class SelectInterviewAction
implements ActionListener {
    MainForm mainForm;

    public SelectInterviewAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Vector<String> interviews = new Vector<String>();
        if (Constants.IS_COLLABORATIVE_CORPUS == null) {
            try {
                XPath xpath = XPathFactory.newInstance().newXPath();
                xpath.setNamespaceContext(new TEINamespaceContext());
                String xpathExpression = "/tei:teiCorpus/tei:TEI";
                NodeList list = (NodeList)xpath.evaluate(xpathExpression, DocumentSingleton.getInstance(), XPathConstants.NODESET);
                for (int i = 0; i < list.getLength(); ++i) {
                    xpathExpression = "/tei:teiCorpus/tei:TEI[" + (i + 1) + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/text()";
                    String interviewTitleElement = (String)xpath.evaluate(xpathExpression, DocumentSingleton.getInstance(), XPathConstants.STRING);
                    if (interviewTitleElement.equals("")) {
                        interviews.add("" + (i + 1) + " " + Internationalization.NO_DOCUMENT_TITLE);
                        continue;
                    }
                    interviews.add("" + (i + 1) + " " + interviewTitleElement);
                }
            }
            catch (XPathExpressionException event) {}
        } else {
            if (Constants.IS_TAXONOMY_LOCKED) {
                JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), Internationalization.TAXNOMY_DO_NOT_OPEN_DOCS);
                return;
            }
            try {
                Collection<String> i = CMTManager.getCMT().getAllAvailableDocument(Constants.IS_COLLABORATIVE_CORPUS);
                interviews.addAll(i);
            }
            catch (Exception e1) {
                JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), e1.getMessage());
            }
        }
        if (interviews.size() == 0) {
            JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.SELECT_INTERVIEW_ERROR_1);
        } else {
            SelectDocument doc = new SelectDocument(this.mainForm);
            doc.getDocumentComboBox().setModel(new DefaultComboBoxModel(interviews));
            doc.getDocumentComboBox().setSelectedIndex(0);
            for (ItemListener it : doc.getDocumentComboBox().getItemListeners()) {
                it.itemStateChanged(new ItemEvent(doc.getDocumentComboBox(), 1, doc, 1));
            }
            doc.pack();
            doc.setLocationRelativeTo(this.mainForm.getTabbedPane());
            doc.setVisible(true);
        }
    }
}

