/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.interview;

import annotador.cmt.CMTManager;
import annotador.gui.DeleteDocument;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DeleteInterviewAction
implements ActionListener {
    MainForm mainForm;

    public DeleteInterviewAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Vector<String> interviews = new Vector<String>();
            if (Constants.IS_COLLABORATIVE_CORPUS == null) {
                XPath xpath = XPathFactory.newInstance().newXPath();
                xpath.setNamespaceContext(new TEINamespaceContext());
                String xpathExpression = "/tei:teiCorpus/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title";
                NodeList list = (NodeList)xpath.evaluate(xpathExpression, DocumentSingleton.getInstance(), XPathConstants.NODESET);
                for (int i = 0; i < list.getLength(); ++i) {
                    Element element = (Element)list.item(i);
                    String interviewTitleElement = element.getTextContent();
                    if (interviewTitleElement.equals("")) {
                        interviews.add("" + (i + 1) + " " + Internationalization.NO_DOCUMENT_TITLE);
                        continue;
                    }
                    interviews.add("" + (i + 1) + " " + interviewTitleElement);
                }
                Utils.setChangedCorpus(true);
            } else {
                try {
                    Collection<String> i = CMTManager.getCMT().getAllAvailableDocument(Constants.IS_COLLABORATIVE_CORPUS);
                    interviews.addAll(i);
                }
                catch (Exception e1) {
                    JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), e1.getMessage());
                }
            }
            if (interviews.size() == 0) {
                JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.DELETE_INTERVIEW_ERROR_1);
            } else {
                DeleteDocument doc = new DeleteDocument(this.mainForm);
                doc.getDocumentComboBox().setModel(new DefaultComboBoxModel(interviews));
                doc.setTitle(Internationalization.DELETE_INTERVIEW_TITLE_2);
                doc.getIconLabel().setIcon(new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.MENU_ICON_INTERVIEW_DELETE)));
                doc.pack();
                doc.setLocationRelativeTo(this.mainForm.getTabbedPane());
                doc.setVisible(true);
            }
        }
        catch (XPathExpressionException event) {
            System.out.println("SelectInterviewAction.actionPerformed() -> XPathExpressionException");
        }
    }
}

