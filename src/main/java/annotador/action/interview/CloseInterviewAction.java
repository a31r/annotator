/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.interview;

import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CloseInterviewAction
implements ActionListener {
    MainForm mainForm;
    int option = -1;

    public CloseInterviewAction(MainForm mainForm) {
        this.mainForm = mainForm;
        this.option = -1;
    }

    public int getOption() {
        return this.option;
    }

    public void setOption(int option) {
        this.option = option;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            if (Constants.IS_COLLABORATIVE_CORPUS != null && Constants.DOCUMENT_OPENED != null) {
                if (Utils.isChangedCorpus()) {
                    if (this.option == -1) {
                        this.option = JOptionPane.showConfirmDialog(this.mainForm.getMainPanel(), Internationalization.SAVE_DOCUMENT_ASK_1, Internationalization.SAVE_CHANGES, 0, 1, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.MENU_ICON_CORPUS_SAVE)));
                    }
                    switch (this.option) {
                        case 0: {
                            if (DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI").getLength() == 1) {
                                Node tei = DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI").item(0);
                                String interview = DOMUtils.DOMToString(tei, Constants.ENCODING);
                                DocumentSingleton.getInstance().getDocumentElement().removeChild(tei);
                                DocumentSingleton.getInstance().normalizeDocument();
                                CMTManager.getCMT().unlockUpdatingDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, Constants.DOCUMENT_OPENED, interview);
                                Constants.DOCUMENT_OPENED = null;
                                Utils.setChangedCorpus(false);
                                break;
                            }
                            System.out.println("IMPORTANT: There are more than 1 TEI Tag in the DOM Structure!!!!!!!");
                            NodeList nl = DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI");
                            for (int i = 0; i < nl.getLength(); ++i) {
                                Node tei = nl.item(i);
                                DocumentSingleton.getInstance().getDocumentElement().removeChild(tei);
                                DocumentSingleton.getInstance().normalizeDocument();
                                CMTManager.getCMT().unlockDiscartingDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, Constants.DOCUMENT_OPENED);
                                Constants.DOCUMENT_OPENED = null;
                                Utils.setChangedCorpus(false);
                            }
                            break;
                        }
                        case 1: {
                            if (DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI").getLength() == 1) {
                                Node tei = DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI").item(0);
                                DocumentSingleton.getInstance().getDocumentElement().removeChild(tei);
                                DocumentSingleton.getInstance().normalizeDocument();
                                CMTManager.getCMT().unlockDiscartingDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, Constants.DOCUMENT_OPENED);
                                Constants.DOCUMENT_OPENED = null;
                                Utils.setChangedCorpus(false);
                                break;
                            }
                            System.out.println("IMPORTANT: There are more than 1 TEI Tag in the DOM Structure!!!!!!!");
                            NodeList nl = DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI");
                            for (int i = 0; i < nl.getLength(); ++i) {
                                Node tei = nl.item(i);
                                DocumentSingleton.getInstance().getDocumentElement().removeChild(tei);
                                DocumentSingleton.getInstance().normalizeDocument();
                                CMTManager.getCMT().unlockDiscartingDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, Constants.DOCUMENT_OPENED);
                                Constants.DOCUMENT_OPENED = null;
                                Utils.setChangedCorpus(false);
                            }
                            break;
                        }
                    }
                    this.option = -1;
                } else if (DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI").getLength() == 1) {
                    Node tei = DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI").item(0);
                    DocumentSingleton.getInstance().getDocumentElement().removeChild(tei);
                    DocumentSingleton.getInstance().normalizeDocument();
                    CMTManager.getCMT().unlockDiscartingDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, Constants.DOCUMENT_OPENED);
                    Constants.DOCUMENT_OPENED = null;
                    Utils.setChangedCorpus(false);
                } else {
                    System.out.println("IMPORTANT: There are more than 1 TEI Tag in the DOM Structure!!!!!!!");
                    NodeList nl = DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI");
                    for (int i = 0; i < nl.getLength(); ++i) {
                        Node tei = nl.item(i);
                        DocumentSingleton.getInstance().getDocumentElement().removeChild(tei);
                        DocumentSingleton.getInstance().normalizeDocument();
                        CMTManager.getCMT().unlockDiscartingDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, Constants.DOCUMENT_OPENED);
                        Constants.DOCUMENT_OPENED = null;
                        Utils.setChangedCorpus(false);
                    }
                }
            }
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
        Constants.DOCUMENT_OPENED = null;
        Utils.changeInterviewEvent(this.mainForm, -1);
    }
}

