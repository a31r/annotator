/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.interview;

import annotador.action.xml.XMLViewComponentListener;
import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.interview.Conversor;
import annotador.interview.ConversorExcepion;
import annotador.upgrade.ImportFrom1xx;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Properties;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.filechooser.FileFilter;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AddInterviewAction
implements ActionListener {
    MainForm mainForm;
    String workinDirectory = null;

    public AddInterviewAction(MainForm mainForm) {
        this.mainForm = mainForm;
        this.workinDirectory = null;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (DocumentSingleton.getInstance() != null) {
            JFileChooser chooser = new JFileChooser();
            chooser.setMultiSelectionEnabled(false);
            if (this.workinDirectory != null) {
                chooser.setCurrentDirectory(new File(this.workinDirectory));
            }
            Properties extension = new Properties();
            try {
                extension.loadFromXML(new FileInputStream(Constants.WORKING_PATH + Constants.FILE_NAME_CONFIG_CONVERSOR));
            }
            catch (IOException ioe) {
                try {
                    extension.loadFromXML(this.getClass().getResourceAsStream(Constants.FILE_NAME_CONFIG_CONVERSOR_RESOURCE));
                    extension.storeToXML(new FileOutputStream(Constants.WORKING_PATH + Constants.FILE_NAME_CONFIG_CONVERSOR), "Conversor Configurator");
                }
                catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            final String def = extension.getProperty("default");
            Enumeration en = extension.propertyNames();
            while (en.hasMoreElements()) {
                final String key = (String)en.nextElement();
                if (key.equals("default") || key.equals(def)) continue;
                chooser.addChoosableFileFilter(new FileFilter(){

                    @Override
                    public boolean accept(File f) {
                        return true;
                    }

                    @Override
                    public String getDescription() {
                        return key;
                    }
                });
            }
            if (def != null) {
                chooser.addChoosableFileFilter(new FileFilter(){

                    @Override
                    public boolean accept(File f) {
                        return true;
                    }

                    @Override
                    public String getDescription() {
                        return def;
                    }
                });
            }
            if (0 == chooser.showOpenDialog(this.mainForm.getTabbedPane())) {
                block20 : {
                    this.workinDirectory = chooser.getSelectedFile().getParentFile().getAbsolutePath();
                    this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(3));
                    File selectedFile = chooser.getSelectedFile();
                    try {
                        Enumeration en2 = extension.propertyNames();
                        Conversor convesor = null;
                        FileFilter filter = chooser.getFileFilter();
                        convesor = (Conversor)Class.forName(extension.getProperty(filter.getDescription())).newInstance();
                        try {
                            Document doc = DocumentSingleton.getInstance();
                            if (doc != null) {
                                XPath xpath = XPathFactory.newInstance().newXPath();
                                xpath.setNamespaceContext(new TEINamespaceContext());
                                String xpathExpression = "//tei:TEI";
                                NodeList documents = (NodeList)xpath.evaluate(xpathExpression, doc, XPathConstants.NODESET);
                                Node interview = convesor.getInterview(selectedFile.getAbsolutePath(), chooser.getFileFilter().getDescription().substring(chooser.getFileFilter().getDescription().indexOf("(") + 1, chooser.getFileFilter().getDescription().indexOf(")")), documents.getLength());
                                Node interviewCopy = doc.importNode(interview, true);
                                ImportFrom1xx.fixWhiteSpaces(interviewCopy);
                                doc.getDocumentElement().appendChild(interviewCopy);
                                XMLViewComponentListener l = new XMLViewComponentListener(this.mainForm);
                                l.componentShown(new ComponentEvent(this.mainForm.getXmlPane(), 1));
                                if (Constants.IS_COLLABORATIVE_CORPUS != null) {
                                    xpathExpression = "//tei:title/text()";
                                    String title = (String)xpath.evaluate(xpathExpression, interview, XPathConstants.STRING);
                                    CMTManager.getCMT().addDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, title, DOMUtils.DOMToString(interview, Constants.ENCODING));
                                } else {
                                    Utils.setChangedCorpus(true);
                                }
                                JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.ADD_INTERVIEW_INFORMATION_MESSAGE_1, Internationalization.INFORMATION_WORD, 1, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_INFORM)));
                                break block20;
                            }
                            JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.ADD_INTERVIEW_ERROR_MESSAGE_1, Internationalization.WARNING_WORD, 2, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)));
                        }
                        catch (ConversorExcepion conversorExcepion) {
                            JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), conversorExcepion.getMessage(), Internationalization.WARNING_WORD, 2, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)));
                        }
                        catch (XPathExpressionException e1) {
                            e1.printStackTrace();
                        }
                        catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                    catch (IllegalAccessException e1) {
                        e1.printStackTrace();
                    }
                    catch (ClassNotFoundException e1) {
                        e1.printStackTrace();
                    }
                    catch (InstantiationException e1) {
                        e1.printStackTrace();
                    }
                }
                this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(0));
            }
        }
    }

}

