/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.interview;

import annotador.gui.MainForm;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.ShowedCategory;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.awt.ItemSelectable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.PrintStream;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DownAction
implements ActionListener {
    private JComboBox documentComboBox;
    MainForm mainForm;

    public DownAction(MainForm mainForm, JComboBox documentComboBox) {
        this.mainForm = mainForm;
        this.documentComboBox = documentComboBox;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.documentComboBox.getSelectedIndex() != -1) {
            try {
                String a = (String)this.documentComboBox.getSelectedItem();
                int index = Integer.parseInt(a.substring(0, a.indexOf(32)));
                Document doc = DocumentSingleton.getInstance();
                XPath xpath = XPathFactory.newInstance().newXPath();
                xpath.setNamespaceContext(new TEINamespaceContext());
                Node actual = (Node)xpath.evaluate("//tei:TEI[" + index + "]", doc, XPathConstants.NODE);
                Node parent = actual.getParentNode();
                Node next = actual;
                while (!next.getNextSibling().getNodeName().equals("TEI")) {
                    next = next.getNextSibling();
                }
                next = next.getNextSibling();
                parent.removeChild(actual);
                Node secondNext = next;
                while (secondNext.getNextSibling() != null && !secondNext.getNextSibling().getNodeName().equals("TEI")) {
                    secondNext = secondNext.getNextSibling();
                }
                if ((secondNext = secondNext.getNextSibling()) != null) {
                    parent.insertBefore(actual, secondNext);
                } else {
                    parent.appendChild(actual);
                }
                doc.normalizeDocument();
                Utils.setChangedCorpus(true);
                Utils.changeInterviewEvent(this.mainForm, -1);
                ShowedCategory.removeAllShowedCategories();
                Vector<String> interviews = new Vector<String>();
                xpath = XPathFactory.newInstance().newXPath();
                xpath.setNamespaceContext(new TEINamespaceContext());
                String xpathExpression = "/tei:teiCorpus/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title";
                NodeList list = (NodeList)xpath.evaluate(xpathExpression, DocumentSingleton.getInstance(), XPathConstants.NODESET);
                for (int i = 0; i < list.getLength(); ++i) {
                    Element element = (Element)list.item(i);
                    String interviewTitleElement = element.getTextContent();
                    if (interviewTitleElement.equals("")) {
                        interviews.add("" + (i + 1) + " " + Internationalization.NO_DOCUMENT_TITLE);
                        continue;
                    }
                    interviews.add("" + (i + 1) + " " + interviewTitleElement);
                }
                this.documentComboBox.setModel(new DefaultComboBoxModel(interviews));
                this.documentComboBox.setSelectedIndex(index++);
                for (ItemListener it : this.documentComboBox.getItemListeners()) {
                    it.itemStateChanged(new ItemEvent(this.documentComboBox, 1, doc, 1));
                }
            }
            catch (XPathExpressionException e1) {
                e1.printStackTrace();
            }
        } else {
            System.out.println("NO SELECEED");
        }
    }
}

