/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.corpus;

import annotador.action.corpus.SaveCorpusFile;
import annotador.gui.MainForm;
import annotador.gui.NewCorpusDialog;
import annotador.utils.Internationalization;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NewCorpusFile
implements ActionListener {
    MainForm mainForm;

    public NewCorpusFile(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SaveCorpusFile saveCorpusFile = new SaveCorpusFile(this.mainForm, true);
        saveCorpusFile.actionPerformed(e);
        if (!saveCorpusFile.isWasCancel()) {
            NewCorpusDialog newCorpusDialog = new NewCorpusDialog(this.mainForm, Internationalization.NEW_CORPUS_TITLE);
            newCorpusDialog.pack();
            newCorpusDialog.setVisible(true);
        }
    }
}

