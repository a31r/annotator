/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.corpus;

import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class SaveAsCorpusFile
implements ActionListener {
    MainForm mainForm;

    public SaveAsCorpusFile(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (DocumentSingleton.getInstance() != null) {
            JFileChooser chooser = new JFileChooser();
            if (0 == chooser.showSaveDialog(this.mainForm.getTabbedPane())) {
                try {
                    String file = chooser.getSelectedFile().getAbsolutePath();
                    if (!file.endsWith(".xml")) {
                        file = file + ".xml";
                    }
                    if ((Constants.LAST_OPENED_CORPUS_FILE_PATH = file) != null) {
                        this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(3));
                        FileOutputStream fw = new FileOutputStream(Constants.LAST_OPENED_CORPUS_FILE_PATH, false);
                        OutputStreamWriter osw = new OutputStreamWriter((OutputStream)fw, Constants.ENCODING);
                        BufferedWriter writer = new BufferedWriter(osw);
                        String xml = "";
                        xml = Constants.IS_COLLABORATIVE_CORPUS == null ? DOMUtils.DOMToString(DocumentSingleton.getInstance(), Constants.ENCODING) : CMTManager.getCMT().getFullTEICorpusFile(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS);
                        writer.write(xml);
                        writer.flush();
                        osw.flush();
                        fw.flush();
                        writer.close();
                        osw.close();
                        fw.close();
                        this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(0));
                    }
                }
                catch (Exception e1) {
                    JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.SAVE_CORPUS_ERROR_1, Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)));
                }
            }
            if (Constants.IS_COLLABORATIVE_CORPUS == null) {
                Utils.setChangedCorpus(false);
            }
        }
    }
}

