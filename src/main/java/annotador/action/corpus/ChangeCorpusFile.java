/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.corpus;

import annotador.action.corpus.SaveCorpusFile;
import annotador.gui.MainForm;
import annotador.upgrade.ImportFrom1xx;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.xml.xpath.XPathExpressionException;

public class ChangeCorpusFile
implements ActionListener {
    MainForm mainForm;

    public ChangeCorpusFile(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.mainForm.getVentanaPrincipal().setCursor(Cursor.getPredefinedCursor(3));
        SaveCorpusFile saveCorpusFile = new SaveCorpusFile(this.mainForm, true);
        saveCorpusFile.actionPerformed(e);
        if (!saveCorpusFile.isWasCancel()) {
            JFileChooser chooser = new JFileChooser();
            chooser.setMultiSelectionEnabled(false);
            if (chooser.showOpenDialog(this.mainForm.getTabbedPane()) == 0) {
                String pathOftheFile;
                DocumentSingleton.loadFromFile("");
                Utils.changeCorpusEvent(this.mainForm);
                this.mainForm.getTaxonomyTree().repaint();
                File file = chooser.getSelectedFile();
                Constants.LAST_OPENED_CORPUS_FILE_PATH = pathOftheFile = file.getAbsolutePath();
                DocumentSingleton.loadFromFile(pathOftheFile);
                Utils.changeCorpusEvent(this.mainForm);
                Constants.IS_COLLABORATIVE_CORPUS = null;
                Constants.DOCUMENT_OPENED = null;
                if (Constants.ALLOW_AUTOMATIC_IMPORT.equalsIgnoreCase("true")) {
                    try {
                        ImportFrom1xx.convertFromAnyTo2xx(DocumentSingleton.getInstance());
                        DocumentSingleton.getInstance().normalizeDocument();
                        saveCorpusFile = new SaveCorpusFile(this.mainForm, false);
                        saveCorpusFile.actionPerformed(e);
                        DocumentSingleton.loadFromFile(pathOftheFile);
                        Utils.changeCorpusEvent(this.mainForm);
                    }
                    catch (XPathExpressionException e1) {
                        JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.IMPORT_MESSAGE_ERROR, Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE)));
                    }
                }
            }
        }
        this.mainForm.getVentanaPrincipal().setCursor(Cursor.getPredefinedCursor(0));
    }
}

