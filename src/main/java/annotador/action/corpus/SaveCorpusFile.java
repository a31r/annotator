/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.corpus;

import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SaveCorpusFile
implements ActionListener {
    MainForm mainForm;
    boolean wasCancel = false;
    private boolean show = false;

    public boolean isWasCancel() {
        return this.wasCancel;
    }

    public SaveCorpusFile(MainForm mainForm, boolean show) {
        this.mainForm = mainForm;
        this.show = show;
    }

    public boolean isShow() {
        return this.show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.wasCancel = false;
        if (Utils.isChangedCorpus()) {
            if (this.show) {
                int option = JOptionPane.showConfirmDialog(this.mainForm.getTabbedPane(), Internationalization.SAVE_CORPUS_ASK_1, Internationalization.SAVE_CORPUS_TITLE, 1, 1, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)));
                switch (option) {
                    case -1: 
                    case 2: {
                        this.wasCancel = true;
                        break;
                    }
                    case 0: {
                        this.save();
                        break;
                    }
                    case 1: {
                        this.discard();
                    }
                }
            } else {
                this.save();
            }
        } else {
            this.save();
        }
    }

    private void discard() {
    }

    private void save() {
        try {
            this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(3));
            if (Constants.IS_COLLABORATIVE_CORPUS == null) {
                if (Constants.LAST_OPENED_CORPUS_FILE_PATH != null && !Constants.LAST_OPENED_CORPUS_FILE_PATH.equals("") && DocumentSingleton.getInstance() != null) {
                    FileOutputStream fw = new FileOutputStream(Constants.LAST_OPENED_CORPUS_FILE_PATH, false);
                    OutputStreamWriter osw = new OutputStreamWriter((OutputStream)fw, Constants.ENCODING);
                    BufferedWriter writer = new BufferedWriter(osw);
                    String xml = DOMUtils.DOMToString(DocumentSingleton.getInstance(), Constants.ENCODING);
                    writer.write(xml);
                    writer.flush();
                    osw.flush();
                    fw.flush();
                    writer.close();
                    osw.close();
                    fw.close();
                }
            } else {
                if (Constants.DOCUMENT_OPENED != null) {
                    if (DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI").getLength() == 1) {
                        Node tei = DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI").item(0);
                        String interview = DOMUtils.DOMToString(tei, Constants.ENCODING);
                        CMTManager.getCMT().unlockUpdatingDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, Constants.DOCUMENT_OPENED, interview);
                        CMTManager.getCMT().lockDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, Constants.DOCUMENT_OPENED);
                    } else {
                        System.out.println("DOCUMENT NOT SAVED. MORE THAN 1 TEI TAGS IN THE DOM STRUCTURE");
                    }
                }
                if (Constants.IS_TAXONOMY_LOCKED) {
                    try {
                        String doc = DOMUtils.DOMToString(DocumentSingleton.getInstance(), Constants.ENCODING);
                        CMTManager.getCMT().unlockUpdatingTaxonomyTree(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, doc);
                        CMTManager.getCMT().lockTaxonomyTree(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS);
                    }
                    catch (Exception e) {
                        // empty catch block
                    }
                }
            }
            this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(0));
            Utils.setChangedCorpus(false);
        }
        catch (Exception e1) {
            JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.SAVE_CORPUS_ERROR_1, Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)));
        }
    }
}

