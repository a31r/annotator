/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.corpus;

import annotador.action.corpus.SaveCorpusFile;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.Utils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTabbedPane;
import javax.swing.JTree;

public class CloseCorpusFile
implements ActionListener {
    MainForm mainForm;

    public CloseCorpusFile(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SaveCorpusFile saveCorpusFile = new SaveCorpusFile(this.mainForm, true);
        saveCorpusFile.actionPerformed(e);
        if (!saveCorpusFile.isWasCancel()) {
            Constants.LAST_OPENED_CORPUS_FILE_PATH = null;
            DocumentSingleton.loadFromFile("");
            Utils.changeCorpusEvent(this.mainForm);
            this.mainForm.getTaxonomyTree().repaint();
            this.mainForm.getTabbedPane().setEnabledAt(2, false);
            Constants.IS_COLLABORATIVE_CORPUS = null;
            Constants.DOCUMENT_OPENED = null;
        }
    }
}

