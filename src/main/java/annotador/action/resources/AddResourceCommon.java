/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.resources;

import annotador.action.resources.Resource;
import annotador.resources.ResourcesTableModel;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AddResourceCommon {
    private AddResourceCommon() {
    }

    public static void addResource(Node div, Resource resource, JTable table) {
        Utils.setChangedCorpus(true);
        Element e = (Element)div;
        Element linkgrp = null;
        Element ptr = e.getOwnerDocument().createElement(TEIConstants.TEI_TAG_PTR);
        NodeList list = e.getChildNodes();
        for (int i = 0; i < list.getLength(); ++i) {
            if (!list.item(i).getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_LINKGRP)) continue;
            linkgrp = (Element)list.item(i);
        }
        if (linkgrp == null) {
            linkgrp = e.getOwnerDocument().createElement(TEIConstants.TEI_TAG_LINKGRP);
            try {
                e.insertBefore(linkgrp, e.getFirstChild());
            }
            catch (DOMException de) {
                e.appendChild(linkgrp);
            }
        } else {
            linkgrp = (Element)list.item(0);
        }
        linkgrp.appendChild(ptr);
        TableModel tm = table.getModel();
        if (tm instanceof ResourcesTableModel) {
            ResourcesTableModel rtm = (ResourcesTableModel)tm;
            rtm.setLinkGroup(linkgrp);
        }
        TableColumnModel cm = table.getColumnModel();
        for (int i2 = 0; i2 < cm.getColumnCount(); ++i2) {
            cm.getColumn(i2).sizeWidthToFit();
        }
        if (!resource.getDescription().equals("")) {
            ptr.setAttribute(TEIConstants.TEI_ATTRIBUTE_REND, resource.getDescription());
        }
        if (!resource.getLink().equals("")) {
            ptr.setAttribute(TEIConstants.TEI_ATTRIBUTE_TARGET, resource.getLink());
        }
        if (!resource.getName().equals("")) {
            ptr.setAttribute(TEIConstants.TEI_ATTRIBUTE_ANA, resource.getName());
        }
        if (!resource.getType().equals("")) {
            ptr.setAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE, resource.getType());
        }
        linkgrp.getOwnerDocument().normalizeDocument();
    }
}

