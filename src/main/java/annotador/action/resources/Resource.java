/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.resources;

public class Resource {
    String name;
    String type;
    String description;
    String link;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public boolean equals(Object obj) {
        Resource rs;
        if (obj instanceof Resource && (rs = (Resource)obj).getDescription().equals(this.getDescription()) && rs.getLink().equals(this.getLink()) && rs.getName().equals(this.getName()) && this.getType().equals(this.getType())) {
            return true;
        }
        return false;
    }
}

