/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.resources;

import annotador.action.resources.AddResourceCommon;
import annotador.action.resources.Resource;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ImportResourcesFromFile
implements ActionListener {
    Node div;
    JTable table;
    MainForm mainForm;

    public ImportResourcesFromFile(MainForm mainForm, JTable table, Node div) {
        this.mainForm = mainForm;
        this.div = div;
        this.table = table;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser chooser = new JFileChooser();
        if (chooser.showOpenDialog(this.mainForm.getTabbedPane()) == 0) {
            String path = chooser.getSelectedFile().getAbsolutePath();
            try {
                FileInputStream fin = new FileInputStream(path);
                InputStreamReader isr = new InputStreamReader((InputStream)fin, Constants.ENCODING_FOR_IMPORT);
                BufferedReader br = new BufferedReader(isr);
                String aux = null;
                String total = "";
                while ((aux = br.readLine()) != null) {
                    total = total + aux + "\n";
                }
                Document doc = DOMUtils.StringToDOM(total, Constants.ENCODING_FOR_IMPORT, null);
                NodeList list = doc.getDocumentElement().getElementsByTagName(Constants.RES_RESOURCE);
                for (int i = 0; i < list.getLength(); ++i) {
                    Element r = (Element)list.item(i);
                    Resource res = new Resource();
                    NodeList na = r.getElementsByTagName(Constants.RES_NAME);
                    if (na.getLength() > 0) {
                        res.setName(na.item(0).getTextContent());
                    }
                    if ((na = r.getElementsByTagName(Constants.RES_DESCRIPTION)).getLength() > 0) {
                        res.setDescription(na.item(0).getTextContent());
                    }
                    if ((na = r.getElementsByTagName(Constants.RES_LINK)).getLength() > 0) {
                        res.setLink(na.item(0).getTextContent());
                    }
                    if ((na = r.getElementsByTagName(Constants.RES_TYPE)).getLength() > 0) {
                        res.setType(na.item(0).getTextContent());
                    }
                    AddResourceCommon.addResource(this.div, res, this.table);
                }
                Utils.setChangedCorpus(true);
            }
            catch (IOException e1) {
                JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.IMPORT_ERROR);
            }
            catch (Exception e1) {
                JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.IMPORT_ERROR);
            }
        }
        this.table.updateUI();
    }
}

