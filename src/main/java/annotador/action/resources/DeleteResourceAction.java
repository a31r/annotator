/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.resources;

import annotador.resources.ResourcesTableModel;
import annotador.utils.Utils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DeleteResourceAction
implements ActionListener {
    Node ptr;
    JTable table;

    public DeleteResourceAction(Node ptr, JTable table) {
        this.table = table;
        this.ptr = ptr;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.ptr.getParentNode().getChildNodes().getLength() <= 1) {
            this.ptr.getParentNode().getParentNode().removeChild(this.ptr.getParentNode());
            TableModel tm = this.table.getModel();
            if (tm instanceof ResourcesTableModel) {
                ResourcesTableModel rtm = (ResourcesTableModel)tm;
                rtm.setLinkGroup(null);
            }
        }
        this.ptr.getParentNode().removeChild(this.ptr);
        Utils.setChangedCorpus(true);
        this.table.updateUI();
    }
}

