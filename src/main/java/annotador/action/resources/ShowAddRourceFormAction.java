/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.resources;

import annotador.gui.AddResourceForm;
import annotador.gui.MainForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import org.w3c.dom.Node;

public class ShowAddRourceFormAction
implements ActionListener {
    Node divs;
    MainForm dialog;
    JTable table;

    public ShowAddRourceFormAction(MainForm dialog, JTable table, Node divs) {
        this.dialog = dialog;
        this.table = table;
        this.divs = divs;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        AddResourceForm form = new AddResourceForm(this.dialog, this.table, this.divs);
    }
}

