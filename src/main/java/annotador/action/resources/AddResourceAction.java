/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.resources;

import annotador.action.resources.AddResourceCommon;
import annotador.action.resources.Resource;
import annotador.gui.AddResourceForm;
import annotador.gui.MainForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.JTextField;
import org.w3c.dom.Node;

public class AddResourceAction
implements ActionListener {
    AddResourceForm addResourceForm;
    MainForm sectionResourceManagementForm;
    Node div;
    JTable table;

    public AddResourceAction(MainForm dialog, JTable table, AddResourceForm addResourceForm, Node div) {
        this.sectionResourceManagementForm = dialog;
        this.addResourceForm = addResourceForm;
        this.div = div;
        this.table = table;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Resource res = new Resource();
        res.setDescription(this.addResourceForm.getDescriptionField().getText());
        res.setLink(this.addResourceForm.getLinkField().getText());
        res.setName(this.addResourceForm.getNameField().getText());
        res.setType((String)this.addResourceForm.getTypeField().getSelectedItem());
        AddResourceCommon.addResource(this.div, res, this.table);
        this.addResourceForm.getMainDialog().setVisible(false);
        this.table.updateUI();
    }
}

