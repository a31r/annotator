/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.metadata;

import annotador.gui.MainForm;
import annotador.gui.ModifyPersonForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import org.w3c.dom.Element;

public class ShowPersonInformationAction
implements ActionListener {
    JComboBox box;
    MainForm mainForm;

    public ShowPersonInformationAction(JComboBox box, MainForm mainForm) {
        this.box = box;
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object selected = this.box.getSelectedItem();
        if (selected instanceof Element) {
            Element person = (Element)selected;
            ModifyPersonForm form = new ModifyPersonForm(person, this.mainForm.getVentanaPrincipal(), this.mainForm);
            this.box.repaint();
        }
    }
}

