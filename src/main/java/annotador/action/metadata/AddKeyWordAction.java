/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.metadata;

import annotador.gui.MainForm;
import annotador.metadata.KeyWordModel;
import annotador.text.TEITableModel;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class AddKeyWordAction
implements ActionListener {
    MainForm mainForm;

    public AddKeyWordAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String keyWord = JOptionPane.showInputDialog(this.mainForm.getTabbedPane(), (Object)Internationalization.KEYWORD_ADD_MESSAGE);
        if (keyWord != null && !keyWord.equals("")) {
            try {
                Node textClass;
                Element list;
                Element keywords;
                Node profileDesc;
                Node teiHeader;
                Document doc = DocumentSingleton.getInstance();
                XPath xpath = XPathFactory.newInstance().newXPath();
                xpath.setNamespaceContext(new TEINamespaceContext());
                int interview = 0;
                TableModel model = this.mainForm.getDocumentTable().getModel();
                if (model instanceof TEITableModel) {
                    TEITableModel teiTableModel = (TEITableModel)model;
                    interview = teiTableModel.getIndexOfInterview();
                }
                if ((teiHeader = (Node)xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader", doc, XPathConstants.NODE)) == null) {
                    Element tei = (Element)xpath.evaluate("//tei:TEI[" + interview + "]", doc, XPathConstants.NODE);
                    Element teiHeaderN = tei.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "teiHeader");
                    if (tei.getFirstChild() != null) {
                        tei.insertBefore(teiHeaderN, tei.getFirstChild());
                    } else {
                        tei.appendChild(teiHeaderN);
                    }
                    doc.normalizeDocument();
                }
                if ((profileDesc = (Node)xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc", doc, XPathConstants.NODE)) == null) {
                    teiHeader = (Node)xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader", doc, XPathConstants.NODE);
                    profileDesc = teiHeader.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "profileDesc");
                    teiHeader.appendChild(profileDesc);
                    doc.normalizeDocument();
                }
                if ((textClass = (Node)xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc/tei:textClass", doc, XPathConstants.NODE)) == null) {
                    profileDesc = (Node)xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc", doc, XPathConstants.NODE);
                    textClass = profileDesc.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "textClass");
                    profileDesc.appendChild(textClass);
                    doc.normalizeDocument();
                }
                if ((keywords = (Element)xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc/tei:textClass/tei:keywords[1]", doc, XPathConstants.NODE)) == null) {
                    textClass = (Node)xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc/tei:textClass", doc, XPathConstants.NODE);
                    keywords = textClass.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "keywords");
                    keywords.setAttribute("scheme", Constants.KEYWORD_SCHEME);
                    textClass.appendChild(keywords);
                    doc.normalizeDocument();
                }
                if ((list = (Element)xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc/tei:textClass/tei:keywords/tei:list[1]", doc, XPathConstants.NODE)) == null) {
                    keywords = (Element)xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc/tei:textClass/tei:keywords", doc, XPathConstants.NODE);
                    list = keywords.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "list");
                    keywords.appendChild(list);
                    doc.normalizeDocument();
                }
                this.mainForm.getKeyWordcomboBox().setModel(new KeyWordModel(list));
                Element item = list.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "item");
                item.setTextContent(keyWord);
                list.appendChild(item);
                Utils.setChangedCorpus(true);
                this.mainForm.getKeyWordcomboBox().repaint();
            }
            catch (XPathExpressionException ex) {
                // empty catch block
            }
        }
    }
}

