/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.metadata;

import annotador.utils.DocumentSingleton;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DeletePersonAction
implements ActionListener {
    JComboBox box;

    public DeletePersonAction(JComboBox box) {
        this.box = box;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object selected = this.box.getSelectedItem();
        if (selected instanceof Element) {
            Element person = (Element)selected;
            Element parent = (Element)person.getParentNode();
            person.getParentNode().removeChild(person);
            if (parent.getElementsByTagName(TEIConstants.TEI_TAG_PERSON).getLength() == 0) {
                parent.getParentNode().removeChild(parent);
            }
            DocumentSingleton.getInstance().normalizeDocument();
            Utils.setChangedCorpus(true);
            this.box.setSelectedIndex(-1);
            this.box.repaint();
        }
    }
}

