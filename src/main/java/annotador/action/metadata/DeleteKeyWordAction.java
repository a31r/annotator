/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.metadata;

import annotador.gui.MainForm;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import org.w3c.dom.Node;

public class DeleteKeyWordAction
implements ActionListener {
    MainForm mainForm;

    public DeleteKeyWordAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.mainForm.getKeyWordcomboBox().getSelectedIndex() != -1) {
            Utils.setChangedCorpus(true);
            Node item = (Node)this.mainForm.getKeyWordcomboBox().getSelectedItem();
            item.getParentNode().removeChild(item);
            JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.KEYWORD_REMOVE_MESSAGE);
            this.mainForm.getKeyWordcomboBox().setSelectedIndex(-1);
            this.mainForm.getKeyWordcomboBox().repaint();
        }
    }
}

