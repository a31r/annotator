/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.metadata;

import annotador.gui.AddPersonForm;
import annotador.gui.MainForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JFrame;

public class AddPersonAction
implements ActionListener {
    MainForm mainForm;

    public AddPersonAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        AddPersonForm form = new AddPersonForm(this.mainForm.getVentanaPrincipal(), this.mainForm);
        this.mainForm.getPersonBox().repaint();
    }
}

