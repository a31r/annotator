/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.metadata;

import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.TEIConstants;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ApplyChangesAction
implements ActionListener {
    MainForm mainForm;
    int interview;

    public ApplyChangesAction(MainForm mainForm, int selectedInteview) {
        this.mainForm = mainForm;
        this.interview = selectedInteview;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Utils.setChangedCorpus(true);
        try {
            Node principal;
            Node transcriber;
            Node sponsor;
            Node locale;
            Node title;
            Element recordingStmt;
            Node titleStmt;
            Node description;
            Node creation;
            Node publicationStmt;
            Element language;
            Date date;
            Node setting;
            Node idno;
            Element mediaFile;
            Node dateTrans;
            Node fileDesc;
            Document doc = DocumentSingleton.getInstance();
            XPath xpath = XPathFactory.newInstance().newXPath();
            xpath.setNamespaceContext(new TEINamespaceContext());
            Node teiHeader = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader", doc, XPathConstants.NODE);
            if (teiHeader == null) {
                Element tei = (Element)xpath.evaluate("//tei:TEI[" + this.interview + "]", doc, XPathConstants.NODE);
                Element teiHeaderN = tei.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "teiHeader");
                if (tei.getFirstChild() != null) {
                    tei.insertBefore(teiHeaderN, tei.getFirstChild());
                } else {
                    tei.appendChild(teiHeaderN);
                }
                doc.normalizeDocument();
            }
            if ((fileDesc = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc", doc, XPathConstants.NODE)) == null) {
                teiHeader = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader", doc, XPathConstants.NODE);
                fileDesc = teiHeader.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "fileDesc");
                if (teiHeader.getChildNodes().getLength() > 0) {
                    teiHeader.insertBefore(fileDesc, teiHeader.getChildNodes().item(0));
                } else {
                    teiHeader.appendChild(fileDesc);
                }
                doc.normalizeDocument();
            }
            if ((titleStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt", doc, XPathConstants.NODE)) == null) {
                fileDesc = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc", doc, XPathConstants.NODE);
                Element titleStmtN = fileDesc.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "titleStmt");
                fileDesc.appendChild(titleStmtN);
                doc.normalizeDocument();
            }
            if ((title = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title", doc, XPathConstants.NODE)) == null) {
                titleStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt", doc, XPathConstants.NODE);
                title = titleStmt.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "title");
                titleStmt.appendChild(title);
                doc.normalizeDocument();
            }
            title.setTextContent(this.mainForm.getTitleField().getText());
            Node editor = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:editor", doc, XPathConstants.NODE);
            if (editor == null) {
                titleStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt", doc, XPathConstants.NODE);
                editor = titleStmt.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "editor");
                titleStmt.appendChild(editor);
                doc.normalizeDocument();
            }
            editor.setTextContent(this.mainForm.getEditorField().getText());
            Node respStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:respStmt", doc, XPathConstants.NODE);
            if (respStmt == null) {
                titleStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt", doc, XPathConstants.NODE);
                respStmt = titleStmt.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "respStmt");
                titleStmt.appendChild(respStmt);
                doc.normalizeDocument();
            }
            if ((transcriber = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:respStmt/tei:name", doc, XPathConstants.NODE)) == null) {
                respStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:respStmt", doc, XPathConstants.NODE);
                transcriber = respStmt.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "name");
                respStmt.appendChild(transcriber);
                doc.normalizeDocument();
            }
            transcriber.setTextContent(this.mainForm.getTranscriberField().getText());
            Node resp = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:respStmt/tei:resp", doc, XPathConstants.NODE);
            if (resp == null) {
                respStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:respStmt", doc, XPathConstants.NODE);
                resp = respStmt.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "resp");
                resp.setTextContent(Constants.RESERVER_WORD_TO_TRANSCRIBER);
                respStmt.appendChild(resp);
                doc.normalizeDocument();
            }
            if ((principal = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:principal", doc, XPathConstants.NODE)) == null) {
                titleStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt", doc, XPathConstants.NODE);
                principal = titleStmt.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "principal");
                titleStmt.appendChild(principal);
                doc.normalizeDocument();
            }
            principal.setTextContent(this.mainForm.getPrincipalInvestigatorField().getText());
            Node researcher = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author", doc, XPathConstants.NODE);
            if (researcher == null) {
                titleStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt", doc, XPathConstants.NODE);
                researcher = titleStmt.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "author");
                titleStmt.appendChild(researcher);
                doc.normalizeDocument();
            }
            researcher.setTextContent(this.mainForm.getResearcherField().getText());
            Node funder = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:funder", doc, XPathConstants.NODE);
            if (funder == null) {
                titleStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt", doc, XPathConstants.NODE);
                funder = titleStmt.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "funder");
                funder.setTextContent(Constants.FUNDER_PROJECT);
                titleStmt.appendChild(funder);
                doc.normalizeDocument();
            }
            if ((sponsor = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:sponsor", doc, XPathConstants.NODE)) == null) {
                titleStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt", doc, XPathConstants.NODE);
                sponsor = titleStmt.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "sponsor");
                sponsor.setTextContent(Constants.SPONSOR_PROJECT);
                titleStmt.appendChild(sponsor);
                doc.normalizeDocument();
            }
            if ((publicationStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:publicationStmt", doc, XPathConstants.NODE)) == null) {
                fileDesc = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc", doc, XPathConstants.NODE);
                publicationStmt = fileDesc.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "publicationStmt");
                fileDesc.appendChild(publicationStmt);
                doc.normalizeDocument();
            }
            if ((idno = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:idno", doc, XPathConstants.NODE)) == null) {
                publicationStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:publicationStmt", doc, XPathConstants.NODE);
                idno = publicationStmt.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "idno");
                publicationStmt.appendChild(idno);
                doc.normalizeDocument();
            }
            idno.setTextContent(this.mainForm.getIdField().getText());
            Node autority = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:authority", doc, XPathConstants.NODE);
            if (autority == null) {
                publicationStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:publicationStmt", doc, XPathConstants.NODE);
                autority = publicationStmt.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "authority");
                publicationStmt.appendChild(autority);
                doc.normalizeDocument();
            }
            autority.setTextContent(this.mainForm.getAutorityField().getText());
            Node noteStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:notesStmt", doc, XPathConstants.NODE);
            if (noteStmt == null) {
                fileDesc = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc", doc, XPathConstants.NODE);
                noteStmt = fileDesc.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "notesStmt");
                fileDesc.appendChild(noteStmt);
                doc.normalizeDocument();
            }
            if ((description = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:notesStmt/tei:note", doc, XPathConstants.NODE)) == null) {
                noteStmt = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:notesStmt", doc, XPathConstants.NODE);
                description = noteStmt.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "note");
                noteStmt.appendChild(description);
                doc.normalizeDocument();
            }
            description.setTextContent(this.mainForm.getDescriptionField().getText());
            Element sourceDesc = (Element)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:sourceDesc", doc, XPathConstants.NODE);
            if (sourceDesc == null) {
                fileDesc = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc", doc, XPathConstants.NODE);
                sourceDesc = fileDesc.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "sourceDesc");
                fileDesc.appendChild(sourceDesc);
                doc.normalizeDocument();
            }
            if ((recordingStmt = (Element)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:recordingStmt", doc, XPathConstants.NODE)) == null) {
                sourceDesc = (Element)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:sourceDesc", doc, XPathConstants.NODE);
                recordingStmt = sourceDesc.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "recordingStmt");
                sourceDesc.appendChild(recordingStmt);
                doc.normalizeDocument();
            }
            if ((mediaFile = (Element)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:recordingStmt/tei:recording", doc, XPathConstants.NODE)) == null) {
                recordingStmt = (Element)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:recordingStmt", doc, XPathConstants.NODE);
                mediaFile = recordingStmt.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "recording");
                recordingStmt.appendChild(mediaFile);
                doc.normalizeDocument();
            }
            if (!this.mainForm.getMediaFileField().getText().equals("")) {
                mediaFile.setAttribute(TEIConstants.TEI_ATTRIBUTE_REND, this.mainForm.getMediaFileField().getText());
            } else {
                mediaFile.removeAttribute(TEIConstants.TEI_ATTRIBUTE_REND);
            }
            Element dateRecordingN = (Element)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:recordingStmt/tei:recording/tei:date", doc, XPathConstants.NODE);
            if (dateRecordingN == null) {
                Element recoding = (Element)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:recordingStmt/tei:recording", doc, XPathConstants.NODE);
                dateRecordingN = recoding.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "date");
                recoding.appendChild(dateRecordingN);
            }
            dateRecordingN.setAttribute(TEIConstants.TEI_ATTRIBUTE_WHEN, this.mainForm.getDateRecordingField().getText());
            Node profileDesc = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc", doc, XPathConstants.NODE);
            if (profileDesc == null) {
                teiHeader = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader", doc, XPathConstants.NODE);
                profileDesc = teiHeader.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "profileDesc");
                teiHeader.appendChild(profileDesc);
                doc.normalizeDocument();
            }
            if ((creation = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc/tei:creation", doc, XPathConstants.NODE)) == null) {
                profileDesc = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc", doc, XPathConstants.NODE);
                creation = profileDesc.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "creation");
                if (profileDesc.getChildNodes().getLength() > 0) {
                    profileDesc.insertBefore(creation, profileDesc.getChildNodes().item(0));
                } else {
                    profileDesc.appendChild(creation);
                }
                doc.normalizeDocument();
            }
            if ((dateTrans = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc/tei:creation/tei:date", doc, XPathConstants.NODE)) == null) {
                creation = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc/tei:creation", doc, XPathConstants.NODE);
                dateTrans = profileDesc.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "date");
                creation.appendChild(dateTrans);
                doc.normalizeDocument();
            }
            dateTrans.setTextContent(this.mainForm.getDateTranscriptionField().getText());
            Node settingDesc = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc/tei:settingDesc", doc, XPathConstants.NODE);
            if (settingDesc == null) {
                profileDesc = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc", doc, XPathConstants.NODE);
                settingDesc = profileDesc.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "settingDesc");
                profileDesc.appendChild(settingDesc);
                doc.normalizeDocument();
            }
            if ((setting = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc/tei:settingDesc/tei:setting", doc, XPathConstants.NODE)) == null) {
                settingDesc = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc/tei:settingDesc", doc, XPathConstants.NODE);
                setting = profileDesc.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "setting");
                settingDesc.appendChild(setting);
                doc.normalizeDocument();
            }
            if ((locale = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc/tei:settingDesc/tei:setting/tei:locale", doc, XPathConstants.NODE)) == null) {
                setting = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc/tei:settingDesc/tei:setting", doc, XPathConstants.NODE);
                locale = profileDesc.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "locale");
                setting.appendChild(locale);
                doc.normalizeDocument();
            }
            locale.setTextContent(this.mainForm.getLocaleField().getText());
            Node activity = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc/tei:settingDesc/tei:setting/tei:activity", doc, XPathConstants.NODE);
            if (activity == null) {
                setting = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc/tei:settingDesc/tei:setting", doc, XPathConstants.NODE);
                activity = profileDesc.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "activity");
                setting.appendChild(activity);
                doc.normalizeDocument();
            }
            activity.setTextContent(this.mainForm.getActivityField().getText());
            Element langUsage = (Element)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc/tei:langUsage", doc, XPathConstants.NODE);
            if (langUsage == null) {
                profileDesc = (Node)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc", doc, XPathConstants.NODE);
                langUsage = profileDesc.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "langUsage");
                profileDesc.appendChild(langUsage);
                doc.normalizeDocument();
            }
            if ((language = (Element)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language", doc, XPathConstants.NODE)) == null) {
                langUsage = (Element)xpath.evaluate("//tei:TEI[" + this.interview + "]/tei:teiHeader/tei:profileDesc/tei:langUsage", doc, XPathConstants.NODE);
                language = langUsage.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "language");
                langUsage.appendChild(language);
            }
            language.setTextContent(this.mainForm.getLanguageVariety().getText());
            if (!this.mainForm.getLanguageField().getText().equals("")) {
                language.setAttribute(TEIConstants.TEI_ATTRIBUTE_IDENT, this.mainForm.getLanguageField().getText());
            } else {
                language.removeAttribute(TEIConstants.TEI_ATTRIBUTE_IDENT);
                language.getParentNode().getParentNode().removeChild(language.getParentNode());
            }
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            df.setLenient(false);
            ParsePosition p = new ParsePosition(0);
            if (!this.mainForm.getDateTranscriptionField().getText().equals("")) {
                date = df.parse(this.mainForm.getDateTranscriptionField().getText(), p);
                if (p.getErrorIndex() == -1) {
                    this.mainForm.getDateTranscriptionField().setBackground(Color.WHITE);
                } else {
                    this.mainForm.getDateTranscriptionField().setBackground(Color.RED);
                }
            } else {
                this.mainForm.getDateTranscriptionField().setBackground(Color.WHITE);
            }
            df = new SimpleDateFormat("yyyy-MM-dd");
            df.setLenient(false);
            p = new ParsePosition(0);
            date = df.parse(this.mainForm.getDateRecordingField().getText(), p);
            if (!this.mainForm.getDateRecordingField().getText().equals("")) {
                if (p.getErrorIndex() == -1) {
                    this.mainForm.getDateRecordingField().setBackground(Color.WHITE);
                } else {
                    this.mainForm.getDateRecordingField().setBackground(Color.RED);
                }
            } else {
                dateRecordingN.removeAttribute(TEIConstants.TEI_ATTRIBUTE_WHEN);
            }
            doc.normalizeDocument();
        }
        catch (XPathExpressionException e1) {
            e1.printStackTrace();
        }
    }
}

