/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.xml;

import annotador.action.interview.CloseInterviewAction;
import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class CommitXMLAction
implements ActionListener {
    MainForm mainForm;

    public CommitXMLAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String text = this.mainForm.getXmlPane().getText();
        try {
            Document doc = null;
            if (Constants.IS_COLLABORATIVE_CORPUS != null && Constants.DOCUMENT_OPENED == null) {
                try {
                    CMTManager.getCMT().unlockUpdatingTaxonomyTree(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, text);
                    Constants.IS_TAXONOMY_LOCKED = false;
                }
                catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
            if (Constants.DOCUMENT_OPENED == null) {
                doc = DOMUtils.StringToDOM(text, Constants.ENCODING, Constants.TEI_NAMESPACE);
                DocumentSingleton.loadFromDOM(doc);
            } else {
                XPath xpath = XPathFactory.newInstance().newXPath();
                xpath.setNamespaceContext(new TEINamespaceContext());
                String xpathExpression = Constants.IS_COLLABORATIVE_CORPUS == null ? "/tei:teiCorpus/tei:TEI[" + Constants.DOCUMENT_OPENED + "]" : "/tei:teiCorpus/tei:TEI[1]";
                Node node = (Node)xpath.evaluate(xpathExpression, DocumentSingleton.getInstance(), XPathConstants.NODE);
                Document newDocument = DOMUtils.StringToDOM(text, Constants.ENCODING, Constants.TEI_NAMESPACE);
                Node newNode = DocumentSingleton.getInstance().importNode(newDocument.getDocumentElement(), true);
                node.getParentNode().replaceChild(newNode, node);
            }
            Utils.setChangedCorpus(true);
            CloseInterviewAction cia = new CloseInterviewAction(this.mainForm);
            cia.setOption(0);
            cia.actionPerformed(new ActionEvent(this, 0, "save"));
            this.mainForm.getEditXML().setEnabled(true);
        }
        catch (Exception e1) {
            JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), e1.getMessage(), Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE)));
        }
        for (ComponentListener l : this.mainForm.getXmlViewPanel().getComponentListeners()) {
            l.componentShown(new ComponentEvent(this.mainForm.getXmlViewPanel(), 1));
        }
        this.mainForm.getTabbedPane().setEnabledAt(2, false);
    }
}

