/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.xml;

import annotador.action.xml.TaskXML;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.PrintStream;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Node;

public class XMLViewComponentListener
extends ComponentAdapter {
    JTextPane xmlPane;
    Pattern xmlTagPattern;
    Pattern xmlAttPattern;
    MainForm mainForm;
    JDialog ss;
    Timer timer;
    ImageIcon image;

    public XMLViewComponentListener(MainForm mainForm) {
        this.mainForm = mainForm;
        this.xmlPane = mainForm.getXmlPane();
        this.xmlTagPattern = Pattern.compile("(<[/|?]*(\\p{Alnum}|\\n)+.*?>)|<!--(.|\\n)*?-->", 8);
        this.xmlAttPattern = Pattern.compile("\\p{Alnum}+=\".*?\"", 8);
        this.image = new ImageIcon(Utils.getReourceImage(mainForm, Constants.BUILDING_ICON));
        this.ss = new JDialog();
        this.ss.add(new JLabel(this.image));
        this.ss.setModal(false);
        this.ss.setSize(this.image.getIconWidth(), this.image.getIconHeight());
        this.ss.setUndecorated(true);
        this.ss.setResizable(false);
    }

    @Override
    public void componentShown(ComponentEvent e) {
        super.componentShown(e);
        if (this.xmlPane.isShowing()) {
            this.mainForm.getEditXML().setEnabled(true);
            this.mainForm.getCommitXML().setEnabled(false);
            this.mainForm.getDiscartXML().setEnabled(false);
            this.mainForm.getXmlPane().setEditable(false);
            this.ss.setLocationRelativeTo(this.mainForm.getXmlScrollPane());
            this.ss.setVisible(true);
            this.ss.repaint();
            this.ss.update(this.ss.getGraphics());
            this.ss.setCursor(Cursor.getPredefinedCursor(3));
            this.timer = new Timer();
            TaskXML t = new TaskXML(this.mainForm, this.ss, this.timer);
            this.timer.schedule((TimerTask)t, 300, 500);
            Date startD = new Date();
            this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(3));
            try {
                String text = "";
                if (Constants.DOCUMENT_OPENED == null) {
                    text = DOMUtils.DOMToString(DocumentSingleton.getInstance(), Constants.ENCODING);
                } else {
                    XPath xpath = XPathFactory.newInstance().newXPath();
                    xpath.setNamespaceContext(new TEINamespaceContext());
                    String xpathExpression = Constants.IS_COLLABORATIVE_CORPUS == null ? "/tei:teiCorpus/tei:TEI[" + Constants.DOCUMENT_OPENED + "]" : "/tei:teiCorpus/tei:TEI[1]";
                    Node node = (Node)xpath.evaluate(xpathExpression, DocumentSingleton.getInstance(), XPathConstants.NODE);
                    text = DOMUtils.DOMToString(node, Constants.ENCODING);
                }
                this.xmlPane.setText(text);
                if (Constants.SINTAX_HIGHLIGHTING.equalsIgnoreCase("true")) {
                    int end;
                    this.xmlPane.selectAll();
                    SimpleAttributeSet attributeSetCharacter = new SimpleAttributeSet();
                    StyleConstants.setFontFamily(attributeSetCharacter, Constants.FONT_NAME);
                    StyleConstants.setFontSize(attributeSetCharacter, Integer.parseInt(Constants.FONT_SIZE));
                    this.xmlPane.setCharacterAttributes(attributeSetCharacter, true);
                    Matcher matcher = this.xmlTagPattern.matcher(this.xmlPane.getSelectedText());
                    while (matcher.find()) {
                        int start = matcher.start();
                        end = matcher.end();
                        this.xmlPane.select(start, end);
                        String selectedText = this.xmlPane.getSelectedText();
                        if (selectedText.contains("!")) {
                            StyleConstants.setForeground(attributeSetCharacter, Color.decode(Constants.COLOR_COMMENT_TAGS));
                        } else if (selectedText.contains("?")) {
                            StyleConstants.setForeground(attributeSetCharacter, Color.decode(Constants.COLOR_PROCESS_TAGS));
                        } else {
                            StyleConstants.setForeground(attributeSetCharacter, Color.decode(Constants.COLOR_XML_TAGS));
                        }
                        this.xmlPane.setCharacterAttributes(attributeSetCharacter, false);
                    }
                    this.xmlPane.selectAll();
                    this.xmlAttPattern = Pattern.compile("\\p{Alnum}+=\".*?\"", 8);
                    matcher = this.xmlAttPattern.matcher(this.xmlPane.getSelectedText());
                    attributeSetCharacter = new SimpleAttributeSet();
                    StyleConstants.setFontFamily(attributeSetCharacter, Constants.FONT_NAME);
                    StyleConstants.setForeground(attributeSetCharacter, Color.decode(Constants.COLOR_XML_ATT));
                    while (matcher.find()) {
                        int start = matcher.start();
                        end = matcher.end();
                        this.xmlPane.select(start, end);
                        this.xmlPane.setCharacterAttributes(attributeSetCharacter, false);
                    }
                    this.xmlPane.validate();
                    if (Constants.DEBUG) {
                        Date end2 = new Date();
                        long gap = end2.getTime() - startD.getTime();
                        System.out.println("XMLViewComponentListener.componentShown() -> " + gap + " ms");
                    }
                }
            }
            catch (Exception iae) {
                this.xmlPane.setText(Internationalization.XML_PANE_ERROR_1);
            }
        }
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    public void componentHidden(ComponentEvent e) {
        if (this.mainForm.getEditXML().isEnabled()) return;
        if (0 == JOptionPane.showConfirmDialog(this.mainForm.getTabbedPane(), Internationalization.XML_HIDDEN_COMPONENT_TEXT, Internationalization.WARNING_WORD, 0, 2)) {
            ActionListener[] arr$ = this.mainForm.getCommitXML().getActionListeners();
            int len$ = arr$.length;
            int i$ = 0;
            while (i$ < len$) {
                ActionListener l = arr$[i$];
                l.actionPerformed(new ActionEvent(this, 1, "command"));
                ++i$;
            }
            return;
        }
        ActionListener[] arr$ = this.mainForm.getDiscartXML().getActionListeners();
        int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$) {
            ActionListener l = arr$[i$];
            l.actionPerformed(new ActionEvent(this, 1, "command"));
            ++i$;
        }
    }
}

