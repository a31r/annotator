/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.xml;

import annotador.gui.MainForm;
import java.awt.Component;
import java.awt.Cursor;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class TaskXML
extends TimerTask {
    MainForm mainForm;
    JDialog ss;
    Timer timer;

    public TaskXML(MainForm mainForm, JDialog ss, Timer timer) {
        this.mainForm = mainForm;
        this.ss = ss;
        this.timer = timer;
    }

    @Override
    public void run() {
        if (this.mainForm.getXmlPane().isValid()) {
            this.ss.setLocationRelativeTo(this.mainForm.getXmlScrollPane());
            this.ss.setVisible(false);
            this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(0));
            this.timer.cancel();
        }
    }
}

