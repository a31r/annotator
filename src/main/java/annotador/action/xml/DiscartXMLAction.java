/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.xml;

import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.JPanel;

public class DiscartXMLAction
implements ActionListener {
    MainForm mainForm;

    public DiscartXMLAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (Constants.IS_COLLABORATIVE_CORPUS != null && Constants.DOCUMENT_OPENED == null) {
            try {
                CMTManager.getCMT().unlockDiscartingTaxonomyTree(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS);
                Constants.IS_TAXONOMY_LOCKED = false;
            }
            catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        for (ComponentListener l : this.mainForm.getXmlViewPanel().getComponentListeners()) {
            l.componentShown(new ComponentEvent(this.mainForm.getXmlViewPanel(), 1));
        }
    }
}

