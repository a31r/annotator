/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.xml;

import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JTextPane;

public class EditXMLAction
implements ActionListener {
    MainForm form;

    public EditXMLAction(MainForm form) {
        this.form = form;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (Constants.IS_COLLABORATIVE_CORPUS != null && Constants.DOCUMENT_OPENED == null) {
            try {
                CMTManager.getCMT().lockTaxonomyTree(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS);
                Constants.IS_TAXONOMY_LOCKED = true;
            }
            catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        this.form.getXmlPane().setCursor(Cursor.getPredefinedCursor(3));
        this.form.getXmlPane().setEditable(true);
        this.form.getCommitXML().setEnabled(true);
        this.form.getEditXML().setEnabled(false);
        this.form.getDiscartXML().setEnabled(true);
        this.form.getXmlPane().setCursor(Cursor.getPredefinedCursor(0));
    }
}

