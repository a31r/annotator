/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.text;

import annotador.gui.MainForm;
import annotador.text.cellrender.node.NodeTableRendererEditor;
import annotador.text.cellrender.node.TranscriptionRenderer;
import annotador.utils.DocumentSingleton;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellRenderer;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class DeleteKeywordActionListener
implements ActionListener {
    Element element;
    JList list;
    JTable table;
    MainForm mainForm;
    int row;
    Node utterance;

    public DeleteKeywordActionListener(JTable table, Element element, Node utterance, MainForm mainForm, int row) {
        this.row = row;
        this.table = table;
        this.utterance = utterance;
        this.element = element;
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String next = this.element.getAttribute(TEIConstants.TEI_ATTRIBUTE_NEXT);
        Node parent = this.element.getParentNode();
        parent.removeChild(this.element);
        ArrayList<Node> childs = new ArrayList();
        childs = Utils.getInOrderRecursivo(1, this.utterance, childs);
        for (Node n : childs) {
            String id;
            Element el;
            if (!(n instanceof Element) || !(id = (el = (Element)n).getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID)).equals(next)) continue;
            el.getParentNode().removeChild(el);
        }
        Utils.setChangedCorpus(true);
        DocumentSingleton.getInstance().normalizeDocument();
        this.table.editingCanceled(new ChangeEvent(this));
        TableCellRenderer tc = this.table.getCellRenderer(this.row, 3);
        if (tc instanceof NodeTableRendererEditor) {
            NodeTableRendererEditor tr = (NodeTableRendererEditor)tc;
            int sectionRow = Utils.calculateStartSectionRow(this.row, this.table);
            int end = Utils.calculateEndSectionRow(this.row, this.table);
            for (int i = sectionRow; i <= end; ++i) {
                if (tr.getUterranceRenderer().get(i) != null) {
                    tr.getUterranceRenderer().get(i).invalidateField();
                }
                this.table.prepareRenderer(this.table.getDefaultRenderer(Node.class), i, 3);
            }
        }
        this.table.repaint();
    }
}

