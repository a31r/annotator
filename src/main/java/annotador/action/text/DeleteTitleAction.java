/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.text;

import annotador.gui.MainForm;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DeleteTitleAction
implements ActionListener {
    MainForm mainForm;
    Node div;

    public DeleteTitleAction(Node div, MainForm mainForm) {
        this.div = div;
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        NodeList childs = this.div.getChildNodes();
        for (int i = 0; i < childs.getLength(); ++i) {
            Node node = childs.item(i);
            if (!(node instanceof Element)) continue;
            Element n = (Element)node;
            if (!node.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_HEAD)) continue;
            this.div.removeChild(node);
            Utils.setChangedCorpus(true);
        }
        this.mainForm.getDocumentTable().editingCanceled(new ChangeEvent(this));
    }
}

