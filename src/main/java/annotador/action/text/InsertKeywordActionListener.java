/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.text;

import annotador.gui.MainForm;
import annotador.text.TEITableModel;
import annotador.text.cellrender.node.NodeTableRendererEditor;
import annotador.text.cellrender.node.TranscriptionRenderer;
import annotador.text.cellrender.node.appliedtags.TaxonomiesListRenderer;
import annotador.text.cellrender.node.transcriptor.Section;
import annotador.text.cellrender.node.transcriptor.TranscriptionRendererManager;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.ShowedCategory;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import table.model.CellAttribute;
import table.model.CellSpan;

public class InsertKeywordActionListener
implements ActionListener {
    String category;
    Node node;
    MainForm mainForm;
    TranscriptionRendererManager manager;
    JTextPane pane;
    int row;
    public static final int SELECCTION_NONE = 1;
    public static final int SELECCTION_PARCIAL = 2;
    public static final int SELECCTION_TOTAL = 3;
    public static final int SELECCTION_SPECIAL = -1;

    public TranscriptionRendererManager getManager() {
        return this.manager;
    }

    public int random() {
        double rnd = Math.random();
        rnd *= 8999.0;
        return (int)Math.round(rnd += 1000.0);
    }

    public InsertKeywordActionListener(String category, Node node, MainForm mainForm, JTextPane pane, TranscriptionRendererManager manager, int row) {
        this.category = category;
        this.node = node;
        this.mainForm = mainForm;
        this.manager = manager;
        this.pane = pane;
        this.row = row;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(3));
        int selectionStart = this.pane.getSelectionStart();
        int selectionEnd = this.pane.getSelectionEnd();
        if (selectionStart > selectionEnd) {
            int aux = selectionEnd;
            selectionEnd = selectionStart;
            selectionStart = aux;
        }
        if (this.pane.getText().charAt(selectionStart) == ' ' && selectionStart == selectionEnd - 1) {
            selectionStart = selectionEnd;
        } else {
            if (this.pane.getText().charAt(selectionStart) == ' ') {
                ++selectionStart;
            }
            if (selectionEnd > 0 && this.pane.getText().charAt(selectionEnd - 1) == ' ') {
                --selectionEnd;
            }
        }
        if (selectionEnd != selectionStart) {
            ArrayList<Node> childs = new ArrayList();
            childs = Utils.getInOrderRecursivo(1, this.node, childs);
            ArrayList<Integer> impliedNodes = new ArrayList<Integer>();
            ArrayList<Section> sections = this.manager.getSections();
            for (Section actualSection : sections) {
                if (selectionStart > actualSection.getVisualStartIndex() && selectionStart < actualSection.getVisualEndIndex() || selectionEnd > actualSection.getVisualStartIndex() && selectionEnd < actualSection.getVisualEndIndex()) {
                    impliedNodes.add(2);
                }
                if (selectionEnd < actualSection.getVisualStartIndex() || selectionStart >= actualSection.getVisualEndIndex()) {
                    impliedNodes.add(1);
                }
                if (selectionEnd < actualSection.getVisualEndIndex() || selectionStart > actualSection.getVisualStartIndex()) continue;
                impliedNodes.add(3);
            }
            int numOfAnchor = 0;
            int indexOfInterview = 0;
            TableModel tm = this.mainForm.getDocumentTable().getModel();
            if (tm instanceof TEITableModel) {
                TEITableModel teitm = (TEITableModel)tm;
                indexOfInterview = teitm.getIndexOfInterview();
            }
            for (Node selectedNode : childs) {
                if (!selectedNode.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_ANCHOR)) continue;
                Element anchor = (Element)selectedNode;
                String id = anchor.getAttribute(TEIConstants.TEI_ATTRIBUTE_NEXT);
                if (!id.equals("") && Integer.parseInt(id = id.substring(id.indexOf("id") + 2)) > numOfAnchor) {
                    numOfAnchor = Integer.parseInt(id);
                }
                if (!(id = anchor.getAttribute(TEIConstants.TEI_ATTRIBUTE_PREV)).equals("") && Integer.parseInt(id = id.substring(id.indexOf("id") + 2)) > numOfAnchor) {
                    numOfAnchor = Integer.parseInt(id);
                }
                if ((id = anchor.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID)).equals("") || Integer.parseInt(id = id.substring(id.indexOf("id") + 2)) <= numOfAnchor) continue;
                numOfAnchor = Integer.parseInt(id);
            }
            String before = "";
            String next = "";
            int k = -1;
            for (Integer selectedSection : impliedNodes) {
                Element newElementEnd;
                Element newElementStart;
                ++k;
                if (selectedSection != 2 && selectedSection != 3) continue;
                Section section = this.getManager().getSections().get(k);
                Node selectedNode2 = section.getNode();
                Node parentNode = section.getNode().getParentNode();
                if (selectedNode2 instanceof Text) {
                    Text newSelectedText;
                    String paneTextOfSection;
                    if (selectionStart >= section.getVisualStartIndex() && selectionEnd <= section.getVisualEndIndex()) {
                        selectedNode2.setTextContent(this.pane.getText().substring(section.getVisualStartIndex(), selectionStart));
                        newElementStart = parentNode.getOwnerDocument().createElement(TEIConstants.TEI_TAG_ANCHOR);
                        newElementStart.setAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE, this.category);
                        before = "rd" + this.random() + "c" + indexOfInterview + "r" + this.row + "id" + ++numOfAnchor;
                        newElementStart.setAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID, before);
                        next = "rd" + this.random() + "c" + indexOfInterview + "r" + this.row + "id" + (numOfAnchor + 1);
                        newElementStart.setAttribute(TEIConstants.TEI_ATTRIBUTE_NEXT, next);
                        try {
                            parentNode.insertBefore(newElementStart, selectedNode2.getNextSibling());
                        }
                        catch (DOMException d) {
                            parentNode.appendChild(newElementStart);
                        }
                        catch (IndexOutOfBoundsException d) {
                            parentNode.appendChild(newElementStart);
                        }
                        String paneTextOfSection2 = this.pane.getText().substring(selectionStart, selectionEnd);
                        newSelectedText = parentNode.getOwnerDocument().createTextNode(paneTextOfSection2);
                        try {
                            parentNode.insertBefore(newSelectedText, newElementStart.getNextSibling());
                        }
                        catch (DOMException d) {
                            parentNode.appendChild(newSelectedText);
                        }
                        catch (IndexOutOfBoundsException d) {
                            parentNode.appendChild(newSelectedText);
                        }
                        Element newElementEnd2 = parentNode.getOwnerDocument().createElement(TEIConstants.TEI_TAG_ANCHOR);
                        ++numOfAnchor;
                        newElementEnd2.setAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE, this.category);
                        newElementEnd2.setAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID, next);
                        newElementEnd2.setAttribute(TEIConstants.TEI_ATTRIBUTE_PREV, before);
                        try {
                            parentNode.insertBefore(newElementEnd2, newSelectedText.getNextSibling());
                        }
                        catch (DOMException d) {
                            parentNode.appendChild(newElementEnd2);
                        }
                        catch (IndexOutOfBoundsException d) {
                            parentNode.appendChild(newElementEnd2);
                        }
                        Text newText = parentNode.getOwnerDocument().createTextNode(this.pane.getText().substring(selectionEnd, section.getVisualEndIndex()));
                        try {
                            parentNode.insertBefore(newText, newElementEnd2.getNextSibling());
                        }
                        catch (DOMException d) {
                            parentNode.appendChild(newText);
                        }
                        catch (IndexOutOfBoundsException d) {
                            parentNode.appendChild(newText);
                        }
                    } else if (selectionStart >= section.getVisualStartIndex() && selectionEnd > section.getVisualEndIndex()) {
                        paneTextOfSection = this.pane.getText().substring(selectionStart, section.getVisualEndIndex());
                        selectedNode2.setTextContent(this.pane.getText().substring(section.getVisualStartIndex(), selectionStart));
                        Element newElementStart2 = parentNode.getOwnerDocument().createElement(TEIConstants.TEI_TAG_ANCHOR);
                        before = "rd" + this.random() + "c" + indexOfInterview + "r" + this.row + "id" + ++numOfAnchor;
                        next = "rd" + this.random() + "c" + indexOfInterview + "r" + this.row + "id" + (numOfAnchor + 1);
                        newElementStart2.setAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE, this.category);
                        newElementStart2.setAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID, before);
                        newElementStart2.setAttribute(TEIConstants.TEI_ATTRIBUTE_NEXT, next);
                        try {
                            parentNode.insertBefore(newElementStart2, selectedNode2.getNextSibling());
                        }
                        catch (DOMException d) {
                            parentNode.appendChild(newElementStart2);
                        }
                        catch (IndexOutOfBoundsException d) {
                            parentNode.appendChild(newElementStart2);
                        }
                        newSelectedText = parentNode.getOwnerDocument().createTextNode(paneTextOfSection);
                        try {
                            parentNode.insertBefore(newSelectedText, newElementStart2.getNextSibling());
                        }
                        catch (DOMException d) {
                            parentNode.appendChild(newSelectedText);
                        }
                        catch (IndexOutOfBoundsException d) {
                            parentNode.appendChild(newSelectedText);
                        }
                    } else if (selectionEnd <= section.getVisualEndIndex() && selectionStart < section.getVisualStartIndex()) {
                        paneTextOfSection = this.pane.getText().substring(section.getVisualStartIndex(), selectionEnd);
                        selectedNode2.setTextContent(paneTextOfSection);
                        newElementEnd = parentNode.getOwnerDocument().createElement(TEIConstants.TEI_TAG_ANCHOR);
                        ++numOfAnchor;
                        newElementEnd.setAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE, this.category);
                        newElementEnd.setAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID, next);
                        newElementEnd.setAttribute(TEIConstants.TEI_ATTRIBUTE_PREV, before);
                        next = "";
                        before = "";
                        if (selectedNode2.getNextSibling() != null) {
                            parentNode.insertBefore(newElementEnd, selectedNode2.getNextSibling());
                        } else {
                            parentNode.appendChild(newElementEnd);
                        }
                        newSelectedText = parentNode.getOwnerDocument().createTextNode(this.pane.getText().substring(selectionEnd, section.getVisualEndIndex()));
                        if (newElementEnd.getNextSibling() != null) {
                            parentNode.insertBefore(newSelectedText, newElementEnd.getNextSibling());
                        } else {
                            parentNode.appendChild(newSelectedText);
                        }
                    }
                }
                if (!(selectedNode2 instanceof Element)) continue;
                if (selectionStart >= section.getVisualStartIndex() && selectionEnd <= section.getVisualEndIndex()) {
                    newElementStart = parentNode.getOwnerDocument().createElement(TEIConstants.TEI_TAG_ANCHOR);
                    newElementStart.setAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE, this.category);
                    before = "rd" + this.random() + "c" + indexOfInterview + "r" + this.row + "id" + ++numOfAnchor;
                    next = "rd" + this.random() + "c" + indexOfInterview + "r" + this.row + "id" + (numOfAnchor + 1);
                    newElementStart.setAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID, before);
                    newElementStart.setAttribute(TEIConstants.TEI_ATTRIBUTE_NEXT, next);
                    try {
                        parentNode.insertBefore(newElementStart, selectedNode2);
                    }
                    catch (DOMException d) {
                        parentNode.appendChild(newElementStart);
                    }
                    catch (IndexOutOfBoundsException d) {
                        parentNode.appendChild(newElementStart);
                    }
                    newElementEnd = parentNode.getOwnerDocument().createElement(TEIConstants.TEI_TAG_ANCHOR);
                    ++numOfAnchor;
                    newElementEnd.setAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE, this.category);
                    newElementEnd.setAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID, next);
                    newElementEnd.setAttribute(TEIConstants.TEI_ATTRIBUTE_PREV, before);
                    next = "";
                    before = "";
                    if (selectedNode2.getNextSibling() != null) {
                        parentNode.insertBefore(newElementEnd, selectedNode2.getNextSibling());
                        continue;
                    }
                    parentNode.appendChild(newElementEnd);
                    continue;
                }
                if (selectionStart >= section.getVisualStartIndex() && selectionEnd > section.getVisualEndIndex()) {
                    newElementStart = parentNode.getOwnerDocument().createElement(TEIConstants.TEI_TAG_ANCHOR);
                    newElementStart.setAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE, this.category);
                    before = "rd" + this.random() + "c" + indexOfInterview + "r" + this.row + "id" + ++numOfAnchor;
                    next = "rd" + this.random() + "c" + indexOfInterview + "r" + this.row + "id" + (numOfAnchor + 1);
                    newElementStart.setAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID, before);
                    newElementStart.setAttribute(TEIConstants.TEI_ATTRIBUTE_NEXT, next);
                    try {
                        parentNode.insertBefore(newElementStart, selectedNode2);
                    }
                    catch (DOMException d) {
                        parentNode.appendChild(newElementStart);
                    }
                    catch (IndexOutOfBoundsException d) {
                        parentNode.appendChild(newElementStart);
                    }
                    continue;
                }
                if (selectionEnd > section.getVisualEndIndex() || selectionStart >= section.getVisualStartIndex()) continue;
                Element newElementEnd3 = parentNode.getOwnerDocument().createElement(TEIConstants.TEI_TAG_ANCHOR);
                ++numOfAnchor;
                newElementEnd3.setAttribute(TEIConstants.TEI_ATTRIBUTE_TYPE, this.category);
                newElementEnd3.setAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID, next);
                newElementEnd3.setAttribute(TEIConstants.TEI_ATTRIBUTE_PREV, before);
                next = "";
                before = "";
                if (parentNode.getNextSibling() != null) {
                    parentNode.insertBefore(newElementEnd3, selectedNode2.getNextSibling());
                    continue;
                }
                parentNode.appendChild(newElementEnd3);
            }
            JList list = (JList)this.mainForm.getDocumentTable().prepareRenderer(this.mainForm.getDocumentTable().getCellRenderer(this.calculateDivRow(this.row), 1), this.calculateDivRow(this.row), 1);
            TaxonomiesListRenderer tlr = (TaxonomiesListRenderer)list.getCellRenderer();
            tlr.invalidateCache();
            ShowedCategory.setShowAppliedCategory("I" + indexOfInterview + "R" + this.calculateDivRow(this.row) + this.category, true);
            Utils.setChangedCorpus(true);
            DocumentSingleton.getInstance().normalize();
            DocumentSingleton.getInstance().normalizeDocument();
            int start = Utils.calculateStartSectionRow(this.row, this.mainForm.getDocumentTable());
            int end = Utils.calculateEndSectionRow(this.row, this.mainForm.getDocumentTable());
            for (int i = start; i <= end; ++i) {
                NodeTableRendererEditor tr;
                TableCellRenderer cr = this.mainForm.getDocumentTable().getCellRenderer(this.row, 3);
                if (!(cr instanceof NodeTableRendererEditor) || (tr = (NodeTableRendererEditor)cr).getUterranceRenderer().get(i) == null) continue;
                tr.getUterranceRenderer().get(i).invalidateField();
            }
            TableCellEditor tableCellEditor = this.mainForm.getDocumentTable().getDefaultEditor(Node.class);
            tableCellEditor.cancelCellEditing();
            tableCellEditor.stopCellEditing();
            this.mainForm.getDocumentTable().invalidate();
            this.mainForm.getDocumentTable().repaint();
            this.mainForm.getTaxonomyTree().invalidate();
            this.mainForm.getTaxonomyTree().repaint();
        } else {
            JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.INSERT_CATEGORY_ERROR_MESSAGE_1, Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE)));
        }
        this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(0));
    }

    private int calculateDivRow(int uRow) {
        TEITableModel model = (TEITableModel)this.mainForm.getDocumentTable().getModel();
        CellSpan span = (CellSpan)((Object)model.getCellAttribute());
        for (int i = uRow; i >= 0; --i) {
            if (!span.isVisible(i, 0)) continue;
            return i;
        }
        return -1;
    }
}

