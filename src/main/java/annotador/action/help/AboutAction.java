/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.help;

import annotador.gui.AcercaDeDialogTranspatent;
import annotador.gui.MainForm;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTabbedPane;

public class AboutAction
implements ActionListener {
    MainForm mainForm;

    public AboutAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        AcercaDeDialogTranspatent acercaDeDialogTranspatent = new AcercaDeDialogTranspatent(this.mainForm.getTabbedPane(), this.mainForm);
    }
}

