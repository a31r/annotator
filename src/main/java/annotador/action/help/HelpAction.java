/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.help;

import annotador.gui.HelpForm;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

public class HelpAction
implements ActionListener {
    MainForm mainForm;

    public HelpAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JDialog dialog = new JDialog(this.mainForm.getVentanaPrincipal(), Internationalization.HELP_TITLE, true);
        HelpForm form = new HelpForm(dialog);
        try {
            form.getHelpPanel().setPage("file:///" + Constants.WORKING_PATH + Constants.URL_HTML_PAGE);
        }
        catch (IOException e1) {
            File dir = new File(Constants.WORKING_PATH + Constants.HTML_DIR);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            Utils.createReourceFile("/docs/help.html", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "help.html");
            Utils.createReourceFile("/docs/annotating.html", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "annotating.html");
            Utils.createReourceFile("/docs/config.html", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "config.html");
            Utils.createReourceFile("/docs/essentials.html", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "essentials.html");
            Utils.createReourceFile("/docs/installation.html", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "installation.html");
            Utils.createReourceFile("/docs/others.html", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "others.html");
            Utils.createReourceFile("/docs/references.html", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "references.html");
            Utils.createReourceFile("/docs/collaboration.html", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "collaboration.html");
            Utils.createReourceFile("/docs/1.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "1.jpg");
            Utils.createReourceFile("/docs/2.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "2.jpg");
            Utils.createReourceFile("/docs/3.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "3.jpg");
            Utils.createReourceFile("/docs/4.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "4.jpg");
            Utils.createReourceFile("/docs/5.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "5.jpg");
            Utils.createReourceFile("/docs/6.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "6.jpg");
            Utils.createReourceFile("/docs/7.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "7.jpg");
            Utils.createReourceFile("/docs/8.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "8.jpg");
            Utils.createReourceFile("/docs/9.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "9.jpg");
            Utils.createReourceFile("/docs/10.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "10.jpg");
            Utils.createReourceFile("/docs/11.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "11.jpg");
            Utils.createReourceFile("/docs/12.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "12.jpg");
            Utils.createReourceFile("/docs/13.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "13.jpg");
            Utils.createReourceFile("/docs/14.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "14.jpg");
            Utils.createReourceFile("/docs/15.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "15.jpg");
            Utils.createReourceFile("/docs/16.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "16.jpg");
            Utils.createReourceFile("/docs/17.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "17.jpg");
            Utils.createReourceFile("/docs/18.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "18.jpg");
            Utils.createReourceFile("/docs/19.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "19.jpg");
            Utils.createReourceFile("/docs/20.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "20.jpg");
            Utils.createReourceFile("/docs/21.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "21.jpg");
            Utils.createReourceFile("/docs/22.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "22.jpg");
            Utils.createReourceFile("/docs/23.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "23.jpg");
            Utils.createReourceFile("/docs/24.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "24.jpg");
            Utils.createReourceFile("/docs/25.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "25.jpg");
            Utils.createReourceFile("/docs/26.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "26.jpg");
            Utils.createReourceFile("/docs/27.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "27.jpg");
            Utils.createReourceFile("/docs/28.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "28.jpg");
            Utils.createReourceFile("/docs/29.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "29.jpg");
            Utils.createReourceFile("/docs/30.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "30.jpg");
            Utils.createReourceFile("/docs/31.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "31.jpg");
            Utils.createReourceFile("/docs/32.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "32.jpg");
            Utils.createReourceFile("/docs/33.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "33.jpg");
            Utils.createReourceFile("/docs/34.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "34.jpg");
            Utils.createReourceFile("/docs/35.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "35.jpg");
            Utils.createReourceFile("/docs/36.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "36.jpg");
            Utils.createReourceFile("/docs/37.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "37.jpg");
            Utils.createReourceFile("/docs/38.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "38.jpg");
            Utils.createReourceFile("/docs/39.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "39.jpg");
            Utils.createReourceFile("/docs/40.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "40.jpg");
            Utils.createReourceFile("/docs/41.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "41.jpg");
            Utils.createReourceFile("/docs/42.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "42.jpg");
            Utils.createReourceFile("/docs/43.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "43.jpg");
            Utils.createReourceFile("/docs/44.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "44.jpg");
            Utils.createReourceFile("/docs/45.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "45.jpg");
            Utils.createReourceFile("/docs/46.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "46.jpg");
            Utils.createReourceFile("/docs/47.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "47.jpg");
            Utils.createReourceFile("/docs/48.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "48.jpg");
            Utils.createReourceFile("/docs/49.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "49.jpg");
            Utils.createReourceFile("/docs/50.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "50.jpg");
            Utils.createReourceFile("/docs/51.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "51.jpg");
            Utils.createReourceFile("/docs/backbone_logo.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "backbone_logo.jpg");
            Utils.createReourceFile("/docs/logo_sacodeyl.jpeg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "logo_sacodeyl.jpeg");
            Utils.createReourceFile("/docs/fondo_umu.jpg", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "fondo_umu.jpg");
            Utils.createReourceFile("/docs/umu_escudo_bn.png", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "umu_escudo_bn.png");
            Utils.createReourceFile("/docs/umu_logo_symbol.png", Constants.WORKING_PATH + File.separator + "docs" + File.separator + "umu_logo_symbol.png");
            try {
                form.getHelpPanel().setPage("file:///" + Constants.WORKING_PATH + Constants.URL_HTML_PAGE);
            }
            catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        dialog.pack();
        dialog.setSize(1024, 688);
        dialog.setLocationRelativeTo(this.mainForm.getTabbedPane());
        dialog.setVisible(true);
    }
}

