/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.config;

import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.Internationalization;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DefaultTaxonomyTree
implements ActionListener {
    MainForm mainForm;

    public DefaultTaxonomyTree(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (0 == JOptionPane.showConfirmDialog(this.mainForm.getTabbedPane(), Internationalization.IMPORT_TREE_ASK, Internationalization.WARNING_WORD, 0, 2, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)))) {
            try {
                String file;
                File fileF;
                BufferedReader br = null;
                try {
                    br = new BufferedReader(new InputStreamReader((InputStream)new FileInputStream(Constants.WORKING_PATH + Constants.FILE_NAME_EMPTY_CORPUS), Constants.ENCODING));
                }
                catch (IOException ioe) {
                    br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(Constants.FILE_NAME_EMPTY_CORPUS_RESOURCE), Constants.ENCODING));
                }
                String aux = "";
                StringBuilder total = new StringBuilder();
                while ((aux = br.readLine()) != null) {
                    total.append(aux);
                    total.append("\n");
                }
                Document doc = DOMUtils.StringToDOM(total.toString(), Constants.ENCODING, Constants.TEI_NAMESPACE);
                XPath xpath = XPathFactory.newInstance().newXPath();
                xpath.setNamespaceContext(new TEINamespaceContext());
                String xpathExpression = "//tei:classDecl";
                NodeList list = (NodeList)xpath.evaluate(xpathExpression, doc, XPathConstants.NODESET);
                if (list.getLength() > 0) {
                    Node oldClassDecls = list.item(0);
                    DefaultMutableTreeNode nodeGr = (DefaultMutableTreeNode)this.mainForm.getTaxonomyTree().getModel().getRoot();
                    Node newClassDelcs = (Node)nodeGr.getUserObject();
                    Node newClassDeclsCopy = newClassDelcs.cloneNode(true);
                    Node a = doc.adoptNode(newClassDeclsCopy);
                    oldClassDecls.getParentNode().replaceChild(a, oldClassDecls);
                }
                String out = DOMUtils.DOMToString(doc, Constants.ENCODING);
                String dir = (Constants.WORKING_PATH + Constants.FILE_NAME_EMPTY_CORPUS).substring(0, (Constants.WORKING_PATH + Constants.FILE_NAME_EMPTY_CORPUS).lastIndexOf(File.separator));
                File dirF = new File(dir);
                if (!dirF.exists()) {
                    dirF.mkdirs();
                }
                if (!(fileF = new File(file = Constants.WORKING_PATH + Constants.FILE_NAME_EMPTY_CORPUS)).exists()) {
                    try {
                        fileF.createNewFile();
                    }
                    catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                FileOutputStream fout = new FileOutputStream(fileF, false);
                OutputStreamWriter osw = new OutputStreamWriter((OutputStream)fout, Constants.ENCODING);
                osw.write(out);
                osw.flush();
                osw.close();
                JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.IMPORT_TREE_INFORMATION_MESSAGE_1, Internationalization.INFORMATION_WORD, 1, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_INFORM)));
            }
            catch (Exception ex) {
                JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.IMPORT_TREE_INFORMATION_ERROR_1, Internationalization.ERROR_WORD, 0, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_DELETE)));
            }
        }
    }
}

