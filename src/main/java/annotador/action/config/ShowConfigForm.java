/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.config;

import annotador.action.config.SaveChangesAction;
import annotador.gui.ConfigForm;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

public class ShowConfigForm
implements ActionListener {
    MainForm mainForm;
    ConfigForm form;

    public ShowConfigForm(MainForm mainForm) {
        this.mainForm = mainForm;
        this.form = new ConfigForm(mainForm);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.form.getDialog().setLocationRelativeTo(this.mainForm.getLogicalViewPanel());
        if (Constants.ALLOW_ADD_TAXONOMY.equalsIgnoreCase("true")) {
            this.form.getAllowInsertTaxonomyCheckBox().setSelected(true);
        } else {
            this.form.getAllowInsertTaxonomyCheckBox().setSelected(false);
        }
        if (Constants.ALLOW_DELETE_TAXONOMY.equalsIgnoreCase("true")) {
            this.form.getAllowDeleteTaxonomyCheckBox().setSelected(true);
        } else {
            this.form.getAllowDeleteTaxonomyCheckBox().setSelected(false);
        }
        if (Constants.DEFAULT_SHOWED_CATEGORY.equalsIgnoreCase("true")) {
            this.form.getAppliedCategoriesShownByCheckBox().setSelected(true);
        } else {
            this.form.getAppliedCategoriesShownByCheckBox().setSelected(false);
        }
        if (Constants.ALLOW_AUTOMATIC_IMPORT.equalsIgnoreCase("true")) {
            this.form.getAutomaticImportCheckBox().setSelected(true);
        } else {
            this.form.getAutomaticImportCheckBox().setSelected(false);
        }
        this.form.getColorAlphaSlider().setMaximum(0);
        this.form.getColorAlphaSlider().setMaximum(100);
        this.form.getColorAlphaSlider().setMinorTickSpacing(5);
        this.form.getColorAlphaSlider().setMajorTickSpacing(10);
        this.form.getColorAlphaSlider().setValue(Integer.parseInt(Constants.COLOR_ALPHA));
        this.form.getColorAlphaSlider().setPaintLabels(true);
        this.form.getColorAlphaSlider().setPaintTrack(true);
        this.form.getInterRowSpace().setMaximum(0);
        this.form.getInterRowSpace().setMaximum(35);
        this.form.getInterRowSpace().setMinorTickSpacing(5);
        this.form.getInterRowSpace().setMajorTickSpacing(10);
        this.form.getInterRowSpace().setValue(Constants.ROW_INTERCELL_SPACING);
        this.form.getInterRowSpace().setPaintLabels(true);
        this.form.getInterRowSpace().setPaintTrack(true);
        this.form.getScrollSensibility().setMaximum(100);
        this.form.getScrollSensibility().setMinimum(1);
        this.form.getScrollSensibility().setMajorTickSpacing(10);
        this.form.getScrollSensibility().setMinorTickSpacing(5);
        this.form.getScrollSensibility().setPaintLabels(true);
        this.form.getScrollSensibility().setPaintTrack(true);
        this.form.getScrollSensibility().setValue(Integer.parseInt(Constants.BLOCK_INCREMENT_SCROLL));
        this.form.getSaveChangesButton().addActionListener(new SaveChangesAction(this.mainForm, this.form));
        if (Constants.SINTAX_HIGHLIGHTING.equalsIgnoreCase("true")) {
            this.form.getShowSintaxHighlightCheckBox().setSelected(true);
        } else {
            this.form.getShowSintaxHighlightCheckBox().setSelected(false);
        }
        if (Constants.SHOW_GRID.equalsIgnoreCase("true")) {
            this.form.getShowGridCheckBox().setSelected(true);
        } else {
            this.form.getShowGridCheckBox().setSelected(false);
        }
        this.form.getVrlURL().setText(Constants.VRP_URL);
        this.form.getVrpUser().setText(Constants.VRP_USER);
        this.form.getVrpPass().setText(Constants.VRP_PASS);
        this.form.getCMRUrl().setText(Constants.CMT_URL);
        this.form.getCMTUser().setText(Constants.CMT_USER);
        this.form.getCMTPass().setText(Constants.CMT_PASS);
        this.form.dialog.pack();
        this.form.dialog.setLocationRelativeTo(this.mainForm.getTabbedPane());
        this.form.dialog.setVisible(true);
    }
}

