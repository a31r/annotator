/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.config;

import annotador.action.config.ShowCustomFontChooser;
import annotador.cmt.CMTManager;
import annotador.gui.ConfigForm;
import annotador.gui.MainForm;
import annotador.text.TEIJTable;
import annotador.utils.Constants;
import annotador.utils.Utils;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPasswordField;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;

public class SaveChangesAction
implements ActionListener {
    ConfigForm form;
    MainForm mainForm;

    public SaveChangesAction(MainForm mainForm, ConfigForm form) {
        this.mainForm = mainForm;
        this.form = form;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Constants.ALLOW_DELETE_TAXONOMY = String.valueOf(this.form.getAllowDeleteTaxonomyCheckBox().isSelected());
        Constants.ALLOW_ADD_TAXONOMY = String.valueOf(this.form.getAllowInsertTaxonomyCheckBox().isSelected());
        Constants.ALLOW_AUTOMATIC_IMPORT = String.valueOf(this.form.getAutomaticImportCheckBox().isSelected());
        Constants.COLOR_ALPHA = String.valueOf(this.form.getColorAlphaSlider().getValue());
        Constants.BLOCK_INCREMENT_SCROLL = String.valueOf(this.form.getScrollSensibility().getValue());
        Constants.ROW_INTERCELL_SPACING = this.form.getInterRowSpace().getValue();
        Constants.SHOW_GRID = String.valueOf(this.form.getShowGridCheckBox().isSelected());
        Constants.SINTAX_HIGHLIGHTING = String.valueOf(this.form.getShowSintaxHighlightCheckBox().isSelected());
        Constants.FONT_NAME = this.form.getFontChooser().getSelectedName();
        Constants.VRP_URL = this.form.getVrlURL().getText();
        Constants.VRP_USER = this.form.getVrpUser().getText();
        Constants.VRP_PASS = new String(this.form.getVrpPass().getPassword());
        Constants.CMT_URL = this.form.getCMRUrl().getText();
        Constants.CMT_USER = this.form.getCMTUser().getText();
        Constants.CMT_PASS = new String(this.form.getCMTPass().getPassword());
        Constants.DEFAULT_SHOWED_CATEGORY = String.valueOf(this.form.getAppliedCategoriesShownByCheckBox().isSelected());
        CMTManager.resetCMTConnection();
        if (this.form.getFontChooser().getSelectedSize() != 0) {
            Constants.FONT_SIZE = String.valueOf(this.form.getFontChooser().getSelectedSize());
        }
        Utils.setFont(new Font(Constants.FONT_NAME, 0, Integer.parseInt(Constants.FONT_SIZE)));
        this.mainForm.getDocumentTable().setShowGrid(Boolean.parseBoolean(Constants.SHOW_GRID));
        TEIJTable table = (TEIJTable)this.mainForm.getDocumentTable();
        table.invalidateGraphicsCache();
        this.mainForm.getTaxonomyTree().repaint();
        this.mainForm.getDocumentTable().repaint();
        this.form.getDialog().setVisible(false);
    }
}

