/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.config;

import annotador.gui.CustomFontChooserDialog;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import java.awt.Component;
import java.awt.Frame;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JFrame;

public class ShowCustomFontChooser
extends CustomFontChooserDialog
implements ActionListener {
    MainForm mainForm;
    JComponent config;

    public ShowCustomFontChooser(MainForm mainForm, JComponent config) {
        super(mainForm.getVentanaPrincipal());
        int i;
        this.mainForm = mainForm;
        this.config = config;
        int items = this.fontNameChoice.getItemCount();
        int select = -1;
        for (i = 0; i < items; ++i) {
            if (!this.fontNameChoice.getItem(i).equalsIgnoreCase(Constants.FONT_NAME)) continue;
            select = i;
        }
        if (select != -1) {
            this.fontNameChoice.select(select);
        }
        items = this.fontSizeChoice.getItemCount();
        select = -1;
        for (i = 0; i < items; ++i) {
            if (!this.fontSizeChoice.getItem(i).equalsIgnoreCase(Constants.FONT_SIZE)) continue;
            select = i;
        }
        if (select != -1) {
            this.fontSizeChoice.select(select);
        }
        super.previewFont();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.setLocationRelativeTo(this.config);
        this.setVisible(true);
    }
}

