/*
 * Decompiled with CFR 0_118.
 */
package annotador.action.config;

import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;

public class SelectLanguageAction
implements ActionListener {
    MainForm mainForm;

    public SelectLanguageAction(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        InputStream bin = this.getClass().getResourceAsStream(Constants.FILE_NAME_INTERNATIONALIZATION);
        ZipInputStream zin = new ZipInputStream(bin);
        Vector<String> v = new Vector<String>();
        try {
            ZipEntry entry;
            while ((entry = zin.getNextEntry()) != null) {
                v.add(entry.getName().substring(0, entry.getName().lastIndexOf(".")));
            }
        }
        catch (IOException e1) {
            // empty catch block
        }
        String s = null;
        Constants.FILE_NAME_LANGUAGE_FILE = s = (String)JOptionPane.showInputDialog(this.mainForm.getTabbedPane(), Internationalization.LANGUAGE_CHOOSE_TITLE, Internationalization.LANGUAGE_WORD, 1, null, v.toArray(), null);
    }
}

