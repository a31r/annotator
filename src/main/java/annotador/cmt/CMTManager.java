/*
 * Decompiled with CFR 0_118.
 */
package annotador.cmt;

import annotador.utils.Constants;
import um.backbone.cmtclient.CMTClient;

public class CMTManager {
    private static CMTClient cmtClient;

    private CMTManager() throws Exception {
        cmtClient = new CMTClient(Constants.CMT_URL);
        cmtClient.login(Constants.CMT_USER, Constants.CMT_PASS);
    }

    public static CMTClient getCMT() throws Exception {
        if (cmtClient == null) {
            new CMTManager();
        }
        return cmtClient;
    }

    public static void resetCMTConnection() {
        cmtClient = null;
    }
}

