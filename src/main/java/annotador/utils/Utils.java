/*
 * Decompiled with CFR 0_118.
 */
package annotador.utils;

import annotador.action.metadata.ApplyChangesAction;
import annotador.action.resources.ImportResourcesFromFile;
import annotador.action.resources.ShowAddRourceFormAction;
import annotador.gui.MainForm;
import annotador.metadata.KeyWordModel;
import annotador.metadata.PersonModel;
import annotador.metadata.PersonRenderer;
import annotador.resources.ResourcesTableModel;
import annotador.taxonomy.TEITreeModel;
import annotador.text.TEIJTable;
import annotador.text.TEITableModel;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.ShowedCategory;
import annotador.utils.TEINamespaceContext;
import annotador.vrp.ShowVRPBrowser;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.tree.TreeModel;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import table.model.CellAttribute;
import table.model.CellSpan;

public class Utils {
    private static boolean isChanged = false;

    private Utils() {
    }

    public static int calculateStartSectionRow(int uRow, JTable table) {
        TEITableModel model = (TEITableModel)table.getModel();
        CellSpan span = (CellSpan)((Object)model.getCellAttribute());
        for (int i = uRow; i >= 0; --i) {
            if (!span.isVisible(i, 0)) continue;
            return i;
        }
        return -1;
    }

    public static int calculateEndSectionRow(int uRow, JTable table) {
        TEITableModel model = (TEITableModel)table.getModel();
        CellSpan span = (CellSpan)((Object)model.getCellAttribute());
        for (int i = uRow + 1; i < model.getRowCount(); ++i) {
            if (!span.isVisible(i, 0)) continue;
            return i - 1;
        }
        return model.getRowCount();
    }

    public static void changeCorpusEvent(MainForm mainForm) {
        ComponentListener[] listeners;
        TEITreeModel teiTreeModel;
        ShowedCategory.removeAllShowedCategories();
        Date start = new Date();
        TreeModel model = mainForm.getTaxonomyTree().getModel();
        if (model instanceof TEITreeModel) {
            teiTreeModel = (TEITreeModel)mainForm.getTaxonomyTree().getModel();
            teiTreeModel.setDocument(DocumentSingleton.getInstance());
        } else {
            teiTreeModel = new TEITreeModel(DocumentSingleton.getInstance());
            mainForm.getTaxonomyTree().setModel(teiTreeModel);
        }
        mainForm.getDocumentTable().setModel(new DefaultTableModel());
        TEIJTable table = (TEIJTable)mainForm.getDocumentTable();
        table.invalidateGraphicsCache();
        mainForm.getTaxonomyTree().updateUI();
        mainForm.getDocumentTable().repaint();
        for (ComponentListener listener : listeners = mainForm.getXmlViewPanel().getComponentListeners()) {
            listener.componentShown(new ComponentEvent(mainForm.getXmlPane(), 1));
        }
        if (Constants.DEBUG) {
            Date end = new Date();
            long gap = end.getTime() - start.getTime();
            System.out.println("Utils.changeCorpusEvent() -> " + gap + " ms");
        }
    }

    public static void changeInterviewEvent(MainForm mainForm, int selectInterview) {
        ComponentListener[] listeners;
        if (selectInterview != -1) {
            TEITableModel teiTableModel;
            TEIJTable table = (TEIJTable)mainForm.getDocumentTable();
            table.invalidateGraphicsCache();
            TableModel tableModel = mainForm.getDocumentTable().getModel();
            if (tableModel instanceof TEITableModel) {
                teiTableModel = (TEITableModel)mainForm.getDocumentTable().getModel();
                teiTableModel.setDocument(DocumentSingleton.getInstance());
                teiTableModel.updateTable(selectInterview);
                mainForm.getDocumentTable().setModel(teiTableModel);
            } else {
                teiTableModel = new TEITableModel(DocumentSingleton.getInstance(), selectInterview);
                mainForm.getDocumentTable().setModel(teiTableModel);
            }
            mainForm.getTabbedPane().setEnabledAt(2, true);
            Utils.fillMetaData(mainForm, selectInterview);
        } else {
            mainForm.getDocumentTable().setModel(new DefaultTableModel());
            mainForm.getTabbedPane().setEnabledAt(2, false);
            mainForm.getTabbedPane().setSelectedIndex(0);
            mainForm.getResourcesTable().setModel(new DefaultTableModel());
        }
        for (ComponentListener listener1 : listeners = mainForm.getLogicalViewPanel().getComponentListeners()) {
            listener1.componentResized(new ComponentEvent(mainForm.getLogicalViewPanel(), selectInterview));
        }
        mainForm.getDocumentTable().repaint();
        TableCellRenderer r = mainForm.getDocumentTable().getDefaultRenderer(Node.class);
        for (int i = 0; i < mainForm.getDocumentTable().getRowCount(); ++i) {
            mainForm.getDocumentTable().prepareRenderer(r, i, 3);
        }
        int height = 0;
        for (int i2 = 0; i2 < mainForm.getDocumentTable().getRowCount(); ++i2) {
            height += mainForm.getDocumentTable().getRowHeight(i2);
        }
        mainForm.getDocumentScrollTable().getVerticalScrollBar().setMaximum(0);
        mainForm.getDocumentScrollTable().getVerticalScrollBar().setMaximum(height);
    }

    public static void fillMetaData(MainForm mainForm, int interview) {
        try {
            ActionListener[] li;
            Document doc = DocumentSingleton.getInstance();
            XPath xpath = XPathFactory.newInstance().newXPath();
            xpath.setNamespaceContext(new TEINamespaceContext());
            String title = xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/text()", doc);
            mainForm.getTitleField().setText(title);
            String editor = xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:editor/text()", doc);
            mainForm.getEditorField().setText(editor);
            String transcriber = xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:respStmt/tei:name/text()", doc);
            mainForm.getTranscriberField().setText(transcriber);
            String principal = xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:principal/text()", doc);
            mainForm.getPrincipalInvestigatorField().setText(principal);
            String researcher = xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/text()", doc);
            mainForm.getResearcherField().setText(researcher);
            String idno = xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:idno/text()", doc);
            mainForm.getIdField().setText(idno);
            String autority = xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:authority/text()", doc);
            mainForm.getAutorityField().setText(autority);
            String description = xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:fileDesc/tei:notesStmt/tei:note/text()", doc);
            mainForm.getDescriptionField().setText(description);
            Node mediaFile = (Node)xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:recordingStmt/tei:recording/@rend", doc, XPathConstants.NODE);
            if (mediaFile instanceof Attr) {
                Attr mediAttr = (Attr)mediaFile;
                String mediaF = mediAttr.getValue();
                mainForm.getMediaFileField().setText(mediaF);
            } else {
                mainForm.getMediaFileField().setText("");
            }
            Node dateRecordingN = (Node)xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:recordingStmt/tei:recording/tei:date/@when", doc, XPathConstants.NODE);
            if (dateRecordingN instanceof Attr) {
                Attr deteRecordingAttr = (Attr)dateRecordingN;
                String dateRecording = deteRecordingAttr.getValue();
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    df.setLenient(false);
                    if (!dateRecording.equals("")) {
                        Date date = df.parse(dateRecording);
                    }
                    mainForm.getDateRecordingField().setBackground(Color.WHITE);
                }
                catch (ParseException e) {
                    mainForm.getDateRecordingField().setBackground(Color.RED);
                }
                mainForm.getDateRecordingField().setText(dateRecording);
            } else {
                mainForm.getDateRecordingField().setText("");
            }
            String dateTrans = xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc/tei:creation/tei:date/text()", doc);
            try {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                df.setLenient(false);
                if (!dateTrans.equals("")) {
                    Date date = df.parse(dateTrans);
                }
                mainForm.getDateTranscriptionField().setBackground(Color.WHITE);
            }
            catch (ParseException e) {
                mainForm.getDateTranscriptionField().setBackground(Color.RED);
            }
            mainForm.getDateTranscriptionField().setText(dateTrans);
            String locale = xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc/tei:settingDesc/tei:setting/tei:locale/text()", doc);
            mainForm.getLocaleField().setText(locale);
            String activity = xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc/tei:settingDesc/tei:setting/tei:activity/text()", doc);
            mainForm.getActivityField().setText(activity);
            String language = xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language/@ident", doc);
            mainForm.getLanguageField().setText(language);
            String languageVar = xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language/text()", doc);
            mainForm.getLanguageVariety().setText(languageVar);
            Node particDesc = (Node)xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc/tei:particDesc", doc, XPathConstants.NODE);
            mainForm.getPersonBox().setModel(new PersonModel(particDesc));
            mainForm.getPersonBox().setRenderer(new PersonRenderer());
            if (mainForm.getPersonBox().getItemCount() > 0) {
                mainForm.getPersonBox().setSelectedIndex(0);
            }
            Node list = (Node)xpath.evaluate("//tei:TEI[" + interview + "]/tei:teiHeader/tei:profileDesc/tei:textClass/tei:keywords/tei:list", doc, XPathConstants.NODE);
            mainForm.getKeyWordcomboBox().setModel(new KeyWordModel(list));
            if (mainForm.getKeyWordcomboBox().getItemCount() > 0) {
                mainForm.getKeyWordcomboBox().setSelectedIndex(0);
            }
            Node text = (Node)xpath.evaluate("//tei:TEI[" + interview + "]/tei:text", doc, XPathConstants.NODE);
            ResourcesTableModel rtm = new ResourcesTableModel(text);
            mainForm.getResourcesTable().setModel(rtm);
            for (ActionListener al : li = mainForm.getAddResourceButton().getActionListeners()) {
                mainForm.getAddResourceButton().removeActionListener(al);
            }
            ShowAddRourceFormAction showAddRourceFormAction = new ShowAddRourceFormAction(mainForm, mainForm.getResourcesTable(), text);
            mainForm.getAddResourceButton().addActionListener(showAddRourceFormAction);
            for (ActionListener al2 : li = mainForm.getAttachAVRPResourceButton().getActionListeners()) {
                mainForm.getAttachAVRPResourceButton().removeActionListener(al2);
            }
            ShowVRPBrowser showVRPBrowser = new ShowVRPBrowser(mainForm, mainForm.getResourcesTable(), text);
            mainForm.getAttachAVRPResourceButton().addActionListener(showVRPBrowser);
            for (ActionListener al3 : li = mainForm.getImportResourcesFromFileButton().getActionListeners()) {
                mainForm.getImportResourcesFromFileButton().removeActionListener(al3);
            }
            ImportResourcesFromFile irff = new ImportResourcesFromFile(mainForm, mainForm.getResourcesTable(), text);
            mainForm.getImportResourcesFromFileButton().addActionListener(irff);
            for (ActionListener al4 : li = mainForm.getApplyChangesButton().getActionListeners()) {
                mainForm.getApplyChangesButton().removeActionListener(al4);
            }
            ApplyChangesAction acc = new ApplyChangesAction(mainForm, interview);
            mainForm.getApplyChangesButton().addActionListener(acc);
        }
        catch (XPathExpressionException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList getInOrderRecursivo(int nivel, Node node, ArrayList list) {
        NodeList lista = node.getChildNodes();
        for (int i = 0; i < lista.getLength(); ++i) {
            Node actual = lista.item(i);
            list.add(actual);
            list = Utils.getInOrderRecursivo(nivel + 1, actual, list);
        }
        return list;
    }

    public static Image getReourceImage(MainForm mainForm, String resource) {
        Utils u = new Utils();
        InputStream bin = u.getClass().getResourceAsStream(resource);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            int c;
            while ((c = bin.read()) >= 0) {
                baos.write(c);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return mainForm.getVentanaPrincipal().getToolkit().createImage(baos.toByteArray());
    }

    public static void createReourceFile(String resource, String path) {
        Utils u = new Utils();
        InputStream bin = u.getClass().getResourceAsStream(resource);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            int c;
            while ((c = bin.read()) >= 0) {
                baos.write(c);
            }
            FileOutputStream fout = new FileOutputStream(path);
            fout.write(baos.toByteArray());
            fout.flush();
            fout.close();
            baos.flush();
            baos.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void setChangedCorpus(boolean changed) {
        isChanged = changed;
    }

    public static boolean isChangedCorpus() {
        return isChanged;
    }

    public static void setFont(Font font) {
        UIManager.put("Button.font", new FontUIResource(font));
        UIManager.put("CheckBox.font", new FontUIResource(font));
        UIManager.put("CheckBoxMenuItem.font", new FontUIResource(font));
        UIManager.put("ColorChooser.font", new FontUIResource(font));
        UIManager.put("ComboBox.font", new FontUIResource(font));
        UIManager.put("EditorPane.font", new FontUIResource(font));
        UIManager.put("Label.font", new FontUIResource(font));
        UIManager.put("List.font", new FontUIResource(font));
        UIManager.put("Menu.font", new FontUIResource(font));
        UIManager.put("MenuBar.font", new FontUIResource(font));
        UIManager.put("MenuItem.font", new FontUIResource(font));
        UIManager.put("OptionPane.font", new FontUIResource(font));
        UIManager.put("Panel.font", new FontUIResource(font));
        UIManager.put("PasswordField.font", new FontUIResource(font));
        UIManager.put("PopupMenu.font", new FontUIResource(font));
        UIManager.put("ProgressBar.font", new FontUIResource(font));
        UIManager.put("RadioButton.font", new FontUIResource(font));
        UIManager.put("ScrollPane.font", new FontUIResource(font));
        UIManager.put("Table.font", new FontUIResource(font));
        UIManager.put("TableHeader.font", new FontUIResource(font));
        UIManager.put("Text.font", new FontUIResource(font));
        UIManager.put("TextArea.font", new FontUIResource(font));
        UIManager.put("TextField.font", new FontUIResource(font));
        UIManager.put("TextPane.font", new FontUIResource(font));
        UIManager.put("TitledBorder.font", new FontUIResource(font));
        UIManager.put("ToggleButton.font", new FontUIResource(font));
        UIManager.put("ToolBar.font", new FontUIResource(font));
        UIManager.put("ToolTip.font", new FontUIResource(font));
        UIManager.put("Tree.font", new FontUIResource(font));
        UIManager.put("TabbedPane.font", new FontUIResource(font));
    }
}

