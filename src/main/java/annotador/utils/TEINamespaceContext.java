/*
 * Decompiled with CFR 0_118.
 */
package annotador.utils;

import java.util.ArrayList;
import java.util.Iterator;
import javax.xml.namespace.NamespaceContext;

public class TEINamespaceContext
implements NamespaceContext {
    @Override
    public String getNamespaceURI(String prefix) {
        if (prefix.equals("tei")) {
            return "http://www.tei-c.org/ns/1.0";
        }
        if (prefix.equals("xml")) {
            return "http://www.w3.org/XML/1998/namespace";
        }
        return "";
    }

    @Override
    public String getPrefix(String namespaceURI) {
        if (namespaceURI.equals("http://www.tei-c.org/ns/1.0")) {
            return "tei";
        }
        if (namespaceURI.equals("http://www.w3.org/XML/1998/namespace")) {
            return "xml";
        }
        return "";
    }

    @Override
    public Iterator getPrefixes(String namespaceURI) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("tei");
        list.add("xml");
        return list.iterator();
    }
}

