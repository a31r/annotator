/*
 * Decompiled with CFR 0_118.
 */
package annotador.utils;

import com.sun.org.apache.xerces.internal.dom.DOMInputImpl;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSParser;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

public class DOMUtils {
    private DOMUtils() {
    }

    public static String DOMToString(Document document, String encoding) throws Exception {
        DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
        DOMImplementationLS impl = (DOMImplementationLS)((Object)registry.getDOMImplementation("LS"));
        LSSerializer writer = impl.createLSSerializer();
        writer.getDomConfig().setParameter("namespace-declarations", new Boolean(false));
        String str = writer.writeToString(document);
        String header = "<?xml version=\"1.0\" encoding=\"UTF-16\"?><?oxygen RNGSchema=\"http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_allPlus.rng\" type=\"xml\"?><teiCorpus xmlns=\"http://www.tei-c.org/ns/1.0\">";
        String start = "<teiCorpus>";
        String end = "</teiCorpus>";
        str = header + str.substring(str.indexOf(start) + start.length(), str.indexOf(end) + end.length());
        return str;
    }

    public static String DOMToString(Node node, String encoding) throws Exception {
        DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
        DOMImplementationLS impl = (DOMImplementationLS)((Object)registry.getDOMImplementation("LS"));
        LSSerializer writer = impl.createLSSerializer();
        writer.getDomConfig().setParameter("namespace-declarations", new Boolean(false));
        writer.getDomConfig().setParameter("xml-declaration", new Boolean(false));
        String str = writer.writeToString(node);
        return str;
    }

    public static Document StringToDOM(String xml, String encoding, String url) throws Exception {
        try {
            DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
            DOMImplementationLS impl = (DOMImplementationLS)((Object)registry.getDOMImplementation("LS"));
            LSParser parser = impl.createLSParser((short) 1, "http://www.w3.org/2001/XMLSchema");
            Document doc = parser.parse(new DOMInputImpl(url, url, url, new ByteArrayInputStream(xml.getBytes(encoding)), encoding));
            return doc;
        }
        catch (IOException e) {
            throw new SAXException(e.getMessage());
        }
    }
}

