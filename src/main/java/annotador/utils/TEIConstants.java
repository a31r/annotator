/*
 * Decompiled with CFR 0_118.
 */
package annotador.utils;

public class TEIConstants {
    public static String TEI_TAG_FOREIGN = "foreign";
    public static String TEI_TAG_PERSON = "person";
    public static String TEI_TAG_PERSNAME = "persName";
    public static String TEI_TAG_TEI = "TEI";
    public static String TEI_TAG_PAUSE = "pause";
    public static String TEI_TAG_NOTE = "note";
    public static String TEI_TAG_ITEM = "item";
    public static String TEI_TAG_NOTESTMT = "notesStmt";
    public static String TEI_TAG_GAP = "gap";
    public static String TEI_TAG_TITLE = "title";
    public static String TEI_TAG_HEAD = "head";
    public static String TEI_TAG_UNCLEAR = "unclear";
    public static String TEI_TAG_SEG = "seg";
    public static String TEI_TAG_SUPPLIED = "supplied";
    public static String TEI_TAG_TIME = "time";
    public static String TEI_TAG_U = "u";
    public static String TEI_TAG_P = "p";
    public static String TEI_TAG_DIV = "div";
    public static String TEI_TAG_ANCHOR = "anchor";
    public static String TEI_TAG_TAXONOMY = "taxonomy";
    public static String TEI_TAG_CATEGORY = "category";
    public static String TEI_TAG_CATDESC = "catDesc";
    public static String TEI_TAG_CLASSDECL = "classDecl";
    public static String TEI_TAG_ABBR = "abbr";
    public static String TEI_TAG_HI = "hi";
    public static String TEI_TAG_ADD = "add";
    public static String TEI_TAG_CHOICE = "choice";
    public static String TEI_TAG_ORIG = "orig";
    public static String TEI_TAG_CORR = "corr";
    public static String TEI_TAG_LINKGRP = "linkGrp";
    public static String TEI_TAG_PTR = "ptr";
    public static String TEI_TAG_SPONSOR = "sponsor";
    public static String TEI_TAG_PRINCIPAL = "principal";
    public static String TEI_TAG_SOURCEDESC = "sourceDesc";
    public static String TEI_ATTRIBUTE_TARGET = "target";
    public static String TEI_ATTRIBUTE_ANA = "ana";
    public static String TEI_ATTRIBUTE_REND = "rend";
    public static String TEI_ATTRIBUTE_TYPE = "type";
    public static String TEI_ATTRIBUTE_DECLS = "decls";
    public static String TEI_ATTRIBUTE_NEXT = "next";
    public static String TEI_ATTRIBUTE_PREV = "prev";
    public static String TEI_ATTRIBUTE_N = "n";
    public static String TEI_ATTRIBUTE_XML_ID = "xml:id";
    public static String TEI_ATTRIBUTE_REASON = "reason";
    public static String TEI_ATTRIBUTE_REASON_BREAK = "break";
    public static String TEI_ATTRIBUTE_REASON_CUT = "cut";
    public static String TEI_ATTRIBUTE_WHEN = "when";
    public static String TEI_ATTRIBUTE_WHO = "who";
    public static String TEI_ATTRIBUTE_IDENT = "ident";
}

