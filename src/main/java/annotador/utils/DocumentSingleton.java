/*
 * Decompiled with CFR 0_118.
 */
package annotador.utils;

import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.Internationalization;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.util.Date;
import javax.swing.JOptionPane;
import org.w3c.dom.Document;

public class DocumentSingleton {
    private static Document document;

    private DocumentSingleton() {
    }

    public static Document getInstance() {
        return document;
    }

    public static void loadFromFile(String path) {
        Date start = new Date();
        try {
            Date end;
            long gap;
            FileInputStream fr = new FileInputStream(path);
            InputStreamReader isr = new InputStreamReader((InputStream)fr, Constants.ENCODING);
            BufferedReader br = new BufferedReader(isr);
            String aux = null;
            StringBuilder total = new StringBuilder();
            while ((aux = br.readLine()) != null) {
                total.append(aux);
                total.append("\n");
            }
            if (Constants.DEBUG) {
                end = new Date();
                gap = end.getTime() - start.getTime();
                System.out.println("Reader New Corpus() -> " + gap + " ms");
            }
            start = new Date();
            document = DOMUtils.StringToDOM(total.toString(), Constants.ENCODING, Constants.TEI_NAMESPACE);
            if (Constants.DEBUG) {
                end = new Date();
                gap = end.getTime() - start.getTime();
                System.out.println("File To DOM() -> " + gap + " ms");
            }
        }
        catch (Exception e) {
            if (path != null && !path.equals("")) {
                JOptionPane.showMessageDialog(null, Internationalization.TAXONOMIES_OPEN_ERROR_MESSAGE_1);
            }
            document = null;
        }
    }

    public static void loadFromDOM(Document document) {
        DocumentSingleton.document = document;
    }

    public static void loadFromString(String document) {
        Date start = new Date();
        try {
            DocumentSingleton.document = DOMUtils.StringToDOM(document.toString(), Constants.ENCODING, Constants.TEI_NAMESPACE);
            if (Constants.DEBUG) {
                Date end = new Date();
                long gap = end.getTime() - start.getTime();
                System.out.println("File To DOM() -> " + gap + " ms");
            }
        }
        catch (Exception e) {
            DocumentSingleton.document = null;
        }
    }
}

