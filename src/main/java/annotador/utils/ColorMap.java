/*
 * Decompiled with CFR 0_118.
 */
package annotador.utils;

import annotador.utils.Constants;
import annotador.utils.TEINamespaceContext;
import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ColorMap {
    private static Properties colors;
    private static final String _KEY = "0123456789ABCDEF";

    private ColorMap() {
    }

    public static String getHex(int[] rgb) {
        StringBuffer sb = new StringBuffer();
        sb.append("#");
        sb.append("0123456789ABCDEF".substring((int)Math.floor(rgb[0] / 16), (int)Math.floor(rgb[0] / 16) + 1));
        sb.append("0123456789ABCDEF".substring(rgb[0] % 16, rgb[0] % 16 + 1));
        sb.append("0123456789ABCDEF".substring((int)Math.floor(rgb[1] / 16), (int)Math.floor(rgb[1] / 16) + 1));
        sb.append("0123456789ABCDEF".substring(rgb[1] % 16, rgb[1] % 16 + 1));
        sb.append("0123456789ABCDEF".substring((int)Math.floor(rgb[2] / 16), (int)Math.floor(rgb[2] / 16) + 1));
        sb.append("0123456789ABCDEF".substring(rgb[2] % 16, rgb[2] % 16 + 1));
        return sb.toString();
    }

    public static void loadColors(String path) {
        try {
            colors = new Properties();
            colors.loadFromXML(new FileInputStream(path));
        }
        catch (IOException e) {
            try {
                TEINamespaceContext t = new TEINamespaceContext();
                colors.loadFromXML(t.getClass().getResourceAsStream(Constants.FILE_NAME_CONFIG_COLOR_RESOURCE));
            }
            catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static void saveColors(String path) {
        try {
            colors.storeToXML(new FileOutputStream(path), "Color Configuration");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addColor(String taxonomyId, Color color) {
        int[] rgb = new int[]{color.getRed(), color.getGreen(), color.getBlue()};
        colors.setProperty(taxonomyId, ColorMap.getHex(rgb));
    }

    public static Color getColor(String categoryId) {
        categoryId = categoryId.replaceAll("#", "");
        Pattern pattern = Pattern.compile("\\p{Alnum}+", 64);
        Matcher matcher = pattern.matcher(categoryId);
        ArrayList<Color> applieColors = new ArrayList<Color>();
        while (matcher.find()) {
            Color c;
            int end;
            int start = matcher.start();
            String token = categoryId.substring(start, end = matcher.end());
            String property = colors.getProperty(token);
            if (property != null) {
                c = Color.decode(property);
                applieColors.add(new Color(c.getRed(), c.getGreen(), c.getBlue(), Integer.parseInt(Constants.COLOR_ALPHA)));
                continue;
            }
            c = Color.BLUE;
            ColorMap.addColor(token, new Color(c.getRed(), c.getGreen(), c.getBlue(), Integer.parseInt(Constants.COLOR_ALPHA)));
            applieColors.add(new Color(c.getRed(), c.getGreen(), c.getBlue(), Integer.parseInt(Constants.COLOR_ALPHA)));
        }
        Color applied = null;
        int r = 0;
        int g = 0;
        int b = 0;
        int c = 0;
        for (Color actualColor : applieColors) {
            ++c;
            r += actualColor.getRed();
            g += actualColor.getGreen();
            b += actualColor.getBlue();
        }
        if (c != 0) {
            applied = new Color(r /= c, g /= c, b /= c, Integer.parseInt(Constants.COLOR_ALPHA));
        }
        return applied;
    }

    public static void deleteColor(String categoryId) {
        colors.remove(categoryId);
        ColorMap.saveColors(Constants.WORKING_PATH + Constants.FILE_NAME_CONFIG_COLOR);
    }

    public static boolean existColor(String categoryId) {
        if (colors.getProperty(categoryId) == null) {
            return false;
        }
        return true;
    }
}

