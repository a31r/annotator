/*
 * Decompiled with CFR 0_118.
 */
package annotador.utils;

import java.awt.Component;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.Properties;
import java.util.Set;
import javax.swing.JFileChooser;

public class Internationalization {
    public static String TAXONOMIES_WORD = "Taxonomies";
    public static String RESTRICTION_ERROR = "Some feature not work fine with the actual resolution, You need at least 1024x768 resolution.";
    public static String TAXONOMIES_ADD_ERROR_MESSAGE_1 = "Error a toxonomy needs to be selected";
    public static String TAXONOMIES_ADD_ERROR_MESSAGE_2 = "Error some required values are missing";
    public static String TAXONOMIES_ADD_ERROR_MESSAGE_3 = "This CategoryId is assigned to another taxonomy";
    public static String TAXONOMIES_ADD_ERROR_MESSAGE_4 = "Sorry, but the possibility to add a new Taxonomy has been restrited. To allow this, change the config";
    public static String TAXONOMIES_ADD_ERROR_MESSAGE_5 = "Only Letters Without Spaces in the CategoryID !!!!!";
    public static String TAXONOMIES_ADD_ERROR_MESSAGE_6 = "Collaborative Corpus!! taxonomy requires to be locked to perform this action.";
    public static String TAXONOMIES_DELETE_ERROR_MESSAGE_1 = "Error a taxonomy needs to be selected";
    public static String TAXONOMIES_DELETE_ERROR_MESSAGE_2 = "Are you sure that want to delete this taxonomy?";
    public static String TAXONOMIES_DELETE_ERROR_MESSAGE_3 = "Sorry, but the possibility of removing a Taxonomy has been restrited\nTo allow this, change the config file";
    public static String TAXONOMIES_OPEN_ERROR_MESSAGE_1 = "Sorry, but the file that you try to open is not a valid file";
    public static String ADD_INTERVIEW_ERROR_MESSAGE_1 = "No corpus has been selected";
    public static String ADD_INTERVIEW_ERROR_MESSAGE_2 = "There are a symbol '{' or '}' without the other associated";
    public static String NEW_CORPUS_ERROR_1 = "Error no file name selected";
    public static String IMPORT_MESSAGE_ERROR = "Error in the importing process from previous versions";
    public static String IMPORT_ERROR = "Error during the importing resources process";
    public static String CONVERSION_XML_ERROR = "This is not a TEI Valid Interview";
    public static String TXT_CONVERSION_ERROR_1 = "Format Exception. No METADATA Section inside the File";
    public static String TXT_CONVERSION_ERROR_2 = "Format Exception. No BODY Section inside the File";
    public static String XML_PANE_ERROR_1 = "No selected corpus";
    public static String DELETE_INTERVIEW_ERROR_1 = "No documents available";
    public static String SELECT_INTERVIEW_ERROR_1 = "No documents available";
    public static String KEYWORD_ADD_MESSAGE = "Insert the new keyword:";
    public static String KEYWORD_REMOVE_MESSAGE = "The keyword has been deleted correctly:";
    public static String SAVE_CORPUS_ERROR_1 = "Error the corpus cannot be saved";
    public static String INSERT_TITLE_ERROR_1 = "This Section already has a section title";
    public static String INSERT_TITLE_ERROR_2 = "Error this taxonomy has already been applied";
    public static String INSERT_CATEGORY_ERROR_MESSAGE_1 = "Error text needs to be selected before adding a keyword";
    public static String IMPORT_TREE_INFORMATION_ERROR_1 = "The taxonomy tree cannot be imported as default tree.";
    public static String VRP_ERROR = "The VRP Resource Pool is not accessible, please ensure the avability and review the config options";
    public static String SAVE_CORPUS_ASK_1 = "Do you want to save changes?";
    public static String SAVE_DOCUMENT_ASK_1 = "Do you want to save changes on the current document?";
    public static String DELETE_APPLIED_TAXONOMY_MESSAGE_1 = "Are you sure you want to delete the annotation associated to this taxonomy?";
    public static String XML_HIDDEN_COMPONENT_TEXT = "Do you want to save the editing changes?";
    public static String OVERRIDE_TITLE = "Do you really want to overwrite this title name?";
    public static String IMPORT_TREE_ASK = "Do you really want to overwrite the actual default taxonomy tree?";
    public static String ADD_INTERVIEW_INFORMATION_MESSAGE_1 = "A new document has been added correctly";
    public static String DELETE_INTERVIEW_INFORMATION_MESSAGE_1 = "The selected document has been deleted correctly";
    public static String IMPORT_TREE_INFORMATION_MESSAGE_1 = "The taxonomy tree of the current corpora has been set as default tree.";
    public static String TRANSCRIPTOR_MESSAGE_1 = "There aren't notes from transcriptor.";
    public static String TRANSCRIPTOR_MESSAGE_2 = "The transcriptor's note is: ";
    public static String NEW_CORPUS_TITLE = "New Corpus";
    public static String SAVE_CORPUS_TITLE = "Save Corpus";
    public static String HELP_TITLE = "Help";
    public static String FONT_CHOOSER_TITLE = "Font Chooser";
    public static String RESOURCE_MANAGEMENT_TITLE = "Resource Management Form For Section N\u00ba ";
    public static String ABOUT_TITLE = "About...";
    public static String CONFIG_TITLE = "Configuration Panel";
    public static String DELETE_INTERVIEW_TITLE_2 = "Delete the document";
    public static String MODIFY_CATEGORY_TITLE = "Modify Category/Taxonomy";
    public static String SELECT_INTERVIEW_TITLE_2 = "Select the documents";
    public static String NEW_TAXONOMY_TITLE = "Insert new Taxonomy";
    public static String PERSON_INFORMATION_TITLE = "Person Information";
    public static String INSERT_TITLE_MESSAGE_1 = "Insert the title associated to this section";
    public static String NEW_TAXONOMY_COLOR_TITLE = "Select The Color for the Taxonomy";
    public static String DELETE_INTERVIEW_INFORMATION_TITLE_1 = "Delete Document";
    public static String MENU_CORPUS = "Corpus";
    public static String MENU_CORPUS_SELECT = "Select Corpus File";
    public static String MENU_CORPUS_NEW = "New Corpus File";
    public static String MENU_CORPUS_SAVE = "Save Corpus File";
    public static String MENU_CORPUS_SAVE_AS = "Save As Corpus File";
    public static String MENU_CORPUS_CLOSE = "Close Corpus File";
    public static String MENU_INTERVIEW = "Document";
    public static String MENU_INTERVIEW_OPEN = "Open Document";
    public static String MENU_INTERVIEW_ADD = "Add Document";
    public static String MENU_INTERVIEW_DELETE = "Delete Document";
    public static String MENU_INTERVIEW_CLOSE = "Close Document";
    public static String MENU_CMT = "Collaboration";
    public static String MENU_CMT_PUBLISH = "Register Collaborative Corpus ";
    public static String MENU_CMT_RETRIEVE = "Open Collaborative Corpus";
    public static String MENU_CMT_DELETE = "Delete Collaborative Corpus";
    public static String MENU_LOCK_TREE = "Lock Tree";
    public static String MENU_UNLOCK_TREE = "Unlock Tree";
    public static String MENU_CORPUS_VISIBILITY = "Set Corpus Visibility";
    public static String ONLY_COLLABAROTIVE = "This function only work on collaborative corpus";
    public static String TAXNOMY_LOCKED = "Taxonomy tree has been locked";
    public static String TAXNOMY_RELEASED = "Taxonomy tree has been released";
    public static String TAXNOMY_SAVE = "Do you want to save changes on the Taxonomy Tree?";
    public static String TAXNOMY_SAVE_TITLE = "Save Taxonomy Changes";
    public static String TAXNOMY_DO_NOT_OPEN_DOCS = "While Taxonomy Tree is locked, it is forbiden to open documents.";
    public static String TAXNOMY_NOT_OPEN_DOC = "Taxonomy can only be locked when there is not an open document. Please close it and try again.";
    public static String MENU_CONFIG = "Config";
    public static String MENU_CONFIG_IMPORT = "Import Taxonomy Tree";
    public static String MENU_HELP = "Help";
    public static String MENU_HELP_HELP = "Help";
    public static String MENU_HELP_ABOUT = "About...";
    public static String MENU_CATEGORY_TITLE = "Category";
    public static String MENU_CATEGORY_ADD = "Add Cat.";
    public static String MENU_CATEGORY_REMOVE = "Delete Cat.";
    public static String MENU_CATEGORY_UP = "Up Cat.";
    public static String MENU_CATEGORY_DOWN = "Down Cat.";
    public static String ADD_NEW_RESOURCE = "Add New Resource";
    public static String ADD_RESOURCE = "Add Resource";
    public static String ADD_RESOURCE_FROM_FILE = "Import Resource From File";
    public static String ATTACH_VRP_RS = "Attach a VRP Resource Sheet";
    public static String XML_EDIT_TEXT = "Edit XML";
    public static String APPLY_CHANGES = "Apply Changes";
    public static String DISCARD_CHANGES = "Discart Changes";
    public static String VIEW_INFO = "View Infor";
    public static String DELETE_PERSON = "Delete Person";
    public static String CHOOSE_COLOR = "Choose Color";
    public static String LANGUAGUE_CHOOSER = "Choose Language";
    public static String ADD_KEYWORD = "Add Keyword";
    public static String REMOVE_KEYWORD = "Remove Keyword";
    public static String ADD_PERSON = "Add Person";
    public static String DELETE_TITLE_POP_UP = "Delete Title";
    public static String NO_DOCUMENT_TITLE = "- No Document Title -";
    public static String NAME_WORD = "Name";
    public static String TYPE_WORD = "Type";
    public static String DESCRIPTION_WORD = "Description";
    public static String LINK_WORD = "Link";
    public static String ROLE_WORD = "Role";
    public static String AGE_WORD = "Age";
    public static String CATEGORY_ID = "Category ID (No blank, no simbols, only letters)";
    public static String CATEGORY_NAME = "Category Name";
    public static String CATEGORY_COLOR = "Category Color";
    public static String SEX_PERSON = "Sex (M|F)";
    public static String ID_PERSON = "ID in the text";
    public static String SAVE_CHANGES = "Save Changes";
    public static String SELECT_DOCUMENT = "Select one of following documents";
    public static String TITLE_WORD = "Title";
    public static String DATE_RECORDING = "Date Recording (yyyy-mm-dd)";
    public static String DATE_TRANSCRIPTING = "Date Transcription (yyyy-mm-dd)";
    public static String LOCALE_WORD = "Locale";
    public static String RESEARCHER_WORD = "Researcher";
    public static String TRANSCRIBER_WORD = "Transcriber";
    public static String EDITOR_WORD = "Editor";
    public static String AUTORITY_WORD = "Authority";
    public static String ID_WORD = "ID";
    public static String MEDIA_FILE_NAME = "Media File Name";
    public static String LANGUAGE_WORD = "Language";
    public static String PARTICIPATION_INFORMATION = "Participators' Information";
    public static String PRINCIPAL_INVESTIGATOR = "Principal Investigator";
    public static String CORPUS_NAME = "Corpus Name";
    public static String SPONSOR = "Sponsor";
    public static String PUBLICATION_STATEMENT = "Publication Statement";
    public static String SOURCE_STATEMENTE = "Source Statement";
    public static String FILE_NAME = "File Name";
    public static String EXPLORE = "Explore...";
    public static String ALLOW_DELETE_TAXONOMY = "Allow Delete Taxonomies";
    public static String ALLOW_INSERT_TAXONOMY = "Allow Insert new Taxonomies";
    public static String SHOW_GRID = "Show Grid";
    public static String AUTOMATIC_IMPORT = "Automatic Import";
    public static String SCROLL_SENSIBILITY = "Scroll Sensibility (pixel)";
    public static String COLOR_ALPHA = "% Transparency in Color";
    public static String PIXEL_INTERROW_SPACE = "Interrow Space (pixel)";
    public static String SHOW_SINTAX_HIGHLIGHING = "Show Sintax Highlight";
    public static String FONT_CHOOSER = "Font Selection...";
    public static String REORDER_PANE = "Reorder Doc.";
    public static String NEW_RESOURCE_TITLE = "New Resource";
    public static String NEW_CATEGORY_TITLE = "New Category";
    public static String PERSON_TITLE = "Person Information";
    public static String SELECT_DOCUMENT_TITLE = "Select Document";
    public static String DELETE_DOCUMENT_TITLE = "Delete Document";
    public static String RESOURCE_MANAGEMENT_INTERNAL_TITLE = "Resource Management";
    public static String LOGICAL_VIEW_TITLE = "Logical View";
    public static String XML_VIEW_TITLE = "XML View";
    public static String DOCUMENT_METADATA_TITLE = "Document's Metadata";
    public static String DOCUMENT_RESOURCES_TITLE = "Document's Resources";
    public static String CONFIG_FONT_TITLE = "Font Configuration";
    public static String SAVE_WORD = "Save";
    public static String BEHAVIOUR_PANE_TITLE = "Config Behaviour";
    public static String XML_PANE_TITLE = "Config XML View";
    public static String RESOURCE_SHEET_BROWSER = "Resource Sheet Browser";
    public static String LANGUAGE_PANEL_TITLE = "Config Language";
    public static String LANGUAGE_CHOOSE_TITLE = "Config Language* (Need restart)";
    public static String TIP_CORPUS_SELECT = "Select the Working Corpus";
    public static String TIP_CORPUS_CLOSE = "Close the Working Corpus";
    public static String TIP_CORPUS_SAVE = "Save the Changes made in the working Corpus";
    public static String TIP_CORPUS_SAVE_AS = "Save As a New File the Changes made in the working Corpus";
    public static String TIP_CORPUS_NEW = "Create a New Corpus";
    public static String TIP_INTERVIEW_SELECT = "Select a document from the current Corpus";
    public static String TIP_INTERVIEW_ADD = "Add a new document to the current Corpus";
    public static String TIP_INTERVIEW_DELETE = "Delete a document from the current Corpus";
    public static String TIP_INTERVIEW_CLOSE = "Close the current Document";
    public static String TIP_CATEGORY_NEW = "Add a New Category as a child of the selected node";
    public static String TIP_CATEGORY_DELETE = "Delete a seleced Category from the Taxonomy Tree";
    public static String TIP_CONFIG = "Configuration Panel";
    public static String TIP_CONFIG_IMPORT = "Import Taxonomy Tree";
    public static String TIP_HELP_HELP = "Show the application help";
    public static String TIP_HELP_ABOUT = "About ...";
    public static String TIP_PUBLIC_COLLABORATIVE = "Register collaborative corpus";
    public static String TIP_CORPUS_VISIBILITY = "Set Corpus Visibility";
    public static String TIP_RETRIEVE_COLLABORATIVE = "Open collaborative corpus";
    public static String TIP_REMOVE_COLLABORATIVE = "Remove collaborative corpus";
    public static String CLOSE_WORD = "Close";
    public static String RECTRICTION_WORD = "Restriction";
    public static String INFORMATION_WORD = "Information";
    public static String WARNING_WORD = "Warning";
    public static String ERROR_WORD = "Error";
    public static String DELETE_WORD = "Delete";
    public static String APPLY_WORD = "Apply";
    public static String PREVIEW_WORD = "Preview";
    public static String CANCEL_WORD = "Cancel";
    public static String OK_WORD = "OK";
    public static String COLUMN_NAME_NUM = "Sec N\u00ba";
    public static String COLUMN_NAME_APPLIED_TAX = "Applied Categories";
    public static String COLUMN_NAME_SPEAKER = "Speaker";
    public static String COLUMN_NAME_TURN = "Transcription";
    public static String COLUMN_NAME_NUMBER = "Number";
    public static String COLUMN_NAME_NAME = "Name";
    public static String COLUMN_NAME_TYPE = "Type";
    public static String COLUMN_NAME_DESCRIPTION = "Description";
    public static String COLUMN_NAME_ADDRESS = "Address";
    public static String COLUMN_NAME_ACTION = "Action";
    public static String CMT_PUBLISH = "Register Collaborative Corpus";
    public static String CMT_PUBLISH_SURE = "A new corpus or the current non-collaborative corpus will be registered to be collaborative. Insert a corpus ID ...";
    public static String CMT_PUBLISH_CONFIR = "The corpus has been registered correctly. Now it is a collaborative Corpus";
    public static String CMT_RETRIEVE = "Choose the collaborative corpus you want to use...";
    public static String CMT_VISIBILITY = "Choose the collaborative corpus you want to change the visibility ...";
    public static String CMT_REMOVE = "Choose the collaborative corpus you want to remove...";
    public static String CMT_REMOVE_SURE = "Are you sure you want to permanently delete the selected colaborative corpus from CMT?";
    public static String CMT_REMOVE_TITLE = "Remove Collaborative Corpus";
    public static String CMT_RETRIEVE_TITLE = "Select Collaborative Corpus";
    public static String CMT_VISIBILITY_TITLE = "Select Collaborative Corpus To Change Visibility";
    public static String CMT_VISIBILITY_PUB = "Now, The corpus is a public corpus.";
    public static String CMT_VISIBILITY_PRIV = "Now, The corpus is a private corpus.";
    public static String CMT_RELEASE_DOCUMENT = "Current document will be closed. Do you like to save the changes?";
    public static String CMT_RELEASE_DOCUMENT_TITLE = "Closing Document ...";
    public static String CMT_RETRIEVE_NODOCS = "There is no document available. Please register something before. ";
    public static String CMT_RETRIEVE_NOCORPUS = "There is no corpus available. Please register something before. ";
    public static String CMT_PUBLISH_NOCORPUS = "There is not any corpus open. To register a corpus, you first should to open/create a corpus.";
    public static String CMT_REPUBLISH = "Current corpus is already collaborative. You can not re-register this corpus again since collaborative functions will be lost.";
    public static String CMT_REPUBLISH_TITLE = "Trying to re-registering a collaborative corpus";
    public static String CMT_PUBLISH_CHAR = "The corpus ID contains not allowed chars like '_'. ";
    public static String CMT_PUBLISH_CHAR_TITLE = "Non-allowed chars";

    public static void saveInternationalization(String path) {
        Properties p = new Properties();
        try {
            for (Field actual : Internationalization.class.getDeclaredFields()) {
                p.setProperty(actual.getName(), (String)actual.get(null));
            }
            p.storeToXML(new FileOutputStream(path), "Internationalization File", "UTF-16");
        }
        catch (Exception e) {
            System.out.println("Error during saving the Internationalization File");
        }
    }

    public static void loadInternationalization(InputStream in) {
        Properties p = new Properties();
        try {
            p.loadFromXML(in);
        }
        catch (IOException e) {
            System.out.println("IOException ->" + e.getMessage());
        }
        for (Object key : p.keySet()) {
            String value = p.getProperty((String) key);
            try {
                Internationalization.class.getField((String) key).set(null, value);
            }
            catch (IllegalAccessException e) {
                System.out.println("Can not be internationalization");
            }
            catch (NoSuchFieldException e) {
                System.out.println("Can not be internationalization");
            }
        }
    }

    public static void main(String[] args) {
        boolean update = false;
        if (args.length > 1) {
            update = Boolean.getBoolean(args[0]);
        }
        JFileChooser f = new JFileChooser();
        String fileName = "default.xml";
        if (!update) {
            if (0 == f.showOpenDialog(null)) {
                File file = f.getSelectedFile();
                fileName = file.getAbsolutePath();
                try {
                    Internationalization.loadInternationalization(new FileInputStream(file));
                }
                catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            Internationalization.saveInternationalization(fileName.substring(0, fileName.length() - 4) + "-full.xml");
        } else {
            Properties original = new Properties();
            try {
                if (0 == f.showOpenDialog(null)) {
                    File file = f.getSelectedFile();
                    fileName = file.getAbsolutePath();
                    original.loadFromXML(new FileInputStream(file));
                    Properties updates = new Properties();
                    for (int i = 0; i < Internationalization.class.getDeclaredFields().length; ++i) {
                        if (original.getProperty(Internationalization.class.getDeclaredFields()[i].getName()) != null) continue;
                        try {
                            updates.setProperty(Internationalization.class.getDeclaredFields()[i].getName(), Internationalization.class.getDeclaredFields()[i].get(Internationalization.class.newInstance()).toString());
                            continue;
                        }
                        catch (IllegalAccessException e) {
                            e.printStackTrace();
                            continue;
                        }
                        catch (InstantiationException e) {
                            e.printStackTrace();
                        }
                    }
                    updates.storeToXML(new FileOutputStream(fileName.substring(0, fileName.length() - 4) + "-update.xml"), "Internationalization File", "UTF-16");
                }
            }
            catch (IOException e) {
                System.out.println("IOException ->" + e.getMessage());
            }
        }
    }
}

