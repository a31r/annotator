/*
 * Decompiled with CFR 0_118.
 */
package annotador.utils;

import annotador.utils.Constants;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Properties;
import java.util.Set;

public class ShowedCategory {
    private static Properties map = null;

    private ShowedCategory() {
        map = new Properties();
    }

    public static boolean isShowedAppliedCategory(String categoryId) {
        if (map == null) {
            new ShowedCategory();
        }
        if (map.getProperty(categoryId) == null) {
            map.setProperty(categoryId, Constants.DEFAULT_SHOWED_CATEGORY);
        }
        return Boolean.parseBoolean(map.getProperty(categoryId));
    }

    public static boolean isShowedAnyInstanceOf(String categoryID) {
        if (map == null) {
            new ShowedCategory();
        }
        for (Object key : map.keySet()) {
            String k = (String)key;
            if (!k.endsWith(categoryID) || !Boolean.parseBoolean((String)map.get(key))) continue;
            return true;
        }
        return false;
    }

    public static void setShowOfCategory(String categoryID, boolean isShow) {
        if (map == null) {
            new ShowedCategory();
        }
        for (Object key : map.keySet()) {
            String k = (String)key;
            if (!k.endsWith(categoryID)) continue;
            map.setProperty(k, String.valueOf(isShow));
        }
    }

    public static void setShowAppliedCategory(String categoryId, boolean isShow) {
        if (map == null) {
            new ShowedCategory();
        }
        if (map.getProperty(categoryId) != null) {
            map.remove(categoryId);
        }
        map.setProperty(categoryId, String.valueOf(isShow));
    }

    public static void loadShowedCategory(String path) {
        map = new Properties();
        try {
            map.loadFromXML(new FileInputStream(path));
        }
        catch (IOException e) {
            System.out.println("ShowedCategory.loadShowedCategory() -> Not Loaded.");
        }
    }

    public static void removeAllShowedCategories() {
        map = new Properties();
    }

    public static void saveShowedCategory(String path) {
        try {
            map.storeToXML(new FileOutputStream(path), "Showed Categories");
        }
        catch (IOException e) {
            System.out.println("ShowedCategory.saveShowedCategory() -> Not Saved.");
        }
    }
}

