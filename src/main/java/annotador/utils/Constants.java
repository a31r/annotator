/*
 * Decompiled with CFR 0_118.
 */
package annotador.utils;

import java.io.File;
import java.util.Properties;
import password.Base64Coder;

public class Constants {
    public static final String APPLICATION_NAME = "BACKBONE Annotator 3.8";
    public static String CMT_KEEPALIVE = "60000";
    public static String CMT_URL = "http://localhost:9000/CMTWebService?wsdl";
    public static String CMT_USER = "admin";
    public static String CMT_PASS = "admin";
    public static String RENDER_DOUBLE_COMMAS = "true";
    public static String WEB_PAGE_DIRECTION = "http://www.um.es/backbone";
    public static String NUMBER_CHAR_IN_DELETE_INDIVIDUAL_TAXONOMY = "10";
    public static String VRP_URL = "http://purl.org/sacodeyl/vrp/";
    public static String VRP_SUFFIX_ALLFOLDER = "folders";
    public static String VRP_SUFFIX_ALLSHEET = "allsheets?folder_id=";
    public static String VRP_SUFFIX_ASHEET = "getsheet?id=";
    public static String VRP_SUFFIX_BROWSER_ASHEET = "getsheet?id=";
    public static String VRP_ID_ROOT = "-1";
    public static String VRP_USER = "sacodeyl";
    public static String VRP_PASS = "piloting";
    public static String VRP_TYPE = "vrp";
    public static String WORKING_PATH = ".";
    public static String IS_COLLABORATIVE_CORPUS = null;
    public static String DOCUMENT_OPENED = null;
    public static boolean IS_TAXONOMY_LOCKED = false;
    public static boolean DEBUG = false;
    public static int ROW_INTERCELL_SPACING = 5;
    public static String KEYWORD_SCHEME = "SACODEYL Scheme";
    public static String ENCODING = "UTF-16";
    public static String ENCODING_FOR_IMPORT = "UTF-8";
    public static String SHOW_GRID = "false";
    public static String DEFAULT_SHOWED_CATEGORY = "true";
    public static String COLOR_BORDER = "#ffc3c8";
    public static String SELECTION_COLOR = "#ABE5B6";
    public static String URL_HTML_PAGE = "/docs/help.html";
    public static String ALLOW_AUTOMATIC_IMPORT = "false";
    public static String SINTAX_HIGHLIGHTING = "false";
    public static String HTML_DIR = File.separator + "docs";
    public static String TEI_NAMESPACE = "http://www.tei-c.org/ns/1.0";
    public static String XML_NAMESPACE = "http://www.w3.org/XML/1998/namespace";
    public static String FILE_NAME_CONFIG_APP = File.separator + "config.xml";
    public static String FILE_NAME_CONFIG_COLOR = File.separator + "configColor.xml";
    public static String FILE_NAME_CONFIG_COLOR_RESOURCE = "/config/configColor.xml";
    public static String FILE_NAME_EMPTY_CORPUS = File.separator + "templates" + File.separator + "empty-corpus-backbone.xml";
    public static String FILE_NAME_EMPTY_CORPUS_RESOURCE = "/templates/empty-corpus-backbone.xml";
    public static String FILE_NAME_CONFIG_CONVERSOR = File.separator + "configConversor.xml";
    public static String FILE_NAME_CONFIG_CONVERSOR_RESOURCE = "/config/configConversor.xml";
    public static String FILE_NAME_INTERNATIONALIZATION = "/config/languages.xml";
    public static String FILE_NAME_LANGUAGE_FILE = "English";
    public static String FILE_NAME_CONFIG_SHOWED_CATEGORY = File.separator + "showedCategory.xml";
    public static String SELECT_DOCUMENT_UP_ICON = "/images/doc_up.png";
    public static String SELECT_DOCUMENT_DOWN_ICON = "/images/doc_down.png";
    public static String ICON_IMPORT_TAX = "/images/import_tax.png";
    public static String ICON_TRANSCRIPTOR = "/images/transcriptor.png";
    public static String ICON_INFORM = "/images/inform.png";
    public static String ICON_SHOW_KEYWORD = "/images/eye.png";
    public static String ICON_NOSHOW_KEYWORD = "/images/noeye.png";
    public static String ICON_DELETE_MINI = "/images/delete.png";
    public static String MENU_ICON_CMT_PUBLISH = "/images/database_up.png";
    public static String MENU_ICON_CMT_REMOVE = "/images/database_delete.png";
    public static String MENU_ICON_CMT_RETRIEVE = "/images/database_down.png";
    public static String MENU_ICON_CMT_VISIBILITY = "/images/search.png";
    public static String MENU_ICON_CORPUS_SELECT = "/images/corpus_load.png";
    public static String MENU_ICON_CORPUS_NEW = "/images/corpus_add.png";
    public static String MENU_ICON_CORPUS_CLOSE = "/images/corpus_close.png";
    public static String MENU_ICON_CORPUS_SAVE = "/images/corpus_save.png";
    public static String MENU_ICON_CORPUS_SAVE_AS = "/images/corpus_save_as.png";
    public static String MENU_ICON_CONFIG = "/images/config.png";
    public static String MENU_ICON_INTERVIEW_SELECT = "/images/doc_edit.png";
    public static String MENU_ICON_INTERVIEW_NEW = "/images/doc_add.png";
    public static String MENU_ICON_INTERVIEW_DELETE = "/images/doc_delete.png";
    public static String MENU_ICON_INTERVIEW_CLOSE = "/images/doc_close.png";
    public static String MENU_ICON_HELP_ITEM = "/images/about.png";
    public static String MENU_ICON_HELP_ABOUT = "/images/photo.png";
    public static String MENU_ICON_TAXONOMY_NEW = "/images/favorite_add.png";
    public static String MENU_ICON_TAXONOMY_DELETE = "/images/favorite_remove.png";
    public static String MENU_ICON_TAXONOMY_UP = "/images/cat_up.png";
    public static String MENU_LOCK_TREE = "/images/lock.png";
    public static String MENU_UNLOCK_TREE = "/images/unlock.png";
    public static String MENU_ICON_TAXONOMY_DOWN = "/images/cat_down.png";
    public static String MENU_ABOUT = "/images/acerca.png";
    public static String ICON_TREE_OPEN = "/images/tree_open.png";
    public static String ICON_TREE_CLOSE = "/images/tree_close.png";
    public static String ICON_TREE_LEAF = "/images/tree_leaf.png";
    public static String ICON_DELETE = "/images/delete2.png";
    public static String ICON_ADD = "/images/add2.png";
    public static String ICON_WARNING = "/images/warning.png";
    public static String ICON_POPUP_CATEGORY = "/images/pin.png";
    public static String TABLE_ICON_DELETE = "/images/delete.png";
    public static String TABLE_ICON_USER = "/images/speaker.png";
    public static String SPLASH_SCREEN_ICON = "/images/splash.png";
    public static String BUILDING_ICON = "/images/building.png";
    public static String SACODEYL_LOGO = "/images/formicon.png";
    public static String XML_EDIT = "/images/edit_xml.png";
    public static String XML_COMMIT = "/images/commit_xml.png";
    public static String XML_DISCARD = "/images/discart_xml.png";
    public static String RES_RESOURCE = "resource";
    public static String RES_NAME = "name";
    public static String RES_DESCRIPTION = "description";
    public static String RES_LINK = "link";
    public static String RES_TYPE = "type";
    public static String RESERVED_NAME_TO_TITLE_TAXONOMY = "Title";
    public static String RESERVED_WORD_NOTE_TYPE_TITLE = "title";
    public static String RESERVED_WORD_NOTE_TYPE_TRANSCRIPTOR = "transcriptor";
    public static String RESERVER_WORD_TO_TRANSCRIBER = "Transcriber";
    public static String FUNDER_PROJECT = "BACKBONE Project";
    public static String SPONSOR_PROJECT = "BACKBONE Project";
    public static String MIME_TYPE_JAVA_OBJECT = "application/x-java-jvm-local-objectref";
    public static String COLOR_XML_TAGS = "#ff0000";
    public static String COLOR_COMMENT_TAGS = "#777777";
    public static String COLOR_PROCESS_TAGS = "#0000ff";
    public static String COLOR_XML_ATT = "#ff00ff";
    public static String COLOR_ALPHA = "60";
    public static String ALLOW_ADD_TAXONOMY = "true";
    public static String ALLOW_DELETE_TAXONOMY = "true";
    public static String FONT_NAME = "Arial Unicode MS";
    public static String FONT_SIZE = "14";
    public static String BLOCK_INCREMENT_SCROLL = "30";
    public static String ENABLE_INTEGER_CACHE = "false";
    public static String APPL_NAME_READED = "BACKBONE Annotator 3.8";//"";
    public static String LAST_OPENED_CORPUS_FILE_PATH = "";

    private Constants() {
    }

    public static Properties toProperties() {
        Properties properties = new Properties();
        if (LAST_OPENED_CORPUS_FILE_PATH == null) {
            properties.setProperty("LAST_OPENED_CORPUS_FILE_PATH", "");
        } else {
            properties.setProperty("LAST_OPENED_CORPUS_FILE_PATH", LAST_OPENED_CORPUS_FILE_PATH);
        }
        properties.setProperty("COLOR_XML_TAGS", COLOR_XML_TAGS);
        properties.setProperty("COLOR_COMMENT_TAGS", COLOR_COMMENT_TAGS);
        properties.setProperty("COLOR_PROCESS_TAGS", COLOR_PROCESS_TAGS);
        properties.setProperty("COLOR_XML_ATT", COLOR_XML_ATT);
        properties.setProperty("COLOR_ALPHA", COLOR_ALPHA);
        properties.setProperty("ALLOW_ADD_TAXONOMY", ALLOW_ADD_TAXONOMY);
        properties.setProperty("ALLOW_DELETE_TAXONOMY", ALLOW_DELETE_TAXONOMY);
        properties.setProperty("FONT_NAME", FONT_NAME);
        properties.setProperty("FONT_SIZE", FONT_SIZE);
        properties.setProperty("SELECTION_COLOR", SELECTION_COLOR);
        properties.setProperty("SHOW_GRID", SHOW_GRID);
        properties.setProperty("BLOCK_INCREMENT_SCROLL", String.valueOf(BLOCK_INCREMENT_SCROLL));
        properties.setProperty("SINTAX_HIGHLIGHTING", SINTAX_HIGHLIGHTING);
        properties.setProperty("COLOR_GRID", COLOR_BORDER);
        properties.setProperty("ALLOW_AUTOMATIC_IMPORT", ALLOW_AUTOMATIC_IMPORT);
        properties.setProperty("ROW_INTERCELL_SPACING", String.valueOf(ROW_INTERCELL_SPACING));
        properties.setProperty("APP_VERSION", "BACKBONE Annotator 3.8");
        properties.setProperty("LANGUAGE_FILE", FILE_NAME_LANGUAGE_FILE);
        properties.setProperty("VRP_URL", VRP_URL);
        properties.setProperty("RENDER_DOUBLE_COMMAS", RENDER_DOUBLE_COMMAS);
        properties.setProperty("VRP_USER", VRP_USER);
        properties.setProperty("VRP_PASS", Base64Coder.encodeString(VRP_PASS));
        properties.setProperty("ENABLE_INTEGER_CACHE", ENABLE_INTEGER_CACHE);
        properties.setProperty("CMT_URL", CMT_URL);
        properties.setProperty("CMT_USER", CMT_USER);
        properties.setProperty("CMT_PASS", CMT_PASS);
        properties.setProperty("CMT_KEEPALIVE", CMT_KEEPALIVE);
        properties.setProperty("COLOR_ALPHA", COLOR_ALPHA);
        properties.setProperty("RENDER_DOUBLE_COMMAS", RENDER_DOUBLE_COMMAS);
        return properties;
    }

    public static void configureFromProperties(Properties properties) {
        LAST_OPENED_CORPUS_FILE_PATH = properties.getProperty("LAST_OPENED_CORPUS_FILE_PATH");
        if (LAST_OPENED_CORPUS_FILE_PATH.equals("")) {
            LAST_OPENED_CORPUS_FILE_PATH = null;
        }
        if (properties.getProperty("COLOR_XML_TAGS") != null) {
            COLOR_XML_TAGS = properties.getProperty("COLOR_XML_TAGS");
        }
        if (properties.getProperty("COLOR_COMMENT_TAGS") != null) {
            COLOR_COMMENT_TAGS = properties.getProperty("COLOR_COMMENT_TAGS");
        }
        if (properties.getProperty("COLOR_PROCESS_TAGS") != null) {
            COLOR_PROCESS_TAGS = properties.getProperty("COLOR_PROCESS_TAGS");
        }
        if (properties.getProperty("COLOR_XML_ATT") != null) {
            COLOR_XML_ATT = properties.getProperty("COLOR_XML_ATT");
        }
        if (properties.getProperty("COLOR_ALPHA") != null) {
            COLOR_ALPHA = properties.getProperty("COLOR_ALPHA");
        }
        if (properties.getProperty("ALLOW_ADD_TAXONOMY") != null) {
            ALLOW_ADD_TAXONOMY = properties.getProperty("ALLOW_ADD_TAXONOMY");
        }
        if (properties.getProperty("ALLOW_DELETE_TAXONOMY") != null) {
            ALLOW_DELETE_TAXONOMY = properties.getProperty("ALLOW_DELETE_TAXONOMY");
        }
        if (properties.getProperty("FONT_NAME") != null) {
            FONT_NAME = properties.getProperty("FONT_NAME");
        }
        if (properties.getProperty("FONT_SIZE") != null) {
            FONT_SIZE = properties.getProperty("FONT_SIZE");
        }
        if (properties.getProperty("SELECTION_COLOR") != null) {
            SELECTION_COLOR = properties.getProperty("SELECTION_COLOR");
        }
        if (properties.getProperty("SHOW_GRID") != null) {
            SHOW_GRID = properties.getProperty("SHOW_GRID");
        }
        if (properties.getProperty("BLOCK_INCREMENT_SCROLL") != null) {
            BLOCK_INCREMENT_SCROLL = properties.getProperty("BLOCK_INCREMENT_SCROLL");
        }
        if (properties.getProperty("SINTAX_HIGHLIGHTING") != null) {
            SINTAX_HIGHLIGHTING = properties.getProperty("SINTAX_HIGHLIGHTING");
        }
        if (properties.getProperty("VRP_URL") != null) {
            VRP_URL = properties.getProperty("VRP_URL");
        }
        if (properties.getProperty("COLOR_GRID") != null) {
            COLOR_BORDER = properties.getProperty("COLOR_GRID");
        }
        if (properties.getProperty("ALLOW_AUTOMATIC_IMPORT") != null) {
            ALLOW_AUTOMATIC_IMPORT = properties.getProperty("ALLOW_AUTOMATIC_IMPORT");
        }
        if (properties.getProperty("ROW_INTERCELL_SPACING") != null) {
            ROW_INTERCELL_SPACING = Integer.parseInt(properties.getProperty("ROW_INTERCELL_SPACING"));
        }
        if (properties.getProperty("APP_VERSION") != null) {
            APPL_NAME_READED = properties.getProperty("APP_VERSION");
        }
        if (properties.getProperty("LANGUAGE_FILE") != null) {
            FILE_NAME_LANGUAGE_FILE = properties.getProperty("LANGUAGE_FILE");
        }
        if (properties.getProperty("RENDER_DOUBLE_COMMAS") != null) {
            RENDER_DOUBLE_COMMAS = properties.getProperty("RENDER_DOUBLE_COMMAS");
        }
        if (properties.getProperty("VRP_USER") != null) {
            VRP_USER = properties.getProperty("VRP_USER");
        }
        if (properties.getProperty("VRP_PASS") != null) {
            VRP_PASS = Base64Coder.decodeString(properties.getProperty("VRP_PASS"));
        }
        if (properties.getProperty("ENABLE_INTEGER_CACHE") != null) {
            ENABLE_INTEGER_CACHE = properties.getProperty("ENABLE_INTEGER_CACHE");
        }
        if (properties.getProperty("CMT_URL") != null) {
            CMT_URL = properties.getProperty("CMT_URL");
        }
        if (properties.getProperty("CMT_USER") != null) {
            CMT_USER = properties.getProperty("CMT_USER");
        }
        if (properties.getProperty("CMT_PASS") != null) {
            CMT_PASS = properties.getProperty("CMT_PASS");
        }
        if (properties.get("CMT_KEEPALIVE") != null) {
            CMT_KEEPALIVE = properties.getProperty("CMT_KEEPALIVE");
        }
        if (properties.get("COLOR_ALPHA") != null) {
            COLOR_ALPHA = properties.getProperty("COLOR_ALPHA");
        }
        if (properties.get("RENDER_DOUBLE_COMMAS") != null) {
            RENDER_DOUBLE_COMMAS = properties.getProperty("RENDER_DOUBLE_COMMAS");
        }
    }
}

