/*
 * Decompiled with CFR 0_118.
 */
package annotador;

import annotador.action.cmt.CMTKeepAliveTimer;
import annotador.gui.MainForm;
import annotador.gui.SplashScreen;
import annotador.upgrade.AfterInstall;
import annotador.utils.ColorMap;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.ShowedCategory;
import annotador.utils.Utils;
import com.stefankrause.xplookandfeel.XPLookAndFeel;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Root {
    public static void main(String[] args) {
        File dir;
        try {
            XPLookAndFeel xp = new XPLookAndFeel();
            if (xp.isSupportedLookAndFeel()) {
                UIManager.setLookAndFeel(xp);
            }
        }
        catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        if (!(args.length <= 0 || (dir = new File(Constants.WORKING_PATH = args[0])).exists() || dir.mkdirs() || (dir = new File(Constants.WORKING_PATH = System.getProperty("user.home") + File.separator + "sacodeyl" + File.separator + "annotator")).exists())) {
            dir.mkdirs();
        }
        ColorMap.loadColors(Constants.WORKING_PATH + Constants.FILE_NAME_CONFIG_COLOR);
        try {
            FileInputStream fin = new FileInputStream(Constants.WORKING_PATH + Constants.FILE_NAME_CONFIG_APP);
            Properties prop = new Properties();
            prop.loadFromXML(fin);
            Constants.configureFromProperties(prop);
        }
        catch (FileNotFoundException e) {
            try {
                File newed = new File(Constants.WORKING_PATH + Constants.FILE_NAME_CONFIG_APP);
                newed.createNewFile();
                Properties prop = Constants.toProperties();
                FileOutputStream defaultConfiguration = new FileOutputStream(newed);
                prop.storeToXML(defaultConfiguration, "Annotator Tool Configurator");
            }
            catch (IOException e1) {}
        }
        catch (InvalidPropertiesFormatException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        InputStream in = ColorMap.class.getResourceAsStream(Constants.FILE_NAME_INTERNATIONALIZATION);
        ZipInputStream zin = new ZipInputStream(in);
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        try {
            ZipEntry entry;
            while ((entry = zin.getNextEntry()) != null) {
                if (!entry.getName().substring(0, entry.getName().lastIndexOf(".")).equals(Constants.FILE_NAME_LANGUAGE_FILE)) continue;
                while (zin.available() != 0) {
                    bout.write(zin.read());
                }
            }
        }
        catch (IOException e1) {
            // empty catch block
        }
        try {
            String s = bout.toString(Constants.ENCODING);
            if (s.length() > 0) {
                s = s.substring(0, s.length() - 1);
            }
            ByteArrayInputStream bin = new ByteArrayInputStream(s.getBytes(Constants.ENCODING));
            Internationalization.loadInternationalization(bin);
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Utils.setFont(new Font(Constants.FONT_NAME, 0, Integer.parseInt(Constants.FONT_SIZE)));
        MainForm mainForm = new MainForm("BACKBONE Annotator 3.8");
        mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(3));
        SplashScreen ss = new SplashScreen(new ImageIcon(Utils.getReourceImage(mainForm, Constants.SPLASH_SCREEN_ICON)));
        ss.setCursor(Cursor.getPredefinedCursor(3));
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension size = tk.getScreenSize();
        ss.setLocation((int)(size.getWidth() / 2.0) - ss.getWidth() / 2, (int)(size.getHeight() / 2.0) - ss.getHeight() / 2);
        ss.setProgress(-1);
        ss.setScreenVisible(true);
        Timer t = new Timer();
        t.schedule((TimerTask)new CMTKeepAliveTimer(mainForm), 1000, (long)Integer.parseInt(Constants.CMT_KEEPALIVE));
        mainForm.getDocumentTable().setGridColor(Color.decode(Constants.COLOR_BORDER));
        mainForm.getDocumentTable().setShowGrid(Boolean.parseBoolean(Constants.SHOW_GRID));
        DocumentSingleton.loadFromFile(Constants.LAST_OPENED_CORPUS_FILE_PATH);
        Utils.changeCorpusEvent(mainForm);
        ShowedCategory.loadShowedCategory(Constants.WORKING_PATH + Constants.FILE_NAME_CONFIG_SHOWED_CATEGORY);
        if (!Constants.APPL_NAME_READED.equals("BACKBONE Annotator 3.8")) {
            new AfterInstall();
        }
        ss.setCursor(Cursor.getPredefinedCursor(0));
        ss.setScreenVisible(false);
        Utils.setChangedCorpus(false);
        mainForm.setVisible(true);
        mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(0));
    }
}

