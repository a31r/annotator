/*
 * Decompiled with CFR 0_118.
 */
package annotador.vrp;

import annotador.vrp.ResourceSheet;
import java.io.PrintStream;
import java.util.Comparator;

public class VRPResourceSheetSort
implements Comparator {
    public int compare(Object o1, Object o2) {
        System.out.println("O1" + o1);
        System.out.println("O2" + o2);
        if (o1 instanceof ResourceSheet && o2 instanceof ResourceSheet) {
            ResourceSheet r1 = (ResourceSheet)o1;
            ResourceSheet r2 = (ResourceSheet)o2;
            return r1.getName().compareTo(r2.getName());
        }
        return 0;
    }
}

