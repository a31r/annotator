/*
 * Decompiled with CFR 0_118.
 */
package annotador.vrp;

import annotador.gui.MainForm;
import annotador.gui.VRPBrowser;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import org.w3c.dom.Node;

public class ShowVRPBrowser
implements ActionListener {
    Node divs;
    MainForm dialog;
    JTable table;

    public ShowVRPBrowser(MainForm dialog, JTable table, Node divs) {
        this.dialog = dialog;
        this.table = table;
        this.divs = divs;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        VRPBrowser vrp = new VRPBrowser(this.dialog, this.table, this.divs);
    }
}

