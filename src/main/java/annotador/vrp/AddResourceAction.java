/*
 * Decompiled with CFR 0_118.
 */
package annotador.vrp;

import annotador.action.resources.AddResourceCommon;
import annotador.action.resources.Resource;
import annotador.gui.MainForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import org.w3c.dom.Node;

public class AddResourceAction
implements ActionListener {
    MainForm dialog;
    JTable resourceTable;
    JTree vrp;
    Node div;

    public AddResourceAction(MainForm dialog, JTable resourceTable, JTree vrp, Node div) {
        this.dialog = dialog;
        this.resourceTable = resourceTable;
        this.div = div;
        this.vrp = vrp;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Resource res = (Resource)this.vrp.getSelectionPath().getLastPathComponent();
        AddResourceCommon.addResource(this.div, res, this.resourceTable);
        this.resourceTable.updateUI();
    }
}

