/*
 * Decompiled with CFR 0_118.
 */
package annotador.vrp;

import annotador.action.resources.Resource;
import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;

public class ResourceSheet
extends DefaultMutableTreeNode {
    String id;
    String name;
    String description;
    ArrayList<Resource> resources = new ArrayList();

    public ArrayList<Resource> getResources() {
        return this.resources;
    }

    public void addResource(Resource newResource) {
        this.resources.add(newResource);
    }

    public void removeResource(Resource oldResource) {
        this.resources.remove(oldResource);
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean equals(Object obj) {
        ResourceSheet rs;
        if (obj instanceof ResourceSheet && (rs = (ResourceSheet)obj).getId().equals(this.getId()) && rs.getName().equals(this.getName())) {
            return true;
        }
        return false;
    }
}

