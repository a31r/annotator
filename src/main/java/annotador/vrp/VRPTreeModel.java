/*
 * Decompiled with CFR 0_118.
 */
package annotador.vrp;

import annotador.action.resources.Resource;
import annotador.utils.Constants;
import annotador.vrp.Folder;
import annotador.vrp.ResourceSheet;
import annotador.vrp.VRPResourceSheetSort;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class VRPTreeModel
implements TreeModel {
    String url;
    Folder root;
    String xmlFolderStructure;
    static HashMap<String, ArrayList<ResourceSheet>> sheetCache;
    static HashMap<String, ArrayList<Folder>> folderCache;

    public VRPTreeModel(String url) {
        this.url = url;
        this.root = new Folder("-1", "VRP");
        if (sheetCache == null) {
            sheetCache = new HashMap();
        }
        if (folderCache == null) {
            folderCache = new HashMap();
        }
        HttpURLConnection.setFollowRedirects(true);
    }

    public ArrayList<ResourceSheet> getSheets(String folderId) {
        if (sheetCache.containsKey(folderId)) {
            return sheetCache.get(folderId);
        }
        ArrayList<ResourceSheet> ret = new ArrayList<ResourceSheet>();
        try {
            String finalURL = Constants.VRP_URL.substring(Constants.VRP_URL.indexOf("//") + 2);
            finalURL = Constants.VRP_URL.substring(0, Constants.VRP_URL.indexOf("//") + 2) + Constants.VRP_USER + ":" + Constants.VRP_PASS + "@" + finalURL + Constants.VRP_SUFFIX_ALLSHEET + folderId;
            System.out.println("VRPTreeModel.getSheets() ->" + folderId + ": Using this URL: " + finalURL);
            URL uAll = new URL(finalURL);
            Authenticator.setDefault(new Authenticator(){

                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(Constants.VRP_USER, Constants.VRP_PASS.toCharArray());
                }
            });
            URLConnection connAll = uAll.openConnection();
            BufferedReader brAll = new BufferedReader(new InputStreamReader(connAll.getInputStream()));
            StringBuilder sbAll = new StringBuilder();
            String aux = null;
            while ((aux = brAll.readLine()) != null) {
                sbAll.append(aux);
                sbAll.append("\n");
            }
            StringReader sr = new StringReader(sbAll.toString());
            XPath xpath = XPathFactory.newInstance().newXPath();
            System.out.println(sbAll.toString());
            NodeList l = (NodeList)xpath.evaluate("//sheet", new InputSource(sr), XPathConstants.NODESET);
            for (int i = 0; i < l.getLength(); ++i) {
                ResourceSheet rs = new ResourceSheet();
                ret.add(rs);
                Element sheet = (Element)l.item(i);
                if (sheet.getElementsByTagName("name").item(0) != null) {
                    rs.setName(sheet.getElementsByTagName("name").item(0).getTextContent());
                }
                if (sheet.getElementsByTagName("id").item(0) != null) {
                    rs.setId(sheet.getElementsByTagName("id").item(0).getTextContent());
                }
                if (sheet.getElementsByTagName("description").item(0) != null) {
                    rs.setDescription(sheet.getElementsByTagName("description").item(0).getTextContent());
                }
                URL uRs = new URL(Constants.VRP_URL + Constants.VRP_SUFFIX_ASHEET + rs.getId());
                URLConnection connRs = uRs.openConnection();
                BufferedReader bRs = new BufferedReader(new InputStreamReader(connRs.getInputStream(), "UTF-8"));
                StringBuilder sbRs = new StringBuilder();
                aux = null;
                while ((aux = bRs.readLine()) != null) {
                    sbRs.append(aux);
                    sbRs.append("\n");
                }
                StringReader srRs = new StringReader(sbRs.toString());
                XPath xpathRs = XPathFactory.newInstance().newXPath();
                NodeList lRs = (NodeList)xpathRs.evaluate("//resource", new InputSource(srRs), XPathConstants.NODESET);
                for (int j = 0; j < lRs.getLength(); ++j) {
                    Element resource = (Element)lRs.item(j);
                    Resource r = new Resource();
                    if (resource.getElementsByTagName("type").item(0) != null) {
                        r.setType(resource.getElementsByTagName("type").item(0).getTextContent());
                    }
                    if (resource.getElementsByTagName("desc").item(0) != null) {
                        r.setDescription(resource.getElementsByTagName("desc").item(0).getTextContent());
                    }
                    if (resource.getElementsByTagName("addr").item(0) != null) {
                        r.setLink(resource.getElementsByTagName("addr").item(0).getTextContent());
                    }
                    if (resource.getElementsByTagName("id").item(0) != null) {
                        r.setName(resource.getElementsByTagName("id").item(0).getTextContent());
                    }
                    rs.addResource(r);
                }
            }
            Collections.sort(ret, new VRPResourceSheetSort());
            sheetCache.put(folderId, ret);
            return ret;
        }
        catch (MalformedURLException e) {
            System.out.println("VRPTreeModel.getSheets() -> MalformedURL");
        }
        catch (IOException e) {
            System.out.println("VRPTreeModel.getSheets() -> IOException" + e.getMessage());
            e.printStackTrace();
        }
        catch (XPathExpressionException e) {
            System.out.println("VRPTreeModel.getSheets() -> XPathException");
        }
        return ret;
    }

    public ArrayList<Folder> getFolders(String folderId) {
        if (folderCache.containsKey(folderId)) {
            return folderCache.get(folderId);
        }
        ArrayList<Folder> ret = new ArrayList<Folder>();
        try {
            if (this.xmlFolderStructure == null) {
                String finalURL = Constants.VRP_URL.substring(Constants.VRP_URL.indexOf("//") + 2);
                finalURL = Constants.VRP_URL.substring(0, Constants.VRP_URL.indexOf("//") + 2) + Constants.VRP_USER + ":" + Constants.VRP_PASS + "@" + finalURL + Constants.VRP_SUFFIX_ALLFOLDER;
                URL uAll = new URL(finalURL);
                Authenticator.setDefault(new Authenticator(){

                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(Constants.VRP_USER, Constants.VRP_PASS.toCharArray());
                    }
                });
                URLConnection connAll = uAll.openConnection();
                BufferedReader brAll = new BufferedReader(new InputStreamReader(connAll.getInputStream(), "UTF-8"));
                StringBuilder sbAll = new StringBuilder();
                String aux = null;
                while ((aux = brAll.readLine()) != null) {
                    sbAll.append(aux);
                    sbAll.append("\n");
                }
                this.xmlFolderStructure = sbAll.toString();
            }
        }
        catch (MalformedURLException e) {
            return ret;
        }
        catch (IOException e) {
            return ret;
        }
        StringReader sr = new StringReader(this.xmlFolderStructure);
        StringReader sr2 = new StringReader(this.xmlFolderStructure);
        XPath xpath = XPathFactory.newInstance().newXPath();
        try {
            NodeList id = (NodeList)xpath.evaluate("//id[text()='" + folderId + "']/../subfolders/id", new InputSource(sr), XPathConstants.NODESET);
            NodeList name = (NodeList)xpath.evaluate("//id[text()='" + folderId + "']/../subfolders/name", new InputSource(sr2), XPathConstants.NODESET);
            for (int i = 0; i < id.getLength(); ++i) {
                ret.add(new Folder(id.item(i).getTextContent(), name.item(i).getTextContent()));
            }
        }
        catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        folderCache.put(folderId, ret);
        return ret;
    }

    @Override
    public Object getRoot() {
        return this.root;
    }

    @Override
    public Object getChild(Object parent, int index) {
        if (parent instanceof Folder) {
            Folder f = (Folder)parent;
            String id = f.getFolderId();
            ArrayList<Folder> folders = this.getFolders(id);
            if (index < folders.size()) {
                return folders.get(index);
            }
            ArrayList<ResourceSheet> sheets = this.getSheets(id);
            return sheets.get(index -= folders.size());
        }
        if (parent instanceof ResourceSheet) {
            ResourceSheet rs = (ResourceSheet)parent;
            return rs.getResources().get(index);
        }
        return null;
    }

    @Override
    public int getChildCount(Object parent) {
        if (parent instanceof Folder) {
            Folder f = (Folder)parent;
            String id = f.getFolderId();
            ArrayList<ResourceSheet> sheets = this.getSheets(id);
            ArrayList<Folder> folders = this.getFolders(id);
            return sheets.size() + folders.size();
        }
        if (parent instanceof ResourceSheet) {
            ResourceSheet rs = (ResourceSheet)parent;
            return rs.getResources().size();
        }
        return 0;
    }

    @Override
    public boolean isLeaf(Object node) {
        if (node instanceof Folder) {
            return false;
        }
        if (node instanceof ResourceSheet) {
            return false;
        }
        return true;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        if (parent instanceof Folder) {
            String id;
            Folder f;
            if (child instanceof Folder) {
                f = (Folder)parent;
                id = f.getFolderId();
                ArrayList<Folder> folders = this.getFolders(id);
                for (int i = 0; i < folders.size(); ++i) {
                    if (!folders.get(i).equals(child)) continue;
                    return i;
                }
            }
            if (child instanceof ResourceSheet) {
                f = (Folder)parent;
                id = f.getFolderId();
                ArrayList<ResourceSheet> sheets = this.getSheets(id);
                ArrayList<Folder> folders = this.getFolders(id);
                for (int i = 0; i < sheets.size(); ++i) {
                    if (!sheets.get(i).equals(child)) continue;
                    return i + folders.size();
                }
            }
        }
        if (parent instanceof ResourceSheet) {
            ResourceSheet rs = (ResourceSheet)parent;
            for (int i = 0; i < rs.getResources().size(); ++i) {
                if (!rs.getResources().get(i).equals(child)) continue;
                return i;
            }
            return rs.getResources().size();
        }
        return -1;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
    }

}

