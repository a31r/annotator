/*
 * Decompiled with CFR 0_118.
 */
package annotador.vrp;

import annotador.action.resources.Resource;
import annotador.gui.VRPBrowser;
import annotador.vrp.Folder;
import annotador.vrp.ResourceSheet;
import javax.swing.JButton;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

public class VRPChangeSelectionListener
implements TreeSelectionListener {
    VRPBrowser browser;

    public VRPChangeSelectionListener(VRPBrowser browser) {
        this.browser = browser;
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        Object selection = e.getNewLeadSelectionPath().getLastPathComponent();
        if (selection == null) {
            this.browser.getAddResourceSheet().setEnabled(false);
        }
        if (selection instanceof Resource) {
            this.browser.getAddResourceSheet().setEnabled(true);
        }
        if (selection instanceof Folder) {
            this.browser.getAddResourceSheet().setEnabled(false);
        }
        if (selection instanceof ResourceSheet) {
            this.browser.getAddResourceSheet().setEnabled(true);
        }
    }
}

