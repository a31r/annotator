/*
 * Decompiled with CFR 0_118.
 */
package annotador.vrp;

import annotador.action.resources.AddResourceCommon;
import annotador.action.resources.Resource;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.vrp.ResourceSheet;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import org.w3c.dom.Node;

public class AddResourceSheetAction
implements ActionListener {
    MainForm dialog;
    JTable resourceTable;
    JTree vrp;
    Node div;

    public AddResourceSheetAction(MainForm dialog, JTable resourceTable, JTree vrp, Node div) {
        this.dialog = dialog;
        this.resourceTable = resourceTable;
        this.div = div;
        this.vrp = vrp;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ResourceSheet rs = null;
        if (this.vrp.getSelectionPath().getLastPathComponent() != null) {
            if (this.vrp.getSelectionPath().getLastPathComponent() instanceof Resource) {
                rs = (ResourceSheet)this.vrp.getSelectionPath().getPath()[this.vrp.getSelectionPath().getPath().length - 2];
            }
            if (this.vrp.getSelectionPath().getLastPathComponent() instanceof ResourceSheet) {
                rs = (ResourceSheet)this.vrp.getSelectionPath().getLastPathComponent();
            }
            if (rs != null) {
                Resource res = new Resource();
                res.setDescription(rs.getDescription());
                res.setLink(Constants.VRP_URL + Constants.VRP_SUFFIX_BROWSER_ASHEET + rs.getId());
                res.setName(rs.getName());
                res.setType(Constants.VRP_TYPE);
                AddResourceCommon.addResource(this.div, res, this.resourceTable);
                this.resourceTable.updateUI();
            }
        }
    }
}

