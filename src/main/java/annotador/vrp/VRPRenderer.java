/*
 * Decompiled with CFR 0_118.
 */
package annotador.vrp;

import annotador.action.resources.Resource;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.Utils;
import annotador.vrp.ResourceSheet;
import java.awt.Component;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

public class VRPRenderer
extends DefaultTreeCellRenderer {
    public VRPRenderer(MainForm mainForm) {
        this.setOpenIcon(new ImageIcon(Utils.getReourceImage(mainForm, Constants.ICON_TREE_OPEN)));
        this.setClosedIcon(new ImageIcon(Utils.getReourceImage(mainForm, Constants.ICON_TREE_CLOSE)));
        this.setLeafIcon(new ImageIcon(Utils.getReourceImage(mainForm, Constants.ICON_TREE_LEAF)));
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        Component c = super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        if (c instanceof JLabel) {
            JLabel label = (JLabel)c;
            if (value instanceof Resource) {
                Resource r = (Resource)value;
                label.setText(r.getType() + " - " + r.getDescription() + " - " + r.getLink());
            }
            if (value instanceof ResourceSheet) {
                ResourceSheet rs = (ResourceSheet)value;
                label.setText(rs.getName() + " - " + rs.getId() + " - " + rs.getDescription());
            }
            return label;
        }
        return null;
    }
}

