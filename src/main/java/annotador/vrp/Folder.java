/*
 * Decompiled with CFR 0_118.
 */
package annotador.vrp;

import javax.swing.tree.DefaultMutableTreeNode;

public class Folder
extends DefaultMutableTreeNode {
    String folderId;
    String folderName;

    public String getFolderName() {
        return this.folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderId() {
        return this.folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public Folder(String folderId, String folderName) {
        super(folderId);
        this.folderId = folderId;
        this.folderName = folderName;
    }

    public boolean equals(Object obj) {
        Folder f;
        if (obj instanceof Folder && this.folderId.equals((f = (Folder)obj).getFolderId()) && this.folderName.equals(f.getFolderName())) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return this.folderName;
    }
}

