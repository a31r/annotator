/*
 * Decompiled with CFR 0_118.
 */
package annotador.vrp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;

public class CancelButton
implements ActionListener {
    JDialog dialog;

    public CancelButton(JDialog dialog) {
        this.dialog = dialog;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.dialog.setVisible(false);
    }
}

