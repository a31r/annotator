/*
 * Decompiled with CFR 0_118.
 */
package annotador.upgrade;

import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.TEIConstants;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class ImportFrom1xx {
    private static int[] ids;

    public static void main(String[] argv) {
        try {
            JFileChooser chooser = new JFileChooser();
            chooser.setApproveButtonText("Import");
            chooser.setApproveButtonToolTipText("Makes the import from a previous versi\u00f3n of the Corpura XML File");
            chooser.setAcceptAllFileFilterUsed(true);
            chooser.setDialogTitle("Select a CORPUS File From 1.xx | 0.xx versi\u00f3n of the SACODEYL Annotator");
            chooser.setMultiSelectionEnabled(false);
            if (chooser.showOpenDialog(null) == 0) {
                File file = chooser.getSelectedFile();
                BufferedReader br = new BufferedReader(new InputStreamReader((InputStream)new FileInputStream(file), Constants.ENCODING));
                String aux = "";
                StringBuilder total = new StringBuilder();
                while ((aux = br.readLine()) != null) {
                    total.append(aux);
                    total.append("\n");
                }
                br.close();
                Document document = DOMUtils.StringToDOM(total.toString(), Constants.ENCODING, Constants.TEI_NAMESPACE);
                ImportFrom1xx.convertFromAnyTo2xx(document);
                String text = DOMUtils.DOMToString(document, Constants.ENCODING);
                chooser = new JFileChooser();
                chooser.setApproveButtonText("Save New Corpus File");
                chooser.setApproveButtonToolTipText("Saves the new versi\u00f3n of the Corpura XML File");
                chooser.setAcceptAllFileFilterUsed(true);
                chooser.setDialogTitle("Save a CORPUS File to 2.xx Version of the Annotator");
                chooser.setMultiSelectionEnabled(false);
                if (chooser.showSaveDialog(null) == 0) {
                    file = chooser.getSelectedFile();
                    String pat = file.getAbsolutePath();
                    if (!pat.endsWith(".XML") && !pat.endsWith(".xml")) {
                        pat = pat + ".xml";
                    }
                    FileOutputStream fw = new FileOutputStream(pat, false);
                    OutputStreamWriter osw = new OutputStreamWriter((OutputStream)fw, Constants.ENCODING);
                    BufferedWriter writer = new BufferedWriter(osw);
                    writer.write(text);
                    writer.flush();
                    osw.flush();
                    fw.flush();
                    writer.close();
                    osw.close();
                    fw.close();
                } else {
                    JOptionPane.showMessageDialog(null, "Conversion Process Cancelled by the User", "Error", 0);
                }
            }
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error in the convert process." + e.getClass().getName(), "Error", 0);
        }
    }

    public static void convertFromAnyTo2xx(Document document) throws XPathExpressionException {
        Element particDesc;
        Node parent;
        int i;
        Element listPerson;
        Node n;
        int i2;
        NodeList childListPerson;
        System.out.println("Starting the Convert Process...");
        ImportFrom1xx.checkRealClean(document);
        System.out.println("Real Clean -> Pass");
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new TEINamespaceContext());
        NodeList list = (NodeList)xpath.evaluate("//tei:date/@value", document, XPathConstants.NODESET);
        for (i2 = 0; i2 < list.getLength(); ++i2) {
            Attr attr = (Attr)list.item(i2);
            String value = attr.getValue();
            Element owner = attr.getOwnerElement();
            owner.removeAttribute(attr.getName());
            owner.setAttribute("when", value);
        }
        System.out.println("Changed <data value> for <data when> -> Pass");
        list = (NodeList)xpath.evaluate("//tei:listPerson", document, XPathConstants.NODESET);
        for (i2 = 0; i2 < list.getLength(); ++i2) {
            listPerson = (Element)list.item(i2);
            parent = listPerson.getParentNode();
            particDesc = listPerson.getOwnerDocument().createElement("particDesc");
            childListPerson = listPerson.getChildNodes();
            ArrayList<Node> aux = new ArrayList<Node>();
            for (int j = 0; j < childListPerson.getLength(); ++j) {
                Node child = childListPerson.item(j);
                aux.add(child);
            }
            for (Node n2 : aux) {
                particDesc.appendChild(n2);
            }
            parent.replaceChild(particDesc, listPerson);
        }
        System.out.println("Changed <listPerson> for <particDesc> -> Pass");
        list = (NodeList)xpath.evaluate("//tei:timeRange", document, XPathConstants.NODESET);
        for (i2 = 0; i2 < list.getLength(); ++i2) {
            listPerson = (Element)list.item(i2);
            parent = listPerson.getParentNode();
            particDesc = listPerson.getOwnerDocument().createElement("time");
            childListPerson = listPerson.getChildNodes();
            NamedNodeMap map = listPerson.getAttributes();
            ArrayList<Node> auxChild = new ArrayList<Node>();
            for (int j = 0; j < childListPerson.getLength(); ++j) {
                Node child = childListPerson.item(j);
                auxChild.add(child);
            }
            ArrayList<Attr> auxAttr = new ArrayList<Attr>();
            for (int j2 = 0; j2 < map.getLength(); ++j2) {
                Node child = map.item(j2);
                auxAttr.add((Attr)child);
            }
            for (Node n3 : auxChild) {
                particDesc.appendChild(n3);
            }
            for (Attr attr : auxAttr) {
                particDesc.setAttributeNode((Attr)attr.cloneNode(true));
            }
            parent.replaceChild(particDesc, listPerson);
        }
        System.out.println("Change <timeRange> TO <time> -> Pass");
        NodeList linkList = (NodeList)xpath.evaluate("//tei:text/tei:link", document, XPathConstants.NODESET);
        NodeList recordingList = (NodeList)xpath.evaluate("//tei:recording", document, XPathConstants.NODESET);
        for (i = 0; i < linkList.getLength(); ++i) {
            Element link = (Element)linkList.item(i);
            String mediaFile = link.getAttribute("targets");
            Node text = link.getParentNode();
            text.removeChild(link);
            Element recording = (Element)recordingList.item(i);
            recording.setAttribute("rend", mediaFile);
        }
        System.out.println("Change <text><link targets> TO <recording rend> -> Pass");
        ImportFrom1xx.randomizationProcess(document);
        ImportFrom1xx.recheckValidationRandom(document);
        System.out.println("Randomization Process of the XML:ID Values -> Pass");
        ImportFrom1xx.cleanNameSpaces(document);
        System.out.println("Name spaces cleaned --> Pass");
        list = (NodeList)xpath.evaluate("//tei:anchor/@type", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            n = (Attr)list.item(i);
            if (!n.getNodeValue().startsWith("#")) continue;
            n.setNodeValue(n.getNodeValue().substring(1));
        }
        System.out.println("Adjusting <anchor type> --> Pass");
        list = (NodeList)xpath.evaluate("//tei:div/tei:note", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            n = (Element)list.item(i);
            if (!n.getAttributes().getNamedItem("type").getNodeValue().equals("")) continue;
            n.getAttributes().getNamedItem("type").setNodeValue("title");
        }
        System.out.println("Adjusting <note type> --> Pass");
        list = (NodeList)xpath.evaluate("//tei:div/tei:note", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            n = (Element)list.item(i);
            if (!n.getAttributes().getNamedItem("type").getNodeValue().equals("title")) continue;
            Node t = n.getFirstChild();
            Element head = n.getOwnerDocument().createElementNS(Constants.TEI_NAMESPACE, "head");
            head.appendChild(t);
            n.getParentNode().insertBefore(head, n);
            n.getParentNode().removeChild(n);
        }
        System.out.println("Changed <note> -> <head>");
        ImportFrom1xx.removeAloneAnchor(document);
        System.out.println("Removed Alone Anchor --> Pass");
        ImportFrom1xx.fixWhiteSpaces(document);
        System.out.println("Fixed white spaces --> Pass");
    }

    public static void fixWhiteSpaces(Node document) throws XPathExpressionException {
        int j;
        Element choice;
        ArrayList nodes;
        int i;
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new TEINamespaceContext());
        NodeList list = (NodeList)xpath.evaluate("//tei:choice", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            choice = (Element)list.item(i);
            nodes = new ArrayList();
            Utils.getInOrderRecursivo(0, choice, nodes);
            for (int j2 = 0; j2 < nodes.size(); ++j2) {
                if (!(nodes.get(j2) instanceof Text)) continue;
                ((Node)nodes.get(j2)).setTextContent(((Node)nodes.get(j2)).getTextContent().trim());
            }
        }
        document.normalize();
        list = (NodeList)xpath.evaluate("//tei:u", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            Node actual = list.item(i);
            if (!(actual instanceof Element)) continue;
            Element u = (Element)actual;
            ArrayList nodes2 = new ArrayList();
            Utils.getInOrderRecursivo(0, u, nodes2);
            ArrayList<Boolean> procesed = new ArrayList<Boolean>();
            for (int j3 = 0; j3 < nodes2.size(); ++j3) {
                procesed.add(false);
            }
            Text before = null;
            for (int j4 = 0; j4 < nodes2.size(); ++j4) {
                Node next;
                Node node = (Node)nodes2.get(j4);
                if (node instanceof Text) {
                    if (!((Boolean)procesed.get(j4)).booleanValue()) {
                        node.setTextContent(node.getTextContent().trim());
                    }
                    before = (Text)node;
                }
                if (!(node instanceof Element)) continue;
                Element element = (Element)node;
                if (element.getNodeName().equals(TEIConstants.TEI_TAG_ANCHOR) && !element.getAttribute(TEIConstants.TEI_ATTRIBUTE_NEXT).equals("") && before != null && before.getTextContent().length() > 0 && before.getTextContent().charAt(before.getTextContent().length() - 1) != ' ') {
                    before.setTextContent(before.getTextContent() + " ");
                }
                if (element.getNodeName().equals(TEIConstants.TEI_TAG_ANCHOR) && !element.getAttribute(TEIConstants.TEI_ATTRIBUTE_PREV).equals("")) {
                    Text next2 = null;
                    int index = 0;
                    for (int k = j4 + 1; k < nodes2.size(); ++k) {
                        if (!(nodes2.get(k) instanceof Text)) continue;
                        next2 = (Text)nodes2.get(k);
                        index = k;
                        break;
                    }
                    if (next2 != null) {
                        next2.setTextContent(" " + next2.getTextContent().trim());
                        procesed.set(index, true);
                        before = null;
                    }
                }
                if (element.getNodeName().equals(TEIConstants.TEI_TAG_ANCHOR) || element.getNodeName().equals(TEIConstants.TEI_TAG_TIME) || element.getNodeName().equals(TEIConstants.TEI_TAG_CORR) || element.getNodeName().equals(TEIConstants.TEI_TAG_ORIG)) continue;
                Node prev = element.getPreviousSibling();
                if (prev instanceof Text) {
                    Text pre = (Text)prev;
                    if (pre.getTextContent().length() > 0 && pre.getTextContent().charAt(pre.getTextContent().length() - 1) != ' ') {
                        pre.setTextContent(pre.getTextContent() + " ");
                    }
                } else {
                    Text nuevo = element.getOwnerDocument().createTextNode(" ");
                    element.getParentNode().insertBefore(nuevo, element);
                }
                for (next = element.getNextSibling(); next != null && !(next instanceof Text); next = next.getNextSibling()) {
                }
                if (next == null) continue;
                int index = 0;
                for (int k = j4 + 1; k < nodes2.size(); ++k) {
                    if (nodes2.get(k) != next) continue;
                    index = k;
                    break;
                }
                next.setTextContent(" " + next.getTextContent().trim());
                procesed.set(index, true);
            }
        }
        list = (NodeList)xpath.evaluate("//tei:add", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            choice = (Element)list.item(i);
            nodes = new ArrayList();
            Utils.getInOrderRecursivo(0, choice, nodes);
            for (j = 0; j < nodes.size(); ++j) {
                if (!(nodes.get(j) instanceof Text)) continue;
                ((Node)nodes.get(j)).setTextContent(((Node)nodes.get(j)).getTextContent().trim());
            }
        }
        list = (NodeList)xpath.evaluate("//tei:unclear", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            choice = (Element)list.item(i);
            nodes = new ArrayList();
            Utils.getInOrderRecursivo(0, choice, nodes);
            for (j = 0; j < nodes.size(); ++j) {
                if (!(nodes.get(j) instanceof Text)) continue;
                ((Node)nodes.get(j)).setTextContent(((Node)nodes.get(j)).getTextContent().trim());
            }
        }
        list = (NodeList)xpath.evaluate("//tei:abbr", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            choice = (Element)list.item(i);
            nodes = new ArrayList();
            Utils.getInOrderRecursivo(0, choice, nodes);
            for (j = 0; j < nodes.size(); ++j) {
                if (!(nodes.get(j) instanceof Text)) continue;
                ((Node)nodes.get(j)).setTextContent(((Node)nodes.get(j)).getTextContent().trim());
            }
        }
        list = (NodeList)xpath.evaluate("//tei:foreign", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            choice = (Element)list.item(i);
            nodes = new ArrayList();
            Utils.getInOrderRecursivo(0, choice, nodes);
            for (j = 0; j < nodes.size(); ++j) {
                if (!(nodes.get(j) instanceof Text)) continue;
                ((Node)nodes.get(j)).setTextContent(((Node)nodes.get(j)).getTextContent().trim());
            }
        }
        list = (NodeList)xpath.evaluate("//tei:hi", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            choice = (Element)list.item(i);
            nodes = new ArrayList();
            Utils.getInOrderRecursivo(0, choice, nodes);
            for (j = 0; j < nodes.size(); ++j) {
                if (!(nodes.get(j) instanceof Text)) continue;
                ((Node)nodes.get(j)).setTextContent(((Node)nodes.get(j)).getTextContent().trim());
            }
        }
        list = (NodeList)xpath.evaluate("//tei:corr", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            choice = (Element)list.item(i);
            nodes = new ArrayList();
            Utils.getInOrderRecursivo(0, choice, nodes);
            for (j = 0; j < nodes.size(); ++j) {
                if (!(nodes.get(j) instanceof Text)) continue;
                ((Node)nodes.get(j)).setTextContent(((Node)nodes.get(j)).getTextContent().trim());
            }
        }
        list = (NodeList)xpath.evaluate("//tei:orig", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            choice = (Element)list.item(i);
            nodes = new ArrayList();
            Utils.getInOrderRecursivo(0, choice, nodes);
            for (j = 0; j < nodes.size(); ++j) {
                if (!(nodes.get(j) instanceof Text)) continue;
                ((Node)nodes.get(j)).setTextContent(((Node)nodes.get(j)).getTextContent().trim());
            }
        }
    }

    private static void cleanNameSpaces(Document document) throws XPathExpressionException {
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new TEINamespaceContext());
        NodeList list = (NodeList)xpath.evaluate("//@xml:id", document, XPathConstants.NODESET);
        for (int i = 0; i < list.getLength(); ++i) {
            Node actual = list.item(i);
            if (!(actual instanceof Attr)) continue;
            Attr att = (Attr)actual;
            Element e = att.getOwnerElement();
            e.removeAttributeNode(att);
            NamedNodeMap atts = e.getAttributes();
            Node del = null;
            for (int j = 0; j < atts.getLength(); ++j) {
                Node n = atts.item(j);
                if (!n.getNodeValue().equals("http://www.w3.org/XML/1998/namespace")) continue;
                del = n;
            }
            if (del != null) {
                e.removeAttributeNode((Attr)del);
            }
            e.setAttribute("xml:id", att.getValue());
        }
        document.getDocumentElement().setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xml", "http://www.w3.org/XML/1998/namespace");
    }

    private static void recheckValidationRandom(Document document) throws XPathExpressionException {
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new TEINamespaceContext());
        NodeList bodyList = (NodeList)xpath.evaluate("//tei:body", document, XPathConstants.NODESET);
        for (int i = 0; i < bodyList.getLength(); ++i) {
            Element body = (Element)bodyList.item(i);
            NodeList divs = (NodeList)xpath.evaluate("/tei:teiCorpus/tei:TEI[" + (i + 1) + "]/tei:text/tei:body/tei:div", document, XPathConstants.NODESET);
            NodeList allUs = (NodeList)xpath.evaluate("/tei:teiCorpus/tei:TEI[" + (i + 1) + "]/tei:text/tei:body/tei:div/tei:u|" + "/tei:teiCorpus/tei:TEI[" + (i + 1) + "]/tei:text/tei:body/tei:div/tei:p", document, XPathConstants.NODESET);
            for (int m = 0; m < divs.getLength(); ++m) {
                NodeList us = (NodeList)xpath.evaluate("/tei:teiCorpus/tei:TEI[" + (i + 1) + "]/tei:text/tei:body/tei:div[" + (m + 1) + "]/tei:u|" + "/tei:teiCorpus/tei:TEI[" + (i + 1) + "]/tei:text/tei:body/tei:div[" + (m + 1) + "]/tei:p", document, XPathConstants.NODESET);
                for (int j = 0; j < us.getLength(); ++j) {
                    Node u = us.item(j);
                    NodeList anchors = (NodeList)xpath.evaluate("/tei:teiCorpus/tei:TEI[" + (i + 1) + "]/tei:text/tei:body/tei:div[" + (m + 1) + "]/tei:u[" + (j + 1) + "]/tei:anchor|" + "/tei:teiCorpus/tei:TEI[" + (i + 1) + "]/tei:text/tei:body/tei:div[" + (m + 1) + "]/tei:p[" + (j + 1) + "]/tei:anchor", document, XPathConstants.NODESET);
                    HashMap<String, String> map = new HashMap<String, String>();
                    ArrayList<Element> anchorDelete = new ArrayList<Element>();
                    ids = new int[anchors.getLength()];
                    for (int l = 0; l < ids.length; ++l) {
                        ImportFrom1xx.ids[l] = l + 1;
                    }
                    for (int k = 0; k < anchors.getLength(); ++k) {
                        Element anchor = (Element)anchors.item(k);
                        String idBefore = anchor.getAttribute("xml:id");
                        if (idBefore.equals("")) {
                            anchorDelete.add(anchor);
                            continue;
                        }
                        String idAfter = null;
                        if (map.containsKey(idBefore)) {
                            idAfter = (String)map.get(idBefore);
                        } else {
                            idAfter = "rd" + ImportFrom1xx.random() + "c" + (i + 1) + "r" + ImportFrom1xx.calculateRow(allUs, u) + "id" + ImportFrom1xx.getId();
                            map.put(idBefore, idAfter);
                        }
                        anchor.setAttributeNS("http://www.w3.org/XML/1998/namespace", "xml:id", idAfter);
                        if (anchor.hasAttribute("next")) {
                            idBefore = anchor.getAttribute("next");
                            if (map.containsKey(idBefore)) {
                                idAfter = (String)map.get(idBefore);
                            } else {
                                idAfter = "rd" + ImportFrom1xx.random() + "c" + (i + 1) + "r" + ImportFrom1xx.calculateRow(allUs, u) + "id" + ImportFrom1xx.getId();
                                map.put(idBefore, idAfter);
                            }
                            anchor.setAttribute("next", idAfter);
                            continue;
                        }
                        idBefore = anchor.getAttribute("prev");
                        if (map.containsKey(idBefore)) {
                            idAfter = (String)map.get(idBefore);
                        } else {
                            System.out.println("RARE. IT SHOULD BE TO STAY IN CACHE.");
                            idAfter = "RARO";
                            map.put(idBefore, idAfter);
                        }
                        anchor.setAttribute("prev", idAfter);
                    }
                    for (Element e : anchorDelete) {
                        e.getParentNode().removeChild(e);
                    }
                }
            }
        }
    }

    private static void updateTimeStamping(Document document) throws XPathExpressionException {
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new TEINamespaceContext());
        NodeList body = (NodeList)xpath.evaluate("//tei:body", document, XPathConstants.NODESET);
        String previousTO = null;
        for (int index = 0; index < body.getLength(); ++index) {
            NodeList timeList = (NodeList)xpath.evaluate("/tei:teiCorpus/tei:TEI[" + index + "]/tei:text/tei:body//tei:time", document, XPathConstants.NODESET);
            previousTO = null;
            for (int i = 0; i < timeList.getLength(); ++i) {
                Element time = (Element)timeList.item(i);
                String from = time.getAttribute("from");
                String to = time.getAttribute("to");
                if (previousTO != null && previousTO.length() > from.length()) {
                    from = from + "0";
                }
                Integer formI = Integer.valueOf(from);
                Integer toI = Integer.valueOf(to);
                while (toI < formI) {
                    to = to + "0";
                    toI = Integer.valueOf(to);
                }
                while (from.length() <= to.length() && !from.equals("0001")) {
                    from = from + "0";
                    formI = Integer.valueOf(from);
                    if (from.length() > 4 && from.startsWith("0")) {
                        from = from.substring(1);
                    }
                    if (to.length() <= 4 || !to.startsWith("0")) continue;
                    to = to.substring(1);
                }
                while (formI > toI) {
                    from = from.substring(0, from.length() - 1);
                    formI = Integer.valueOf(from);
                }
                if (from.length() > 4 && from.startsWith("0")) {
                    from = from.substring(1);
                }
                if (to.length() > 4 && to.startsWith("0")) {
                    to = to.substring(1);
                }
                time.setAttribute("from", from);
                time.setAttribute("to", to);
                previousTO = to;
            }
        }
    }

    private static void updateTimeStampingBK(Document document) throws XPathExpressionException {
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new TEINamespaceContext());
        NodeList body = (NodeList)xpath.evaluate("//tei:body", document, XPathConstants.NODESET);
        String previousTO = null;
        for (int index = 0; index < body.getLength(); ++index) {
            NodeList timeList = (NodeList)xpath.evaluate("/tei:teiCorpus/tei:TEI[" + index + "]/tei:text/tei:body//tei:time", document, XPathConstants.NODESET);
            previousTO = null;
            for (int i = 0; i < timeList.getLength(); ++i) {
                Element time = (Element)timeList.item(i);
                String from = time.getAttribute("from");
                String to = time.getAttribute("to");
                if (from.equals("") || to.equals("")) {
                    System.out.println("ERROR -> Check the timeStamping some of them are empty: from>" + from + " to>" + to);
                }
                if (previousTO != null && previousTO.length() > from.length()) {
                    from = from + "0";
                }
                Integer formI = Integer.valueOf(from);
                Integer toI = Integer.valueOf(to);
                while (toI < formI) {
                    to = to + "0";
                    toI = Integer.valueOf(to);
                }
                while (from.length() <= to.length() && !from.equals("0001")) {
                    from = from + "0";
                    formI = Integer.valueOf(from);
                    if (from.length() > 4 && from.startsWith("0")) {
                        from = from.substring(1);
                    }
                    if (to.length() <= 4 || !to.startsWith("0")) continue;
                    to = to.substring(1);
                }
                while (formI > toI) {
                    from = from.substring(0, from.length() - 1);
                    formI = Integer.valueOf(from);
                }
                if (from.length() > 4 && from.startsWith("0")) {
                    from = from.substring(1);
                }
                if (to.length() > 4 && to.startsWith("0")) {
                    to = to.substring(1);
                }
                time.setAttribute("from", from);
                time.setAttribute("to", to);
                previousTO = to;
            }
        }
    }

    private static int getId() {
        int aux = 0;
        for (int i = 0; i < ids.length; ++i) {
            if (ids[i] == -1) continue;
            aux = ids[i];
            ImportFrom1xx.ids[i] = -1;
            break;
        }
        return aux;
    }

    private static int calculateRow(NodeList allUs, Node u) {
        for (int i = 0; i < allUs.getLength(); ++i) {
            Node actual = allUs.item(i);
            if (actual != u) continue;
            return i;
        }
        return -1;
    }

    private static void checkRealClean(Document document) throws XPathExpressionException {
        Attr attr;
        int i;
        HashMap<String, String> ids = new HashMap<String, String>();
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new TEINamespaceContext());
        NodeList list = (NodeList)xpath.evaluate("//tei:taxonomy/@xml:id|//tei:category/@xml:id", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            attr = (Attr)list.item(i);
            ids.put(attr.getValue(), attr.getValue());
        }
        System.out.println("Real Clean. Get IDs -> Pass");
        list = (NodeList)xpath.evaluate("//tei:div/@decls", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            attr = (Attr)list.item(i);
            String decls = attr.getValue();
            if (decls.equals("")) {
                attr.getOwnerElement().removeAttribute(attr.getName());
                continue;
            }
            Pattern pattern = Pattern.compile("#\\p{Graph}+", 64);
            Matcher matcher = pattern.matcher(decls);
            String newDecls = "";
            while (matcher.find()) {
                String match = decls.substring(matcher.start(), matcher.end());
                if (!ids.containsKey(match = match.substring(1))) continue;
                newDecls = newDecls + "#" + match + " ";
            }
            newDecls = newDecls.trim();
            attr.setValue(newDecls);
        }
        System.out.println("Real Clean. Check DIV Tags -> Pass");
        list = (NodeList)xpath.evaluate("//tei:anchor/@type", document, XPathConstants.NODESET);
        for (i = 0; i < list.getLength(); ++i) {
            attr = (Attr)list.item(i);
            String type = attr.getValue();
            if (type.equals("")) {
                attr.getOwnerElement().getParentNode().removeChild(attr.getOwnerElement());
                continue;
            }
            if (type.startsWith("#")) {
                type = type.substring(1);
            }
            if (ids.containsKey(type)) continue;
            attr.getOwnerElement().getParentNode().removeChild(attr.getOwnerElement());
        }
        System.out.println("Real Clean. Check Anchor Tags -> Pass");
    }

    private static void randomizationProcess(Document document) throws XPathExpressionException {
        NodeList divs;
        Element body;
        int i;
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new TEINamespaceContext());
        NodeList bodyList = (NodeList)xpath.evaluate("//tei:body", document, XPathConstants.NODESET);
        for (i = 0; i < bodyList.getLength(); ++i) {
            body = (Element)bodyList.item(i);
            divs = body.getElementsByTagName("div");
            for (int j = 0; j < divs.getLength(); ++j) {
                Element div = (Element)divs.item(j);
                String att = div.getAttribute("xml:id");
                String decls = div.getAttribute("decls");
                if (decls.equals("")) {
                    div.removeAttribute("decls");
                }
                if (div.getAttribute("xml:id").contains("C")) continue;
                div.setAttributeNS("http://www.w3.org/XML/1998/namespace", "xml:id", "R" + ImportFrom1xx.random() + "C" + i + "D" + String.valueOf(j + 1));
            }
        }
        bodyList = (NodeList)xpath.evaluate("//tei:body", document, XPathConstants.NODESET);
        for (i = 0; i < bodyList.getLength(); ++i) {
            body = (Element)bodyList.item(i);
            divs = (NodeList)xpath.evaluate("/tei:teiCorpus/tei:TEI[" + (i + 1) + "]/tei:text/tei:body/tei:div", document, XPathConstants.NODESET);
            for (int m = 0; m < divs.getLength(); ++m) {
                NodeList us = (NodeList)xpath.evaluate("/tei:teiCorpus/tei:TEI[" + (i + 1) + "]/tei:text/tei:body/tei:div[" + (m + 1) + "]/tei:u|" + "/tei:teiCorpus/tei:TEI[" + (i + 1) + "]/tei:text/tei:body/tei:div[" + (m + 1) + "]/tei:p", document, XPathConstants.NODESET);
                for (int j = 0; j < us.getLength(); ++j) {
                    NodeList anchors = (NodeList)xpath.evaluate("/tei:teiCorpus/tei:TEI[" + (i + 1) + "]/tei:text/tei:body/tei:div[" + (m + 1) + "]/tei:u[" + (j + 1) + "]/tei:anchor|" + "/tei:teiCorpus/tei:TEI[" + (i + 1) + "]/tei:text/tei:body/tei:div[" + (m + 1) + "]/tei:p[" + (j + 1) + "]/tei:anchor", document, XPathConstants.NODESET);
                    HashMap<String, String> map = new HashMap<String, String>();
                    for (int k = 0; k < anchors.getLength(); ++k) {
                        Element anchor = (Element)anchors.item(k);
                        String idBefore = anchor.getAttribute("xml:id");
                        if (idBefore.contains("c")) continue;
                        String idAfter = null;
                        if (map.containsKey(idBefore)) {
                            idAfter = (String)map.get(idBefore);
                        } else {
                            idAfter = "rd" + ImportFrom1xx.random() + "c" + i + "r" + j + "id" + k;
                            map.put(idBefore, idAfter);
                        }
                        anchor.setAttributeNS("http://www.w3.org/XML/1998/namespace", "xml:id", idAfter);
                        if (anchor.hasAttribute("next")) {
                            idBefore = anchor.getAttribute("next");
                            if (map.containsKey(idBefore)) {
                                idAfter = (String)map.get(idBefore);
                            } else {
                                idAfter = "rd" + ImportFrom1xx.random() + "c" + i + "r" + j + "id" + k;
                                map.put(idBefore, idAfter);
                            }
                            anchor.setAttribute("next", idAfter);
                            continue;
                        }
                        idBefore = anchor.getAttribute("prev");
                        if (map.containsKey(idBefore)) {
                            idAfter = (String)map.get(idBefore);
                        } else {
                            idAfter = "rd" + ImportFrom1xx.random() + "c" + i + "r" + j + "id" + k;
                            map.put(idBefore, idAfter);
                        }
                        anchor.setAttribute("prev", idAfter);
                    }
                }
            }
        }
    }

    private static int random() {
        double rnd = Math.random();
        rnd *= 8999.0;
        return (int)Math.round(rnd += 1000.0);
    }

    private static void removeAloneAnchor(Document document) throws XPathExpressionException {
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new TEINamespaceContext());
        HashMap<String, Element> anchors = new HashMap<String, Element>();
        NodeList anchorList = (NodeList)xpath.evaluate("//tei:anchor", document, XPathConstants.NODESET);
        for (int i = 0; i < anchorList.getLength(); ++i) {
            Element anchor = (Element)anchorList.item(i);
            if (!anchor.getAttribute("next").equals("")) {
                anchors.put(anchor.getAttribute("xml:id"), anchor);
                continue;
            }
            if (anchors.containsKey(anchor.getAttribute("prev"))) {
                anchors.remove(anchor.getAttribute("prev"));
                continue;
            }
            anchors.put(anchor.getAttribute("xml:id"), anchor);
        }
        for (Node node : anchors.values()) {
            Element anchor = (Element)node;
            anchor.getParentNode().removeChild(anchor);
        }
    }
}

