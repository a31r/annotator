/*
 * Decompiled with CFR 0_118.
 */
package annotador.upgrade;

import annotador.utils.ColorMap;
import annotador.utils.Constants;
import annotador.utils.ShowedCategory;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Reader;
import java.util.Properties;

public class AfterInstall {
    public AfterInstall() {
        System.out.println("Updating process.....");
        Constants.LAST_OPENED_CORPUS_FILE_PATH = "";
        Constants.ALLOW_AUTOMATIC_IMPORT = "false";
        Constants.FONT_NAME = "Arial Unicode MS";
        Constants.FONT_SIZE = "12";
        Constants.RENDER_DOUBLE_COMMAS = "false";
        Constants.ENABLE_INTEGER_CACHE = "false";
        Constants.VRP_URL = "http://purl.org/sacodeyl/vrp/";
        try {
            File f;
            BufferedReader br = null;
            StringBuilder total = null;
            try {
                br = new BufferedReader(new InputStreamReader((InputStream)new FileInputStream(Constants.WORKING_PATH + Constants.FILE_NAME_EMPTY_CORPUS), Constants.ENCODING));
                String aux = "";
                total = new StringBuilder();
                while ((aux = br.readLine()) != null) {
                    total.append(aux);
                    total.append("\n");
                }
            }
            catch (IOException ioe) {
                // empty catch block
            }
            if ((f = new File(Constants.WORKING_PATH)).exists()) {
                AfterInstall.deleteDirectory(f);
                f = new File(Constants.WORKING_PATH);
            }
            f.mkdirs();
            String dir = (Constants.WORKING_PATH + Constants.FILE_NAME_EMPTY_CORPUS).substring(0, (Constants.WORKING_PATH + Constants.FILE_NAME_EMPTY_CORPUS).lastIndexOf(File.separator));
            File dirF = new File(dir);
            if (!dirF.exists()) {
                dirF.mkdirs();
            }
            String file = Constants.WORKING_PATH + Constants.FILE_NAME_EMPTY_CORPUS;
            File fileF = new File(file);
            if (total != null) {
                FileOutputStream fout = new FileOutputStream(fileF, false);
                OutputStreamWriter osw = new OutputStreamWriter((OutputStream)fout, Constants.ENCODING);
                osw.write(total.toString());
                osw.flush();
                osw.close();
            }
            ColorMap.saveColors(Constants.WORKING_PATH + Constants.FILE_NAME_CONFIG_COLOR);
            ShowedCategory.saveShowedCategory(Constants.WORKING_PATH + Constants.FILE_NAME_CONFIG_SHOWED_CATEGORY);
            Properties prop = Constants.toProperties();
            prop.storeToXML(new FileOutputStream(Constants.WORKING_PATH + Constants.FILE_NAME_CONFIG_APP), "Annotator Tool Configurator");
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; ++i) {
                if (files[i].isDirectory()) {
                    AfterInstall.deleteDirectory(files[i]);
                    continue;
                }
                files[i].delete();
            }
        }
        return path.delete();
    }
}

