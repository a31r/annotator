/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;

public class TransparentBackground
extends JComponent {
    private JFrame frame;
    private Image background;
    private JDialog dialog;

    public TransparentBackground(JFrame frame) {
        this.frame = frame;
        this.updateBackground();
    }

    public TransparentBackground(JDialog dialog) {
        this.dialog = dialog;
        this.updateBackground();
    }

    public void updateBackground() {
        try {
            Robot rbt = new Robot();
            Toolkit tk = Toolkit.getDefaultToolkit();
            Dimension dim = tk.getScreenSize();
            this.background = rbt.createScreenCapture(new Rectangle(0, 0, (int)dim.getWidth(), (int)dim.getHeight()));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        Point pos = this.getLocationOnScreen();
        Point offset = new Point(- pos.x, - pos.y);
    }
}

