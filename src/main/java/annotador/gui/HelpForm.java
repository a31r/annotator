/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.utils.Internationalization;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class HelpForm
implements HyperlinkListener {
    private JPanel mainPanel;
    private JEditorPane helpPanel;
    private JButton cerrar;
    private JScrollPane scrollPane;

    public HelpForm(final JDialog dialog) {
        this.$$$setupUI$$$();
        this.helpPanel.addHyperlinkListener(this);
        this.cerrar.setText(Internationalization.CLOSE_WORD);
        this.cerrar.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);
            }
        });
        dialog.getContentPane().add(this.mainPanel);
        this.getHelpPanel().setEditable(false);
    }

    public JPanel getMainPanel() {
        return this.mainPanel;
    }

    public JEditorPane getHelpPanel() {
        return this.helpPanel;
    }

    public JButton getCerrar() {
        return this.cerrar;
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        try {
            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                this.helpPanel.setPage(e.getURL());
            }
        }
        catch (IOException e1) {
            // empty catch block
        }
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JButton jButton;
        JPanel jPanel;
        JEditorPane jEditorPane;
        JScrollPane jScrollPane;
        this.mainPanel = jPanel = new JPanel();
        jPanel.setLayout(new GridLayoutManager(2, 3, new Insets(0, 0, 0, 0), -1, -1, false, false));
        this.scrollPane = jScrollPane = new JScrollPane();
        jScrollPane.setHorizontalScrollBarPolicy(32);
        jScrollPane.setVerticalScrollBarPolicy(22);
        jPanel.add((Component)jScrollPane, new GridConstraints(0, 0, 1, 3, 0, 3, 7, 7, null, null, null));
        this.helpPanel = jEditorPane = new JEditorPane();
        jScrollPane.setViewportView(jEditorPane);
        this.cerrar = jButton = new JButton();
        jButton.setText("Close");
        jPanel.add((Component)jButton, new GridConstraints(1, 0, 1, 3, 0, 0, 1, 0, null, null, null));
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.mainPanel;
    }

}

