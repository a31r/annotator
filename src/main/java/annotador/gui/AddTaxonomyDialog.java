/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class AddTaxonomyDialog
extends JDialog {
    MainForm mainForm;
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField categoryIdField;
    private JTextField categoryNameField;
    private JLabel iconLabel;
    private JButton chooseColorButton;
    private JPanel colorPanel;
    private JLabel categoryNameLabel;
    private JLabel categoryIdLabel;
    private JLabel categoryColorLabel;
    private JPanel newCategoryPanel;
    boolean isAcepted = true;
    String categoryName;
    String categoryId;
    Color categoryColor;

    public AddTaxonomyDialog(final JComponent positionRelative, MainForm mainForm) {
        super((Frame)null, Internationalization.NEW_TAXONOMY_TITLE, true);
        this.$$$setupUI$$$();
        this.buttonCancel.setText(Internationalization.CANCEL_WORD);
        this.buttonOK.setText(Internationalization.OK_WORD);
        this.categoryColorLabel.setText(Internationalization.CATEGORY_COLOR);
        this.categoryIdLabel.setText(Internationalization.CATEGORY_ID);
        this.categoryNameLabel.setText(Internationalization.CATEGORY_NAME);
        this.chooseColorButton.setText(Internationalization.CHOOSE_COLOR);
        TitledBorder t = new TitledBorder(Internationalization.NEW_CATEGORY_TITLE);
        this.newCategoryPanel.setBorder(t);
        this.mainForm = mainForm;
        this.setContentPane(this.contentPane);
        this.setModal(true);
        this.getRootPane().setDefaultButton(this.buttonOK);
        this.categoryColor = Color.BLACK;
        this.colorPanel.setBackground(Color.BLACK);
        this.chooseColorButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Color color = JColorChooser.showDialog(positionRelative, Internationalization.NEW_TAXONOMY_COLOR_TITLE, Color.BLACK);
                if (color != null) {
                    AddTaxonomyDialog.this.categoryColor = color;
                    AddTaxonomyDialog.this.colorPanel.setBackground(color);
                }
            }
        });
        this.buttonOK.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                AddTaxonomyDialog.this.onOK();
            }
        });
        this.buttonCancel.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                AddTaxonomyDialog.this.onCancel();
            }
        });
        this.setDefaultCloseOperation(0);
        this.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosing(WindowEvent e) {
                AddTaxonomyDialog.this.onCancel();
            }
        });
        this.contentPane.registerKeyboardAction(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                AddTaxonomyDialog.this.onCancel();
            }
        }, KeyStroke.getKeyStroke(27, 0), 1);
        this.iconLabel.setIcon(new ImageIcon(Utils.getReourceImage(mainForm, Constants.ICON_ADD)));
        this.pack();
        this.setLocationRelativeTo(mainForm.getTabbedPane());
        this.setVisible(true);
    }

    private void onOK() {
        if (this.categoryIdField.getText().matches("\\p{Alpha}+")) {
            this.isAcepted = true;
            this.setCategoryName(this.categoryNameField.getText());
            this.setCategoryId(this.categoryIdField.getText());
            this.setVisible(false);
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.TAXONOMIES_ADD_ERROR_MESSAGE_5, Internationalization.WARNING_WORD, 2, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)));
        }
    }

    private void onCancel() {
        this.isAcepted = false;
        this.setVisible(false);
        this.dispose();
    }

    public boolean isAcepted() {
        return this.isAcepted;
    }

    public String getCategoryName() {
        return this.categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Color getCategoryColor() {
        return this.categoryColor;
    }

    public void setCategoryColor(Color categoryColor) {
        this.categoryColor = categoryColor;
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JPanel jPanel;
        JTextField jTextField;
        JLabel jLabel;
        JPanel jPanel2;
        JTextField jTextField2;
        JButton jButton;
        JLabel jLabel2;
        JPanel jPanel3;
        JLabel jLabel3;
        JLabel jLabel4;
        JButton jButton2;
        JButton jButton3;
        this.contentPane = jPanel3 = new JPanel();
        jPanel3.setLayout(new GridLayoutManager(2, 2, new Insets(10, 10, 10, 10), -1, -1, false, false));
        jPanel3.setBorder(BorderFactory.createTitledBorder(null, "", 0, 0, null, null));
        JPanel jPanel4 = new JPanel();
        jPanel4.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel3.add((Component)jPanel4, new GridConstraints(1, 1, 1, 1, 0, 3, 3, 1, null, null, null));
        Spacer spacer = new Spacer();
        jPanel4.add((Component)spacer, new GridConstraints(0, 0, 1, 1, 0, 1, 6, 1, null, null, null));
        JPanel jPanel5 = new JPanel();
        jPanel5.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, true, false));
        jPanel4.add((Component)jPanel5, new GridConstraints(0, 1, 1, 1, 0, 3, 3, 3, null, null, null));
        this.buttonOK = jButton = new JButton();
        jButton.setText("OK");
        jPanel5.add((Component)jButton, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.buttonCancel = jButton3 = new JButton();
        jButton3.setText("Cancel");
        jPanel5.add((Component)jButton3, new GridConstraints(0, 1, 1, 1, 0, 1, 3, 0, null, null, null));
        this.newCategoryPanel = jPanel2 = new JPanel();
        jPanel2.setLayout(new GridLayoutManager(3, 3, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel3.add((Component)jPanel2, new GridConstraints(0, 1, 1, 1, 0, 3, 3, 3, null, null, null));
        jPanel2.setBorder(BorderFactory.createTitledBorder(null, "New Category", 0, 0, null, null));
        this.categoryIdField = jTextField = new JTextField();
        jPanel2.add((Component)jTextField, new GridConstraints(1, 0, 1, 2, 4, 0, 6, 0, null, new Dimension(150, -1), null));
        this.categoryNameField = jTextField2 = new JTextField();
        jPanel2.add((Component)jTextField2, new GridConstraints(0, 0, 1, 2, 4, 0, 6, 0, null, new Dimension(150, -1), null));
        this.categoryIdLabel = jLabel4 = new JLabel();
        jLabel4.setText("Category Id (no blanks,no simbols,only letters) ");
        jPanel2.add((Component)jLabel4, new GridConstraints(1, 2, 1, 1, 8, 0, 0, 0, null, null, null));
        this.categoryNameLabel = jLabel2 = new JLabel();
        jLabel2.setText("Category Name");
        jPanel2.add((Component)jLabel2, new GridConstraints(0, 2, 1, 1, 8, 0, 0, 0, null, null, null));
        this.colorPanel = jPanel = new JPanel();
        jPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel2.add((Component)jPanel, new GridConstraints(2, 0, 1, 1, 0, 3, 3, 3, null, null, null));
        this.chooseColorButton = jButton2 = new JButton();
        jButton2.setText("Choose Color");
        jPanel2.add((Component)jButton2, new GridConstraints(2, 1, 1, 1, 0, 1, 3, 0, null, null, null));
        this.categoryColorLabel = jLabel3 = new JLabel();
        jLabel3.setText("Category Color");
        jPanel2.add((Component)jLabel3, new GridConstraints(2, 2, 1, 1, 8, 0, 0, 0, null, null, null));
        this.iconLabel = jLabel = new JLabel();
        jLabel.setText("");
        jPanel3.add((Component)jLabel, new GridConstraints(0, 0, 1, 1, 8, 0, 0, 0, null, null, null));
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.contentPane;
    }

}

