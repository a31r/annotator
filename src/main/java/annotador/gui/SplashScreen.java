/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;

public class SplashScreen
extends JWindow {
    BorderLayout borderLayout1 = new BorderLayout();
    JLabel imageLabel = new JLabel();
    JPanel southPanel = new JPanel();
    FlowLayout southPanelFlowLayout = new FlowLayout();
    JProgressBar progressBar = new JProgressBar();
    ImageIcon imageIcon;

    public SplashScreen(ImageIcon imageIcon) {
        this.imageIcon = imageIcon;
        try {
            this.jbInit();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void jbInit() throws Exception {
        this.imageLabel.setIcon(this.imageIcon);
        this.getContentPane().setLayout(this.borderLayout1);
        this.southPanel.setLayout(this.southPanelFlowLayout);
        this.southPanel.setBackground(Color.BLACK);
        this.getContentPane().add((Component)this.imageLabel, "Center");
        this.getContentPane().add((Component)this.southPanel, "South");
        this.southPanel.add((Component)this.progressBar, (Object)null);
        this.pack();
    }

    public void setProgressMax(int maxProgress) {
        this.progressBar.setMaximum(maxProgress);
    }

    public void setProgress(int progress) {
        final int theProgress = progress;
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                SplashScreen.this.progressBar.setValue(theProgress);
            }
        });
    }

    public void setProgress(String message, int progress) {
        final int theProgress = progress;
        final String theMessage = message;
        this.setProgress(progress);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                SplashScreen.this.progressBar.setValue(theProgress);
                SplashScreen.this.setMessage(theMessage);
            }
        });
    }

    public void setScreenVisible(boolean b) {
        final boolean boo = b;
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                SplashScreen.this.setVisible(boo);
            }
        });
    }

    private void setMessage(String message) {
        if (message == null) {
            message = "";
            this.progressBar.setStringPainted(false);
        } else {
            this.progressBar.setStringPainted(true);
        }
        this.progressBar.setString(message);
    }

}

