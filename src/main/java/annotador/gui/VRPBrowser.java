/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.vrp.AddResourceSheetAction;
import annotador.vrp.CancelButton;
import annotador.vrp.VRPChangeSelectionListener;
import annotador.vrp.VRPRenderer;
import annotador.vrp.VRPTreeModel;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import org.w3c.dom.Node;

public class VRPBrowser {
    private JPanel mainPanel;
    private JTree RSTree;
    private JButton cancelButton;
    private JButton addResourceSheet;
    Node divs;
    MainForm dialog;
    JTable table;

    public VRPBrowser(MainForm dialog, JTable table, Node divs) {
        this.dialog = dialog;
        this.table = table;
        this.divs = divs;
        this.$$$setupUI$$$();
        JDialog d = new JDialog(this.dialog.getVentanaPrincipal(), Internationalization.RESOURCE_SHEET_BROWSER, true);
        d.setContentPane(this.getMainPanel());
        d.setSize(500, 400);
        d.setLocationRelativeTo(this.dialog.getMainPanel());
        this.cancelButton.addActionListener(new CancelButton(d));
        this.addResourceSheet.addActionListener(new AddResourceSheetAction(dialog, table, this.RSTree, divs));
        this.RSTree.addTreeSelectionListener(new VRPChangeSelectionListener(this));
        this.addResourceSheet.setEnabled(false);
        this.getRSTree().setModel(new VRPTreeModel(Constants.VRP_URL));
        this.getRSTree().setCellRenderer(new VRPRenderer(dialog));
        d.setVisible(true);
    }

    public JPanel getMainPanel() {
        return this.mainPanel;
    }

    public void setMainPanel(JPanel mainPanel) {
        this.mainPanel = mainPanel;
    }

    public JTree getRSTree() {
        return this.RSTree;
    }

    public void setRSTree(JTree RSTree) {
        this.RSTree = RSTree;
    }

    public JButton getCancelButton() {
        return this.cancelButton;
    }

    public void setCancelButton(JButton cancelButton) {
        this.cancelButton = cancelButton;
    }

    public JButton getAddResourceSheet() {
        return this.addResourceSheet;
    }

    public void setAddResourceSheet(JButton addResourceSheet) {
        this.addResourceSheet = addResourceSheet;
    }

    private void createUIComponents() {
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JButton jButton;
        JButton jButton2;
        JTree jTree;
        JPanel jPanel;
        this.mainPanel = jPanel = new JPanel();
        jPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        JScrollPane jScrollPane = new JScrollPane();
        jPanel.add((Component)jScrollPane, new GridConstraints(0, 0, 1, 1, 0, 3, 7, 7, null, null, null));
        this.RSTree = jTree = new JTree();
        jScrollPane.setViewportView(jTree);
        JPanel jPanel2 = new JPanel();
        jPanel2.setLayout(new GridLayoutManager(1, 4, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel.add((Component)jPanel2, new GridConstraints(1, 0, 1, 1, 0, 3, 3, 3, null, null, null));
        this.addResourceSheet = jButton2 = new JButton();
        jButton2.setText("Add Resource Sheet");
        jPanel2.add((Component)jButton2, new GridConstraints(0, 1, 1, 1, 0, 1, 3, 0, null, null, null));
        Spacer spacer = new Spacer();
        jPanel2.add((Component)spacer, new GridConstraints(0, 3, 1, 1, 0, 1, 6, 1, null, null, null));
        Spacer spacer2 = new Spacer();
        jPanel2.add((Component)spacer2, new GridConstraints(0, 0, 1, 1, 0, 1, 6, 1, null, null, null));
        this.cancelButton = jButton = new JButton();
        jButton.setText("Close");
        jPanel2.add((Component)jButton, new GridConstraints(0, 2, 1, 1, 0, 1, 3, 0, null, null, null));
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.mainPanel;
    }
}

