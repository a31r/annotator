/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.action.resources.ImportResourcesFromFile;
import annotador.action.resources.ShowAddRourceFormAction;
import annotador.gui.MainForm;
import annotador.resources.PtrNodeCellRenderer;
import annotador.resources.ResourcesTableModel;
import annotador.utils.Internationalization;
import annotador.vrp.ShowVRPBrowser;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import org.w3c.dom.Node;

public class SectionResourceManagementForm {
    private JPanel mainPanel;
    private JTable resourceTable;
    private JButton addResourceButton;
    private JButton importResourcesButton;
    private JScrollPane scrolTable;
    private JButton attachAVRPResouceButton;
    public JDialog mainDialog;
    private MainForm mainForm;
    int sectionNumber;
    Node div;

    public JTable getResourceTable() {
        return this.resourceTable;
    }

    public void setResourceTable(JTable resourceTable) {
        this.resourceTable = resourceTable;
    }

    public SectionResourceManagementForm(MainForm mainForm, Node div, int sectionNumber) {
        this.sectionNumber = sectionNumber;
        this.mainForm = mainForm;
        this.div = div;
        this.$$$setupUI$$$();
        this.mainDialog = new JDialog(this.mainForm.getVentanaPrincipal(), Internationalization.RESOURCE_MANAGEMENT_TITLE + sectionNumber, false);
        this.mainDialog.setContentPane(this.mainPanel);
        this.addResourceButton.setText(Internationalization.ADD_RESOURCE);
        this.importResourcesButton.setText(Internationalization.ADD_RESOURCE_FROM_FILE);
        this.attachAVRPResouceButton.setText(Internationalization.ATTACH_VRP_RS);
        TitledBorder t = new TitledBorder(Internationalization.RESOURCE_MANAGEMENT_INTERNAL_TITLE);
        this.mainPanel.setBorder(t);
        ResourcesTableModel rtm = new ResourcesTableModel(div);
        this.resourceTable.setModel(rtm);
        this.resourceTable.setShowGrid(true);
        this.resourceTable.setAutoResizeMode(4);
        this.resourceTable.setDefaultRenderer(Node.class, new PtrNodeCellRenderer(mainForm, this.getResourceTable()));
        this.resourceTable.setDefaultEditor(Node.class, new PtrNodeCellRenderer(mainForm, this.getResourceTable()));
        this.resourceTable.setColumnSelectionAllowed(true);
        this.resourceTable.setRowSelectionAllowed(true);
        this.resourceTable.setRowMargin(10);
        this.resourceTable.setRowHeight(this.resourceTable.getRowMargin() + this.resourceTable.getFont().getSize() * 2);
        ShowAddRourceFormAction showAddRourceFormAction = new ShowAddRourceFormAction(this.mainForm, this.getResourceTable(), div);
        this.addResourceButton.addActionListener(showAddRourceFormAction);
        ShowVRPBrowser showVRPBrowser = new ShowVRPBrowser(this.mainForm, this.getResourceTable(), div);
        this.attachAVRPResouceButton.addActionListener(showVRPBrowser);
        ImportResourcesFromFile irff = new ImportResourcesFromFile(this.mainForm, this.getResourceTable(), div);
        this.importResourcesButton.addActionListener(irff);
        this.mainDialog.setSize(800, 300);
        this.mainDialog.setLocationRelativeTo(mainForm.getTabbedPane());
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JTable jTable;
        JButton jButton;
        JPanel jPanel;
        JScrollPane jScrollPane;
        JButton jButton2;
        JButton jButton3;
        this.mainPanel = jPanel = new JPanel();
        jPanel.setLayout(new GridLayoutManager(2, 6, new Insets(0, 0, 0, 0), -1, -1, false, false));
        this.scrolTable = jScrollPane = new JScrollPane();
        jScrollPane.setHorizontalScrollBarPolicy(32);
        jScrollPane.setVerticalScrollBarPolicy(22);
        jPanel.add((Component)jScrollPane, new GridConstraints(0, 0, 1, 6, 0, 3, 7, 7, null, null, null));
        this.resourceTable = jTable = new JTable();
        jTable.setAutoResizeMode(1);
        jScrollPane.setViewportView(jTable);
        Spacer spacer = new Spacer();
        jPanel.add((Component)spacer, new GridConstraints(1, 5, 1, 1, 0, 2, 1, 6, null, null, null));
        this.addResourceButton = jButton = new JButton();
        jButton.setText("Add Single Resource");
        jPanel.add((Component)jButton, new GridConstraints(1, 1, 1, 1, 0, 1, 3, 0, null, null, null));
        this.importResourcesButton = jButton3 = new JButton();
        jButton3.setText("Import Resources From File");
        jPanel.add((Component)jButton3, new GridConstraints(1, 3, 1, 1, 0, 1, 3, 0, null, null, null));
        Spacer spacer2 = new Spacer();
        jPanel.add((Component)spacer2, new GridConstraints(1, 4, 1, 1, 0, 1, 6, 1, null, null, null));
        Spacer spacer3 = new Spacer();
        jPanel.add((Component)spacer3, new GridConstraints(1, 0, 1, 1, 0, 1, 6, 1, null, null, null));
        this.attachAVRPResouceButton = jButton2 = new JButton();
        jButton2.setText("Attach a VRP Resouce Sheet");
        jPanel.add((Component)jButton2, new GridConstraints(1, 2, 1, 1, 0, 1, 3, 0, null, null, null));
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.mainPanel;
    }
}

