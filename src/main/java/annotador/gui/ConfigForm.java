/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.action.config.SelectLanguageAction;
import annotador.action.config.ShowCustomFontChooser;
import annotador.gui.MainForm;
import annotador.utils.Internationalization;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class ConfigForm {
    private JButton selectFontButton;
    private JCheckBox allowDeleteTaxonomyCheckBox;
    private JCheckBox allowInsertTaxonomyCheckBox;
    private JSlider colorAlphaSlider;
    private JCheckBox showSintaxHighlightCheckBox;
    private JCheckBox showGridCheckBox;
    private JSlider scrollSensibility;
    private JPanel panelPrincipal;
    ShowCustomFontChooser fontChooser;
    private JButton saveChangesButtonButton;
    private JCheckBox automaticImportCheckBox;
    private JSlider interRowSpace;
    private JLabel scrollSensibilityLabel;
    private JLabel colorAlphaLabel;
    private JLabel pixelInterRowLabel;
    private JPanel savePanel;
    private JPanel xmlPanel;
    private JPanel behaviourPane;
    private JPanel fontPanel;
    private JButton languageButton;
    private JPanel languagePanel;
    private JTextField vrlURL;
    private JTextField vrpUser;
    private JPasswordField vrpPass;
    private JTextField CMRUrl;
    private JTextField CMTUser;
    private JPasswordField CMTPass;
    private JCheckBox appliedCategoriesShownByCheckBox;
    public JDialog dialog;
    MainForm mainForm;

    public JCheckBox getAppliedCategoriesShownByCheckBox() {
        return this.appliedCategoriesShownByCheckBox;
    }

    public void setAppliedCategoriesShownByCheckBox(JCheckBox appliedCategoriesShownByCheckBox) {
        this.appliedCategoriesShownByCheckBox = appliedCategoriesShownByCheckBox;
    }

    public JPasswordField getVrpPass() {
        return this.vrpPass;
    }

    public void setVrpPass(JPasswordField vrpPass) {
        this.vrpPass = vrpPass;
    }

    public JTextField getVrpUser() {
        return this.vrpUser;
    }

    public void setVrpUser(JTextField vrpUser) {
        this.vrpUser = vrpUser;
    }

    public JTextField getVrlURL() {
        return this.vrlURL;
    }

    public void setVrlURL(JTextField vrlURL) {
        this.vrlURL = vrlURL;
    }

    public JCheckBox getAutomaticImportCheckBox() {
        return this.automaticImportCheckBox;
    }

    public void setAutomaticImportCheckBox(JCheckBox automaticImportCheckBox) {
        this.automaticImportCheckBox = automaticImportCheckBox;
    }

    public JButton getSelectFontButton() {
        return this.selectFontButton;
    }

    public void setSelectFontButton(JButton selectFontButton) {
        this.selectFontButton = selectFontButton;
    }

    public JCheckBox getAllowDeleteTaxonomyCheckBox() {
        return this.allowDeleteTaxonomyCheckBox;
    }

    public void setAllowDeleteTaxonomyCheckBox(JCheckBox allowDeleteTaxonomyCheckBox) {
        this.allowDeleteTaxonomyCheckBox = allowDeleteTaxonomyCheckBox;
    }

    public JCheckBox getAllowInsertTaxonomyCheckBox() {
        return this.allowInsertTaxonomyCheckBox;
    }

    public void setAllowInsertTaxonomyCheckBox(JCheckBox allowInsertTaxonomyCheckBox) {
        this.allowInsertTaxonomyCheckBox = allowInsertTaxonomyCheckBox;
    }

    public JSlider getColorAlphaSlider() {
        return this.colorAlphaSlider;
    }

    public void setColorAlphaSlider(JSlider colorAlphaSlider) {
        this.colorAlphaSlider = colorAlphaSlider;
    }

    public JCheckBox getShowSintaxHighlightCheckBox() {
        return this.showSintaxHighlightCheckBox;
    }

    public void setShowSintaxHighlightCheckBox(JCheckBox showSintaxHighlightCheckBox) {
        this.showSintaxHighlightCheckBox = showSintaxHighlightCheckBox;
    }

    public JCheckBox getShowGridCheckBox() {
        return this.showGridCheckBox;
    }

    public void setShowGridCheckBox(JCheckBox showGridCheckBox) {
        this.showGridCheckBox = showGridCheckBox;
    }

    public JSlider getScrollSensibility() {
        return this.scrollSensibility;
    }

    public void setScrollSensibility(JSlider scrollSensibility) {
        this.scrollSensibility = scrollSensibility;
    }

    public JPanel getPanelPrincipal() {
        return this.panelPrincipal;
    }

    public void setPanelPrincipal(JPanel panelPrincipal) {
        this.panelPrincipal = panelPrincipal;
    }

    public JButton getSaveChangesButton() {
        return this.saveChangesButtonButton;
    }

    public void setSaveChangesButton(JButton saveChangesButton) {
        this.saveChangesButtonButton = saveChangesButton;
    }

    public JDialog getDialog() {
        return this.dialog;
    }

    public void setDialog(JDialog dialog) {
        this.dialog = dialog;
    }

    public MainForm getMainForm() {
        return this.mainForm;
    }

    public void setMainForm(MainForm mainForm) {
        this.mainForm = mainForm;
    }

    public JSlider getInterRowSpace() {
        return this.interRowSpace;
    }

    public void setInterRowSpace(JSlider interRowSpace) {
        this.interRowSpace = interRowSpace;
    }

    public JTextField getCMRUrl() {
        return this.CMRUrl;
    }

    public void setCMRUrl(JTextField CMRUrl) {
        this.CMRUrl = CMRUrl;
    }

    public JTextField getCMTUser() {
        return this.CMTUser;
    }

    public void setCMTUser(JTextField CMTUser) {
        this.CMTUser = CMTUser;
    }

    public JPasswordField getCMTPass() {
        return this.CMTPass;
    }

    public void setCMTPass(JPasswordField CMTPass) {
        this.CMTPass = CMTPass;
    }

    public ConfigForm(MainForm mainForm) {
        this.mainForm = mainForm;
        this.$$$setupUI$$$();
        this.dialog = new JDialog(mainForm.getVentanaPrincipal(), Internationalization.CONFIG_TITLE, true);
        this.fontChooser = new ShowCustomFontChooser(mainForm, mainForm.getTabbedPane());
        this.allowDeleteTaxonomyCheckBox.setText(Internationalization.ALLOW_DELETE_TAXONOMY);
        this.allowInsertTaxonomyCheckBox.setText(Internationalization.ALLOW_INSERT_TAXONOMY);
        this.showGridCheckBox.setText(Internationalization.SHOW_GRID);
        this.showSintaxHighlightCheckBox.setText(Internationalization.SHOW_SINTAX_HIGHLIGHING);
        this.automaticImportCheckBox.setText(Internationalization.AUTOMATIC_IMPORT);
        this.scrollSensibilityLabel.setText(Internationalization.SCROLL_SENSIBILITY);
        this.colorAlphaLabel.setText(Internationalization.COLOR_ALPHA);
        this.pixelInterRowLabel.setText(Internationalization.PIXEL_INTERROW_SPACE);
        this.behaviourPane.setBorder(new TitledBorder(Internationalization.BEHAVIOUR_PANE_TITLE));
        this.savePanel.setBorder(new TitledBorder(Internationalization.SAVE_WORD));
        this.xmlPanel.setBorder(new TitledBorder(Internationalization.XML_PANE_TITLE));
        this.fontPanel.setBorder(new TitledBorder(Internationalization.FONT_CHOOSER_TITLE));
        this.selectFontButton.setText(Internationalization.FONT_CHOOSER);
        this.saveChangesButtonButton.setText(Internationalization.SAVE_CHANGES);
        this.languageButton.setText(Internationalization.LANGUAGUE_CHOOSER);
        this.languagePanel.setBorder(new TitledBorder(Internationalization.LANGUAGE_PANEL_TITLE));
        this.getSelectFontButton().addActionListener(this.fontChooser);
        this.languageButton.addActionListener(new SelectLanguageAction(this.mainForm));
        this.dialog.setContentPane(this.panelPrincipal);
    }

    public ShowCustomFontChooser getFontChooser() {
        return this.fontChooser;
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JPanel jPanel;
        JSlider jSlider;
        JSlider jSlider2;
        JButton jButton;
        JPasswordField jPasswordField;
        JPanel jPanel2;
        JPanel jPanel3;
        JPanel jPanel4;
        JLabel jLabel;
        JPanel jPanel5;
        JLabel jLabel2;
        JButton jButton2;
        JSlider jSlider3;
        JCheckBox jCheckBox;
        JCheckBox jCheckBox2;
        JCheckBox jCheckBox3;
        JCheckBox jCheckBox4;
        JPasswordField jPasswordField2;
        JTextField jTextField;
        JCheckBox jCheckBox5;
        JTextField jTextField2;
        JTextField jTextField3;
        JLabel jLabel3;
        JButton jButton3;
        JTextField jTextField4;
        JCheckBox jCheckBox6;
        JPanel jPanel6;
        this.panelPrincipal = jPanel5 = new JPanel();
        jPanel5.setLayout(new FormLayout("fill:d:noGrow,fill:d:noGrow,left:d:noGrow,fill:d:noGrow,left:d:noGrow,fill:d:noGrow,fill:m:noGrow,left:p:noGrow,fill:d:noGrow,fill:226px:noGrow", "top:p:noGrow,top:4dlu:noGrow,fill:d:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:47dlu:noGrow,top:50px:noGrow,top:d:noGrow,center:max(d;4px):noGrow,top:d:noGrow,center:103px:noGrow,top:4dlu:noGrow,top:110px:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow"));
        jPanel5.setBorder(BorderFactory.createTitledBorder(null, "", 0, 0, null, null));
        this.fontPanel = jPanel6 = new JPanel();
        jPanel6.setLayout(new FormLayout("fill:max(d;4px):noGrow,left:4dlu:noGrow,fill:d:grow", "center:d:grow"));
        jPanel5.add((Component)jPanel6, new CellConstraints(1, 1, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        jPanel6.setBorder(BorderFactory.createTitledBorder(null, "Font Config", 0, 0, null, null));
        this.selectFontButton = jButton3 = new JButton();
        jButton3.setText("Select Font");
        jPanel6.add((Component)jButton3, new CellConstraints(1, 1, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.behaviourPane = jPanel2 = new JPanel();
        jPanel2.setLayout(new FormLayout("fill:d:grow,left:4dlu:noGrow,fill:d:grow", "fill:d:noGrow,top:4dlu:noGrow,fill:d:noGrow,top:4dlu:noGrow,fill:d:noGrow,top:4dlu:noGrow,fill:d:noGrow,top:4dlu:noGrow,fill:35px:noGrow,top:4dlu:noGrow,fill:41px:noGrow,top:4dlu:noGrow,fill:35px:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow"));
        jPanel5.add((Component)jPanel2, new CellConstraints(1, 3, 1, 11, CellConstraints.FILL, CellConstraints.FILL, new Insets(0, 0, 0, 0)));
        jPanel2.setBorder(BorderFactory.createTitledBorder(null, "Application Behavior", 0, 0, null, null));
        this.allowDeleteTaxonomyCheckBox = jCheckBox6 = new JCheckBox();
        jCheckBox6.setText("Allow Delete Taxonomy");
        jPanel2.add((Component)jCheckBox6, new CellConstraints(1, 1, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.allowInsertTaxonomyCheckBox = jCheckBox = new JCheckBox();
        jCheckBox.setText("Allow Insert Taxonomy");
        jPanel2.add((Component)jCheckBox, new CellConstraints(1, 3, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.colorAlphaLabel = jLabel3 = new JLabel();
        jLabel3.setText("Color Alpha");
        jPanel2.add((Component)jLabel3, new CellConstraints(3, 11, 1, 1, CellConstraints.LEFT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.colorAlphaSlider = jSlider2 = new JSlider();
        jPanel2.add((Component)jSlider2, new CellConstraints(1, 11, 1, 1, CellConstraints.FILL, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.showGridCheckBox = jCheckBox3 = new JCheckBox();
        jCheckBox3.setText("Show Grid");
        jPanel2.add((Component)jCheckBox3, new CellConstraints(1, 5, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.scrollSensibility = jSlider3 = new JSlider();
        jPanel2.add((Component)jSlider3, new CellConstraints(1, 9, 1, 1, CellConstraints.FILL, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.scrollSensibilityLabel = jLabel = new JLabel();
        jLabel.setText("Scroll Sensibility");
        jPanel2.add((Component)jLabel, new CellConstraints(3, 9, 1, 1, CellConstraints.LEFT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.automaticImportCheckBox = jCheckBox4 = new JCheckBox();
        jCheckBox4.setText("Automatic Import");
        jPanel2.add((Component)jCheckBox4, new CellConstraints(1, 7, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.interRowSpace = jSlider = new JSlider();
        jPanel2.add((Component)jSlider, new CellConstraints(1, 13, 1, 1, CellConstraints.FILL, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.pixelInterRowLabel = jLabel2 = new JLabel();
        jLabel2.setText("Pixel InterRow Space");
        jPanel2.add((Component)jLabel2, new CellConstraints(3, 13, 1, 1, CellConstraints.LEFT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.appliedCategoriesShownByCheckBox = jCheckBox2 = new JCheckBox();
        jCheckBox2.setText("Applied Categories Shown by default");
        jPanel2.add((Component)jCheckBox2, new CellConstraints(1, 15, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.xmlPanel = jPanel = new JPanel();
        jPanel.setLayout(new FormLayout("fill:d:grow", "top:55px:grow,top:4dlu:noGrow,center:d:grow"));
        jPanel5.add((Component)jPanel, new CellConstraints(9, 2, 2, 4, CellConstraints.DEFAULT, CellConstraints.TOP, new Insets(0, 0, 0, 0)));
        jPanel.setBorder(BorderFactory.createTitledBorder(null, "XML View Config", 0, 0, null, null));
        this.showSintaxHighlightCheckBox = jCheckBox5 = new JCheckBox();
        jCheckBox5.setText("Show Sintax Highlight");
        jPanel.add((Component)jCheckBox5, new CellConstraints(1, 1, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.savePanel = jPanel4 = new JPanel();
        jPanel4.setLayout(new FormLayout("fill:d:noGrow,left:4dlu:noGrow,fill:d:grow", "center:d:grow"));
        jPanel5.add((Component)jPanel4, new CellConstraints(10, 1, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        jPanel4.setBorder(BorderFactory.createTitledBorder(null, "Save", 0, 0, null, null));
        this.saveChangesButtonButton = jButton = new JButton();
        jButton.setText("Save Changes");
        jPanel4.add((Component)jButton, new CellConstraints(1, 1, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        Spacer spacer = new Spacer();
        jPanel4.add((Component)spacer, new CellConstraints(3, 1, 1, 1, CellConstraints.FILL, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.languagePanel = jPanel3 = new JPanel();
        jPanel3.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel5.add((Component)jPanel3, new CellConstraints(10, 7, 1, 3, CellConstraints.DEFAULT, CellConstraints.TOP, new Insets(0, 0, 0, 0)));
        jPanel3.setBorder(BorderFactory.createTitledBorder(null, "Language Selection", 0, 0, null, null));
        this.languageButton = jButton2 = new JButton();
        jButton2.setText("Language");
        jPanel3.add((Component)jButton2, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        Spacer spacer2 = new Spacer();
        jPanel3.add((Component)spacer2, new GridConstraints(0, 1, 1, 1, 0, 1, 6, 1, null, null, null));
        JPanel jPanel7 = new JPanel();
        jPanel7.setLayout(new GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel5.add((Component)jPanel7, new CellConstraints(10, 11, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        jPanel7.setBorder(BorderFactory.createTitledBorder(null, "VRP Config", 0, 0, null, null));
        this.vrlURL = jTextField3 = new JTextField();
        jPanel7.add((Component)jTextField3, new GridConstraints(0, 0, 1, 1, 8, 1, 6, 0, null, new Dimension(150, 21), null));
        JLabel jLabel4 = new JLabel();
        jLabel4.setText("VRP Url");
        jPanel7.add((Component)jLabel4, new GridConstraints(0, 1, 1, 1, 8, 0, 6, 0, null, new Dimension(39, 21), null));
        this.vrpUser = jTextField4 = new JTextField();
        jPanel7.add((Component)jTextField4, new GridConstraints(1, 0, 1, 1, 8, 1, 6, 0, null, new Dimension(150, -1), null));
        JLabel jLabel5 = new JLabel();
        jLabel5.setText("User");
        jPanel7.add((Component)jLabel5, new GridConstraints(1, 1, 1, 1, 8, 0, 6, 0, null, new Dimension(39, 21), null));
        JLabel jLabel6 = new JLabel();
        jLabel6.setText("Pass");
        jPanel7.add((Component)jLabel6, new GridConstraints(2, 1, 1, 1, 8, 0, 6, 0, null, new Dimension(39, 21), null));
        this.vrpPass = jPasswordField2 = new JPasswordField();
        jPanel7.add((Component)jPasswordField2, new GridConstraints(2, 0, 1, 1, 8, 1, 6, 0, null, new Dimension(150, -1), null));
        JPanel jPanel8 = new JPanel();
        jPanel8.setLayout(new FormLayout("fill:d:noGrow,left:4dlu:noGrow,fill:max(d;4px):noGrow", "center:d:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow"));
        jPanel5.add((Component)jPanel8, new CellConstraints(10, 13, 1, 2, CellConstraints.DEFAULT, CellConstraints.TOP, new Insets(0, 0, 0, 0)));
        jPanel8.setBorder(BorderFactory.createTitledBorder(null, "CMT Config", 0, 0, null, null));
        this.CMRUrl = jTextField2 = new JTextField();
        jTextField2.setColumns(20);
        jPanel8.add((Component)jTextField2, new CellConstraints(1, 1, 1, 1, CellConstraints.FILL, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.CMTUser = jTextField = new JTextField();
        jTextField.setColumns(20);
        jPanel8.add((Component)jTextField, new CellConstraints(1, 3, 1, 1, CellConstraints.FILL, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.CMTPass = jPasswordField = new JPasswordField();
        jPasswordField.setColumns(20);
        jPanel8.add((Component)jPasswordField, new CellConstraints(1, 5, 1, 1, CellConstraints.FILL, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        JLabel jLabel7 = new JLabel();
        jLabel7.setText("Pass");
        jPanel8.add((Component)jLabel7, new CellConstraints(3, 5, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        JLabel jLabel8 = new JLabel();
        jLabel8.setText("User");
        jPanel8.add((Component)jLabel8, new CellConstraints(3, 3, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        JLabel jLabel9 = new JLabel();
        jLabel9.setText("CMT Url");
        jPanel8.add((Component)jLabel9, new CellConstraints(3, 1, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.panelPrincipal;
    }
}

