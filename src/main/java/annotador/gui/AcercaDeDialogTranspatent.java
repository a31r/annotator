/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.gui.MainForm;
import annotador.gui.TransparentBackground;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class AcercaDeDialogTranspatent {
    public AcercaDeDialogTranspatent(Component comp, MainForm mainForm) {
        final JDialog jdAcercaDe = new JDialog();
        jdAcercaDe.setUndecorated(false);
        jdAcercaDe.setResizable(true);
        jdAcercaDe.setModal(true);
        TransparentBackground bg = new TransparentBackground(jdAcercaDe);
        bg.setBorder(BorderFactory.createBevelBorder(0));
        BorderLayout b = new BorderLayout();
        bg.setLayout(b);
        JPanel jpNorte = new JPanel(new GridLayout(25, 1));
        JPanel jpCentro = new JPanel();
        JPanel jpSur = new JPanel(new FlowLayout());
        ImageIcon iiImagen = new ImageIcon(Utils.getReourceImage(mainForm, Constants.MENU_ABOUT));
        JLabel jlImagen = new JLabel(iiImagen, 0);
        JButton jbOk = new JButton(Internationalization.CLOSE_WORD);
        jbOk.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                jdAcercaDe.setVisible(false);
            }
        });
        jpNorte.setOpaque(false);
        jpNorte.add(new JLabel(""));
        jpNorte.add(new JLabel("BACKBONE Annotator 3.8", 0));
        jpNorte.add(new JLabel(""));
        jpNorte.add(new JLabel("This freeware application developed for its use on BACKBONE: ", 0));
        jpNorte.add(new JLabel("Corpora for Content & Language Integrated Learning", 0));
        jpNorte.add(new JLabel("", 0));
        jpNorte.add(new JLabel(""));
        jpNorte.add(new JLabel("Jos\u00e9 Mar\u00eda Alcaraz: BACKBONE IT Manager", 0));
        jpNorte.add(new JLabel("Email: jmalcaraz@gmail.com", 0));
        jpNorte.add(new JLabel(""));
        jpNorte.add(new JLabel("For more information visit http://www.um.es/backbone", 0));
        jpNorte.add(new JLabel("or e-mail us: @um.es", 0));
        jpNorte.add(new JLabel(""));
        jpNorte.add(new JLabel("This program is free software; you can redistribute it and/or modify", 0));
        jpNorte.add(new JLabel("it under the terms of the GNU General Public License as published by", 0));
        jpNorte.add(new JLabel("the Free Software Foundation; either version 3 of the License, or", 0));
        jpNorte.add(new JLabel("(at your option) any later version.", 0));
        jpNorte.add(new JLabel(""));
        jpNorte.add(new JLabel("This program is distributed in the hope that it will be useful,", 0));
        jpNorte.add(new JLabel("but WITHOUT ANY WARRANTY; without even the implied warranty of", 0));
        jpNorte.add(new JLabel("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the", 0));
        jpNorte.add(new JLabel("GNU General Public License for more details.", 0));
        jpNorte.add(new JLabel(""));
        jpNorte.add(new JLabel("You should have received a copy of the GNU General Public License", 0));
        jpNorte.add(new JLabel("along with this program.  If not, see <http://www.gnu.org/licenses/>.", 0));
        jpCentro.setOpaque(false);
        jpCentro.add(jlImagen);
        jpSur.setOpaque(false);
        jpSur.add(new JLabel(""));
        jpSur.add(jbOk);
        bg.add((Component)jpNorte, "North");
        bg.add((Component)jpCentro, "Center");
        bg.add((Component)jpSur, "South");
        jdAcercaDe.getContentPane().add(bg);
        jdAcercaDe.setTitle(Internationalization.ABOUT_TITLE);
        Point p = jdAcercaDe.getLocation();
        jdAcercaDe.setSize(550, 650);
        jdAcercaDe.setLocationRelativeTo(comp);
        jdAcercaDe.setResizable(true);
        jdAcercaDe.setVisible(true);
    }

}

