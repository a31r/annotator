/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.action.interview.CloseInterviewAction;
import annotador.action.interview.DownAction;
import annotador.action.interview.UpAction;
import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.PrintStream;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SelectDocument
extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox documentComboBox;
    private JLabel iconLabel;
    private JLabel selectDocumentLabel;
    private JButton upButton;
    private JButton downButton;
    private JPanel reorderPane;
    MainForm mainForm;

    public JLabel getIconLabel() {
        return this.iconLabel;
    }

    public JComboBox getDocumentComboBox() {
        return this.documentComboBox;
    }

    public void setDocumentComboBox(JComboBox documentComboBox) {
        this.documentComboBox = documentComboBox;
    }

    public SelectDocument(MainForm mainForm) {
        this.mainForm = mainForm;
        this.$$$setupUI$$$();
        this.setContentPane(this.contentPane);
        this.setModal(true);
        this.getRootPane().setDefaultButton(this.buttonOK);
        this.setTitle(Internationalization.SELECT_INTERVIEW_TITLE_2);
        this.getIconLabel().setIcon(new ImageIcon(Utils.getReourceImage(mainForm, Constants.MENU_ICON_CORPUS_SELECT)));
        this.buttonCancel.setText(Internationalization.CANCEL_WORD);
        this.buttonOK.setText(Internationalization.OK_WORD);
        this.reorderPane.setBorder(BorderFactory.createTitledBorder(Internationalization.REORDER_PANE));
        this.selectDocumentLabel.setText(Internationalization.SELECT_DOCUMENT);
        TitledBorder t = new TitledBorder(Internationalization.SELECT_DOCUMENT_TITLE);
        this.contentPane.setBorder(t);
        this.upButton.addActionListener(new UpAction(mainForm, this.documentComboBox));
        this.upButton.setEnabled(false);
        this.upButton.setIcon(new ImageIcon(Utils.getReourceImage(mainForm, Constants.SELECT_DOCUMENT_UP_ICON)));
        this.downButton.addActionListener(new DownAction(mainForm, this.documentComboBox));
        this.downButton.setEnabled(false);
        this.downButton.setIcon(new ImageIcon(Utils.getReourceImage(mainForm, Constants.SELECT_DOCUMENT_DOWN_ICON)));
        this.buttonOK.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                SelectDocument.this.onOK();
            }
        });
        this.buttonCancel.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                SelectDocument.this.onCancel();
            }
        });
        this.documentComboBox.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (Constants.IS_COLLABORATIVE_CORPUS != null) {
                    SelectDocument.this.upButton.setEnabled(false);
                    SelectDocument.this.downButton.setEnabled(false);
                } else {
                    int index = SelectDocument.this.documentComboBox.getSelectedIndex();
                    if (index > 0) {
                        SelectDocument.this.upButton.setEnabled(true);
                    } else {
                        SelectDocument.this.upButton.setEnabled(false);
                    }
                    if (index == SelectDocument.this.documentComboBox.getItemCount() - 1) {
                        SelectDocument.this.downButton.setEnabled(false);
                    } else {
                        SelectDocument.this.downButton.setEnabled(true);
                    }
                }
            }
        });
        this.setDefaultCloseOperation(0);
        this.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosing(WindowEvent e) {
                SelectDocument.this.onCancel();
            }
        });
        this.contentPane.registerKeyboardAction(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                SelectDocument.this.onCancel();
            }
        }, KeyStroke.getKeyStroke(27, 0), 1);
    }

    private void onOK() {
        if (this.documentComboBox.getSelectedIndex() != -1) {
            String a = (String)this.documentComboBox.getSelectedItem();
            this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(3));
            this.setCursor(Cursor.getPredefinedCursor(3));
            if (Constants.IS_COLLABORATIVE_CORPUS == null) {
                int index = Integer.parseInt(a.substring(0, a.indexOf(32)));
                Constants.DOCUMENT_OPENED = String.valueOf(index);
                Utils.changeInterviewEvent(this.mainForm, index);
            } else {
                try {
                    CloseInterviewAction cia = new CloseInterviewAction(this.mainForm);
                    cia.actionPerformed(new ActionEvent(this, 1, "selectDocument"));
                    String interview = CMTManager.getCMT().lockDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, a);
                    Document newInt = DOMUtils.StringToDOM(interview, Constants.ENCODING, Constants.TEI_NAMESPACE);
                    NodeList nl = DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI");
                    for (int i = 0; i < nl.getLength(); ++i) {
                        System.out.println("REMOVING TEI TAGS.....");
                        Node n = nl.item(i);
                        DocumentSingleton.getInstance().getDocumentElement().removeChild(n);
                    }
                    DocumentSingleton.getInstance().getDocumentElement().appendChild(DocumentSingleton.getInstance().importNode(newInt.getDocumentElement(), true));
                    Constants.DOCUMENT_OPENED = a;
                    Utils.changeInterviewEvent(this.mainForm, 1);
                }
                catch (Exception e) {
                    JOptionPane.showMessageDialog(this.mainForm.getMainPanel(), e.getMessage());
                }
            }
            this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(0));
            this.setCursor(Cursor.getPredefinedCursor(0));
        }
        this.dispose();
    }

    private void onCancel() {
        this.dispose();
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JButton jButton;
        JPanel jPanel;
        JLabel jLabel;
        JComboBox jComboBox;
        JPanel jPanel2;
        JButton jButton2;
        JButton jButton3;
        JLabel jLabel2;
        JButton jButton4;
        this.contentPane = jPanel2 = new JPanel();
        jPanel2.setLayout(new BorderLayout(0, 0));
        jPanel2.setBorder(BorderFactory.createTitledBorder(null, "Select Document", 0, 0, null, null));
        JPanel jPanel3 = new JPanel();
        jPanel3.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel2.add((Component)jPanel3, "South");
        this.buttonOK = jButton2 = new JButton();
        jButton2.setText("OK");
        jPanel3.add((Component)jButton2, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.buttonCancel = jButton = new JButton();
        jButton.setText("Cancel");
        jPanel3.add((Component)jButton, new GridConstraints(0, 1, 1, 1, 0, 1, 3, 0, null, null, null));
        JPanel jPanel4 = new JPanel();
        jPanel4.setLayout(new GridLayoutManager(3, 4, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel2.add((Component)jPanel4, "Center");
        this.selectDocumentLabel = jLabel = new JLabel();
        jLabel.setHorizontalAlignment(11);
        jLabel.setText("Select one of following documents");
        jPanel4.add((Component)jLabel, new GridConstraints(0, 0, 1, 1, 2, 0, 2, 0, null, null, null));
        this.documentComboBox = jComboBox = new JComboBox();
        jPanel4.add(jComboBox, new GridConstraints(1, 0, 1, 1, 1, 1, 2, 0, new Dimension(80, -1), null, null));
        this.iconLabel = jLabel2 = new JLabel();
        jLabel2.setText("");
        jPanel4.add((Component)jLabel2, new GridConstraints(0, 1, 2, 1, 8, 0, 0, 0, null, null, null));
        JPanel jPanel5 = new JPanel();
        jPanel5.setLayout(new BorderLayout(0, 0));
        jPanel4.add((Component)jPanel5, new GridConstraints(0, 3, 2, 1, 0, 3, 3, 3, null, null, null));
        Spacer spacer = new Spacer();
        jPanel4.add((Component)spacer, new GridConstraints(2, 0, 1, 1, 0, 2, 1, 2, new Dimension(-1, 20), null, null));
        this.reorderPane = jPanel = new JPanel();
        jPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel4.add((Component)jPanel, new GridConstraints(0, 2, 2, 1, 0, 3, 3, 3, null, null, null));
        this.upButton = jButton4 = new JButton();
        jButton4.setText("Up");
        jPanel.add((Component)jButton4, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.downButton = jButton3 = new JButton();
        jButton3.setText("Down");
        jPanel.add((Component)jButton3, new GridConstraints(1, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        jLabel.setLabelFor(jComboBox);
        jLabel2.setLabelFor(jComboBox);
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.contentPane;
    }

}

