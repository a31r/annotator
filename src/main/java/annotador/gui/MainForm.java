/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.action.cmt.CorpusVisibility;
import annotador.action.cmt.LockTaxonomyTree;
import annotador.action.cmt.OpenCollaborativeCorpus;
import annotador.action.cmt.RegisterCurrentCorpusToCollaborative;
import annotador.action.cmt.RemoveCollaborativeCorpus;
import annotador.action.cmt.UnlockTaxonomyTree;
import annotador.action.config.DefaultTaxonomyTree;
import annotador.action.config.ShowConfigForm;
import annotador.action.corpus.ChangeCorpusFile;
import annotador.action.corpus.CloseCorpusFile;
import annotador.action.corpus.NewCorpusFile;
import annotador.action.corpus.SaveAsCorpusFile;
import annotador.action.corpus.SaveCorpusFile;
import annotador.action.help.AboutAction;
import annotador.action.help.HelpAction;
import annotador.action.interview.AddInterviewAction;
import annotador.action.interview.CloseInterviewAction;
import annotador.action.interview.DeleteInterviewAction;
import annotador.action.interview.SelectInterviewAction;
import annotador.action.metadata.AddKeyWordAction;
import annotador.action.metadata.AddPersonAction;
import annotador.action.metadata.DeleteKeyWordAction;
import annotador.action.metadata.DeletePersonAction;
import annotador.action.metadata.ShowPersonInformationAction;
import annotador.action.taxonomy.AddTaxonomy;
import annotador.action.taxonomy.DeleteTaxonomy;
import annotador.action.taxonomy.DownTaxonomy;
import annotador.action.taxonomy.UpTaxonomy;
import annotador.action.xml.CommitXMLAction;
import annotador.action.xml.DiscartXMLAction;
import annotador.action.xml.EditXMLAction;
import annotador.action.xml.XMLViewComponentListener;
import annotador.cmt.CMTManager;
import annotador.metadata.KeywordRenderer;
import annotador.resources.PtrNodeCellRenderer;
import annotador.taxonomy.TEIJTree;
import annotador.taxonomy.cellrender.TaxonomyMouseListener;
import annotador.text.TEIJTable;
import annotador.text.TEITableComponentListener;
import annotador.text.TableMouseWhell;
import annotador.utils.ColorMap;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.ShowedCategory;
import annotador.utils.Utils;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Properties;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import org.w3c.dom.Node;

public class MainForm
extends WindowAdapter
implements Runnable {
    private JTable documentTable;
    private JPanel mainPanel;
    private JScrollPane documentScrollTable;
    private JTree taxonomyTree;
    private JScrollPane taxonomyScrollTree;
    private JSplitPane mainSplitPane;
    private JToolBar menuToolBar;
    private JToolBar statusToolBar;
    private JToolBar taxonomyToolBar;
    private JTextPane xmlPane;
    private JPanel xmlViewPanel;
    private JPanel logicalViewPanel;
    private JPanel xmlUpPanel;
    private JScrollPane xmlScrollPane;
    private JTextField titleField;
    private JTextField editorField;
    private JComboBox personBox;
    private JButton viewInfoButton;
    private JButton deletePersonButton;
    private JTable resourcesTable;
    private JButton addResourceButton;
    private JButton importResourcesFromFileButton;
    private JButton applyChangesButton;
    private JPanel metadataPanel;
    private JTextField dateRecordingField;
    private JTextField dateTranscriptionField;
    private JTextField localeField;
    private JTextField principalInvestigatorField;
    private JTextField researcherField;
    private JTextField transcriberField;
    private JTextArea descriptionField;
    private JTextField autorityField;
    private JTextField idField;
    private JTextField languageField;
    private JTextField mediaFileField;
    private JTabbedPane tabbedPane;
    private JLabel titleLabel;
    private JLabel dateRecordingLabel;
    private JLabel dateTranscriptionLabel;
    private JLabel localeLabel;
    private JPanel documentMetadataPanel;
    private JLabel researcherLabel;
    private JLabel transcriberLabel;
    private JLabel editorLabel;
    private JLabel descriptionLabel;
    private JLabel autorityLabel;
    private JLabel idLabel;
    private JLabel languagueLabel;
    private JLabel mediaLabel;
    private JPanel participanInformationPanel;
    private JPanel documentLinksPanel;
    private JLabel PrincipalInvestigatorLabe;
    private JPanel treePane;
    private JTextField activityField;
    private JButton addPersonButton;
    private JComboBox keyWordcomboBox;
    private JButton addKeywordButton;
    private JButton deleteKeywordButton;
    private JButton attachAVRPResourceButton;
    private JTextField languageVariety;
    private JButton upTaxonomy;
    private JButton downTaxonomy;
    private JButton discartXML;
    private JButton commitXML;
    private JButton editXML;
    JFrame ventanaPrincipal;
    String title;

    @Override
    public void run() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension scrnsize = toolkit.getScreenSize();
        if (scrnsize.getHeight() < 768.0) {
            JOptionPane.showMessageDialog(null, Internationalization.RESTRICTION_ERROR, Internationalization.WARNING_WORD, 2, new ImageIcon(Utils.getReourceImage(this, Constants.ICON_WARNING)));
        }
        this.ventanaPrincipal.setVisible(true);
    }

    public MainForm(String title) {
        this.title = title;
        this.$$$setupUI$$$();
        ChangeCorpusFile changeCorpusFileListener = new ChangeCorpusFile(this);
        SaveCorpusFile saveCorpusListener = new SaveCorpusFile(this, false);
        SaveAsCorpusFile saveAsCorpusListener = new SaveAsCorpusFile(this);
        CloseCorpusFile closeCorpusListener = new CloseCorpusFile(this);
        NewCorpusFile newCorpusListener = new NewCorpusFile(this);
        OpenCollaborativeCorpus openCollaborativeCorpusListener = new OpenCollaborativeCorpus(this);
        RegisterCurrentCorpusToCollaborative publisCollaborativeCorpusListener = new RegisterCurrentCorpusToCollaborative(this);
        RemoveCollaborativeCorpus removeCollaborativeCorpusListener = new RemoveCollaborativeCorpus(this);
        CorpusVisibility setVisibilityListener = new CorpusVisibility(this);
        SelectInterviewAction openInterviewListener = new SelectInterviewAction(this);
        DeleteInterviewAction deleteInterviewListener = new DeleteInterviewAction(this);
        AddInterviewAction addInterviewListener = new AddInterviewAction(this);
        CloseInterviewAction closeInterviewListener = new CloseInterviewAction(this);
        ShowConfigForm configListener = new ShowConfigForm(this);
        DefaultTaxonomyTree importTaxListener = new DefaultTaxonomyTree(this);
        AboutAction aboutListener = new AboutAction(this);
        HelpAction helpListener = new HelpAction(this);
        AddTaxonomy addTaxonomyListener = new AddTaxonomy(this);
        DeleteTaxonomy deleteTaxonomyListener = new DeleteTaxonomy(this);
        LockTaxonomyTree lockTaxonolyListener = new LockTaxonomyTree(this);
        UnlockTaxonomyTree unlockTaxonomyListener = new UnlockTaxonomyTree(this);
        XMLViewComponentListener xmlPaneListener = new XMLViewComponentListener(this);
        TEITableComponentListener logicalVieListener = new TEITableComponentListener(this.getDocumentTable());
        JMenuBar bar = new JMenuBar();
        JMenu corpus = new JMenu(Internationalization.MENU_CORPUS);
        JMenuItem selectCorpus = new JMenuItem(Internationalization.MENU_CORPUS_SELECT, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CORPUS_SELECT)));
        selectCorpus.setAccelerator(KeyStroke.getKeyStroke(79, 2));
        selectCorpus.addActionListener(changeCorpusFileListener);
        JMenuItem createNewCorpus = new JMenuItem(Internationalization.MENU_CORPUS_NEW, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CORPUS_NEW)));
        createNewCorpus.addActionListener(newCorpusListener);
        createNewCorpus.setAccelerator(KeyStroke.getKeyStroke(78, 2));
        JMenuItem saveCorpus = new JMenuItem(Internationalization.MENU_CORPUS_SAVE, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CORPUS_SAVE)));
        saveCorpus.setAccelerator(KeyStroke.getKeyStroke(83, 2));
        saveCorpus.addActionListener(saveCorpusListener);
        JMenuItem saveCorpusAs = new JMenuItem(Internationalization.MENU_CORPUS_SAVE_AS, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CORPUS_SAVE_AS)));
        saveCorpusAs.addActionListener(saveAsCorpusListener);
        saveCorpusAs.setAccelerator(KeyStroke.getKeyStroke(88, 2));
        JMenuItem closeCorpus = new JMenuItem(Internationalization.MENU_CORPUS_CLOSE, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CORPUS_CLOSE)));
        closeCorpus.addActionListener(closeCorpusListener);
        closeCorpus.setAccelerator(KeyStroke.getKeyStroke(67, 2));
        corpus.add(selectCorpus);
        corpus.add(createNewCorpus);
        corpus.add(saveCorpus);
        corpus.add(saveCorpusAs);
        corpus.add(closeCorpus);
        JMenu interview = new JMenu(Internationalization.MENU_INTERVIEW);
        JMenuItem selectInterview = new JMenuItem(Internationalization.MENU_INTERVIEW_OPEN, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_INTERVIEW_SELECT)));
        selectInterview.addActionListener(openInterviewListener);
        selectInterview.setAccelerator(KeyStroke.getKeyStroke(87, 2));
        JMenuItem addInterview = new JMenuItem(Internationalization.MENU_INTERVIEW_ADD, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_INTERVIEW_NEW)));
        addInterview.addActionListener(addInterviewListener);
        addInterview.setAccelerator(KeyStroke.getKeyStroke(65, 2));
        JMenuItem deleteInterview = new JMenuItem(Internationalization.MENU_INTERVIEW_DELETE, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_INTERVIEW_DELETE)));
        deleteInterview.addActionListener(deleteInterviewListener);
        deleteInterview.setAccelerator(KeyStroke.getKeyStroke(68, 2));
        JMenuItem closeInterview = new JMenuItem(Internationalization.MENU_INTERVIEW_CLOSE, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_INTERVIEW_CLOSE)));
        closeInterview.addActionListener(closeInterviewListener);
        interview.add(selectInterview);
        interview.add(addInterview);
        interview.add(deleteInterview);
        interview.add(closeInterview);
        JMenu config = new JMenu(Internationalization.MENU_CONFIG);
        JMenuItem configItem = new JMenuItem(Internationalization.MENU_CONFIG, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CONFIG)));
        configItem.addActionListener(configListener);
        configItem.setAccelerator(KeyStroke.getKeyStroke(84, 2));
        JMenuItem importItem = new JMenuItem(Internationalization.MENU_CONFIG_IMPORT, new ImageIcon(Utils.getReourceImage(this, Constants.ICON_IMPORT_TAX)));
        importItem.addActionListener(importTaxListener);
        importItem.setAccelerator(KeyStroke.getKeyStroke(73, 2));
        config.add(configItem);
        config.add(importItem);
        JMenu help = new JMenu(Internationalization.MENU_HELP);
        JMenuItem helpItem = new JMenuItem(Internationalization.MENU_HELP_HELP, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_HELP_ITEM)));
        helpItem.addActionListener(helpListener);
        helpItem.setAccelerator(KeyStroke.getKeyStroke(72, 2));
        JMenuItem helpAbout = new JMenuItem(Internationalization.MENU_HELP_ABOUT, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_HELP_ABOUT)));
        helpAbout.addActionListener(aboutListener);
        help.add(helpItem);
        help.add(helpAbout);
        JMenu collaboration = new JMenu(Internationalization.MENU_CMT);
        JMenuItem publish = new JMenuItem(Internationalization.MENU_CMT_PUBLISH, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CMT_PUBLISH)));
        publish.addActionListener(publisCollaborativeCorpusListener);
        JMenuItem retrieve = new JMenuItem(Internationalization.MENU_CMT_RETRIEVE, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CMT_RETRIEVE)));
        retrieve.addActionListener(openCollaborativeCorpusListener);
        JMenuItem remove = new JMenuItem(Internationalization.MENU_CMT_DELETE, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CMT_REMOVE)));
        remove.addActionListener(removeCollaborativeCorpusListener);
        JMenuItem lock = new JMenuItem(Internationalization.MENU_LOCK_TREE, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_LOCK_TREE)));
        lock.addActionListener(lockTaxonolyListener);
        JMenuItem unlock = new JMenuItem(Internationalization.MENU_UNLOCK_TREE, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_UNLOCK_TREE)));
        unlock.addActionListener(unlockTaxonomyListener);
        JMenuItem visibility = new JMenuItem(Internationalization.MENU_CORPUS_VISIBILITY, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CMT_VISIBILITY)));
        visibility.addActionListener(setVisibilityListener);
        collaboration.add(publish);
        collaboration.add(retrieve);
        collaboration.add(visibility);
        collaboration.add(remove);
        collaboration.addSeparator();
        collaboration.add(lock);
        collaboration.add(unlock);
        bar.add(corpus);
        bar.add(collaboration);
        bar.add(interview);
        bar.add(config);
        bar.add(help);
        this.ventanaPrincipal.setJMenuBar(bar);
        JButton addTaxonomy = new JButton(Internationalization.MENU_CATEGORY_ADD, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_TAXONOMY_NEW)));
        addTaxonomy.addActionListener(addTaxonomyListener);
        Font f = new Font(addTaxonomy.getFont().getName(), addTaxonomy.getFont().getStyle(), 8);
        addTaxonomy.setFont(f);
        JButton deleteTaxonomy = new JButton(Internationalization.MENU_CATEGORY_REMOVE, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_TAXONOMY_DELETE)));
        deleteTaxonomy.addActionListener(deleteTaxonomyListener);
        deleteTaxonomy.setFont(f);
        this.upTaxonomy = new JButton(Internationalization.MENU_CATEGORY_UP, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_TAXONOMY_UP)));
        this.upTaxonomy.setEnabled(false);
        this.upTaxonomy.addActionListener(new UpTaxonomy(this));
        this.upTaxonomy.setVerticalTextPosition(3);
        this.upTaxonomy.setHorizontalTextPosition(0);
        this.upTaxonomy.setFont(f);
        JButton lockTaxonomy = new JButton(Internationalization.MENU_LOCK_TREE, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_LOCK_TREE)));
        lockTaxonomy.setEnabled(true);
        lockTaxonomy.addActionListener(lockTaxonolyListener);
        lockTaxonomy.setVerticalTextPosition(3);
        lockTaxonomy.setHorizontalTextPosition(0);
        lockTaxonomy.setFont(f);
        JButton unlockTaxonomy = new JButton(Internationalization.MENU_UNLOCK_TREE, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_UNLOCK_TREE)));
        unlockTaxonomy.setEnabled(true);
        unlockTaxonomy.addActionListener(unlockTaxonomyListener);
        unlockTaxonomy.setVerticalTextPosition(3);
        unlockTaxonomy.setHorizontalTextPosition(0);
        unlockTaxonomy.setFont(f);
        this.downTaxonomy = new JButton(Internationalization.MENU_CATEGORY_DOWN, new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_TAXONOMY_DOWN)));
        this.downTaxonomy.setEnabled(false);
        this.downTaxonomy.addActionListener(new DownTaxonomy(this));
        this.downTaxonomy.setVerticalTextPosition(3);
        this.downTaxonomy.setHorizontalTextPosition(0);
        this.downTaxonomy.setFont(f);
        TitledBorder t = new TitledBorder(Internationalization.MENU_CATEGORY_TITLE);
        this.getTaxonomyToolBar().setBorder(BorderFactory.createTitledBorder(t));
        this.getTaxonomyToolBar().add(addTaxonomy);
        this.getTaxonomyToolBar().addSeparator(new Dimension(2, 5));
        this.getTaxonomyToolBar().add(deleteTaxonomy);
        this.getTaxonomyToolBar().addSeparator(new Dimension(2, 5));
        this.getTaxonomyToolBar().add(this.upTaxonomy);
        this.getTaxonomyToolBar().addSeparator(new Dimension(2, 5));
        this.getTaxonomyToolBar().add(this.downTaxonomy);
        this.getTaxonomyToolBar().addSeparator(new Dimension(2, 5));
        this.getTaxonomyToolBar().add(lockTaxonomy);
        this.getTaxonomyToolBar().addSeparator(new Dimension(2, 5));
        this.getTaxonomyToolBar().add(unlockTaxonomy);
        addTaxonomy.setVerticalTextPosition(3);
        addTaxonomy.setHorizontalTextPosition(0);
        deleteTaxonomy.setVerticalTextPosition(3);
        deleteTaxonomy.setHorizontalTextPosition(0);
        this.getTaxonomyTree().setModel(new DefaultTreeModel(null));
        this.getTaxonomyTree().addMouseListener(new TaxonomyMouseListener(this));
        JButton selectCorpusButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CORPUS_SELECT)));
        selectCorpusButton.addActionListener(changeCorpusFileListener);
        selectCorpusButton.setToolTipText(Internationalization.TIP_CORPUS_SELECT);
        JButton createCorpusButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CORPUS_NEW)));
        createCorpusButton.addActionListener(newCorpusListener);
        createCorpusButton.setToolTipText(Internationalization.TIP_CORPUS_NEW);
        JButton closeCorpusButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CORPUS_CLOSE)));
        closeCorpusButton.addActionListener(closeCorpusListener);
        closeCorpusButton.setToolTipText(Internationalization.TIP_CORPUS_CLOSE);
        JButton saveCorpusButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CORPUS_SAVE)));
        saveCorpusButton.addActionListener(saveCorpusListener);
        saveCorpusButton.setToolTipText(Internationalization.TIP_CORPUS_SAVE);
        JButton saveAsCorpusButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CORPUS_SAVE_AS)));
        saveAsCorpusButton.addActionListener(saveAsCorpusListener);
        saveAsCorpusButton.setToolTipText(Internationalization.TIP_CORPUS_SAVE_AS);
        JButton selectInterviewButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_INTERVIEW_SELECT)));
        selectInterviewButton.addActionListener(openInterviewListener);
        selectInterviewButton.setToolTipText(Internationalization.TIP_INTERVIEW_SELECT);
        JButton deleteInterviewButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_INTERVIEW_DELETE)));
        deleteInterviewButton.addActionListener(deleteInterviewListener);
        deleteInterviewButton.setToolTipText(Internationalization.TIP_INTERVIEW_DELETE);
        JButton newInterviewButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_INTERVIEW_NEW)));
        newInterviewButton.addActionListener(addInterviewListener);
        newInterviewButton.setToolTipText(Internationalization.TIP_INTERVIEW_ADD);
        JButton closeInterviewButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_INTERVIEW_CLOSE)));
        closeInterviewButton.addActionListener(closeInterviewListener);
        closeInterviewButton.setToolTipText(Internationalization.TIP_INTERVIEW_CLOSE);
        JButton addTaxonomyButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_TAXONOMY_NEW)));
        addTaxonomyButton.addActionListener(addTaxonomyListener);
        addTaxonomyButton.setToolTipText(Internationalization.TIP_CATEGORY_NEW);
        JButton deleteTaxonomyButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_TAXONOMY_DELETE)));
        deleteTaxonomyButton.addActionListener(deleteTaxonomyListener);
        deleteTaxonomyButton.setToolTipText(Internationalization.TIP_CATEGORY_DELETE);
        JButton publicButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CMT_PUBLISH)));
        publicButton.addActionListener(publisCollaborativeCorpusListener);
        publicButton.setToolTipText(Internationalization.TIP_PUBLIC_COLLABORATIVE);
        JButton visibilityButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CMT_VISIBILITY)));
        visibilityButton.addActionListener(setVisibilityListener);
        visibilityButton.setToolTipText(Internationalization.TIP_CORPUS_VISIBILITY);
        JButton retrieveColltButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CMT_RETRIEVE)));
        retrieveColltButton.addActionListener(openCollaborativeCorpusListener);
        retrieveColltButton.setToolTipText(Internationalization.TIP_RETRIEVE_COLLABORATIVE);
        JButton removeCollButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CMT_REMOVE)));
        removeCollButton.addActionListener(removeCollaborativeCorpusListener);
        removeCollButton.setToolTipText(Internationalization.TIP_REMOVE_COLLABORATIVE);
        JButton configButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_CONFIG)));
        configButton.addActionListener(configListener);
        configButton.setToolTipText(Internationalization.TIP_CONFIG);
        JButton importButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.ICON_IMPORT_TAX)));
        importButton.addActionListener(importTaxListener);
        importButton.setToolTipText(Internationalization.TIP_CONFIG_IMPORT);
        JButton helpButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_HELP_ITEM)));
        helpButton.addActionListener(helpListener);
        helpButton.setToolTipText(Internationalization.TIP_HELP_HELP);
        JButton helpAboutButton = new JButton(new ImageIcon(Utils.getReourceImage(this, Constants.MENU_ICON_HELP_ABOUT)));
        helpAboutButton.addActionListener(aboutListener);
        helpAboutButton.setToolTipText(Internationalization.TIP_HELP_ABOUT);
        this.getMenuToolBar().add(selectCorpusButton);
        this.getMenuToolBar().add(createCorpusButton);
        this.getMenuToolBar().add(saveCorpusButton);
        this.getMenuToolBar().add(saveAsCorpusButton);
        this.getMenuToolBar().add(closeCorpusButton);
        this.getMenuToolBar().addSeparator();
        this.getMenuToolBar().add(publicButton);
        this.getMenuToolBar().add(retrieveColltButton);
        this.getMenuToolBar().add(visibilityButton);
        this.getMenuToolBar().add(removeCollButton);
        this.getMenuToolBar().addSeparator();
        this.getMenuToolBar().add(selectInterviewButton);
        this.getMenuToolBar().add(deleteInterviewButton);
        this.getMenuToolBar().add(newInterviewButton);
        this.getMenuToolBar().add(closeInterviewButton);
        this.getMenuToolBar().addSeparator();
        this.getMenuToolBar().add(configButton);
        this.getMenuToolBar().add(importButton);
        this.getMenuToolBar().addSeparator();
        this.getMenuToolBar().add(helpButton);
        this.getMenuToolBar().add(helpAboutButton);
        this.xmlViewPanel.addComponentListener(xmlPaneListener);
        this.xmlPane.setDoubleBuffered(true);
        this.xmlUpPanel.setLayout(new FlowLayout());
        this.editXML = new JButton(Internationalization.XML_EDIT_TEXT, new ImageIcon(Utils.getReourceImage(this, Constants.XML_EDIT)));
        EditXMLAction editAction = new EditXMLAction(this);
        this.editXML.addActionListener(editAction);
        this.commitXML = new JButton(Internationalization.APPLY_CHANGES, new ImageIcon(Utils.getReourceImage(this, Constants.XML_COMMIT)));
        CommitXMLAction commitAction = new CommitXMLAction(this);
        this.commitXML.addActionListener(commitAction);
        this.commitXML.setEnabled(false);
        this.discartXML = new JButton(Internationalization.DISCARD_CHANGES, new ImageIcon(Utils.getReourceImage(this, Constants.XML_DISCARD)));
        DiscartXMLAction discartAction = new DiscartXMLAction(this);
        this.discartXML.addActionListener(discartAction);
        this.discartXML.setEnabled(false);
        this.xmlUpPanel.add(this.editXML);
        this.xmlUpPanel.add(this.commitXML);
        this.xmlUpPanel.add(this.discartXML);
        this.logicalViewPanel.addComponentListener(logicalVieListener);
        this.tabbedPane.setTitleAt(0, Internationalization.LOGICAL_VIEW_TITLE);
        this.tabbedPane.setTitleAt(1, Internationalization.XML_VIEW_TITLE);
        this.tabbedPane.setTitleAt(2, Internationalization.DOCUMENT_METADATA_TITLE);
        this.titleLabel.setText(Internationalization.TITLE_WORD);
        this.dateRecordingLabel.setText(Internationalization.DATE_RECORDING);
        this.dateTranscriptionLabel.setText(Internationalization.DATE_TRANSCRIPTING);
        this.localeLabel.setText(Internationalization.LOCALE_WORD);
        this.researcherLabel.setText(Internationalization.RESEARCHER_WORD);
        this.transcriberLabel.setText(Internationalization.TRANSCRIBER_WORD);
        this.editorLabel.setText(Internationalization.EDITOR_WORD);
        this.descriptionLabel.setText(Internationalization.DESCRIPTION_WORD);
        this.autorityLabel.setText(Internationalization.AUTORITY_WORD);
        this.idLabel.setText(Internationalization.ID_WORD);
        this.languagueLabel.setText(Internationalization.LANGUAGE_WORD);
        this.mediaLabel.setText(Internationalization.MEDIA_FILE_NAME);
        this.participanInformationPanel.setBorder(new TitledBorder(Internationalization.PARTICIPATION_INFORMATION));
        this.PrincipalInvestigatorLabe.setText(Internationalization.PRINCIPAL_INVESTIGATOR);
        this.documentMetadataPanel.setBorder(new TitledBorder(Internationalization.DOCUMENT_METADATA_TITLE));
        this.documentLinksPanel.setBorder(new TitledBorder(Internationalization.DOCUMENT_RESOURCES_TITLE));
        this.viewInfoButton.setText(Internationalization.VIEW_INFO);
        this.deletePersonButton.setText(Internationalization.DELETE_PERSON);
        this.addResourceButton.setText(Internationalization.ADD_RESOURCE);
        this.importResourcesFromFileButton.setText(Internationalization.ADD_RESOURCE_FROM_FILE);
        this.attachAVRPResourceButton.setText(Internationalization.ATTACH_VRP_RS);
        this.applyChangesButton.setText(Internationalization.APPLY_CHANGES);
        this.addPersonButton.setText(Internationalization.ADD_PERSON);
        this.addKeywordButton.setText(Internationalization.ADD_KEYWORD);
        this.deleteKeywordButton.setText(Internationalization.REMOVE_KEYWORD);
        this.documentScrollTable.setAutoscrolls(false);
        this.documentScrollTable.getViewport().setAutoscrolls(false);
        this.documentScrollTable.getHorizontalScrollBar().setAutoscrolls(false);
        this.documentScrollTable.getVerticalScrollBar().setAutoscrolls(false);
        this.documentScrollTable.setDoubleBuffered(false);
        this.getDocumentScrollTable().getVerticalScrollBar().setUnitIncrement(Integer.valueOf(Constants.BLOCK_INCREMENT_SCROLL));
        this.getDocumentScrollTable().getVerticalScrollBar().setBlockIncrement(Integer.valueOf(Constants.BLOCK_INCREMENT_SCROLL));
        this.getDocumentScrollTable().setWheelScrollingEnabled(false);
        this.getDocumentScrollTable().addMouseWheelListener(new TableMouseWhell(this));
        this.getTabbedPane().setEnabledAt(2, false);
        this.resourcesTable.setShowGrid(true);
        this.resourcesTable.setAutoResizeMode(4);
        this.resourcesTable.setDefaultRenderer(Node.class, new PtrNodeCellRenderer(this, this.getResourcesTable()));
        this.resourcesTable.setDefaultEditor(Node.class, new PtrNodeCellRenderer(this, this.getResourcesTable()));
        this.resourcesTable.setColumnSelectionAllowed(true);
        this.resourcesTable.setRowSelectionAllowed(true);
        this.resourcesTable.setRowMargin(10);
        this.resourcesTable.setRowHeight(this.resourcesTable.getRowMargin() + this.resourcesTable.getFont().getSize() * 2);
        this.deletePersonButton.addActionListener(new DeletePersonAction(this.personBox));
        this.viewInfoButton.addActionListener(new ShowPersonInformationAction(this.personBox, this));
        this.addPersonButton.addActionListener(new AddPersonAction(this));
        this.addKeywordButton.addActionListener(new AddKeyWordAction(this));
        this.keyWordcomboBox.setRenderer(new KeywordRenderer());
        this.deleteKeywordButton.addActionListener(new DeleteKeyWordAction(this));
        this.keyWordcomboBox.setOpaque(false);
        this.ventanaPrincipal.addWindowListener(this);
        this.ventanaPrincipal.setDefaultCloseOperation(0);
        this.ventanaPrincipal.getContentPane().add(this.getMainPanel());
        this.ventanaPrincipal.setSize(1024, 748);
        this.ventanaPrincipal.setExtendedState(6);
        this.ventanaPrincipal.setIconImage(Utils.getReourceImage(this, Constants.SACODEYL_LOGO));
    }

    public void setVisible(boolean visible) {
        if (visible) {
            SwingUtilities.invokeLater(this);
        }
    }

    @Override
    public void windowClosing(WindowEvent e) {
        block4 : {
            try {
                super.windowClosing(e);
                SaveCorpusFile scf = !Utils.isChangedCorpus() ? new SaveCorpusFile(this, false) : new SaveCorpusFile(this, true);
                scf.actionPerformed(new ActionEvent(this, 1, "command"));
                if (scf.isWasCancel()) break block4;
                ColorMap.saveColors(Constants.WORKING_PATH + Constants.FILE_NAME_CONFIG_COLOR);
                ShowedCategory.saveShowedCategory(Constants.WORKING_PATH + Constants.FILE_NAME_CONFIG_SHOWED_CATEGORY);
                Properties prop = Constants.toProperties();
                try {
                    prop.storeToXML(new FileOutputStream(Constants.WORKING_PATH + Constants.FILE_NAME_CONFIG_APP), "Annotator Tool Configurator");
                }
                catch (IOException e1) {
                    System.out.println(e1.getMessage());
                }
                System.exit(0);
                CMTManager.getCMT().unlockDiscartingDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, Constants.DOCUMENT_OPENED);
                CMTManager.getCMT().unlockDiscartingTaxonomyTree(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS);
                CMTManager.getCMT().logout(Constants.CMT_USER);
            }
            catch (Exception e2) {
                System.out.println(e2.getMessage());
            }
        }
    }

    private void createUIComponents() {
        this.ventanaPrincipal = new JFrame("BACKBONE Annotator 3.8");
        this.documentScrollTable = new JScrollPane();
        this.documentTable = new TEIJTable(this);
        this.taxonomyTree = new TEIJTree(this);
    }

    public JSplitPane getMainSplitPane() {
        return this.mainSplitPane;
    }

    public void setMainSplitPane(JSplitPane mainSplitPane) {
        this.mainSplitPane = mainSplitPane;
    }

    public JPanel getXmlUpPanel() {
        return this.xmlUpPanel;
    }

    public void setXmlUpPanel(JPanel xmlUpPanel) {
        this.xmlUpPanel = xmlUpPanel;
    }

    public JTextField getTitleField() {
        return this.titleField;
    }

    public void setTitleField(JTextField titleField) {
        this.titleField = titleField;
    }

    public JTextField getEditorField() {
        return this.editorField;
    }

    public void setEditorField(JTextField editorField) {
        this.editorField = editorField;
    }

    public JComboBox getPersonBox() {
        return this.personBox;
    }

    public void setPersonBox(JComboBox personBox) {
        this.personBox = personBox;
    }

    public JButton getViewInfoButton() {
        return this.viewInfoButton;
    }

    public void setViewInfoButton(JButton viewInfoButton) {
        this.viewInfoButton = viewInfoButton;
    }

    public JButton getDeletePersonButton() {
        return this.deletePersonButton;
    }

    public void setDeletePersonButton(JButton deletePersonButton) {
        this.deletePersonButton = deletePersonButton;
    }

    public JTable getResourcesTable() {
        return this.resourcesTable;
    }

    public void setResourcesTable(JTable resourcesTable) {
        this.resourcesTable = resourcesTable;
    }

    public JButton getAddResourceButton() {
        return this.addResourceButton;
    }

    public void setAddResourceButton(JButton addResourceButton) {
        this.addResourceButton = addResourceButton;
    }

    public JButton getImportResourcesFromFileButton() {
        return this.importResourcesFromFileButton;
    }

    public void setImportResourcesFromFileButton(JButton importResourcesFromFileButton) {
        this.importResourcesFromFileButton = importResourcesFromFileButton;
    }

    public JButton getApplyChangesButton() {
        return this.applyChangesButton;
    }

    public void setApplyChangesButton(JButton applyChangesButton) {
        this.applyChangesButton = applyChangesButton;
    }

    public JPanel getMetadataPanel() {
        return this.metadataPanel;
    }

    public void setMetadataPanel(JPanel metadataPanel) {
        this.metadataPanel = metadataPanel;
    }

    public JTextField getDateRecordingField() {
        return this.dateRecordingField;
    }

    public void setDateRecordingField(JTextField dateRecordingField) {
        this.dateRecordingField = dateRecordingField;
    }

    public JTextField getDateTranscriptionField() {
        return this.dateTranscriptionField;
    }

    public void setDateTranscriptionField(JTextField dateTranscriptionField) {
        this.dateTranscriptionField = dateTranscriptionField;
    }

    public JTextField getLocaleField() {
        return this.localeField;
    }

    public void setLocaleField(JTextField localeField) {
        this.localeField = localeField;
    }

    public JTextField getPrincipalInvestigatorField() {
        return this.principalInvestigatorField;
    }

    public void setPrincipalInvestigatorField(JTextField principalInvestigatorField) {
        this.principalInvestigatorField = principalInvestigatorField;
    }

    public JTextField getResearcherField() {
        return this.researcherField;
    }

    public void setResearcherField(JTextField researcherField) {
        this.researcherField = researcherField;
    }

    public JTextField getTranscriberField() {
        return this.transcriberField;
    }

    public void setTranscriberField(JTextField transcriberField) {
        this.transcriberField = transcriberField;
    }

    public JTextArea getDescriptionField() {
        return this.descriptionField;
    }

    public void setDescriptionField(JTextArea descriptionField) {
        this.descriptionField = descriptionField;
    }

    public JTextField getAutorityField() {
        return this.autorityField;
    }

    public void setAutorityField(JTextField autorityField) {
        this.autorityField = autorityField;
    }

    public JTextField getIdField() {
        return this.idField;
    }

    public void setIdField(JTextField idField) {
        this.idField = idField;
    }

    public JTextField getLanguageField() {
        return this.languageField;
    }

    public void setLanguageField(JTextField languageField) {
        this.languageField = languageField;
    }

    public JTextField getMediaFileField() {
        return this.mediaFileField;
    }

    public void setMediaFileField(JTextField mediaFileField) {
        this.mediaFileField = mediaFileField;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public JTabbedPane getTabbedPane() {
        return this.tabbedPane;
    }

    public void setTabbedPane(JTabbedPane tabbedPane) {
        this.tabbedPane = tabbedPane;
    }

    public JScrollPane getXmlScrollPane() {
        return this.xmlScrollPane;
    }

    public void setXmlScrollPane(JScrollPane xmlScrollPane) {
        this.xmlScrollPane = xmlScrollPane;
    }

    public JButton getUpTaxonomy() {
        return this.upTaxonomy;
    }

    public void setUpTaxonomy(JButton upTaxonomy) {
        this.upTaxonomy = upTaxonomy;
    }

    public JButton getDownTaxonomy() {
        return this.downTaxonomy;
    }

    public void setDownTaxonomy(JButton downTaxonomy) {
        this.downTaxonomy = downTaxonomy;
    }

    public JTextField getActivityField() {
        return this.activityField;
    }

    public void setActivityField(JTextField activityField) {
        this.activityField = activityField;
    }

    public JPanel getXmlViewPanel() {
        return this.xmlViewPanel;
    }

    public JTextPane getXmlPane() {
        return this.xmlPane;
    }

    public JTable getDocumentTable() {
        return this.documentTable;
    }

    public JPanel getMainPanel() {
        return this.mainPanel;
    }

    public JScrollPane getDocumentScrollTable() {
        return this.documentScrollTable;
    }

    public JPanel getLogicalViewPanel() {
        return this.logicalViewPanel;
    }

    public JTree getTaxonomyTree() {
        return this.taxonomyTree;
    }

    public JScrollPane getTaxonomyScrollTree() {
        return this.taxonomyScrollTree;
    }

    public JToolBar getMenuToolBar() {
        return this.menuToolBar;
    }

    public JToolBar getStatusToolBar() {
        return this.statusToolBar;
    }

    public JToolBar getTaxonomyToolBar() {
        return this.taxonomyToolBar;
    }

    public JFrame getVentanaPrincipal() {
        return this.ventanaPrincipal;
    }

    public JButton getDiscartXML() {
        return this.discartXML;
    }

    public JButton getCommitXML() {
        return this.commitXML;
    }

    public JButton getEditXML() {
        return this.editXML;
    }

    public JButton getAddPersonButton() {
        return this.addPersonButton;
    }

    public void setAddPersonButton(JButton addPersonButton) {
        this.addPersonButton = addPersonButton;
    }

    public JButton getAddKeywordButton() {
        return this.addKeywordButton;
    }

    public void setAddKeywordButton(JButton addKeywordButton) {
        this.addKeywordButton = addKeywordButton;
    }

    public JButton getDeleteKeywordButton() {
        return this.deleteKeywordButton;
    }

    public void setDeleteKeywordButton(JButton deleteKeywordButton) {
        this.deleteKeywordButton = deleteKeywordButton;
    }

    public JComboBox getKeyWordcomboBox() {
        return this.keyWordcomboBox;
    }

    public void setKeyWordcomboBox(JComboBox keyWordcomboBox) {
        this.keyWordcomboBox = keyWordcomboBox;
    }

    public JButton getAttachAVRPResourceButton() {
        return this.attachAVRPResourceButton;
    }

    public void setAttachAVRPResourceButton(JButton attachAVRPResourceButton) {
        this.attachAVRPResourceButton = attachAVRPResourceButton;
    }

    public JTextField getLanguageVariety() {
        return this.languageVariety;
    }

    public void setLanguageVariety(JTextField languageVariety) {
        this.languageVariety = languageVariety;
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JTextField jTextField;
        JPanel jPanel;
        JTextField jTextField2;
        JLabel jLabel;
        JPanel jPanel2;
        JButton jButton;
        JLabel jLabel2;
        JTextField jTextField3;
        JButton jButton2;
        JTextField jTextField4;
        JToolBar jToolBar;
        JComboBox jComboBox;
        JComboBox jComboBox2;
        JTextField jTextField5;
        JTabbedPane jTabbedPane;
        JLabel jLabel3;
        JLabel jLabel4;
        JButton jButton3;
        JTextField jTextField6;
        JTextField jTextField7;
        JPanel jPanel3;
        JScrollPane jScrollPane;
        JTextField jTextField8;
        JToolBar jToolBar2;
        JLabel jLabel5;
        JPanel jPanel4;
        JButton jButton4;
        JTextField jTextField9;
        JLabel jLabel6;
        JButton jButton5;
        JLabel jLabel7;
        JTextArea jTextArea;
        JLabel jLabel8;
        JPanel jPanel5;
        JLabel jLabel9;
        JLabel jLabel10;
        JPanel jPanel6;
        JLabel jLabel11;
        JTextPane jTextPane;
        JPanel jPanel7;
        JLabel jLabel12;
        JPanel jPanel8;
        JTextField jTextField10;
        JButton jButton6;
        JButton jButton7;
        JLabel jLabel13;
        JTextField jTextField11;
        JPanel jPanel9;
        JToolBar jToolBar3;
        JTextField jTextField12;
        JTextField jTextField13;
        JSplitPane jSplitPane;
        JTextField jTextField14;
        JButton jButton8;
        JButton jButton9;
        JScrollPane jScrollPane2;
        JTable jTable;
        this.createUIComponents();
        this.mainPanel = jPanel7 = new JPanel();
        jPanel7.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        this.mainSplitPane = jSplitPane = new JSplitPane();
        jSplitPane.setOneTouchExpandable(true);
        jSplitPane.setEnabled(true);
        jPanel7.add((Component)jSplitPane, new GridConstraints(1, 0, 1, 1, 0, 3, 3, 3, null, new Dimension(200, 200), null));
        this.treePane = jPanel3 = new JPanel();
        jPanel3.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jSplitPane.setLeftComponent(jPanel3);
        this.taxonomyScrollTree = jScrollPane2 = new JScrollPane();
        jScrollPane2.setVerticalScrollBarPolicy(20);
        jPanel3.add((Component)jScrollPane2, new GridConstraints(1, 0, 1, 1, 0, 3, 7, 7, null, null, null));
        jScrollPane2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null, 0, 0, null, null));
        JTree jTree = this.taxonomyTree;
        jTree.setShowsRootHandles(true);
        jTree.setRootVisible(true);
        jTree.setEditable(false);
        jTree.setEnabled(true);
        jScrollPane2.setViewportView(jTree);
        this.taxonomyToolBar = jToolBar3 = new JToolBar();
        jPanel3.add((Component)jToolBar3, new GridConstraints(0, 0, 1, 1, 0, 1, 6, 0, null, new Dimension(-1, 20), null));
        jToolBar3.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null, 0, 0, null, null));
        this.tabbedPane = jTabbedPane = new JTabbedPane();
        jTabbedPane.setTabLayoutPolicy(1);
        jTabbedPane.setEnabled(true);
        jSplitPane.setRightComponent(jTabbedPane);
        this.logicalViewPanel = jPanel2 = new JPanel();
        jPanel2.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jTabbedPane.addTab("Logical View", null, jPanel2, null);
        JScrollPane jScrollPane3 = this.documentScrollTable;
        jScrollPane3.setVerticalScrollBarPolicy(22);
        jScrollPane3.setHorizontalScrollBarPolicy(32);
        jPanel2.add((Component)jScrollPane3, new GridConstraints(0, 0, 1, 1, 0, 3, 0, 0, null, null, null));
        jScrollPane3.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null, 0, 0, null, null));
        JTable jTable2 = this.documentTable;
        jTable2.setAutoResizeMode(0);
        jScrollPane3.setViewportView(jTable2);
        this.xmlViewPanel = jPanel5 = new JPanel();
        jPanel5.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jTabbedPane.addTab("XML View", null, jPanel5, null);
        jPanel5.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null, 0, 0, null, null));
        this.xmlScrollPane = jScrollPane = new JScrollPane();
        jScrollPane.setVerticalScrollBarPolicy(22);
        jScrollPane.setHorizontalScrollBarPolicy(32);
        jPanel5.add((Component)jScrollPane, new GridConstraints(1, 0, 1, 1, 0, 3, 7, 7, null, null, null));
        this.xmlPane = jTextPane = new JTextPane();
        jTextPane.setEditable(false);
        jTextPane.setEnabled(true);
        jTextPane.setContentType("text/plain");
        jScrollPane.setViewportView(jTextPane);
        this.xmlUpPanel = jPanel9 = new JPanel();
        jPanel9.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel5.add((Component)jPanel9, new GridConstraints(0, 0, 1, 1, 8, 2, 3, 3, null, null, null));
        this.metadataPanel = jPanel8 = new JPanel();
        jPanel8.setLayout(new GridLayoutManager(5, 3, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel8.setPreferredSize(new Dimension(-1, -1));
        jPanel8.setMinimumSize(new Dimension(-1, -1));
        jTabbedPane.addTab("Document Metadata", null, jPanel8, null);
        jPanel8.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null, 0, 0, null, null));
        this.documentMetadataPanel = jPanel = new JPanel();
        jPanel.setLayout(new GridLayoutManager(10, 6, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel8.add((Component)jPanel, new GridConstraints(0, 1, 1, 1, 0, 3, 0, 3, null, new Dimension(290, 209), null, 0, true));
        jPanel.setBorder(BorderFactory.createTitledBorder(null, "Document's Metadata", 0, 0, null, null));
        this.editorField = jTextField2 = new JTextField();
        jTextField2.setHorizontalAlignment(2);
        jPanel.add((Component)jTextField2, new GridConstraints(0, 4, 1, 1, 8, 0, 3, 0, null, new Dimension(140, 20), null));
        this.autorityField = jTextField10 = new JTextField();
        jTextField10.setHorizontalAlignment(2);
        jPanel.add((Component)jTextField10, new GridConstraints(5, 4, 1, 1, 8, 0, 3, 0, null, new Dimension(140, 20), null));
        this.idField = jTextField13 = new JTextField();
        jTextField13.setHorizontalAlignment(2);
        jPanel.add((Component)jTextField13, new GridConstraints(6, 4, 1, 1, 8, 0, 3, 0, null, new Dimension(140, 20), null));
        this.languageField = jTextField3 = new JTextField();
        jTextField3.setHorizontalAlignment(2);
        jPanel.add((Component)jTextField3, new GridConstraints(7, 4, 1, 1, 8, 0, 3, 0, null, new Dimension(140, 20), null));
        this.participanInformationPanel = jPanel6 = new JPanel();
        jPanel6.setLayout(new GridLayoutManager(1, 6, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel.add((Component)jPanel6, new GridConstraints(9, 0, 1, 6, 0, 3, 3, 3, null, new Dimension(206, 29), null));
        jPanel6.setBorder(BorderFactory.createTitledBorder(null, "Participants  Information", 0, 0, null, null));
        Spacer spacer = new Spacer();
        jPanel6.add((Component)spacer, new GridConstraints(0, 0, 1, 1, 0, 1, 6, 1, null, null, null));
        JPanel jPanel10 = new JPanel();
        jPanel10.setLayout(new GridLayoutManager(1, 4, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel6.add((Component)jPanel10, new GridConstraints(0, 1, 1, 4, 0, 3, 3, 3, null, null, null));
        this.personBox = jComboBox2 = new JComboBox();
        jPanel10.add(jComboBox2, new GridConstraints(0, 0, 1, 1, 0, 0, 0, 0, null, new Dimension(250, -1), new Dimension(250, -1)));
        this.viewInfoButton = jButton5 = new JButton();
        jButton5.setText("View Info");
        jPanel10.add((Component)jButton5, new GridConstraints(0, 1, 1, 1, 0, 0, 0, 0, null, null, null));
        this.deletePersonButton = jButton8 = new JButton();
        jButton8.setText("Delete Person");
        jPanel10.add((Component)jButton8, new GridConstraints(0, 2, 1, 1, 0, 0, 0, 0, null, null, null));
        this.addPersonButton = jButton3 = new JButton();
        jButton3.setText("Add Person");
        jPanel10.add((Component)jButton3, new GridConstraints(0, 3, 1, 1, 0, 1, 3, 0, null, null, null));
        Spacer spacer2 = new Spacer();
        jPanel6.add((Component)spacer2, new GridConstraints(0, 5, 1, 1, 0, 1, 6, 1, null, null, null));
        this.titleLabel = jLabel12 = new JLabel();
        jLabel12.setText("Title");
        jPanel.add((Component)jLabel12, new GridConstraints(0, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.titleField = jTextField6 = new JTextField();
        jPanel.add((Component)jTextField6, new GridConstraints(0, 1, 1, 1, 8, 0, 3, 0, null, new Dimension(150, -1), null));
        this.dateRecordingLabel = jLabel13 = new JLabel();
        jLabel13.setVerticalAlignment(1);
        jLabel13.setText("Date Recording (yyyy-mm-dd)");
        jPanel.add((Component)jLabel13, new GridConstraints(1, 0, 1, 1, 9, 0, 0, 0, null, new Dimension(149, -1), null));
        this.editorLabel = jLabel9 = new JLabel();
        jLabel9.setText("Editor");
        jPanel.add((Component)jLabel9, new GridConstraints(0, 3, 1, 1, 8, 0, 0, 0, null, null, null));
        this.descriptionLabel = jLabel6 = new JLabel();
        jLabel6.setText("Description");
        jPanel.add((Component)jLabel6, new GridConstraints(1, 3, 4, 1, 8, 0, 0, 0, null, new Dimension(57, 124), null));
        this.autorityLabel = jLabel3 = new JLabel();
        jLabel3.setText("Autority");
        jPanel.add((Component)jLabel3, new GridConstraints(5, 3, 1, 1, 8, 0, 0, 0, null, null, null));
        this.idLabel = jLabel7 = new JLabel();
        jLabel7.setText("ID");
        jPanel.add((Component)jLabel7, new GridConstraints(6, 3, 1, 1, 8, 0, 0, 0, null, null, null));
        this.languagueLabel = jLabel5 = new JLabel();
        jLabel5.setText("Languge");
        jPanel.add((Component)jLabel5, new GridConstraints(7, 3, 1, 1, 8, 0, 0, 0, null, null, null));
        JScrollPane jScrollPane4 = new JScrollPane();
        jScrollPane4.setVerticalScrollBarPolicy(22);
        jScrollPane4.setHorizontalScrollBarPolicy(32);
        jPanel.add((Component)jScrollPane4, new GridConstraints(1, 4, 4, 1, 8, 2, 3, 0, null, new Dimension(140, 124), new Dimension(140, -1)));
        this.descriptionField = jTextArea = new JTextArea();
        jTextArea.setLineWrap(true);
        jTextArea.setWrapStyleWord(true);
        jTextArea.setColumns(20);
        jTextArea.setRows(5);
        jScrollPane4.setViewportView(jTextArea);
        Spacer spacer3 = new Spacer();
        jPanel.add((Component)spacer3, new GridConstraints(1, 5, 4, 1, 0, 1, 2, 1, null, new Dimension(11, 11), null));
        Spacer spacer4 = new Spacer();
        jPanel.add((Component)spacer4, new GridConstraints(1, 2, 4, 1, 0, 1, 2, 1, null, null, null));
        this.dateRecordingField = jTextField7 = new JTextField();
        jTextField7.setColumns(10);
        jPanel.add((Component)jTextField7, new GridConstraints(1, 1, 1, 1, 8, 0, 3, 0, null, new Dimension(150, -1), null));
        this.dateTranscriptionLabel = jLabel4 = new JLabel();
        jLabel4.setText("Date Transcription (yyyy-mm-dd)");
        jPanel.add((Component)jLabel4, new GridConstraints(2, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.dateTranscriptionField = jTextField11 = new JTextField();
        jTextField11.setColumns(10);
        jPanel.add((Component)jTextField11, new GridConstraints(2, 1, 1, 1, 8, 0, 3, 0, null, new Dimension(150, -1), null));
        this.localeLabel = jLabel10 = new JLabel();
        jLabel10.setText("Locale");
        jPanel.add((Component)jLabel10, new GridConstraints(3, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.localeField = jTextField = new JTextField();
        jPanel.add((Component)jTextField, new GridConstraints(3, 1, 1, 1, 8, 0, 3, 0, null, new Dimension(150, -1), null));
        this.PrincipalInvestigatorLabe = jLabel = new JLabel();
        jLabel.setText("Principal Investigaor");
        jPanel.add((Component)jLabel, new GridConstraints(4, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.principalInvestigatorField = jTextField12 = new JTextField();
        jPanel.add((Component)jTextField12, new GridConstraints(4, 1, 1, 1, 8, 0, 3, 0, null, new Dimension(150, -1), null));
        this.researcherField = jTextField4 = new JTextField();
        jPanel.add((Component)jTextField4, new GridConstraints(5, 1, 1, 1, 8, 0, 3, 0, null, new Dimension(150, -1), null));
        this.researcherLabel = jLabel2 = new JLabel();
        jLabel2.setText("Researcher");
        jPanel.add((Component)jLabel2, new GridConstraints(5, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.transcriberLabel = jLabel11 = new JLabel();
        jLabel11.setText("Transcriber");
        jPanel.add((Component)jLabel11, new GridConstraints(6, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.transcriberField = jTextField5 = new JTextField();
        jPanel.add((Component)jTextField5, new GridConstraints(6, 1, 1, 1, 8, 0, 3, 0, null, new Dimension(150, -1), null));
        JLabel jLabel14 = new JLabel();
        jLabel14.setText("Activity");
        jPanel.add((Component)jLabel14, new GridConstraints(7, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.activityField = jTextField9 = new JTextField();
        jPanel.add((Component)jTextField9, new GridConstraints(7, 1, 1, 1, 8, 0, 3, 0, null, new Dimension(150, -1), null));
        this.mediaLabel = jLabel8 = new JLabel();
        jLabel8.setText("Media File Name");
        jPanel.add((Component)jLabel8, new GridConstraints(8, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.mediaFileField = jTextField14 = new JTextField();
        jTextField14.setHorizontalAlignment(2);
        jPanel.add((Component)jTextField14, new GridConstraints(8, 1, 1, 1, 8, 0, 3, 0, null, new Dimension(140, 20), null));
        JLabel jLabel15 = new JLabel();
        jLabel15.setText("Language Variety");
        jPanel.add((Component)jLabel15, new GridConstraints(8, 3, 1, 1, 8, 0, 0, 0, null, null, null));
        this.languageVariety = jTextField8 = new JTextField();
        jPanel.add((Component)jTextField8, new GridConstraints(8, 4, 1, 1, 8, 0, 6, 0, null, new Dimension(140, 20), null));
        JPanel jPanel11 = new JPanel();
        jPanel11.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel8.add((Component)jPanel11, new GridConstraints(3, 1, 1, 1, 0, 3, 3, 3, null, new Dimension(290, 29), null));
        this.applyChangesButton = jButton4 = new JButton();
        jButton4.setText("Apply Changes");
        jPanel11.add((Component)jButton4, new GridConstraints(0, 0, 1, 1, 0, 0, 0, 0, null, null, null));
        this.documentLinksPanel = jPanel4 = new JPanel();
        jPanel4.setLayout(new GridLayoutManager(2, 7, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel8.add((Component)jPanel4, new GridConstraints(2, 1, 1, 1, 0, 3, 7, 7, null, new Dimension(290, 493), null));
        jPanel4.setBorder(BorderFactory.createTitledBorder(null, "Documnet's Links", 0, 0, null, null));
        JScrollPane jScrollPane5 = new JScrollPane();
        jScrollPane5.setVerticalScrollBarPolicy(22);
        jScrollPane5.setHorizontalScrollBarPolicy(32);
        jPanel4.add((Component)jScrollPane5, new GridConstraints(0, 0, 1, 7, 0, 3, 7, 7, new Dimension(-1, 90), null, null));
        this.resourcesTable = jTable = new JTable();
        jScrollPane5.setViewportView(jTable);
        this.addResourceButton = jButton6 = new JButton();
        jButton6.setText("Add Resource");
        jPanel4.add((Component)jButton6, new GridConstraints(1, 1, 1, 2, 4, 0, 1, 0, null, null, null));
        this.importResourcesFromFileButton = jButton = new JButton();
        jButton.setText("Import Resources From File");
        jPanel4.add((Component)jButton, new GridConstraints(1, 4, 1, 2, 8, 0, 1, 0, null, null, null));
        this.attachAVRPResourceButton = jButton7 = new JButton();
        jButton7.setText("Attach a VRP Resource Sheet");
        jButton7.setHorizontalTextPosition(0);
        jPanel4.add((Component)jButton7, new GridConstraints(1, 3, 1, 1, 0, 1, 0, 0, null, null, null));
        Spacer spacer5 = new Spacer();
        jPanel4.add((Component)spacer5, new GridConstraints(1, 0, 1, 1, 0, 1, 6, 1, null, null, null));
        Spacer spacer6 = new Spacer();
        jPanel4.add((Component)spacer6, new GridConstraints(1, 6, 1, 1, 0, 1, 6, 1, null, null, null));
        JPanel jPanel12 = new JPanel();
        jPanel12.setLayout(new FormLayout("fill:d:grow,left:4dlu:noGrow,fill:d:grow,left:4dlu:noGrow,fill:max(d;4px):noGrow,left:4dlu:noGrow,fill:max(d;4px):noGrow,left:4dlu:noGrow,fill:d:grow", "center:d:grow,center:max(d;4px):noGrow"));
        jPanel8.add((Component)jPanel12, new GridConstraints(1, 1, 1, 1, 0, 3, 3, 3, null, null, null));
        jPanel12.setBorder(BorderFactory.createTitledBorder(null, "Keywords", 0, 0, null, null));
        this.addKeywordButton = jButton9 = new JButton();
        jButton9.setText("Add keyword");
        jPanel12.add((Component)jButton9, new CellConstraints(5, 2, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.deleteKeywordButton = jButton2 = new JButton();
        jButton2.setText("Delete Keyword");
        jPanel12.add((Component)jButton2, new CellConstraints(7, 2, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        Spacer spacer7 = new Spacer();
        jPanel12.add((Component)spacer7, new CellConstraints(9, 2, 1, 1, CellConstraints.FILL, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        Spacer spacer8 = new Spacer();
        jPanel12.add((Component)spacer8, new CellConstraints(1, 2, 1, 1, CellConstraints.FILL, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        JPanel jPanel13 = new JPanel();
        jPanel13.setLayout(new FormLayout("fill:d:grow", "center:d:grow"));
        jPanel12.add((Component)jPanel13, new CellConstraints(3, 2, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.keyWordcomboBox = jComboBox = new JComboBox();
        jComboBox.setMinimumSize(new Dimension(150, 22));
        jComboBox.setPreferredSize(new Dimension(150, 22));
        jPanel13.add(jComboBox, new CellConstraints(1, 1, 1, 1, CellConstraints.DEFAULT, CellConstraints.DEFAULT, new Insets(0, 0, 0, 0)));
        this.menuToolBar = jToolBar = new JToolBar();
        jPanel7.add((Component)jToolBar, new GridConstraints(0, 0, 1, 1, 0, 1, 6, 0, null, new Dimension(-1, 20), null));
        jToolBar.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null, 0, 0, null, null));
        this.statusToolBar = jToolBar2 = new JToolBar();
        jPanel7.add((Component)jToolBar2, new GridConstraints(2, 0, 1, 1, 0, 1, 6, 0, null, new Dimension(-1, 20), null));
        jToolBar2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null, 0, 0, null, null));
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.mainPanel;
    }
}

