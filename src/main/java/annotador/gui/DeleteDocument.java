/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.action.xml.XMLViewComponentListener;
import annotador.cmt.CMTManager;
import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.PrintStream;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import um.backbone.cmtclient.CMTClient;
import um.backbone.cmtclient.jaxws.CMTServerException_Exception;

public class DeleteDocument
extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox documentComboBox;
    private JLabel iconLabel;
    private JLabel selectField;
    MainForm mainForm;

    public JLabel getIconLabel() {
        return this.iconLabel;
    }

    public JComboBox getDocumentComboBox() {
        return this.documentComboBox;
    }

    public void setDocumentComboBox(JComboBox documentComboBox) {
        this.documentComboBox = documentComboBox;
    }

    public DeleteDocument(MainForm mainForm) {
        this.mainForm = mainForm;
        this.$$$setupUI$$$();
        this.setContentPane(this.contentPane);
        this.setModal(true);
        this.getRootPane().setDefaultButton(this.buttonOK);
        this.selectField.setText(Internationalization.SELECT_DOCUMENT);
        this.buttonCancel.setText(Internationalization.CANCEL_WORD);
        this.buttonOK.setText(Internationalization.OK_WORD);
        TitledBorder t = new TitledBorder(Internationalization.DELETE_DOCUMENT_TITLE);
        this.contentPane.setBorder(t);
        this.buttonOK.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                DeleteDocument.this.onOK();
            }
        });
        this.buttonCancel.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                DeleteDocument.this.onCancel();
            }
        });
        this.setDefaultCloseOperation(0);
        this.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosing(WindowEvent e) {
                DeleteDocument.this.onCancel();
            }
        });
        this.contentPane.registerKeyboardAction(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                DeleteDocument.this.onCancel();
            }
        }, KeyStroke.getKeyStroke(27, 0), 1);
    }

    private void onOK() {
        block11 : {
            try {
                if (this.documentComboBox.getSelectedIndex() == -1) break block11;
                this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(3));
                if (Constants.IS_COLLABORATIVE_CORPUS == null) {
                    int index = this.documentComboBox.getSelectedIndex();
                    XPath xpath = XPathFactory.newInstance().newXPath();
                    xpath.setNamespaceContext(new TEINamespaceContext());
                    xpath = XPathFactory.newInstance().newXPath();
                    xpath.setNamespaceContext(new TEINamespaceContext());
                    String xpathExpression = "/tei:teiCorpus/tei:TEI[" + ++index + "]";
                    Node node = null;
                    node = (Node)xpath.evaluate(xpathExpression, DocumentSingleton.getInstance(), XPathConstants.NODE);
                    node.getParentNode().removeChild(node);
                    DocumentSingleton.getInstance().normalizeDocument();
                    Utils.changeInterviewEvent(this.mainForm, -1);
                    Utils.setChangedCorpus(true);
                } else {
                    String id = (String)this.documentComboBox.getSelectedItem();
                    CMTClient client = CMTManager.getCMT();
                    if (Constants.DOCUMENT_OPENED != null && id.equals(Constants.DOCUMENT_OPENED)) {
                        if (DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI").getLength() == 1) {
                            Node tei = DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI").item(0);
                            DocumentSingleton.getInstance().getDocumentElement().removeChild(tei);
                            DocumentSingleton.getInstance().normalizeDocument();
                            CMTManager.getCMT().unlockDiscartingDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, Constants.DOCUMENT_OPENED);
                            Constants.DOCUMENT_OPENED = null;
                            Utils.changeInterviewEvent(this.mainForm, -1);
                        } else {
                            System.out.println("IMPORTANT: There are more than 1 TEI Tag in the DOM Structure!!!!!!!");
                            NodeList nl = DocumentSingleton.getInstance().getDocumentElement().getElementsByTagName("TEI");
                            for (int i = 0; i < nl.getLength(); ++i) {
                                Node tei = nl.item(i);
                                DocumentSingleton.getInstance().getDocumentElement().removeChild(tei);
                                DocumentSingleton.getInstance().normalizeDocument();
                                CMTManager.getCMT().unlockDiscartingDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, Constants.DOCUMENT_OPENED);
                                Constants.DOCUMENT_OPENED = null;
                                Utils.setChangedCorpus(false);
                                Utils.changeInterviewEvent(this.mainForm, -1);
                            }
                        }
                    }
                    try {
                        client.removeDocument(Constants.CMT_USER, Constants.IS_COLLABORATIVE_CORPUS, id);
                    }
                    catch (CMTServerException_Exception e) {
                        JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), e.getMessage(), Internationalization.ERROR_WORD, 2);
                        this.dispose();
                        this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(0));
                        return;
                    }
                }
                XMLViewComponentListener l = new XMLViewComponentListener(this.mainForm);
                l.componentShown(new ComponentEvent(this.mainForm.getXmlPane(), 1));
                this.mainForm.getMainPanel().setCursor(Cursor.getPredefinedCursor(0));
                JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.DELETE_INTERVIEW_INFORMATION_MESSAGE_1, Internationalization.DELETE_INTERVIEW_INFORMATION_TITLE_1, 1, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_INFORM)));
            }
            catch (XPathExpressionException e) {
                e.printStackTrace();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.dispose();
    }

    private void onCancel() {
        this.dispose();
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JButton jButton;
        JComboBox jComboBox;
        JPanel jPanel;
        JButton jButton2;
        JLabel jLabel;
        JLabel jLabel2;
        this.contentPane = jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout(0, 0));
        jPanel.setBorder(BorderFactory.createTitledBorder(null, "Delete Document", 0, 0, null, null));
        JPanel jPanel2 = new JPanel();
        jPanel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel.add((Component)jPanel2, "South");
        this.buttonOK = jButton2 = new JButton();
        jButton2.setText("OK");
        jPanel2.add((Component)jButton2, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.buttonCancel = jButton = new JButton();
        jButton.setText("Cancel");
        jPanel2.add((Component)jButton, new GridConstraints(0, 1, 1, 1, 0, 1, 3, 0, null, null, null));
        JPanel jPanel3 = new JPanel();
        jPanel3.setLayout(new GridLayoutManager(3, 3, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel.add((Component)jPanel3, "Center");
        this.selectField = jLabel2 = new JLabel();
        jLabel2.setText("Select one of following documents");
        jLabel2.setHorizontalAlignment(11);
        jPanel3.add((Component)jLabel2, new GridConstraints(0, 0, 1, 1, 2, 0, 2, 0, null, null, null));
        this.documentComboBox = jComboBox = new JComboBox();
        jPanel3.add(jComboBox, new GridConstraints(1, 0, 1, 1, 1, 1, 2, 0, new Dimension(80, -1), null, null));
        this.iconLabel = jLabel = new JLabel();
        jLabel.setText("");
        jPanel3.add((Component)jLabel, new GridConstraints(0, 1, 2, 1, 8, 0, 0, 0, null, null, null));
        JPanel jPanel4 = new JPanel();
        jPanel4.setLayout(new BorderLayout(0, 0));
        jPanel3.add((Component)jPanel4, new GridConstraints(0, 2, 2, 1, 0, 1, 3, 3, null, null, null));
        Spacer spacer = new Spacer();
        jPanel3.add((Component)spacer, new GridConstraints(2, 0, 1, 1, 0, 2, 1, 2, new Dimension(-1, 20), null, null));
        jLabel2.setLabelFor(jComboBox);
        jLabel.setLabelFor(jComboBox);
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.contentPane;
    }

}

