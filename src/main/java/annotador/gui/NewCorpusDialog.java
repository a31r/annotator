/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.gui.MainForm;
import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.Internationalization;
import annotador.utils.TEIConstants;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Reader;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class NewCorpusDialog
extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField corpusName;
    private JTextField sponsor;
    private JTextField principal;
    private JTextArea publication;
    private JTextArea source;
    private JButton iconButton;
    private JButton exploreButton;
    private JLabel nameLabel;
    private JLabel sponsorLabel;
    private JLabel principalInvestigatorLabel;
    private JLabel publicationStatementLabel;
    private JLabel sourceStatementLabel;
    private JLabel fileNameLabel;
    private JPanel newCorpusPanel;
    String path;
    MainForm mainForm;

    public NewCorpusDialog(final MainForm mainForm, String title) {
        super(mainForm.getVentanaPrincipal(), title, true);
        this.mainForm = mainForm;
        this.$$$setupUI$$$();
        this.iconButton.setIcon(new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_ADD)));
        this.path = null;
        this.nameLabel.setText(Internationalization.CORPUS_NAME);
        this.sponsorLabel.setText(Internationalization.SPONSOR);
        this.principalInvestigatorLabel.setText(Internationalization.PRINCIPAL_INVESTIGATOR);
        this.publicationStatementLabel.setText(Internationalization.PUBLICATION_STATEMENT);
        this.sourceStatementLabel.setText(Internationalization.SOURCE_STATEMENTE);
        this.fileNameLabel.setText(Internationalization.FILE_NAME);
        this.exploreButton.setText(Internationalization.EXPLORE);
        this.buttonOK.setText(Internationalization.OK_WORD);
        this.buttonCancel.setText(Internationalization.CANCEL_WORD);
        this.setContentPane(this.contentPane);
        this.setModal(true);
        this.getRootPane().setDefaultButton(this.buttonOK);
        this.buttonOK.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                NewCorpusDialog.this.onOK();
            }
        });
        this.buttonCancel.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                NewCorpusDialog.this.onCancel();
            }
        });
        this.setDefaultCloseOperation(0);
        this.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosing(WindowEvent e) {
                NewCorpusDialog.this.onCancel();
            }
        });
        this.contentPane.registerKeyboardAction(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                NewCorpusDialog.this.onCancel();
            }
        }, KeyStroke.getKeyStroke(27, 0), 1);
        this.exploreButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setMultiSelectionEnabled(false);
                fileChooser.setDialogTitle(Internationalization.NEW_CORPUS_TITLE);
                fileChooser.setDialogType(1);
                if (0 == fileChooser.showSaveDialog(mainForm.getTabbedPane())) {
                    File file = fileChooser.getSelectedFile();
                    NewCorpusDialog.this.path = file.getAbsolutePath();
                }
            }
        });
        this.pack();
        this.setLocationRelativeTo(mainForm.getTabbedPane());
    }

    public void onOK() {
        block17 : {
            String corpusTitle = this.corpusName.getText();
            String sponsor = this.sponsor.getText();
            String principal = this.principal.getText();
            String publicaton = this.publication.getText();
            String source = this.source.getText();
            try {
                if (this.path == null) {
                    JOptionPane.showMessageDialog(this.mainForm.getTabbedPane(), Internationalization.NEW_CORPUS_ERROR_1, Internationalization.WARNING_WORD, 2, new ImageIcon(Utils.getReourceImage(this.mainForm, Constants.ICON_WARNING)));
                    break block17;
                }
                BufferedReader br = null;
                boolean present = true;
                try {
                    br = new BufferedReader(new InputStreamReader((InputStream)new FileInputStream(Constants.WORKING_PATH + Constants.FILE_NAME_EMPTY_CORPUS), Constants.ENCODING));
                }
                catch (IOException ioe) {
                    br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(Constants.FILE_NAME_EMPTY_CORPUS_RESOURCE), Constants.ENCODING));
                    present = false;
                }
                String aux = "";
                StringBuilder total = new StringBuilder();
                while ((aux = br.readLine()) != null) {
                    total.append(aux);
                    total.append("\n");
                }
                if (!present) {
                    File fileF;
                    String file;
                    String dir = (Constants.WORKING_PATH + Constants.FILE_NAME_EMPTY_CORPUS).substring(0, (Constants.WORKING_PATH + Constants.FILE_NAME_EMPTY_CORPUS).lastIndexOf(File.separator));
                    File dirF = new File(dir);
                    if (!dirF.exists()) {
                        dirF.mkdirs();
                    }
                    if (!(fileF = new File(file = Constants.WORKING_PATH + Constants.FILE_NAME_EMPTY_CORPUS)).exists()) {
                        try {
                            fileF.createNewFile();
                        }
                        catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                    FileOutputStream fout = new FileOutputStream(fileF, false);
                    OutputStreamWriter osw = new OutputStreamWriter((OutputStream)fout, Constants.ENCODING);
                    osw.write(total.toString());
                    osw.flush();
                    osw.close();
                }
                Document doc = DOMUtils.StringToDOM(total.toString(), Constants.ENCODING, Constants.TEI_NAMESPACE);
                XPath xpath = XPathFactory.newInstance().newXPath();
                xpath.setNamespaceContext(new TEINamespaceContext());
                String xpathExpression = "//tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:titleStmt";
                Node title = (Node)xpath.evaluate(xpathExpression, doc, XPathConstants.NODE);
                NodeList titleChild = title.getChildNodes();
                for (int i = 0; i < titleChild.getLength(); ++i) {
                    if (titleChild.item(i).getNodeName().equals(TEIConstants.TEI_TAG_TITLE)) {
                        titleChild.item(i).setTextContent(corpusTitle);
                    }
                    if (titleChild.item(i).getNodeName().equals(TEIConstants.TEI_TAG_SPONSOR)) {
                        titleChild.item(i).setTextContent(sponsor);
                    }
                    if (!titleChild.item(i).getNodeName().equals(TEIConstants.TEI_TAG_PRINCIPAL)) continue;
                    titleChild.item(i).setTextContent(principal);
                }
                xpath = XPathFactory.newInstance().newXPath();
                xpath.setNamespaceContext(new TEINamespaceContext());
                xpathExpression = "//tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:p";
                Node publicationP = (Node)xpath.evaluate(xpathExpression, doc, XPathConstants.NODE);
                publicationP.setTextContent(publicaton);
                xpath = XPathFactory.newInstance().newXPath();
                xpath.setNamespaceContext(new TEINamespaceContext());
                xpathExpression = "//tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:p";
                Node sourceDesc = (Node)xpath.evaluate(xpathExpression, doc, XPathConstants.NODE);
                sourceDesc.setTextContent(source);
                DocumentSingleton.loadFromDOM(doc);
                if (!this.path.toUpperCase().contains(".XML")) {
                    this.path = this.path + ".xml";
                }
                Constants.LAST_OPENED_CORPUS_FILE_PATH = this.path;
                Constants.IS_COLLABORATIVE_CORPUS = null;
                Constants.DOCUMENT_OPENED = null;
                Utils.changeCorpusEvent(this.mainForm);
            }
            catch (XPathExpressionException e) {
                System.out.println("TEITableModel.getValueAt() -> XPathExpressionException column(1)");
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.setVisible(false);
        this.dispose();
    }

    public JTextField getCorpusName() {
        return this.corpusName;
    }

    public void setCorpusName(JTextField corpusName) {
        this.corpusName = corpusName;
    }

    public JTextField getSponsor() {
        return this.sponsor;
    }

    public void setSponsor(JTextField sponsor) {
        this.sponsor = sponsor;
    }

    public JTextField getPrincipal() {
        return this.principal;
    }

    public void setPrincipal(JTextField principal) {
        this.principal = principal;
    }

    public JTextArea getPublication() {
        return this.publication;
    }

    public void setPublication(JTextArea publication) {
        this.publication = publication;
    }

    public JTextArea getSource() {
        return this.source;
    }

    public void setSource(JTextArea source) {
        this.source = source;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    private void onCancel() {
        this.setVisible(false);
        this.dispose();
    }

    private void createUIComponents() {
        this.iconButton = new JButton();
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JLabel jLabel;
        JButton jButton;
        JTextField jTextField;
        JLabel jLabel2;
        JLabel jLabel3;
        JTextArea jTextArea;
        JLabel jLabel4;
        JPanel jPanel;
        JTextArea jTextArea2;
        JTextField jTextField2;
        JTextField jTextField3;
        JButton jButton2;
        JLabel jLabel5;
        JPanel jPanel2;
        JLabel jLabel6;
        JButton jButton3;
        this.createUIComponents();
        this.contentPane = jPanel2 = new JPanel();
        jPanel2.setLayout(new GridLayoutManager(2, 2, new Insets(10, 10, 10, 10), -1, -1, false, false));
        JPanel jPanel3 = new JPanel();
        jPanel3.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel2.add((Component)jPanel3, new GridConstraints(1, 1, 1, 1, 0, 3, 3, 1, null, null, null));
        Spacer spacer = new Spacer();
        jPanel3.add((Component)spacer, new GridConstraints(0, 0, 1, 1, 0, 1, 6, 1, null, null, null));
        JPanel jPanel4 = new JPanel();
        jPanel4.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, true, false));
        jPanel3.add((Component)jPanel4, new GridConstraints(0, 1, 1, 1, 0, 3, 3, 3, null, null, null));
        this.buttonOK = jButton2 = new JButton();
        jButton2.setText("OK");
        jPanel4.add((Component)jButton2, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.buttonCancel = jButton3 = new JButton();
        jButton3.setText("Cancel");
        jPanel4.add((Component)jButton3, new GridConstraints(0, 1, 1, 1, 0, 1, 3, 0, null, null, null));
        this.newCorpusPanel = jPanel = new JPanel();
        jPanel.setLayout(new GridLayoutManager(6, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel2.add((Component)jPanel, new GridConstraints(0, 1, 1, 1, 0, 3, 3, 3, null, null, null));
        jPanel.setBorder(BorderFactory.createTitledBorder(null, "New Corpus", 0, 0, null, null));
        this.corpusName = jTextField = new JTextField();
        jTextField.setColumns(30);
        jPanel.add((Component)jTextField, new GridConstraints(0, 1, 1, 1, 8, 0, 6, 0, null, new Dimension(150, -1), null));
        this.sponsor = jTextField3 = new JTextField();
        jTextField3.setColumns(30);
        jPanel.add((Component)jTextField3, new GridConstraints(1, 1, 1, 1, 8, 0, 6, 0, null, new Dimension(150, -1), null));
        this.principal = jTextField2 = new JTextField();
        jTextField2.setColumns(30);
        jPanel.add((Component)jTextField2, new GridConstraints(2, 1, 1, 1, 8, 0, 6, 0, null, new Dimension(150, -1), null));
        this.nameLabel = jLabel5 = new JLabel();
        jLabel5.setText("Corpus Name");
        jPanel.add((Component)jLabel5, new GridConstraints(0, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.sponsorLabel = jLabel3 = new JLabel();
        jLabel3.setText("Sponsor");
        jPanel.add((Component)jLabel3, new GridConstraints(1, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.principalInvestigatorLabel = jLabel = new JLabel();
        jLabel.setText("Principal Investigator");
        jPanel.add((Component)jLabel, new GridConstraints(2, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.publicationStatementLabel = jLabel6 = new JLabel();
        jLabel6.setText("Publication Statement");
        jPanel.add((Component)jLabel6, new GridConstraints(3, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.sourceStatementLabel = jLabel4 = new JLabel();
        jLabel4.setText("Source Statement");
        jPanel.add((Component)jLabel4, new GridConstraints(4, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        JScrollPane jScrollPane = new JScrollPane();
        jPanel.add((Component)jScrollPane, new GridConstraints(3, 1, 1, 1, 8, 2, 3, 7, null, null, null));
        this.publication = jTextArea2 = new JTextArea();
        jTextArea2.setRows(5);
        jTextArea2.setColumns(30);
        jScrollPane.setViewportView(jTextArea2);
        JScrollPane jScrollPane2 = new JScrollPane();
        jPanel.add((Component)jScrollPane2, new GridConstraints(4, 1, 1, 1, 8, 2, 3, 7, null, null, null));
        this.source = jTextArea = new JTextArea();
        jTextArea.setRows(5);
        jTextArea.setColumns(30);
        jScrollPane2.setViewportView(jTextArea);
        this.fileNameLabel = jLabel2 = new JLabel();
        jLabel2.setText("File Name");
        jPanel.add((Component)jLabel2, new GridConstraints(5, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.exploreButton = jButton = new JButton();
        jButton.setText("Explore...");
        jPanel.add((Component)jButton, new GridConstraints(5, 1, 1, 1, 0, 1, 3, 0, null, null, null));
        JButton jButton4 = this.iconButton;
        jButton4.setText("");
        jPanel2.add((Component)jButton4, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.contentPane;
    }

}

