/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.gui.MainForm;
import annotador.text.TEIJTable;
import annotador.utils.ColorMap;
import annotador.utils.Constants;
import annotador.utils.Internationalization;
import annotador.utils.TEIConstants;
import annotador.utils.Utils;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ModifyTaxonomyDialog
extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField categoryIdField;
    private JTextField categoryNameField;
    private JLabel iconLabel;
    private JButton chooseColorButton;
    private JPanel colorPanel;
    private JLabel categoryNameLabel;
    private JLabel categoryIdLabel;
    private JLabel categoryColorLabel;
    private JPanel modifyPanel;
    private MainForm mainForm;
    private Element node;

    public ModifyTaxonomyDialog(final MainForm mainForm, Element node) {
        super(mainForm.getVentanaPrincipal(), Internationalization.MODIFY_CATEGORY_TITLE, true);
        Element el;
        this.mainForm = mainForm;
        this.node = node;
        this.$$$setupUI$$$();
        this.categoryColorLabel.setText(Internationalization.CATEGORY_COLOR);
        this.categoryIdLabel.setText(Internationalization.CATEGORY_ID);
        this.categoryNameLabel.setText(Internationalization.CATEGORY_NAME);
        this.modifyPanel.setBorder(new TitledBorder(Internationalization.MODIFY_CATEGORY_TITLE));
        this.buttonCancel.setText(Internationalization.CANCEL_WORD);
        this.buttonOK.setText(Internationalization.OK_WORD);
        this.chooseColorButton.setText(Internationalization.CHOOSE_COLOR);
        if (node.getNodeName().equals(TEIConstants.TEI_TAG_TAXONOMY)) {
            el = node;
            this.colorPanel.setBackground(ColorMap.getColor(el.getAttribute(TEIConstants.TEI_ATTRIBUTE_N)));
            this.categoryIdField.setText(el.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID));
            this.categoryNameField.setText(el.getAttribute(TEIConstants.TEI_ATTRIBUTE_N));
        }
        if (node.getNodeName().equals(TEIConstants.TEI_TAG_CATEGORY)) {
            el = node;
            NodeList l = el.getChildNodes();
            String id = el.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID);
            String name = null;
            for (int i = 0; i < l.getLength(); ++i) {
                Node a = l.item(i);
                if (!a.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CATDESC)) continue;
                name = a.getTextContent();
            }
            this.colorPanel.setBackground(ColorMap.getColor(id));
            this.categoryIdField.setText(id);
            this.categoryNameField.setText(name);
        }
        this.setContentPane(this.contentPane);
        this.setModal(true);
        this.getRootPane().setDefaultButton(this.buttonOK);
        this.iconLabel.setIcon(new ImageIcon(Utils.getReourceImage(mainForm, Constants.ICON_ADD)));
        this.pack();
        this.setLocationRelativeTo(mainForm.getTabbedPane());
        this.chooseColorButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Color selectedColor = JColorChooser.showDialog(mainForm.getDocumentTable().getParent(), "Select The Color for the Category", Color.BLACK);
                if (selectedColor != null) {
                    ModifyTaxonomyDialog.this.colorPanel.setBackground(selectedColor);
                }
            }
        });
        this.buttonOK.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                ModifyTaxonomyDialog.this.onOK();
            }
        });
        this.buttonCancel.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                ModifyTaxonomyDialog.this.onCancel();
            }
        });
        this.setDefaultCloseOperation(0);
        this.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosing(WindowEvent e) {
                ModifyTaxonomyDialog.this.onCancel();
            }
        });
        this.contentPane.registerKeyboardAction(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                ModifyTaxonomyDialog.this.onCancel();
            }
        }, KeyStroke.getKeyStroke(27, 0), 1);
    }

    private void onOK() {
        ColorMap.deleteColor(this.categoryIdField.getText());
        if (this.node.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_TAXONOMY)) {
            this.node.setAttribute(TEIConstants.TEI_ATTRIBUTE_N, this.categoryNameField.getText());
            ColorMap.addColor(this.categoryIdField.getText(), this.colorPanel.getBackground());
        }
        if (this.node.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CATEGORY)) {
            Element el = this.node;
            NodeList l = el.getChildNodes();
            String id = el.getAttribute(TEIConstants.TEI_ATTRIBUTE_XML_ID);
            Object name = null;
            for (int i = 0; i < l.getLength(); ++i) {
                Node a = l.item(i);
                if (!a.getNodeName().equalsIgnoreCase(TEIConstants.TEI_TAG_CATDESC)) continue;
                a.setTextContent(this.categoryNameField.getText());
            }
            ColorMap.addColor(this.categoryIdField.getText(), this.colorPanel.getBackground());
        }
        this.setVisible(false);
        JTable table = this.mainForm.getDocumentTable();
        if (table instanceof TEIJTable) {
            TEIJTable t = (TEIJTable)table;
            t.invalidateGraphicsCache();
        }
        Utils.setChangedCorpus(true);
        this.mainForm.getTaxonomyTree().getModel().valueForPathChanged(this.mainForm.getTaxonomyTree().getSelectionPath(), this.node);
        this.mainForm.getTaxonomyTree().clearSelection();
        this.mainForm.getTaxonomyTree().repaint();
        this.mainForm.getDocumentTable().repaint();
    }

    private void onCancel() {
        this.setVisible(false);
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JPanel jPanel;
        JTextField jTextField;
        JLabel jLabel;
        JPanel jPanel2;
        JTextField jTextField2;
        JButton jButton;
        JLabel jLabel2;
        JPanel jPanel3;
        JLabel jLabel3;
        JLabel jLabel4;
        JButton jButton2;
        JButton jButton3;
        this.contentPane = jPanel3 = new JPanel();
        jPanel3.setLayout(new GridLayoutManager(2, 2, new Insets(10, 10, 10, 10), -1, -1, false, false));
        jPanel3.setBorder(BorderFactory.createTitledBorder(null, "", 0, 0, null, null));
        JPanel jPanel4 = new JPanel();
        jPanel4.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel3.add((Component)jPanel4, new GridConstraints(1, 1, 1, 1, 0, 3, 3, 1, null, null, null));
        Spacer spacer = new Spacer();
        jPanel4.add((Component)spacer, new GridConstraints(0, 0, 1, 1, 0, 1, 6, 1, null, null, null));
        JPanel jPanel5 = new JPanel();
        jPanel5.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, true, false));
        jPanel4.add((Component)jPanel5, new GridConstraints(0, 1, 1, 1, 0, 3, 3, 3, null, null, null));
        this.buttonOK = jButton = new JButton();
        jButton.setText("OK");
        jPanel5.add((Component)jButton, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null));
        this.buttonCancel = jButton3 = new JButton();
        jButton3.setText("Cancel");
        jPanel5.add((Component)jButton3, new GridConstraints(0, 1, 1, 1, 0, 1, 3, 0, null, null, null));
        this.modifyPanel = jPanel2 = new JPanel();
        jPanel2.setLayout(new GridLayoutManager(3, 3, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel3.add((Component)jPanel2, new GridConstraints(0, 1, 1, 1, 0, 3, 3, 3, null, null, null));
        jPanel2.setBorder(BorderFactory.createTitledBorder(null, "Modify Category", 0, 0, null, null));
        this.categoryIdField = jTextField = new JTextField();
        jTextField.setEnabled(false);
        jTextField.setEditable(false);
        jPanel2.add((Component)jTextField, new GridConstraints(1, 0, 1, 2, 4, 0, 6, 0, null, new Dimension(150, -1), null));
        this.categoryNameField = jTextField2 = new JTextField();
        jTextField2.setEnabled(true);
        jTextField2.setEditable(true);
        jPanel2.add((Component)jTextField2, new GridConstraints(0, 0, 1, 2, 4, 0, 6, 0, null, new Dimension(150, -1), null));
        this.categoryIdLabel = jLabel4 = new JLabel();
        jLabel4.setText("Category Id");
        jPanel2.add((Component)jLabel4, new GridConstraints(1, 2, 1, 1, 8, 0, 0, 0, null, null, null));
        this.categoryNameLabel = jLabel2 = new JLabel();
        jLabel2.setText("Category Name");
        jPanel2.add((Component)jLabel2, new GridConstraints(0, 2, 1, 1, 8, 0, 0, 0, null, null, null));
        this.colorPanel = jPanel = new JPanel();
        jPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel2.add((Component)jPanel, new GridConstraints(2, 0, 1, 1, 0, 3, 3, 3, null, null, null));
        this.chooseColorButton = jButton2 = new JButton();
        jButton2.setText("Choose Color");
        jPanel2.add((Component)jButton2, new GridConstraints(2, 1, 1, 1, 0, 1, 3, 0, null, null, null));
        this.categoryColorLabel = jLabel3 = new JLabel();
        jLabel3.setText("Category Color");
        jPanel2.add((Component)jLabel3, new GridConstraints(2, 2, 1, 1, 8, 0, 0, 0, null, null, null));
        this.iconLabel = jLabel = new JLabel();
        jLabel.setText("");
        jPanel3.add((Component)jLabel, new GridConstraints(0, 0, 1, 1, 8, 0, 0, 0, null, null, null));
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.contentPane;
    }

}

