/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.gui.MainForm;
import annotador.utils.Internationalization;
import annotador.utils.Utils;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ModifyPersonForm
implements ActionListener {
    private JTextField nameField;
    private JTextArea descriptionField;
    private JTextField idField;
    private JTextField ageField;
    private JTextField sexField;
    private JTextField roleField;
    private JButton saveChangesButton;
    private JPanel mainPanel;
    private JLabel nameLabel;
    private JLabel descriptionLabel;
    private JLabel roleLabel;
    private JLabel sexLabel;
    private JLabel ageLabel;
    private JLabel idLabel;
    Element personE;
    JDialog dialog;
    MainForm mainForm;

    public ModifyPersonForm(Element personE, JFrame owner, MainForm mainForm) {
        String sex;
        this.mainForm = mainForm;
        this.personE = personE;
        this.$$$setupUI$$$();
        this.dialog = new JDialog(owner, Internationalization.PERSON_INFORMATION_TITLE, true);
        this.dialog.setContentPane(this.mainPanel);
        this.nameLabel.setText(Internationalization.NAME_WORD);
        this.descriptionLabel.setText(Internationalization.DESCRIPTION_WORD);
        this.roleLabel.setText(Internationalization.ROLE_WORD);
        this.sexLabel.setText(Internationalization.SEX_PERSON);
        this.ageLabel.setText(Internationalization.AGE_WORD);
        this.idLabel.setText(Internationalization.ID_PERSON);
        this.saveChangesButton.setText(Internationalization.SAVE_CHANGES);
        TitledBorder t = new TitledBorder(Internationalization.PERSON_TITLE);
        this.mainPanel.setBorder(t);
        this.saveChangesButton.addActionListener(this);
        this.idField.setText(personE.getAttribute("rend"));
        this.ageField.setText(personE.getAttribute("age"));
        this.roleField.setText(personE.getAttribute("role"));
        NodeList l = personE.getElementsByTagName("persName");
        if (l.getLength() > 0) {
            this.nameField.setText(l.item(0).getTextContent());
        }
        if ((sex = personE.getAttribute("sex")).equals("1")) {
            this.sexField.setText("M");
        }
        if (sex.equals("2")) {
            this.sexField.setText("F");
        }
        if (!(sex.equals("") || sex.equals("1") || sex.equals("2"))) {
            this.sexField.setBackground(Color.RED);
        }
        if ((l = personE.getElementsByTagName("note")).getLength() > 0) {
            this.descriptionField.setText(l.item(0).getTextContent());
        }
        this.dialog.pack();
        this.dialog.setLocationRelativeTo(mainForm.getTabbedPane());
        this.dialog.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Element persName;
        Utils.setChangedCorpus(true);
        if (!this.idField.getText().equals("")) {
            this.personE.setAttribute("rend", this.idField.getText());
        } else {
            this.personE.removeAttribute("rend");
        }
        if (!this.ageField.getText().equals("")) {
            this.personE.setAttribute("age", this.ageField.getText());
        } else {
            this.personE.removeAttribute("age");
        }
        if (!this.roleField.getText().equals("")) {
            this.personE.setAttribute("role", this.roleField.getText());
        } else {
            this.personE.removeAttribute("role");
        }
        NodeList l = this.personE.getElementsByTagName("persName");
        if (l.getLength() > 0) {
            l.item(0).setTextContent(this.nameField.getText());
        } else {
            persName = this.personE.getOwnerDocument().createElement("persName");
            this.personE.appendChild(persName);
            persName.setTextContent(this.nameField.getText());
        }
        l = this.personE.getElementsByTagName("note");
        if (l.getLength() > 0) {
            l.item(0).setTextContent(this.descriptionField.getText());
        } else {
            persName = this.personE.getOwnerDocument().createElement("note");
            this.personE.appendChild(persName);
            persName.setTextContent(this.descriptionField.getText());
        }
        if (this.sexField.getText().equalsIgnoreCase("M")) {
            this.personE.setAttribute("sex", "1");
        }
        if (this.sexField.getText().equalsIgnoreCase("F")) {
            this.personE.setAttribute("sex", "2");
        }
        if (!this.sexField.getText().equalsIgnoreCase("M") && !this.sexField.getText().equalsIgnoreCase("F")) {
            this.personE.removeAttribute("sex");
        }
        this.dialog.setVisible(false);
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JLabel jLabel;
        JTextField jTextField;
        JTextArea jTextArea;
        JLabel jLabel2;
        JLabel jLabel3;
        JTextField jTextField2;
        JButton jButton;
        JTextField jTextField3;
        JPanel jPanel;
        JLabel jLabel4;
        JLabel jLabel5;
        JTextField jTextField4;
        JLabel jLabel6;
        JTextField jTextField5;
        this.mainPanel = jPanel = new JPanel();
        jPanel.setLayout(new GridLayoutManager(3, 4, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel.setBorder(BorderFactory.createTitledBorder(null, "Person Information", 0, 0, null, null));
        JPanel jPanel2 = new JPanel();
        jPanel2.setLayout(new GridLayoutManager(6, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel.add((Component)jPanel2, new GridConstraints(0, 1, 1, 2, 0, 3, 3, 3, null, null, null));
        this.roleField = jTextField4 = new JTextField();
        jPanel2.add((Component)jTextField4, new GridConstraints(2, 1, 1, 1, 8, 1, 6, 0, null, new Dimension(215, 20), null));
        this.sexField = jTextField2 = new JTextField();
        jPanel2.add((Component)jTextField2, new GridConstraints(3, 1, 1, 1, 8, 1, 6, 0, null, new Dimension(215, 20), null));
        this.ageField = jTextField = new JTextField();
        jPanel2.add((Component)jTextField, new GridConstraints(4, 1, 1, 1, 8, 1, 6, 0, null, new Dimension(215, 20), null));
        this.idField = jTextField5 = new JTextField();
        jPanel2.add((Component)jTextField5, new GridConstraints(5, 1, 1, 1, 8, 1, 6, 0, null, new Dimension(215, 20), null));
        this.idLabel = jLabel5 = new JLabel();
        jLabel5.setText("ID in Text");
        jPanel2.add((Component)jLabel5, new GridConstraints(5, 0, 1, 1, 8, 0, 0, 0, null, new Dimension(215, 14), null));
        this.ageLabel = jLabel6 = new JLabel();
        jLabel6.setText("Age");
        jPanel2.add((Component)jLabel6, new GridConstraints(4, 0, 1, 1, 8, 0, 0, 0, null, new Dimension(215, 14), null));
        this.sexLabel = jLabel3 = new JLabel();
        jLabel3.setText("Sex (M|F)");
        jPanel2.add((Component)jLabel3, new GridConstraints(3, 0, 1, 1, 8, 0, 0, 0, null, new Dimension(215, 14), null));
        this.roleLabel = jLabel4 = new JLabel();
        jLabel4.setText("Role");
        jPanel2.add((Component)jLabel4, new GridConstraints(2, 0, 1, 1, 8, 0, 0, 0, null, null, null));
        this.nameField = jTextField3 = new JTextField();
        jPanel2.add((Component)jTextField3, new GridConstraints(0, 1, 1, 1, 8, 1, 6, 0, null, null, null));
        this.nameLabel = jLabel2 = new JLabel();
        jLabel2.setText("Name");
        jPanel2.add((Component)jLabel2, new GridConstraints(0, 0, 1, 1, 8, 0, 0, 0, null, new Dimension(215, 14), null));
        this.descriptionLabel = jLabel = new JLabel();
        jLabel.setText("Description");
        jPanel2.add((Component)jLabel, new GridConstraints(1, 0, 1, 1, 9, 0, 0, 0, null, new Dimension(215, 14), null));
        JScrollPane jScrollPane = new JScrollPane();
        jScrollPane.setVerticalScrollBarPolicy(22);
        jPanel2.add((Component)jScrollPane, new GridConstraints(1, 1, 1, 1, 0, 3, 7, 7, null, null, null));
        this.descriptionField = jTextArea = new JTextArea();
        jTextArea.setRows(3);
        jScrollPane.setViewportView(jTextArea);
        this.saveChangesButton = jButton = new JButton();
        jButton.setText("Save Changes");
        jPanel.add((Component)jButton, new GridConstraints(1, 1, 1, 1, 0, 1, 3, 3, null, new Dimension(215, 25), null));
        Spacer spacer = new Spacer();
        jPanel.add((Component)spacer, new GridConstraints(0, 0, 2, 1, 0, 1, 6, 1, null, null, null));
        Spacer spacer2 = new Spacer();
        jPanel.add((Component)spacer2, new GridConstraints(0, 3, 1, 1, 0, 1, 6, 1, null, null, null));
        Spacer spacer3 = new Spacer();
        jPanel.add((Component)spacer3, new GridConstraints(2, 1, 1, 1, 0, 2, 1, 6, new Dimension(-1, 30), new Dimension(-1, 30), null));
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.mainPanel;
    }
}

