/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.utils.Internationalization;
import java.awt.Checkbox;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.List;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class CustomFontChooserDialog
extends JDialog {
    public Font resultFont;
    protected String resultName;
    protected int resultSize;
    protected boolean isBold;
    protected boolean isItalic;
    protected String displayText = "BACKBONE Annotator 3.8";
    protected String[] fontList;
    protected List fontNameChoice;
    protected List fontSizeChoice;
    Checkbox bold;
    Checkbox italic;
    protected String[] fontSizes = new String[]{"8", "10", "11", "12", "14", "16", "18", "20", "24", "30", "36", "40", "48", "60", "72"};
    protected static final int DEFAULT_SIZE = 4;
    protected JLabel previewArea;

    public CustomFontChooserDialog(Frame f) {
        super(f, Internationalization.FONT_CHOOSER_TITLE, true);
        int i;
        Container cp = this.getContentPane();
        Panel top = new Panel();
        top.setLayout(new FlowLayout());
        this.fontNameChoice = new List(8);
        top.add(this.fontNameChoice);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        this.fontList = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        for (i = 0; i < this.fontList.length; ++i) {
            this.fontNameChoice.add(this.fontList[i]);
        }
        this.fontNameChoice.select(0);
        this.fontSizeChoice = new List(8);
        top.add(this.fontSizeChoice);
        for (i = 0; i < this.fontSizes.length; ++i) {
            this.fontSizeChoice.add(this.fontSizes[i]);
        }
        this.fontSizeChoice.select(4);
        cp.add((Component)top, "North");
        Panel attrs = new Panel();
        top.add(attrs);
        attrs.setLayout(new GridLayout(0, 1));
        this.bold = new Checkbox("Bold", false);
        attrs.add(this.bold);
        this.italic = new Checkbox("Italic", false);
        attrs.add(this.italic);
        this.previewArea = new JLabel(this.displayText, 0);
        this.previewArea.setSize(200, 50);
        cp.add((Component)this.previewArea, "Center");
        Panel bot = new Panel();
        JButton okButton = new JButton(Internationalization.APPLY_WORD);
        bot.add(okButton);
        okButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                CustomFontChooserDialog.this.previewFont();
                CustomFontChooserDialog.this.dispose();
                CustomFontChooserDialog.this.setVisible(false);
            }
        });
        JButton pvButton = new JButton(Internationalization.PREVIEW_WORD);
        bot.add(pvButton);
        pvButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                CustomFontChooserDialog.this.previewFont();
            }
        });
        JButton canButton = new JButton(Internationalization.CANCEL_WORD);
        bot.add(canButton);
        canButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                CustomFontChooserDialog.this.resultFont = null;
                CustomFontChooserDialog.this.resultName = null;
                CustomFontChooserDialog.this.resultSize = 0;
                CustomFontChooserDialog.this.isBold = false;
                CustomFontChooserDialog.this.isItalic = false;
                CustomFontChooserDialog.this.dispose();
                CustomFontChooserDialog.this.setVisible(false);
            }
        });
        cp.add((Component)bot, "South");
        this.previewFont();
        this.pack();
        this.setLocation(100, 100);
    }

    protected void previewFont() {
        this.resultName = this.fontNameChoice.getSelectedItem();
        String resultSizeName = this.fontSizeChoice.getSelectedItem();
        this.resultSize = Integer.parseInt(resultSizeName);
        this.isBold = this.bold.getState();
        this.isItalic = this.italic.getState();
        int attrs = 0;
        if (this.isBold) {
            attrs = 1;
        }
        if (this.isItalic) {
            attrs |= 2;
        }
        this.resultFont = new Font(this.resultName, attrs, this.resultSize);
        this.previewArea.setFont(this.resultFont);
        this.pack();
    }

    public String getSelectedName() {
        return this.resultName;
    }

    public int getSelectedSize() {
        return this.resultSize;
    }

    public Font getSelectedFont() {
        return this.resultFont;
    }

    public static void main(String[] args) {
        final JFrame f = new JFrame("FontChooser Startup");
        final CustomFontChooserDialog fc = new CustomFontChooserDialog(f);
        Container cp = f.getContentPane();
        cp.setLayout(new GridLayout(0, 1));
        JButton theButton = new JButton("Change font");
        cp.add(theButton);
        final JLabel theLabel = new JLabel("Java is great!", 0);
        cp.add(theLabel);
        theButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                fc.setVisible(true);
                Font myNewFont = fc.getSelectedFont();
                System.out.println("You chose " + myNewFont);
                theLabel.setFont(myNewFont);
                f.pack();
                fc.dispose();
            }
        });
        f.setSize(150, 100);
        f.setVisible(true);
        f.setDefaultCloseOperation(3);
    }

}

