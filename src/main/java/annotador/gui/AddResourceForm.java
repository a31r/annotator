/*
 * Decompiled with CFR 0_118.
 */
package annotador.gui;

import annotador.action.resources.AddResourceAction;
import annotador.gui.MainForm;
import annotador.resources.TypeResourcesModel;
import annotador.utils.Internationalization;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import org.w3c.dom.Node;

public class AddResourceForm {
    private JTextField nameField;
    private JComboBox typeField;
    private JTextField descriptionField;
    private JTextField linkField;
    private JButton addResourceButton;
    private JPanel mainPanel;
    private JLabel nameLabel;
    private JLabel typeLabel;
    private JLabel descriptionLabel;
    private JLabel addressLabel;
    private JPanel newResoucePanel;
    public JDialog mainDialog;
    Node div;
    MainForm mainForm;

    public JDialog getMainDialog() {
        return this.mainDialog;
    }

    public void setMainDialog(JDialog mainDialog) {
        this.mainDialog = mainDialog;
    }

    public JTextField getNameField() {
        return this.nameField;
    }

    public void setNameField(JTextField nameField) {
        this.nameField = nameField;
    }

    public JComboBox getTypeField() {
        return this.typeField;
    }

    public JTextField getDescriptionField() {
        return this.descriptionField;
    }

    public void setDescriptionField(JTextField descriptionField) {
        this.descriptionField = descriptionField;
    }

    public JTextField getLinkField() {
        return this.linkField;
    }

    public void setLinkField(JTextField linkField) {
        this.linkField = linkField;
    }

    public AddResourceForm(MainForm mainForm, JTable table, Node div) {
        this.div = div;
        this.mainForm = mainForm;
        this.$$$setupUI$$$();
        this.addressLabel.setText(Internationalization.LINK_WORD);
        this.nameLabel.setText(Internationalization.NAME_WORD);
        this.descriptionLabel.setText(Internationalization.DESCRIPTION_WORD);
        this.typeLabel.setText(Internationalization.TYPE_WORD);
        this.addResourceButton.setText(Internationalization.ADD_RESOURCE);
        TitledBorder t = new TitledBorder(Internationalization.NEW_RESOURCE_TITLE);
        this.newResoucePanel.setBorder(t);
        try {
            this.mainDialog = new JDialog(mainForm.getVentanaPrincipal(), Internationalization.ADD_NEW_RESOURCE, true);
        }
        catch (ClassCastException cce) {
            this.mainDialog = new JDialog(mainForm.getVentanaPrincipal(), Internationalization.ADD_NEW_RESOURCE, true);
        }
        this.typeField.setModel(new TypeResourcesModel());
        this.mainDialog.setContentPane(this.mainPanel);
        this.mainDialog.pack();
        this.mainDialog.setLocationRelativeTo(mainForm.getTabbedPane());
        this.addResourceButton.addActionListener(new AddResourceAction(mainForm, table, this, this.div));
        this.mainDialog.setVisible(true);
    }

    private /* synthetic */ void $$$setupUI$$$() {
        JPanel jPanel;
        JComboBox jComboBox;
        JLabel jLabel;
        JTextField jTextField;
        JTextField jTextField2;
        JButton jButton;
        JLabel jLabel2;
        JPanel jPanel2;
        JLabel jLabel3;
        JLabel jLabel4;
        JTextField jTextField3;
        this.mainPanel = jPanel2 = new JPanel();
        jPanel2.setLayout(new GridLayoutManager(2, 7, new Insets(0, 0, 0, 0), -1, -1, false, false));
        this.addResourceButton = jButton = new JButton();
        jButton.setText("Add Resource");
        jPanel2.add((Component)jButton, new GridConstraints(1, 2, 1, 3, 0, 1, 3, 0, null, null, null));
        this.newResoucePanel = jPanel = new JPanel();
        jPanel.setLayout(new GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1, false, false));
        jPanel2.add((Component)jPanel, new GridConstraints(0, 1, 1, 5, 0, 3, 3, 3, null, null, null));
        jPanel.setBorder(BorderFactory.createTitledBorder(null, "New Resource", 0, 0, null, null));
        this.nameField = jTextField = new JTextField();
        jPanel.add((Component)jTextField, new GridConstraints(0, 0, 1, 1, 8, 1, 6, 0, null, new Dimension(150, -1), null));
        this.typeField = jComboBox = new JComboBox();
        jComboBox.setEditable(true);
        jPanel.add(jComboBox, new GridConstraints(1, 0, 1, 1, 8, 1, 6, 0, null, new Dimension(150, -1), null));
        this.descriptionField = jTextField3 = new JTextField();
        jPanel.add((Component)jTextField3, new GridConstraints(2, 0, 1, 1, 8, 1, 6, 0, null, new Dimension(150, -1), null));
        this.linkField = jTextField2 = new JTextField();
        jPanel.add((Component)jTextField2, new GridConstraints(3, 0, 1, 1, 8, 1, 6, 0, null, new Dimension(150, -1), null));
        this.nameLabel = jLabel4 = new JLabel();
        jLabel4.setText("Name");
        jPanel.add((Component)jLabel4, new GridConstraints(0, 1, 1, 1, 8, 0, 0, 0, null, null, null));
        this.typeLabel = jLabel = new JLabel();
        jLabel.setText("Type");
        jPanel.add((Component)jLabel, new GridConstraints(1, 1, 1, 1, 8, 0, 0, 0, null, null, null));
        this.descriptionLabel = jLabel3 = new JLabel();
        jLabel3.setText("Description");
        jPanel.add((Component)jLabel3, new GridConstraints(2, 1, 1, 1, 8, 0, 0, 0, null, null, null));
        this.addressLabel = jLabel2 = new JLabel();
        jLabel2.setText("link");
        jPanel.add((Component)jLabel2, new GridConstraints(3, 1, 1, 1, 8, 0, 0, 0, null, null, null));
        Spacer spacer = new Spacer();
        jPanel2.add((Component)spacer, new GridConstraints(0, 6, 1, 1, 0, 1, 6, 1, null, null, null));
        Spacer spacer2 = new Spacer();
        jPanel2.add((Component)spacer2, new GridConstraints(0, 0, 1, 1, 0, 1, 6, 1, null, null, null));
        Spacer spacer3 = new Spacer();
        jPanel2.add((Component)spacer3, new GridConstraints(1, 0, 1, 2, 0, 1, 6, 1, null, null, null));
        Spacer spacer4 = new Spacer();
        jPanel2.add((Component)spacer4, new GridConstraints(1, 5, 1, 2, 0, 1, 6, 1, null, null, null));
    }

    public /* synthetic */ JComponent $$$getRootComponent$$$() {
        return this.mainPanel;
    }
}

