/*
 * Decompiled with CFR 0_118.
 */
package table.model;

public interface CellSpan {
    public static final int ROW = 0;
    public static final int COLUMN = 1;

    public int[] getSpan(int var1, int var2);

    public void setSpan(int[] var1, int var2, int var3);

    public boolean isVisible(int var1, int var2);

    public void combine(int[] var1, int[] var2);

    public void split(int var1, int var2);
}

