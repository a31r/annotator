/*
 * Decompiled with CFR 0_118.
 */
package table.model;

import java.awt.Color;

public interface ColoredCell {
    public Color getForeground(int var1, int var2);

    public void setForeground(Color var1, int var2, int var3);

    public void setForeground(Color var1, int[] var2, int[] var3);

    public Color getBackground(int var1, int var2);

    public void setBackground(Color var1, int var2, int var3);

    public void setBackground(Color var1, int[] var2, int[] var3);
}

