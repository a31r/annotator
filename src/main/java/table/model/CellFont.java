/*
 * Decompiled with CFR 0_118.
 */
package table.model;

import java.awt.Font;

public interface CellFont {
    public Font getFont(int var1, int var2);

    public void setFont(Font var1, int var2, int var3);

    public void setFont(Font var1, int[] var2, int[] var3);
}

