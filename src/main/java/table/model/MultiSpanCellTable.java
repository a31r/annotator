/*
 * Decompiled with CFR 0_118.
 */
package table.model;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Enumeration;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.plaf.TableUI;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import table.model.AttributiveCellTableModel;
import table.model.CellAttribute;
import table.model.CellSpan;
import table.model.MultiSpanCellTableUI;

public class MultiSpanCellTable
extends JTable {
    public MultiSpanCellTable(TableModel model, JScrollPane scroll) {
        super(model);
        this.setUI(new MultiSpanCellTableUI(scroll));
        this.getColumnModel().setColumnMargin(0);
        this.setIntercellSpacing(new Dimension(0, 0));
        this.setRowMargin(0);
        this.getTableHeader().setReorderingAllowed(false);
        this.setCellSelectionEnabled(true);
        this.setSelectionMode(1);
    }

    @Override
    public Rectangle getCellRect(int row, int column, boolean includeSpacing) {
        int i;
        Rectangle sRect = super.getCellRect(row, column, includeSpacing);
        if (row < 0 || column < 0 || this.getRowCount() <= row || this.getColumnCount() <= column) {
            return sRect;
        }
        CellSpan cellAtt = (CellSpan)((Object)((AttributiveCellTableModel)this.getModel()).getCellAttribute());
        if (!cellAtt.isVisible(row, column)) {
            int temp_row = row;
            int temp_column = column;
            row += cellAtt.getSpan(temp_row, temp_column)[0];
            column += cellAtt.getSpan(temp_row, temp_column)[1];
        }
        int[] n = cellAtt.getSpan(row, column);
        int index = 0;
        int columnMargin = this.getColumnModel().getColumnMargin();
        Rectangle cellFrame = new Rectangle();
        cellFrame.y = 0;
        for (i = 0; i < row; ++i) {
            cellFrame.y += this.getRowHeight(i) + this.rowMargin;
        }
        cellFrame.height = 0;
        for (i = row; i < row + n[0]; ++i) {
            cellFrame.height += this.getRowHeight(i) + this.rowMargin;
        }
        Enumeration<TableColumn> enumeration = this.getColumnModel().getColumns();
        while (enumeration.hasMoreElements()) {
            TableColumn aColumn = enumeration.nextElement();
            cellFrame.width = aColumn.getWidth() + columnMargin;
            if (index == column) break;
            cellFrame.x += cellFrame.width;
            ++index;
        }
        for (int i2 = 0; i2 < n[1] - 1; ++i2) {
            TableColumn aColumn = enumeration.nextElement();
            cellFrame.width += aColumn.getWidth() + columnMargin;
        }
        if (!includeSpacing) {
            Dimension spacing = this.getIntercellSpacing();
            cellFrame.setBounds(cellFrame.x + spacing.width / 2, cellFrame.y + spacing.height / 2, cellFrame.width - spacing.width, cellFrame.height - spacing.height);
        }
        return cellFrame;
    }

    private int[] rowColumnAtPoint(Point point) {
        int[] retValue = new int[]{-1, -1};
        int row = super.rowAtPoint(point);
        if (row < 0 || this.getRowCount() <= row) {
            return retValue;
        }
        int column = this.getColumnModel().getColumnIndexAtX(point.x);
        CellSpan cellAtt = (CellSpan)((Object)((AttributiveCellTableModel)this.getModel()).getCellAttribute());
        if (cellAtt.isVisible(row, column)) {
            retValue[1] = column;
            retValue[0] = row;
        } else {
            retValue[1] = column + cellAtt.getSpan(row, column)[1];
            retValue[0] = row + cellAtt.getSpan(row, column)[0];
        }
        return retValue;
    }

    @Override
    public int rowAtPoint(Point point) {
        return this.rowColumnAtPoint(point)[0];
    }

    @Override
    public int columnAtPoint(Point point) {
        return this.rowColumnAtPoint(point)[1];
    }

    @Override
    public void columnSelectionChanged(ListSelectionEvent e) {
        this.repaint();
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        int i;
        int row;
        boolean isAdjusting = e.getValueIsAdjusting();
        if (isAdjusting || this.getModel() instanceof DefaultTableModel) {
            return;
        }
        int firstIndex = e.getFirstIndex();
        int lastIndex = e.getLastIndex();
        if (firstIndex == -1 && lastIndex == -1) {
            this.repaint();
        }
        CellSpan cellAtt = (CellSpan)((Object)((AttributiveCellTableModel)this.getModel()).getCellAttribute());
        int minus = firstIndex;
        int max = lastIndex;
        for (i = 0; i < this.getColumnCount(); ++i) {
            if (cellAtt.isVisible(firstIndex, i) || (row = firstIndex + cellAtt.getSpan(firstIndex, i)[0]) >= minus) continue;
            minus = row;
        }
        for (i = 0; i < this.getColumnCount(); ++i) {
            if (cellAtt.isVisible(lastIndex, i)) continue;
            row = lastIndex + cellAtt.getSpan(firstIndex, i)[0];
            if ((row += cellAtt.getSpan(row, i)[0]) <= max) continue;
            max = row;
        }
        Rectangle firstRowRect = this.getCellRect(minus, 0, false);
        Rectangle lastRowRect = this.getCellRect(max, this.getColumnCount() - 1, false);
        Rectangle dirtyRegion = firstRowRect.union(lastRowRect);
        this.repaint(dirtyRegion);
    }

    @Override
    public void setShowGrid(boolean showGrid) {
        super.setShowGrid(showGrid);
        this.setShowHorizontalLines(showGrid);
        this.setShowVerticalLines(showGrid);
    }
}

