/*
 * Decompiled with CFR 0_118.
 */
package table.model;

import java.awt.Dimension;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import table.model.CellAttribute;
import table.model.DefaultCellAttribute;

public class AttributiveCellTableModel
extends DefaultTableModel {
    protected CellAttribute cellAtt;

    public AttributiveCellTableModel() {
        this((Vector)null, 0);
    }

    public AttributiveCellTableModel(int numRows, int numColumns) {
        Vector names = new Vector(numColumns);
        names.setSize(numColumns);
        this.setColumnIdentifiers(names);
        this.dataVector = new Vector();
        this.setNumRows(numRows);
        this.cellAtt = new DefaultCellAttribute(numRows, numColumns);
    }

    public AttributiveCellTableModel(Vector columnNames, int numRows) {
        this.setColumnIdentifiers(columnNames);
        this.dataVector = new Vector();
        this.setNumRows(numRows);
        this.cellAtt = new DefaultCellAttribute(numRows, columnNames.size());
    }

    public AttributiveCellTableModel(Object[] columnNames, int numRows) {
        this(AttributiveCellTableModel.convertToVector(columnNames), numRows);
    }

    public AttributiveCellTableModel(Vector data, Vector columnNames) {
        this.setDataVector(data, columnNames);
    }

    public AttributiveCellTableModel(Object[][] data, Object[] columnNames) {
        this.setDataVector(data, columnNames);
    }

    @Override
    public void setDataVector(Vector newData, Vector columnNames) {
        this.dataVector = newData != null ? newData : new Vector();
        this.columnIdentifiers = columnNames != null ? columnNames : new Vector();
        this.cellAtt = new DefaultCellAttribute(this.dataVector.size(), this.columnIdentifiers.size());
        this.newRowsAdded(new TableModelEvent(this, 0, this.getRowCount() - 1, -1, 1));
    }

    @Override
    public void addColumn(Object columnName, Vector columnData) {
        if (columnName == null) {
            throw new IllegalArgumentException("addColumn() - null parameter");
        }
        this.columnIdentifiers.addElement(columnName);
        int index = 0;
        Enumeration enumeration = this.dataVector.elements();
        while (enumeration.hasMoreElements()) {
            Object value = columnData != null && index < columnData.size() ? (Object)columnData.elementAt(index) : null;
            ((Vector)enumeration.nextElement()).addElement(value);
            ++index;
        }
        this.cellAtt.addColumn();
        this.fireTableStructureChanged();
    }

    @Override
    public void addRow(Vector rowData) {
        Vector newData = null;
        if (rowData == null) {
            newData = new Vector(this.getColumnCount());
        } else {
            rowData.setSize(this.getColumnCount());
        }
        this.dataVector.addElement(newData);
        this.cellAtt.addRow();
        this.newRowsAdded(new TableModelEvent(this, this.getRowCount() - 1, this.getRowCount() - 1, -1, 1));
    }

    @Override
    public void insertRow(int row, Vector rowData) {
        if (rowData == null) {
            rowData = new Vector(this.getColumnCount());
        } else {
            rowData.setSize(this.getColumnCount());
        }
        this.dataVector.insertElementAt(rowData, row);
        this.cellAtt.insertRow(row);
        this.newRowsAdded(new TableModelEvent(this, row, row, -1, 1));
    }

    public CellAttribute getCellAttribute() {
        return this.cellAtt;
    }

    public void setCellAttribute(CellAttribute newCellAtt) {
        int numColumns = this.getColumnCount();
        int numRows = this.getRowCount();
        if (newCellAtt.getSize().width != numColumns || newCellAtt.getSize().height != numRows) {
            newCellAtt.setSize(new Dimension(numRows, numColumns));
        }
        this.cellAtt = newCellAtt;
        this.fireTableDataChanged();
    }
}

