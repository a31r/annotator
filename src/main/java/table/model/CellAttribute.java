/*
 * Decompiled with CFR 0_118.
 */
package table.model;

import java.awt.Dimension;

public interface CellAttribute {
    public void addColumn();

    public void addRow();

    public void insertRow(int var1);

    public Dimension getSize();

    public void setSize(Dimension var1);
}

