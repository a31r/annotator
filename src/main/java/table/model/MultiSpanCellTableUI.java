/*
 * Decompiled with CFR 0_118.
 */
package table.model;

import annotador.utils.Constants;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.PrintStream;
import java.util.Date;
import javax.swing.CellRendererPane;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.plaf.basic.BasicTableUI;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import table.model.AttributiveCellTableModel;
import table.model.CellAttribute;
import table.model.CellSpan;

public class MultiSpanCellTableUI
extends BasicTableUI {
    JScrollPane scroll;
    int spacingHeight;
    int spacingWidth;

    public MultiSpanCellTableUI(JScrollPane scroll) {
        this.scroll = scroll;
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        int lastIndex;
        Date start = new Date();
        this.rendererPane.removeAll();
        this.spacingHeight = this.table.getRowMargin();
        this.spacingWidth = this.table.getColumnModel().getColumnMargin();
        Rectangle clipBounds = g.getClipBounds();
        int firstIndex = this.table.rowAtPoint(new Point(clipBounds.x, clipBounds.y));
        if (this.scroll.getViewport() != null) {
            lastIndex = this.table.rowAtPoint(new Point(clipBounds.x + clipBounds.width, clipBounds.y + clipBounds.height));
            if (lastIndex == 0 || lastIndex == -1) {
                lastIndex = this.table.getRowCount() - 1;
            }
        } else {
            lastIndex = this.table.getRowCount() - 1;
        }
        for (int index = firstIndex; index <= lastIndex; ++index) {
            this.paintRow(g, index);
        }
        if (Constants.DEBUG) {
            Date end = new Date();
            long gap = end.getTime() - start.getTime();
            System.out.println("MultiSpanCellTableUI.paint() -> From Row " + firstIndex + " To Row " + lastIndex + " In " + gap + " ms");
        }
    }

    private void paintRow(Graphics g, int row) {
        Rectangle rect = g.getClipBounds();
        boolean drawn = false;
        AttributiveCellTableModel tableModel = (AttributiveCellTableModel)this.table.getModel();
        CellSpan cellAtt = (CellSpan)((Object)tableModel.getCellAttribute());
        int numColumns = this.table.getColumnCount();
        for (int column = 0; column < numColumns; ++column) {
            int cellColumn;
            int cellRow;
            Rectangle cellRect = this.table.getCellRect(row, column, true);
            if (cellAtt.isVisible(row, column)) {
                cellRow = row;
                cellColumn = column;
            } else {
                cellRow = row + cellAtt.getSpan(row, column)[0];
                cellColumn = column + cellAtt.getSpan(row, column)[1];
            }
            if (cellRect.intersects(rect)) {
                drawn = true;
                this.paintCell(g, cellRect, cellRow, cellColumn);
                continue;
            }
            if (drawn) break;
        }
    }

    private void paintCell(Graphics g, Rectangle cellRect, int row, int column) {
        Date start = new Date();
        if (this.table.getShowHorizontalLines() && this.table.getShowVerticalLines()) {
            Color c = g.getColor();
            g.setColor(this.table.getGridColor());
            g.drawRect(cellRect.x, cellRect.y, cellRect.width - 1, cellRect.height - 1);
            g.setColor(c);
        }
        if (this.table.isEditing() && this.table.getEditingRow() == row && this.table.getEditingColumn() == column) {
            Component component = this.table.getEditorComponent();
            component.setBounds(cellRect);
            component.validate();
        } else {
            TableCellRenderer renderer = this.table.getCellRenderer(row, column);
            Component component = this.table.prepareRenderer(renderer, row, column);
            this.rendererPane.paintComponent(g, component, this.table, cellRect.x, cellRect.y, cellRect.width, cellRect.height, true);
        }
        if (Constants.DEBUG) {
            Date end = new Date();
            long gap = end.getTime() - start.getTime();
        }
    }
}

