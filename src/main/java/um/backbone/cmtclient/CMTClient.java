/*
 * Decompiled with CFR 0_118.
 */
package um.backbone.cmtclient;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.List;
import javax.xml.namespace.QName;
import um.backbone.cmtclient.CMTException;
import um.backbone.cmtclient.ClientConstants;
import um.backbone.cmtclient.jaxws.CMT;
import um.backbone.cmtclient.jaxws.CMTServerException;
import um.backbone.cmtclient.jaxws.CMTServerException_Exception;
import um.backbone.cmtclient.jaxws.CMTService;
import um.backbone.cmtclient.util.Base64Coder;
import um.backbone.cmtclient.util.FileUtils;

public class CMTClient {
    private static CMTService service;
    private static Boolean useSecure;

    public CMTClient() throws CMTException {
        try {
            service = new CMTService(new URL(ClientConstants.URL_CMT_WEBSERVICE), new QName("http://www.um.es/backbone/services", "CMTService"));
        }
        catch (MalformedURLException e) {
            throw new CMTException();
        }
    }

    public CMTClient(String cmtURL) throws CMTServerException_Exception {
        try {
            useSecure = true;
            service = new CMTService(new URL(cmtURL), new QName("http://www.um.es/backbone/services", "CMTService"));
        }
        catch (MalformedURLException e) {
            throw new CMTServerException_Exception(e.getMessage(), new CMTServerException());
        }
    }

    public CMTClient(String cmtURL, Boolean secureCommunication) throws CMTServerException_Exception {
        try {
            useSecure = secureCommunication;
            service = new CMTService(new URL(cmtURL), new QName("http://www.um.es/backbone/services", "CMTService"));
        }
        catch (MalformedURLException e) {
            throw new CMTServerException_Exception(e.getMessage(), new CMTServerException());
        }
    }

    public void login(String userID, String password) throws CMTServerException_Exception {
        service.getCMTWebServicePort().login(userID, Base64Coder.encodeString(password));
    }

    public void logout(String userID) throws CMTServerException_Exception {
        service.getCMTWebServicePort().logout(userID);
    }

    public void registerEmptyCorpus(String userID, String corpusID, String taxonomyTree) throws CMTServerException_Exception {
        if (useSecure.booleanValue()) {
            String taxSignature = this.getContentSignature(taxonomyTree);
            service.getCMTWebServicePort().registerEmptyCorpusSecure(userID, corpusID, taxonomyTree, taxSignature);
        } else {
            service.getCMTWebServicePort().registerEmptyCorpus(userID, corpusID, taxonomyTree);
        }
    }

    public void registerCorpus(String userID, String corpusID, String wholeCorpus) throws CMTServerException_Exception {
        if (useSecure.booleanValue()) {
            String corpusSig = this.getContentSignature(wholeCorpus);
            service.getCMTWebServicePort().registerCorpusSecure(userID, corpusID, wholeCorpus, corpusSig);
        } else {
            service.getCMTWebServicePort().registerCorpus(userID, corpusID, wholeCorpus);
        }
    }

    public boolean isRegisteredTEICorpus(String userID, String corpusID) throws CMTServerException_Exception {
        return service.getCMTWebServicePort().isRegisteredTEICorpus(userID, corpusID);
    }

    public void unregisterCorpus(String userID, String corpusID) throws CMTServerException_Exception {
        service.getCMTWebServicePort().unregisterCorpus(userID, corpusID);
    }

    public String lockTaxonomyTree(String userID, String corpusID) throws CMTServerException_Exception {
        return service.getCMTWebServicePort().lockTaxonomyTree(userID, corpusID);
    }

    public synchronized void unlockUpdatingTaxonomyTree(String userID, String corpusID, String newTaxonomyTree) throws CMTServerException_Exception {
        if (useSecure.booleanValue()) {
            String taxSignature = this.getContentSignature(newTaxonomyTree);
            service.getCMTWebServicePort().unlockUpdatingTaxonomyTreeSecure(userID, corpusID, newTaxonomyTree, taxSignature);
        } else {
            service.getCMTWebServicePort().unlockUpdatingTaxonomyTree(userID, corpusID, newTaxonomyTree);
        }
    }

    public void unlockDiscartingTaxonomyTree(String userID, String corpusID) throws CMTServerException_Exception {
        service.getCMTWebServicePort().unlockDiscartingTaxonomyTree(userID, corpusID);
    }

    public String getTaxonomyTree(String userID, String corpusID) throws CMTServerException_Exception {
        return service.getCMTWebServicePort().getTaxonomyTree(userID, corpusID);
    }

    public String lockDocument(String userID, String corpusID, String docID) throws CMTServerException_Exception {
        return service.getCMTWebServicePort().lockDocument(userID, corpusID, docID);
    }

    public void unlockUpdatingDocument(String userID, String corpusID, String docID, String newDocumentContent) throws CMTServerException_Exception {
        if (useSecure.booleanValue()) {
            String docSignature = this.getContentSignature(newDocumentContent);
            service.getCMTWebServicePort().unlockUpdatingDocumentSecure(userID, corpusID, docID, newDocumentContent, docSignature);
        } else {
            service.getCMTWebServicePort().unlockUpdatingDocument(userID, corpusID, docID, newDocumentContent);
        }
    }

    public void unlockDiscartingDocument(String userID, String corpusID, String docID) throws CMTServerException_Exception {
        service.getCMTWebServicePort().unlockDiscartingDocument(userID, corpusID, docID);
    }

    public void addDocument(String userID, String corpusID, String docID, String documentContent) throws CMTServerException_Exception {
        if (useSecure.booleanValue()) {
            String docSig = this.getContentSignature(documentContent);
            service.getCMTWebServicePort().addDocumentSecure(userID, corpusID, docID, documentContent, docSig);
        } else {
            service.getCMTWebServicePort().addDocument(userID, corpusID, docID, documentContent);
        }
    }

    public void removeDocument(String userID, String corpusID, String docID) throws CMTServerException_Exception {
        service.getCMTWebServicePort().removeDocument(userID, corpusID, docID);
    }

    public Collection<String> getAllRegisteredCorpus() {
        return service.getCMTWebServicePort().getAllRegisteredCorpus();
    }

    public Collection<String> getAllAvailableDocument(String corpusID) throws CMTServerException_Exception {
        return service.getCMTWebServicePort().getAllAvailableDocument(corpusID);
    }

    public String getFullTEICorpusFile(String userID, String corpusID) throws CMTServerException_Exception {
        return service.getCMTWebServicePort().getFullTEICorpusFile(userID, corpusID);
    }

    public String getDocument(String userID, String corpusID, String docID) throws CMTServerException_Exception {
        return service.getCMTWebServicePort().getDocument(userID, corpusID, docID);
    }

    public void deleteCategory(String userID, String categoryID, String corpusID) throws CMTServerException_Exception {
        service.getCMTWebServicePort().deleteCategory(userID, categoryID, corpusID);
    }

    public Collection<String> getAllPublicCorpus() throws CMTServerException_Exception {
        return service.getCMTWebServicePort().getAllPublicCorpus();
    }

    public String getCorpusSignature(String userID, String corpusID) throws CMTServerException_Exception {
        return service.getCMTWebServicePort().getCorpusSignature(userID, corpusID);
    }

    public String getTaxonomySignature(String userID, String corpusID) throws CMTServerException_Exception {
        return service.getCMTWebServicePort().getTaxonomySignature(userID, corpusID);
    }

    public String getDocumentSignature(String userID, String corpusID, String documentID) throws CMTServerException_Exception {
        return service.getCMTWebServicePort().getDocumentSignature(userID, corpusID, documentID);
    }

    public Boolean isCorpusPublic(String userID, String corpusID) throws CMTServerException_Exception {
        return service.getCMTWebServicePort().isCorpusPublic(userID, corpusID);
    }

    public void setCorpusVisibility(String userID, String corpusID, Boolean visibility) throws CMTServerException_Exception {
        service.getCMTWebServicePort().setCorpusVisibility(userID, corpusID, visibility);
    }

    public synchronized String getContentSignature(String content) {
        String input = content;
        String output = "";
        try {
            byte[] textBytes = input.getBytes("UTF-16");
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(textBytes);
            byte[] code = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte aCode : code) {
                sb.append(aCode);
            }
            output = sb.toString();
        }
        catch (NoSuchAlgorithmException ignored) {
            output = null;
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return output;
    }

    public static void main(String[] args) {
        try {
            CMTClient client = new CMTClient("http://localhost:8080/CMTWebService/CMTWebService");
            try {
                System.out.println("Login .... ");
                client.login("admin", "admin");
                System.out.println("Registering ... English Corpus");
                client.registerEmptyCorpus("admin", "English Corpora", FileUtils.readFile("./probe/English Corpora.taxonomy"));
                System.out.println("Getting registered Corpus");
                for (String corpus222 : client.getAllRegisteredCorpus()) {
                    System.out.println("Registered: " + corpus222);
                }
                System.out.println("Getting public Corpus");
                for (String corpus222 : client.getAllPublicCorpus()) {
                    System.out.println("Public: " + corpus222);
                }
                System.out.println("Setting English Corpora as Public ....");
                client.setCorpusVisibility("admin", "English Corpora", true);
                System.out.println("Getting public Corpus");
                for (String corpus222 : client.getAllPublicCorpus()) {
                    System.out.println("Public: " + corpus222);
                }
                System.out.println("Setting English Corpora as Private ....");
                client.setCorpusVisibility("admin", "English Corpora", false);
                System.out.println("Getting public Corpus");
                for (String corpus222 : client.getAllPublicCorpus()) {
                    System.out.println("Public: " + corpus222);
                }
                System.out.println("Lock corpora... ");
                String taxonomyTree = client.lockTaxonomyTree("admin", "English Corpora");
                taxonomyTree = taxonomyTree + "MODIFIED";
                System.out.println("Unlock corpora .... (saving modified version)");
                client.unlockUpdatingTaxonomyTree("admin", "English Corpora", taxonomyTree);
                System.out.println("Adding English_Amphibious tours document");
                client.addDocument("admin", "English Corpora", "English_Amphibious tourst", FileUtils.readFile("./probe/English_Amphibious tours.document"));
                System.out.println("Adding English_Amphibious vehicles document");
                client.addDocument("admin", "English Corpora", "English_Amphibious vehicles", FileUtils.readFile("./probe/English_Amphibious vehicles.document"));
                String signature = client.getCorpusSignature("admin", "English Corpora");
                System.out.println("Signature = " + signature);
                signature = client.getCorpusSignature("admin", "English Corpora");
                System.out.println("Signature = " + signature);
                String c = client.getFullTEICorpusFile("admin", "English Corpora");
                System.out.println("Corpus Content");
                System.out.println(c);
                System.out.println("Lock document...");
                String docu = client.lockDocument("admin", "English Corpora", "English_Amphibious tourst");
                docu = docu + "MODIFIED";
                System.out.println("Unlock document... (saving modified version)");
                client.unlockUpdatingDocument("admin", "English Corpora", "English_Amphibious tourst", docu);
                signature = client.getCorpusSignature("admin", "English Corpora");
                System.out.println("Signature = " + signature);
                System.out.println("Remove document...");
                client.removeDocument("admin", "English Corpora", "English_Amphibious tourst");
                client.removeDocument("admin", "English Corpora", "English_Amphibious vehicles");
                signature = client.getCorpusSignature("admin", "English Corpora");
                System.out.println("Signature = " + signature);
                System.out.println("Unregister corpus ...");
                client.unregisterCorpus("admin", "English Corpora");
                System.out.println("Registering whole corpus...");
                client.registerCorpus("admin", "English Corpora", FileUtils.readFile("./probe/full.xml"));
                System.out.println("Get Full Corpus...");
                String corpus3 = client.getFullTEICorpusFile("admin", "English Corpora");
                System.out.println("Unregistering whole corpus...");
                client.unregisterCorpus("admin", "English Corpora");
                System.out.println("Logout ...");
                client.logout("admin");
            }
            catch (CMTServerException_Exception e) {
                e.printStackTrace();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        catch (CMTServerException_Exception e) {
            e.printStackTrace();
        }
    }
}

