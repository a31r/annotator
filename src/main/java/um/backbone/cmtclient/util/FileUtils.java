/*
 * Decompiled with CFR 0_118.
 */
package um.backbone.cmtclient.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import um.backbone.cmtclient.ClientConstants;

public class FileUtils {
    public static String readFile(String path) throws Exception {
        String aux;
        FileInputStream fr = new FileInputStream(path);
        InputStreamReader isr = new InputStreamReader((InputStream)fr, ClientConstants.DEFAULT_ENCODING);
        BufferedReader br = new BufferedReader(isr);
        String total = "";
        while ((aux = br.readLine()) != null) {
            total = total + aux + "\n";
        }
        br.close();
        isr.close();
        fr.close();
        return total;
    }

    public static void updateFile(String path, String content) throws Exception {
        FileOutputStream fw = new FileOutputStream(path, false);
        OutputStreamWriter osw = new OutputStreamWriter((OutputStream)fw, ClientConstants.DEFAULT_ENCODING);
        osw.write(content);
        osw.flush();
        osw.close();
        fw.close();
    }
}

