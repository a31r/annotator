/*
 * Decompiled with CFR 0_118.
 */
package um.backbone.cmtclient.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(value=XmlAccessType.FIELD)
@XmlType(name="unlockUpdatingDocumentSecure", propOrder={"arg0", "arg1", "arg2", "arg3", "arg4"})
public class UnlockUpdatingDocumentSecure {
    protected String arg0;
    protected String arg1;
    protected String arg2;
    protected String arg3;
    protected String arg4;

    public String getArg0() {
        return this.arg0;
    }

    public void setArg0(String value) {
        this.arg0 = value;
    }

    public String getArg1() {
        return this.arg1;
    }

    public void setArg1(String value) {
        this.arg1 = value;
    }

    public String getArg2() {
        return this.arg2;
    }

    public void setArg2(String value) {
        this.arg2 = value;
    }

    public String getArg3() {
        return this.arg3;
    }

    public void setArg3(String value) {
        this.arg3 = value;
    }

    public String getArg4() {
        return this.arg4;
    }

    public void setArg4(String value) {
        this.arg4 = value;
    }
}

