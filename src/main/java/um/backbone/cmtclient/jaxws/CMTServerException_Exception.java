/*
 * Decompiled with CFR 0_118.
 */
package um.backbone.cmtclient.jaxws;

import javax.xml.ws.WebFault;
import um.backbone.cmtclient.jaxws.CMTServerException;

@WebFault(name="CMTServerException", targetNamespace="http://www.um.es/backbone/services")
public class CMTServerException_Exception
extends Exception {
    private CMTServerException faultInfo;

    public CMTServerException_Exception(String message, CMTServerException faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    public CMTServerException_Exception(String message, CMTServerException faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    public CMTServerException getFaultInfo() {
        return this.faultInfo;
    }
}

