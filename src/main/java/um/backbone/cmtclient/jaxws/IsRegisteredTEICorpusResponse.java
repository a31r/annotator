/*
 * Decompiled with CFR 0_118.
 */
package um.backbone.cmtclient.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(value=XmlAccessType.FIELD)
@XmlType(name="isRegisteredTEICorpusResponse", propOrder={"_return"})
public class IsRegisteredTEICorpusResponse {
    @XmlElement(name="return")
    protected boolean _return;

    public boolean isReturn() {
        return this._return;
    }

    public void setReturn(boolean value) {
        this._return = value;
    }
}

