/*
 * Decompiled with CFR 0_118.
 */
package um.backbone.cmtclient.jaxws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import um.backbone.cmtclient.jaxws.AddDocument;
import um.backbone.cmtclient.jaxws.AddDocumentResponse;
import um.backbone.cmtclient.jaxws.AddDocumentSecure;
import um.backbone.cmtclient.jaxws.AddDocumentSecureResponse;
import um.backbone.cmtclient.jaxws.CMTServerException;
import um.backbone.cmtclient.jaxws.DeleteCategory;
import um.backbone.cmtclient.jaxws.DeleteCategoryResponse;
import um.backbone.cmtclient.jaxws.GetAllAvailableDocument;
import um.backbone.cmtclient.jaxws.GetAllAvailableDocumentResponse;
import um.backbone.cmtclient.jaxws.GetAllPublicCorpus;
import um.backbone.cmtclient.jaxws.GetAllPublicCorpusResponse;
import um.backbone.cmtclient.jaxws.GetAllRegisteredCorpus;
import um.backbone.cmtclient.jaxws.GetAllRegisteredCorpusResponse;
import um.backbone.cmtclient.jaxws.GetContentSignature;
import um.backbone.cmtclient.jaxws.GetContentSignatureResponse;
import um.backbone.cmtclient.jaxws.GetCorpusSignature;
import um.backbone.cmtclient.jaxws.GetCorpusSignatureResponse;
import um.backbone.cmtclient.jaxws.GetDocument;
import um.backbone.cmtclient.jaxws.GetDocumentResponse;
import um.backbone.cmtclient.jaxws.GetDocumentSignature;
import um.backbone.cmtclient.jaxws.GetDocumentSignatureResponse;
import um.backbone.cmtclient.jaxws.GetFullTEICorpusFile;
import um.backbone.cmtclient.jaxws.GetFullTEICorpusFileResponse;
import um.backbone.cmtclient.jaxws.GetTaxonomySignature;
import um.backbone.cmtclient.jaxws.GetTaxonomySignatureResponse;
import um.backbone.cmtclient.jaxws.GetTaxonomyTree;
import um.backbone.cmtclient.jaxws.GetTaxonomyTreeResponse;
import um.backbone.cmtclient.jaxws.IsCorpusPublic;
import um.backbone.cmtclient.jaxws.IsCorpusPublicResponse;
import um.backbone.cmtclient.jaxws.IsRegisteredTEICorpus;
import um.backbone.cmtclient.jaxws.IsRegisteredTEICorpusResponse;
import um.backbone.cmtclient.jaxws.LockDocument;
import um.backbone.cmtclient.jaxws.LockDocumentResponse;
import um.backbone.cmtclient.jaxws.LockTaxonomyTree;
import um.backbone.cmtclient.jaxws.LockTaxonomyTreeResponse;
import um.backbone.cmtclient.jaxws.Login;
import um.backbone.cmtclient.jaxws.LoginResponse;
import um.backbone.cmtclient.jaxws.Logout;
import um.backbone.cmtclient.jaxws.LogoutResponse;
import um.backbone.cmtclient.jaxws.RegisterCorpus;
import um.backbone.cmtclient.jaxws.RegisterCorpusResponse;
import um.backbone.cmtclient.jaxws.RegisterCorpusSecure;
import um.backbone.cmtclient.jaxws.RegisterCorpusSecureResponse;
import um.backbone.cmtclient.jaxws.RegisterEmptyCorpus;
import um.backbone.cmtclient.jaxws.RegisterEmptyCorpusResponse;
import um.backbone.cmtclient.jaxws.RegisterEmptyCorpusSecure;
import um.backbone.cmtclient.jaxws.RegisterEmptyCorpusSecureResponse;
import um.backbone.cmtclient.jaxws.RemoveDocument;
import um.backbone.cmtclient.jaxws.RemoveDocumentResponse;
import um.backbone.cmtclient.jaxws.SetCorpusVisibility;
import um.backbone.cmtclient.jaxws.SetCorpusVisibilityResponse;
import um.backbone.cmtclient.jaxws.UnlockDiscartingDocument;
import um.backbone.cmtclient.jaxws.UnlockDiscartingDocumentResponse;
import um.backbone.cmtclient.jaxws.UnlockDiscartingTaxonomyTree;
import um.backbone.cmtclient.jaxws.UnlockDiscartingTaxonomyTreeResponse;
import um.backbone.cmtclient.jaxws.UnlockUpdatingDocument;
import um.backbone.cmtclient.jaxws.UnlockUpdatingDocumentResponse;
import um.backbone.cmtclient.jaxws.UnlockUpdatingDocumentSecure;
import um.backbone.cmtclient.jaxws.UnlockUpdatingDocumentSecureResponse;
import um.backbone.cmtclient.jaxws.UnlockUpdatingTaxonomyTree;
import um.backbone.cmtclient.jaxws.UnlockUpdatingTaxonomyTreeResponse;
import um.backbone.cmtclient.jaxws.UnlockUpdatingTaxonomyTreeSecure;
import um.backbone.cmtclient.jaxws.UnlockUpdatingTaxonomyTreeSecureResponse;
import um.backbone.cmtclient.jaxws.UnregisterCorpus;
import um.backbone.cmtclient.jaxws.UnregisterCorpusResponse;
import um.backbone.cmtclient.jaxws.UpdateCorpus;
import um.backbone.cmtclient.jaxws.UpdateCorpusResponse;
import um.backbone.cmtclient.jaxws.UpdateCorpusSecure;
import um.backbone.cmtclient.jaxws.UpdateCorpusSecureResponse;

@XmlRegistry
public class ObjectFactory {
    private static final QName _GetAllPublicCorpusResponse_QNAME = new QName("http://www.um.es/backbone/services", "getAllPublicCorpusResponse");
    private static final QName _UnlockDiscartingDocumentResponse_QNAME = new QName("http://www.um.es/backbone/services", "unlockDiscartingDocumentResponse");
    private static final QName _UnlockUpdatingDocumentSecureResponse_QNAME = new QName("http://www.um.es/backbone/services", "unlockUpdatingDocumentSecureResponse");
    private static final QName _RegisterCorpus_QNAME = new QName("http://www.um.es/backbone/services", "registerCorpus");
    private static final QName _GetDocumentSignature_QNAME = new QName("http://www.um.es/backbone/services", "getDocumentSignature");
    private static final QName _GetFullTEICorpusFileResponse_QNAME = new QName("http://www.um.es/backbone/services", "getFullTEICorpusFileResponse");
    private static final QName _UnregisterCorpus_QNAME = new QName("http://www.um.es/backbone/services", "unregisterCorpus");
    private static final QName _Logout_QNAME = new QName("http://www.um.es/backbone/services", "logout");
    private static final QName _UnlockUpdatingDocumentSecure_QNAME = new QName("http://www.um.es/backbone/services", "unlockUpdatingDocumentSecure");
    private static final QName _UnlockDiscartingTaxonomyTree_QNAME = new QName("http://www.um.es/backbone/services", "unlockDiscartingTaxonomyTree");
    private static final QName _GetTaxonomySignature_QNAME = new QName("http://www.um.es/backbone/services", "getTaxonomySignature");
    private static final QName _UnlockUpdatingTaxonomyTree_QNAME = new QName("http://www.um.es/backbone/services", "unlockUpdatingTaxonomyTree");
    private static final QName _CMTServerException_QNAME = new QName("http://www.um.es/backbone/services", "CMTServerException");
    private static final QName _RemoveDocumentResponse_QNAME = new QName("http://www.um.es/backbone/services", "removeDocumentResponse");
    private static final QName _GetTaxonomySignatureResponse_QNAME = new QName("http://www.um.es/backbone/services", "getTaxonomySignatureResponse");
    private static final QName _RegisterEmptyCorpusResponse_QNAME = new QName("http://www.um.es/backbone/services", "registerEmptyCorpusResponse");
    private static final QName _LoginResponse_QNAME = new QName("http://www.um.es/backbone/services", "loginResponse");
    private static final QName _UpdateCorpus_QNAME = new QName("http://www.um.es/backbone/services", "updateCorpus");
    private static final QName _GetDocumentSignatureResponse_QNAME = new QName("http://www.um.es/backbone/services", "getDocumentSignatureResponse");
    private static final QName _IsRegisteredTEICorpusResponse_QNAME = new QName("http://www.um.es/backbone/services", "isRegisteredTEICorpusResponse");
    private static final QName _UnregisterCorpusResponse_QNAME = new QName("http://www.um.es/backbone/services", "unregisterCorpusResponse");
    private static final QName _AddDocument_QNAME = new QName("http://www.um.es/backbone/services", "addDocument");
    private static final QName _RegisterCorpusSecureResponse_QNAME = new QName("http://www.um.es/backbone/services", "registerCorpusSecureResponse");
    private static final QName _SetCorpusVisibilityResponse_QNAME = new QName("http://www.um.es/backbone/services", "setCorpusVisibilityResponse");
    private static final QName _AddDocumentSecureResponse_QNAME = new QName("http://www.um.es/backbone/services", "addDocumentSecureResponse");
    private static final QName _GetContentSignatureResponse_QNAME = new QName("http://www.um.es/backbone/services", "getContentSignatureResponse");
    private static final QName _SetCorpusVisibility_QNAME = new QName("http://www.um.es/backbone/services", "setCorpusVisibility");
    private static final QName _GetFullTEICorpusFile_QNAME = new QName("http://www.um.es/backbone/services", "getFullTEICorpusFile");
    private static final QName _UpdateCorpusSecureResponse_QNAME = new QName("http://www.um.es/backbone/services", "updateCorpusSecureResponse");
    private static final QName _RegisterEmptyCorpusSecureResponse_QNAME = new QName("http://www.um.es/backbone/services", "registerEmptyCorpusSecureResponse");
    private static final QName _RegisterCorpusResponse_QNAME = new QName("http://www.um.es/backbone/services", "registerCorpusResponse");
    private static final QName _LockTaxonomyTree_QNAME = new QName("http://www.um.es/backbone/services", "lockTaxonomyTree");
    private static final QName _GetContentSignature_QNAME = new QName("http://www.um.es/backbone/services", "getContentSignature");
    private static final QName _IsCorpusPublic_QNAME = new QName("http://www.um.es/backbone/services", "isCorpusPublic");
    private static final QName _LockDocumentResponse_QNAME = new QName("http://www.um.es/backbone/services", "lockDocumentResponse");
    private static final QName _DeleteCategoryResponse_QNAME = new QName("http://www.um.es/backbone/services", "deleteCategoryResponse");
    private static final QName _IsCorpusPublicResponse_QNAME = new QName("http://www.um.es/backbone/services", "isCorpusPublicResponse");
    private static final QName _UnlockUpdatingDocument_QNAME = new QName("http://www.um.es/backbone/services", "unlockUpdatingDocument");
    private static final QName _GetAllPublicCorpus_QNAME = new QName("http://www.um.es/backbone/services", "getAllPublicCorpus");
    private static final QName _GetAllAvailableDocument_QNAME = new QName("http://www.um.es/backbone/services", "getAllAvailableDocument");
    private static final QName _RegisterCorpusSecure_QNAME = new QName("http://www.um.es/backbone/services", "registerCorpusSecure");
    private static final QName _GetAllRegisteredCorpus_QNAME = new QName("http://www.um.es/backbone/services", "getAllRegisteredCorpus");
    private static final QName _UpdateCorpusSecure_QNAME = new QName("http://www.um.es/backbone/services", "updateCorpusSecure");
    private static final QName _UnlockDiscartingDocument_QNAME = new QName("http://www.um.es/backbone/services", "unlockDiscartingDocument");
    private static final QName _GetAllAvailableDocumentResponse_QNAME = new QName("http://www.um.es/backbone/services", "getAllAvailableDocumentResponse");
    private static final QName _UnlockDiscartingTaxonomyTreeResponse_QNAME = new QName("http://www.um.es/backbone/services", "unlockDiscartingTaxonomyTreeResponse");
    private static final QName _Login_QNAME = new QName("http://www.um.es/backbone/services", "login");
    private static final QName _UnlockUpdatingTaxonomyTreeSecureResponse_QNAME = new QName("http://www.um.es/backbone/services", "unlockUpdatingTaxonomyTreeSecureResponse");
    private static final QName _RegisterEmptyCorpus_QNAME = new QName("http://www.um.es/backbone/services", "registerEmptyCorpus");
    private static final QName _LogoutResponse_QNAME = new QName("http://www.um.es/backbone/services", "logoutResponse");
    private static final QName _GetTaxonomyTree_QNAME = new QName("http://www.um.es/backbone/services", "getTaxonomyTree");
    private static final QName _AddDocumentSecure_QNAME = new QName("http://www.um.es/backbone/services", "addDocumentSecure");
    private static final QName _RegisterEmptyCorpusSecure_QNAME = new QName("http://www.um.es/backbone/services", "registerEmptyCorpusSecure");
    private static final QName _RemoveDocument_QNAME = new QName("http://www.um.es/backbone/services", "removeDocument");
    private static final QName _UnlockUpdatingDocumentResponse_QNAME = new QName("http://www.um.es/backbone/services", "unlockUpdatingDocumentResponse");
    private static final QName _LockDocument_QNAME = new QName("http://www.um.es/backbone/services", "lockDocument");
    private static final QName _GetCorpusSignatureResponse_QNAME = new QName("http://www.um.es/backbone/services", "getCorpusSignatureResponse");
    private static final QName _GetAllRegisteredCorpusResponse_QNAME = new QName("http://www.um.es/backbone/services", "getAllRegisteredCorpusResponse");
    private static final QName _UnlockUpdatingTaxonomyTreeSecure_QNAME = new QName("http://www.um.es/backbone/services", "unlockUpdatingTaxonomyTreeSecure");
    private static final QName _UnlockUpdatingTaxonomyTreeResponse_QNAME = new QName("http://www.um.es/backbone/services", "unlockUpdatingTaxonomyTreeResponse");
    private static final QName _UpdateCorpusResponse_QNAME = new QName("http://www.um.es/backbone/services", "updateCorpusResponse");
    private static final QName _LockTaxonomyTreeResponse_QNAME = new QName("http://www.um.es/backbone/services", "lockTaxonomyTreeResponse");
    private static final QName _GetDocument_QNAME = new QName("http://www.um.es/backbone/services", "getDocument");
    private static final QName _DeleteCategory_QNAME = new QName("http://www.um.es/backbone/services", "deleteCategory");
    private static final QName _GetCorpusSignature_QNAME = new QName("http://www.um.es/backbone/services", "getCorpusSignature");
    private static final QName _AddDocumentResponse_QNAME = new QName("http://www.um.es/backbone/services", "addDocumentResponse");
    private static final QName _GetTaxonomyTreeResponse_QNAME = new QName("http://www.um.es/backbone/services", "getTaxonomyTreeResponse");
    private static final QName _IsRegisteredTEICorpus_QNAME = new QName("http://www.um.es/backbone/services", "isRegisteredTEICorpus");
    private static final QName _GetDocumentResponse_QNAME = new QName("http://www.um.es/backbone/services", "getDocumentResponse");

    public Login createLogin() {
        return new Login();
    }

    public UnlockUpdatingDocument createUnlockUpdatingDocument() {
        return new UnlockUpdatingDocument();
    }

    public RegisterCorpusSecureResponse createRegisterCorpusSecureResponse() {
        return new RegisterCorpusSecureResponse();
    }

    public LockTaxonomyTree createLockTaxonomyTree() {
        return new LockTaxonomyTree();
    }

    public GetAllAvailableDocument createGetAllAvailableDocument() {
        return new GetAllAvailableDocument();
    }

    public RegisterEmptyCorpusSecure createRegisterEmptyCorpusSecure() {
        return new RegisterEmptyCorpusSecure();
    }

    public GetTaxonomySignatureResponse createGetTaxonomySignatureResponse() {
        return new GetTaxonomySignatureResponse();
    }

    public DeleteCategory createDeleteCategory() {
        return new DeleteCategory();
    }

    public GetTaxonomySignature createGetTaxonomySignature() {
        return new GetTaxonomySignature();
    }

    public UpdateCorpusResponse createUpdateCorpusResponse() {
        return new UpdateCorpusResponse();
    }

    public IsCorpusPublic createIsCorpusPublic() {
        return new IsCorpusPublic();
    }

    public IsRegisteredTEICorpusResponse createIsRegisteredTEICorpusResponse() {
        return new IsRegisteredTEICorpusResponse();
    }

    public Logout createLogout() {
        return new Logout();
    }

    public RegisterEmptyCorpus createRegisterEmptyCorpus() {
        return new RegisterEmptyCorpus();
    }

    public UnlockUpdatingDocumentResponse createUnlockUpdatingDocumentResponse() {
        return new UnlockUpdatingDocumentResponse();
    }

    public GetDocument createGetDocument() {
        return new GetDocument();
    }

    public GetAllPublicCorpusResponse createGetAllPublicCorpusResponse() {
        return new GetAllPublicCorpusResponse();
    }

    public UnlockDiscartingTaxonomyTree createUnlockDiscartingTaxonomyTree() {
        return new UnlockDiscartingTaxonomyTree();
    }

    public LockDocument createLockDocument() {
        return new LockDocument();
    }

    public DeleteCategoryResponse createDeleteCategoryResponse() {
        return new DeleteCategoryResponse();
    }

    public SetCorpusVisibilityResponse createSetCorpusVisibilityResponse() {
        return new SetCorpusVisibilityResponse();
    }

    public GetDocumentSignature createGetDocumentSignature() {
        return new GetDocumentSignature();
    }

    public IsCorpusPublicResponse createIsCorpusPublicResponse() {
        return new IsCorpusPublicResponse();
    }

    public SetCorpusVisibility createSetCorpusVisibility() {
        return new SetCorpusVisibility();
    }

    public RemoveDocumentResponse createRemoveDocumentResponse() {
        return new RemoveDocumentResponse();
    }

    public UpdateCorpus createUpdateCorpus() {
        return new UpdateCorpus();
    }

    public GetAllPublicCorpus createGetAllPublicCorpus() {
        return new GetAllPublicCorpus();
    }

    public UnlockDiscartingTaxonomyTreeResponse createUnlockDiscartingTaxonomyTreeResponse() {
        return new UnlockDiscartingTaxonomyTreeResponse();
    }

    public IsRegisteredTEICorpus createIsRegisteredTEICorpus() {
        return new IsRegisteredTEICorpus();
    }

    public UnlockUpdatingTaxonomyTreeSecureResponse createUnlockUpdatingTaxonomyTreeSecureResponse() {
        return new UnlockUpdatingTaxonomyTreeSecureResponse();
    }

    public RegisterCorpusResponse createRegisterCorpusResponse() {
        return new RegisterCorpusResponse();
    }

    public AddDocumentSecure createAddDocumentSecure() {
        return new AddDocumentSecure();
    }

    public UnlockUpdatingDocumentSecure createUnlockUpdatingDocumentSecure() {
        return new UnlockUpdatingDocumentSecure();
    }

    public UnlockDiscartingDocumentResponse createUnlockDiscartingDocumentResponse() {
        return new UnlockDiscartingDocumentResponse();
    }

    public LockDocumentResponse createLockDocumentResponse() {
        return new LockDocumentResponse();
    }

    public AddDocument createAddDocument() {
        return new AddDocument();
    }

    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }

    public UpdateCorpusSecure createUpdateCorpusSecure() {
        return new UpdateCorpusSecure();
    }

    public GetContentSignature createGetContentSignature() {
        return new GetContentSignature();
    }

    public GetContentSignatureResponse createGetContentSignatureResponse() {
        return new GetContentSignatureResponse();
    }

    public UpdateCorpusSecureResponse createUpdateCorpusSecureResponse() {
        return new UpdateCorpusSecureResponse();
    }

    public RegisterEmptyCorpusSecureResponse createRegisterEmptyCorpusSecureResponse() {
        return new RegisterEmptyCorpusSecureResponse();
    }

    public GetCorpusSignature createGetCorpusSignature() {
        return new GetCorpusSignature();
    }

    public UnregisterCorpus createUnregisterCorpus() {
        return new UnregisterCorpus();
    }

    public GetTaxonomyTreeResponse createGetTaxonomyTreeResponse() {
        return new GetTaxonomyTreeResponse();
    }

    public UnlockUpdatingTaxonomyTreeSecure createUnlockUpdatingTaxonomyTreeSecure() {
        return new UnlockUpdatingTaxonomyTreeSecure();
    }

    public GetAllRegisteredCorpusResponse createGetAllRegisteredCorpusResponse() {
        return new GetAllRegisteredCorpusResponse();
    }

    public UnlockUpdatingTaxonomyTreeResponse createUnlockUpdatingTaxonomyTreeResponse() {
        return new UnlockUpdatingTaxonomyTreeResponse();
    }

    public UnlockUpdatingDocumentSecureResponse createUnlockUpdatingDocumentSecureResponse() {
        return new UnlockUpdatingDocumentSecureResponse();
    }

    public AddDocumentResponse createAddDocumentResponse() {
        return new AddDocumentResponse();
    }

    public GetTaxonomyTree createGetTaxonomyTree() {
        return new GetTaxonomyTree();
    }

    public RemoveDocument createRemoveDocument() {
        return new RemoveDocument();
    }

    public RegisterCorpus createRegisterCorpus() {
        return new RegisterCorpus();
    }

    public LockTaxonomyTreeResponse createLockTaxonomyTreeResponse() {
        return new LockTaxonomyTreeResponse();
    }

    public GetFullTEICorpusFileResponse createGetFullTEICorpusFileResponse() {
        return new GetFullTEICorpusFileResponse();
    }

    public GetAllRegisteredCorpus createGetAllRegisteredCorpus() {
        return new GetAllRegisteredCorpus();
    }

    public GetFullTEICorpusFile createGetFullTEICorpusFile() {
        return new GetFullTEICorpusFile();
    }

    public AddDocumentSecureResponse createAddDocumentSecureResponse() {
        return new AddDocumentSecureResponse();
    }

    public UnregisterCorpusResponse createUnregisterCorpusResponse() {
        return new UnregisterCorpusResponse();
    }

    public CMTServerException createCMTServerException() {
        return new CMTServerException();
    }

    public GetAllAvailableDocumentResponse createGetAllAvailableDocumentResponse() {
        return new GetAllAvailableDocumentResponse();
    }

    public RegisterCorpusSecure createRegisterCorpusSecure() {
        return new RegisterCorpusSecure();
    }

    public UnlockUpdatingTaxonomyTree createUnlockUpdatingTaxonomyTree() {
        return new UnlockUpdatingTaxonomyTree();
    }

    public GetDocumentSignatureResponse createGetDocumentSignatureResponse() {
        return new GetDocumentSignatureResponse();
    }

    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    public UnlockDiscartingDocument createUnlockDiscartingDocument() {
        return new UnlockDiscartingDocument();
    }

    public RegisterEmptyCorpusResponse createRegisterEmptyCorpusResponse() {
        return new RegisterEmptyCorpusResponse();
    }

    public GetDocumentResponse createGetDocumentResponse() {
        return new GetDocumentResponse();
    }

    public GetCorpusSignatureResponse createGetCorpusSignatureResponse() {
        return new GetCorpusSignatureResponse();
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getAllPublicCorpusResponse")
    public JAXBElement<GetAllPublicCorpusResponse> createGetAllPublicCorpusResponse(GetAllPublicCorpusResponse value) {
        return new JAXBElement<GetAllPublicCorpusResponse>(_GetAllPublicCorpusResponse_QNAME, GetAllPublicCorpusResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unlockDiscartingDocumentResponse")
    public JAXBElement<UnlockDiscartingDocumentResponse> createUnlockDiscartingDocumentResponse(UnlockDiscartingDocumentResponse value) {
        return new JAXBElement<UnlockDiscartingDocumentResponse>(_UnlockDiscartingDocumentResponse_QNAME, UnlockDiscartingDocumentResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unlockUpdatingDocumentSecureResponse")
    public JAXBElement<UnlockUpdatingDocumentSecureResponse> createUnlockUpdatingDocumentSecureResponse(UnlockUpdatingDocumentSecureResponse value) {
        return new JAXBElement<UnlockUpdatingDocumentSecureResponse>(_UnlockUpdatingDocumentSecureResponse_QNAME, UnlockUpdatingDocumentSecureResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="registerCorpus")
    public JAXBElement<RegisterCorpus> createRegisterCorpus(RegisterCorpus value) {
        return new JAXBElement<RegisterCorpus>(_RegisterCorpus_QNAME, RegisterCorpus.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getDocumentSignature")
    public JAXBElement<GetDocumentSignature> createGetDocumentSignature(GetDocumentSignature value) {
        return new JAXBElement<GetDocumentSignature>(_GetDocumentSignature_QNAME, GetDocumentSignature.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getFullTEICorpusFileResponse")
    public JAXBElement<GetFullTEICorpusFileResponse> createGetFullTEICorpusFileResponse(GetFullTEICorpusFileResponse value) {
        return new JAXBElement<GetFullTEICorpusFileResponse>(_GetFullTEICorpusFileResponse_QNAME, GetFullTEICorpusFileResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unregisterCorpus")
    public JAXBElement<UnregisterCorpus> createUnregisterCorpus(UnregisterCorpus value) {
        return new JAXBElement<UnregisterCorpus>(_UnregisterCorpus_QNAME, UnregisterCorpus.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="logout")
    public JAXBElement<Logout> createLogout(Logout value) {
        return new JAXBElement<Logout>(_Logout_QNAME, Logout.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unlockUpdatingDocumentSecure")
    public JAXBElement<UnlockUpdatingDocumentSecure> createUnlockUpdatingDocumentSecure(UnlockUpdatingDocumentSecure value) {
        return new JAXBElement<UnlockUpdatingDocumentSecure>(_UnlockUpdatingDocumentSecure_QNAME, UnlockUpdatingDocumentSecure.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unlockDiscartingTaxonomyTree")
    public JAXBElement<UnlockDiscartingTaxonomyTree> createUnlockDiscartingTaxonomyTree(UnlockDiscartingTaxonomyTree value) {
        return new JAXBElement<UnlockDiscartingTaxonomyTree>(_UnlockDiscartingTaxonomyTree_QNAME, UnlockDiscartingTaxonomyTree.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getTaxonomySignature")
    public JAXBElement<GetTaxonomySignature> createGetTaxonomySignature(GetTaxonomySignature value) {
        return new JAXBElement<GetTaxonomySignature>(_GetTaxonomySignature_QNAME, GetTaxonomySignature.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unlockUpdatingTaxonomyTree")
    public JAXBElement<UnlockUpdatingTaxonomyTree> createUnlockUpdatingTaxonomyTree(UnlockUpdatingTaxonomyTree value) {
        return new JAXBElement<UnlockUpdatingTaxonomyTree>(_UnlockUpdatingTaxonomyTree_QNAME, UnlockUpdatingTaxonomyTree.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="CMTServerException")
    public JAXBElement<CMTServerException> createCMTServerException(CMTServerException value) {
        return new JAXBElement<CMTServerException>(_CMTServerException_QNAME, CMTServerException.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="removeDocumentResponse")
    public JAXBElement<RemoveDocumentResponse> createRemoveDocumentResponse(RemoveDocumentResponse value) {
        return new JAXBElement<RemoveDocumentResponse>(_RemoveDocumentResponse_QNAME, RemoveDocumentResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getTaxonomySignatureResponse")
    public JAXBElement<GetTaxonomySignatureResponse> createGetTaxonomySignatureResponse(GetTaxonomySignatureResponse value) {
        return new JAXBElement<GetTaxonomySignatureResponse>(_GetTaxonomySignatureResponse_QNAME, GetTaxonomySignatureResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="registerEmptyCorpusResponse")
    public JAXBElement<RegisterEmptyCorpusResponse> createRegisterEmptyCorpusResponse(RegisterEmptyCorpusResponse value) {
        return new JAXBElement<RegisterEmptyCorpusResponse>(_RegisterEmptyCorpusResponse_QNAME, RegisterEmptyCorpusResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="loginResponse")
    public JAXBElement<LoginResponse> createLoginResponse(LoginResponse value) {
        return new JAXBElement<LoginResponse>(_LoginResponse_QNAME, LoginResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="updateCorpus")
    public JAXBElement<UpdateCorpus> createUpdateCorpus(UpdateCorpus value) {
        return new JAXBElement<UpdateCorpus>(_UpdateCorpus_QNAME, UpdateCorpus.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getDocumentSignatureResponse")
    public JAXBElement<GetDocumentSignatureResponse> createGetDocumentSignatureResponse(GetDocumentSignatureResponse value) {
        return new JAXBElement<GetDocumentSignatureResponse>(_GetDocumentSignatureResponse_QNAME, GetDocumentSignatureResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="isRegisteredTEICorpusResponse")
    public JAXBElement<IsRegisteredTEICorpusResponse> createIsRegisteredTEICorpusResponse(IsRegisteredTEICorpusResponse value) {
        return new JAXBElement<IsRegisteredTEICorpusResponse>(_IsRegisteredTEICorpusResponse_QNAME, IsRegisteredTEICorpusResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unregisterCorpusResponse")
    public JAXBElement<UnregisterCorpusResponse> createUnregisterCorpusResponse(UnregisterCorpusResponse value) {
        return new JAXBElement<UnregisterCorpusResponse>(_UnregisterCorpusResponse_QNAME, UnregisterCorpusResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="addDocument")
    public JAXBElement<AddDocument> createAddDocument(AddDocument value) {
        return new JAXBElement<AddDocument>(_AddDocument_QNAME, AddDocument.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="registerCorpusSecureResponse")
    public JAXBElement<RegisterCorpusSecureResponse> createRegisterCorpusSecureResponse(RegisterCorpusSecureResponse value) {
        return new JAXBElement<RegisterCorpusSecureResponse>(_RegisterCorpusSecureResponse_QNAME, RegisterCorpusSecureResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="setCorpusVisibilityResponse")
    public JAXBElement<SetCorpusVisibilityResponse> createSetCorpusVisibilityResponse(SetCorpusVisibilityResponse value) {
        return new JAXBElement<SetCorpusVisibilityResponse>(_SetCorpusVisibilityResponse_QNAME, SetCorpusVisibilityResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="addDocumentSecureResponse")
    public JAXBElement<AddDocumentSecureResponse> createAddDocumentSecureResponse(AddDocumentSecureResponse value) {
        return new JAXBElement<AddDocumentSecureResponse>(_AddDocumentSecureResponse_QNAME, AddDocumentSecureResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getContentSignatureResponse")
    public JAXBElement<GetContentSignatureResponse> createGetContentSignatureResponse(GetContentSignatureResponse value) {
        return new JAXBElement<GetContentSignatureResponse>(_GetContentSignatureResponse_QNAME, GetContentSignatureResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="setCorpusVisibility")
    public JAXBElement<SetCorpusVisibility> createSetCorpusVisibility(SetCorpusVisibility value) {
        return new JAXBElement<SetCorpusVisibility>(_SetCorpusVisibility_QNAME, SetCorpusVisibility.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getFullTEICorpusFile")
    public JAXBElement<GetFullTEICorpusFile> createGetFullTEICorpusFile(GetFullTEICorpusFile value) {
        return new JAXBElement<GetFullTEICorpusFile>(_GetFullTEICorpusFile_QNAME, GetFullTEICorpusFile.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="updateCorpusSecureResponse")
    public JAXBElement<UpdateCorpusSecureResponse> createUpdateCorpusSecureResponse(UpdateCorpusSecureResponse value) {
        return new JAXBElement<UpdateCorpusSecureResponse>(_UpdateCorpusSecureResponse_QNAME, UpdateCorpusSecureResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="registerEmptyCorpusSecureResponse")
    public JAXBElement<RegisterEmptyCorpusSecureResponse> createRegisterEmptyCorpusSecureResponse(RegisterEmptyCorpusSecureResponse value) {
        return new JAXBElement<RegisterEmptyCorpusSecureResponse>(_RegisterEmptyCorpusSecureResponse_QNAME, RegisterEmptyCorpusSecureResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="registerCorpusResponse")
    public JAXBElement<RegisterCorpusResponse> createRegisterCorpusResponse(RegisterCorpusResponse value) {
        return new JAXBElement<RegisterCorpusResponse>(_RegisterCorpusResponse_QNAME, RegisterCorpusResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="lockTaxonomyTree")
    public JAXBElement<LockTaxonomyTree> createLockTaxonomyTree(LockTaxonomyTree value) {
        return new JAXBElement<LockTaxonomyTree>(_LockTaxonomyTree_QNAME, LockTaxonomyTree.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getContentSignature")
    public JAXBElement<GetContentSignature> createGetContentSignature(GetContentSignature value) {
        return new JAXBElement<GetContentSignature>(_GetContentSignature_QNAME, GetContentSignature.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="isCorpusPublic")
    public JAXBElement<IsCorpusPublic> createIsCorpusPublic(IsCorpusPublic value) {
        return new JAXBElement<IsCorpusPublic>(_IsCorpusPublic_QNAME, IsCorpusPublic.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="lockDocumentResponse")
    public JAXBElement<LockDocumentResponse> createLockDocumentResponse(LockDocumentResponse value) {
        return new JAXBElement<LockDocumentResponse>(_LockDocumentResponse_QNAME, LockDocumentResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="deleteCategoryResponse")
    public JAXBElement<DeleteCategoryResponse> createDeleteCategoryResponse(DeleteCategoryResponse value) {
        return new JAXBElement<DeleteCategoryResponse>(_DeleteCategoryResponse_QNAME, DeleteCategoryResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="isCorpusPublicResponse")
    public JAXBElement<IsCorpusPublicResponse> createIsCorpusPublicResponse(IsCorpusPublicResponse value) {
        return new JAXBElement<IsCorpusPublicResponse>(_IsCorpusPublicResponse_QNAME, IsCorpusPublicResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unlockUpdatingDocument")
    public JAXBElement<UnlockUpdatingDocument> createUnlockUpdatingDocument(UnlockUpdatingDocument value) {
        return new JAXBElement<UnlockUpdatingDocument>(_UnlockUpdatingDocument_QNAME, UnlockUpdatingDocument.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getAllPublicCorpus")
    public JAXBElement<GetAllPublicCorpus> createGetAllPublicCorpus(GetAllPublicCorpus value) {
        return new JAXBElement<GetAllPublicCorpus>(_GetAllPublicCorpus_QNAME, GetAllPublicCorpus.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getAllAvailableDocument")
    public JAXBElement<GetAllAvailableDocument> createGetAllAvailableDocument(GetAllAvailableDocument value) {
        return new JAXBElement<GetAllAvailableDocument>(_GetAllAvailableDocument_QNAME, GetAllAvailableDocument.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="registerCorpusSecure")
    public JAXBElement<RegisterCorpusSecure> createRegisterCorpusSecure(RegisterCorpusSecure value) {
        return new JAXBElement<RegisterCorpusSecure>(_RegisterCorpusSecure_QNAME, RegisterCorpusSecure.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getAllRegisteredCorpus")
    public JAXBElement<GetAllRegisteredCorpus> createGetAllRegisteredCorpus(GetAllRegisteredCorpus value) {
        return new JAXBElement<GetAllRegisteredCorpus>(_GetAllRegisteredCorpus_QNAME, GetAllRegisteredCorpus.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="updateCorpusSecure")
    public JAXBElement<UpdateCorpusSecure> createUpdateCorpusSecure(UpdateCorpusSecure value) {
        return new JAXBElement<UpdateCorpusSecure>(_UpdateCorpusSecure_QNAME, UpdateCorpusSecure.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unlockDiscartingDocument")
    public JAXBElement<UnlockDiscartingDocument> createUnlockDiscartingDocument(UnlockDiscartingDocument value) {
        return new JAXBElement<UnlockDiscartingDocument>(_UnlockDiscartingDocument_QNAME, UnlockDiscartingDocument.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getAllAvailableDocumentResponse")
    public JAXBElement<GetAllAvailableDocumentResponse> createGetAllAvailableDocumentResponse(GetAllAvailableDocumentResponse value) {
        return new JAXBElement<GetAllAvailableDocumentResponse>(_GetAllAvailableDocumentResponse_QNAME, GetAllAvailableDocumentResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unlockDiscartingTaxonomyTreeResponse")
    public JAXBElement<UnlockDiscartingTaxonomyTreeResponse> createUnlockDiscartingTaxonomyTreeResponse(UnlockDiscartingTaxonomyTreeResponse value) {
        return new JAXBElement<UnlockDiscartingTaxonomyTreeResponse>(_UnlockDiscartingTaxonomyTreeResponse_QNAME, UnlockDiscartingTaxonomyTreeResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="login")
    public JAXBElement<Login> createLogin(Login value) {
        return new JAXBElement<Login>(_Login_QNAME, Login.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unlockUpdatingTaxonomyTreeSecureResponse")
    public JAXBElement<UnlockUpdatingTaxonomyTreeSecureResponse> createUnlockUpdatingTaxonomyTreeSecureResponse(UnlockUpdatingTaxonomyTreeSecureResponse value) {
        return new JAXBElement<UnlockUpdatingTaxonomyTreeSecureResponse>(_UnlockUpdatingTaxonomyTreeSecureResponse_QNAME, UnlockUpdatingTaxonomyTreeSecureResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="registerEmptyCorpus")
    public JAXBElement<RegisterEmptyCorpus> createRegisterEmptyCorpus(RegisterEmptyCorpus value) {
        return new JAXBElement<RegisterEmptyCorpus>(_RegisterEmptyCorpus_QNAME, RegisterEmptyCorpus.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="logoutResponse")
    public JAXBElement<LogoutResponse> createLogoutResponse(LogoutResponse value) {
        return new JAXBElement<LogoutResponse>(_LogoutResponse_QNAME, LogoutResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getTaxonomyTree")
    public JAXBElement<GetTaxonomyTree> createGetTaxonomyTree(GetTaxonomyTree value) {
        return new JAXBElement<GetTaxonomyTree>(_GetTaxonomyTree_QNAME, GetTaxonomyTree.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="addDocumentSecure")
    public JAXBElement<AddDocumentSecure> createAddDocumentSecure(AddDocumentSecure value) {
        return new JAXBElement<AddDocumentSecure>(_AddDocumentSecure_QNAME, AddDocumentSecure.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="registerEmptyCorpusSecure")
    public JAXBElement<RegisterEmptyCorpusSecure> createRegisterEmptyCorpusSecure(RegisterEmptyCorpusSecure value) {
        return new JAXBElement<RegisterEmptyCorpusSecure>(_RegisterEmptyCorpusSecure_QNAME, RegisterEmptyCorpusSecure.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="removeDocument")
    public JAXBElement<RemoveDocument> createRemoveDocument(RemoveDocument value) {
        return new JAXBElement<RemoveDocument>(_RemoveDocument_QNAME, RemoveDocument.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unlockUpdatingDocumentResponse")
    public JAXBElement<UnlockUpdatingDocumentResponse> createUnlockUpdatingDocumentResponse(UnlockUpdatingDocumentResponse value) {
        return new JAXBElement<UnlockUpdatingDocumentResponse>(_UnlockUpdatingDocumentResponse_QNAME, UnlockUpdatingDocumentResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="lockDocument")
    public JAXBElement<LockDocument> createLockDocument(LockDocument value) {
        return new JAXBElement<LockDocument>(_LockDocument_QNAME, LockDocument.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getCorpusSignatureResponse")
    public JAXBElement<GetCorpusSignatureResponse> createGetCorpusSignatureResponse(GetCorpusSignatureResponse value) {
        return new JAXBElement<GetCorpusSignatureResponse>(_GetCorpusSignatureResponse_QNAME, GetCorpusSignatureResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getAllRegisteredCorpusResponse")
    public JAXBElement<GetAllRegisteredCorpusResponse> createGetAllRegisteredCorpusResponse(GetAllRegisteredCorpusResponse value) {
        return new JAXBElement<GetAllRegisteredCorpusResponse>(_GetAllRegisteredCorpusResponse_QNAME, GetAllRegisteredCorpusResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unlockUpdatingTaxonomyTreeSecure")
    public JAXBElement<UnlockUpdatingTaxonomyTreeSecure> createUnlockUpdatingTaxonomyTreeSecure(UnlockUpdatingTaxonomyTreeSecure value) {
        return new JAXBElement<UnlockUpdatingTaxonomyTreeSecure>(_UnlockUpdatingTaxonomyTreeSecure_QNAME, UnlockUpdatingTaxonomyTreeSecure.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="unlockUpdatingTaxonomyTreeResponse")
    public JAXBElement<UnlockUpdatingTaxonomyTreeResponse> createUnlockUpdatingTaxonomyTreeResponse(UnlockUpdatingTaxonomyTreeResponse value) {
        return new JAXBElement<UnlockUpdatingTaxonomyTreeResponse>(_UnlockUpdatingTaxonomyTreeResponse_QNAME, UnlockUpdatingTaxonomyTreeResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="updateCorpusResponse")
    public JAXBElement<UpdateCorpusResponse> createUpdateCorpusResponse(UpdateCorpusResponse value) {
        return new JAXBElement<UpdateCorpusResponse>(_UpdateCorpusResponse_QNAME, UpdateCorpusResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="lockTaxonomyTreeResponse")
    public JAXBElement<LockTaxonomyTreeResponse> createLockTaxonomyTreeResponse(LockTaxonomyTreeResponse value) {
        return new JAXBElement<LockTaxonomyTreeResponse>(_LockTaxonomyTreeResponse_QNAME, LockTaxonomyTreeResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getDocument")
    public JAXBElement<GetDocument> createGetDocument(GetDocument value) {
        return new JAXBElement<GetDocument>(_GetDocument_QNAME, GetDocument.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="deleteCategory")
    public JAXBElement<DeleteCategory> createDeleteCategory(DeleteCategory value) {
        return new JAXBElement<DeleteCategory>(_DeleteCategory_QNAME, DeleteCategory.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getCorpusSignature")
    public JAXBElement<GetCorpusSignature> createGetCorpusSignature(GetCorpusSignature value) {
        return new JAXBElement<GetCorpusSignature>(_GetCorpusSignature_QNAME, GetCorpusSignature.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="addDocumentResponse")
    public JAXBElement<AddDocumentResponse> createAddDocumentResponse(AddDocumentResponse value) {
        return new JAXBElement<AddDocumentResponse>(_AddDocumentResponse_QNAME, AddDocumentResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getTaxonomyTreeResponse")
    public JAXBElement<GetTaxonomyTreeResponse> createGetTaxonomyTreeResponse(GetTaxonomyTreeResponse value) {
        return new JAXBElement<GetTaxonomyTreeResponse>(_GetTaxonomyTreeResponse_QNAME, GetTaxonomyTreeResponse.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="isRegisteredTEICorpus")
    public JAXBElement<IsRegisteredTEICorpus> createIsRegisteredTEICorpus(IsRegisteredTEICorpus value) {
        return new JAXBElement<IsRegisteredTEICorpus>(_IsRegisteredTEICorpus_QNAME, IsRegisteredTEICorpus.class, null, value);
    }

    @XmlElementDecl(namespace="http://www.um.es/backbone/services", name="getDocumentResponse")
    public JAXBElement<GetDocumentResponse> createGetDocumentResponse(GetDocumentResponse value) {
        return new JAXBElement<GetDocumentResponse>(_GetDocumentResponse_QNAME, GetDocumentResponse.class, null, value);
    }
}

