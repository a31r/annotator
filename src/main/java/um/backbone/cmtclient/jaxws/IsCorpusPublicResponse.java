/*
 * Decompiled with CFR 0_118.
 */
package um.backbone.cmtclient.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(value=XmlAccessType.FIELD)
@XmlType(name="isCorpusPublicResponse", propOrder={"_return"})
public class IsCorpusPublicResponse {
    @XmlElement(name="return")
    protected Boolean _return;

    public Boolean isReturn() {
        return this._return;
    }

    public void setReturn(Boolean value) {
        this._return = value;
    }
}

