/*
 * Decompiled with CFR 0_118.
 */
package um.backbone.cmtclient.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(value=XmlAccessType.FIELD)
@XmlType(name="lockDocumentResponse", propOrder={"_return"})
public class LockDocumentResponse {
    @XmlElement(name="return")
    protected String _return;

    public String getReturn() {
        return this._return;
    }

    public void setReturn(String value) {
        this._return = value;
    }
}

