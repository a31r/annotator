/*
 * Decompiled with CFR 0_118.
 */
package um.backbone.cmtclient.jaxws;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import um.backbone.cmtclient.jaxws.CMT;

@WebServiceClient(name="CMTService", targetNamespace="http://www.um.es/backbone/services", wsdlLocation="file:/D:/[Proyectos]/[Backbone]/[Development]/[CMT]/[CMT%201.6]/[Development]/[Client]/wsdl/CMTService.wsdl")
public class CMTService
extends Service {
    private static final URL CMTSERVICE_WSDL_LOCATION;
    private static final Logger logger;

    public CMTService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public CMTService() {
        super(CMTSERVICE_WSDL_LOCATION, new QName("http://www.um.es/backbone/services", "CMTService"));
    }

    @WebEndpoint(name="CMTWebServicePort")
    public CMT getCMTWebServicePort() {
        return super.getPort(new QName("http://www.um.es/backbone/services", "CMTWebServicePort"), CMT.class);
    }

    @WebEndpoint(name="CMTWebServicePort")
    public /* varargs */ CMT getCMTWebServicePort(WebServiceFeature ... features) {
        return super.getPort(new QName("http://www.um.es/backbone/services", "CMTWebServicePort"), CMT.class, features);
    }

    static {
        logger = Logger.getLogger(CMTService.class.getName());
        URL url = null;
        try {
            URL baseUrl = CMTService.class.getResource(".");
            url = new URL(baseUrl, "file:/D:/[Proyectos]/[Backbone]/[Development]/[CMT]/[CMT%201.6]/[Development]/[Client]/wsdl/CMTService.wsdl");
        }
        catch (MalformedURLException e) {
            logger.warning("Failed to create URL for the wsdl Location: 'file:/D:/[Proyectos]/[Backbone]/[Development]/[CMT]/[CMT%201.6]/[Development]/[Client]/wsdl/CMTService.wsdl', retrying as a local file");
            logger.warning(e.getMessage());
        }
        CMTSERVICE_WSDL_LOCATION = url;
    }
}

