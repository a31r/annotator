/*
 * Decompiled with CFR 0_118.
 */
package um.backbone.cmtclient.jaxws;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import um.backbone.cmtclient.jaxws.CMTServerException_Exception;
import um.backbone.cmtclient.jaxws.ObjectFactory;

@WebService(name="CMT", targetNamespace="http://www.um.es/backbone/services")
@XmlSeeAlso(value={ObjectFactory.class})
public interface CMT {
    @WebMethod
    @RequestWrapper(localName="login", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.Login")
    @ResponseWrapper(localName="loginResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.LoginResponse")
    public void login(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="logout", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.Logout")
    @ResponseWrapper(localName="logoutResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.LogoutResponse")
    public void logout(@WebParam(name="arg0", targetNamespace="") String var1) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="registerEmptyCorpus", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.RegisterEmptyCorpus")
    @ResponseWrapper(localName="registerEmptyCorpusResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.RegisterEmptyCorpusResponse")
    public void registerEmptyCorpus(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="registerEmptyCorpusSecure", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.RegisterEmptyCorpusSecure")
    @ResponseWrapper(localName="registerEmptyCorpusSecureResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.RegisterEmptyCorpusSecureResponse")
    public void registerEmptyCorpusSecure(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3, @WebParam(name="arg3", targetNamespace="") String var4) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="registerCorpus", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.RegisterCorpus")
    @ResponseWrapper(localName="registerCorpusResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.RegisterCorpusResponse")
    public void registerCorpus(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="registerCorpusSecure", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.RegisterCorpusSecure")
    @ResponseWrapper(localName="registerCorpusSecureResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.RegisterCorpusSecureResponse")
    public void registerCorpusSecure(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3, @WebParam(name="arg3", targetNamespace="") String var4) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="updateCorpus", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UpdateCorpus")
    @ResponseWrapper(localName="updateCorpusResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UpdateCorpusResponse")
    public void updateCorpus(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="updateCorpusSecure", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UpdateCorpusSecure")
    @ResponseWrapper(localName="updateCorpusSecureResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UpdateCorpusSecureResponse")
    public void updateCorpusSecure(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3, @WebParam(name="arg3", targetNamespace="") String var4) throws CMTServerException_Exception;

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="isRegisteredTEICorpus", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.IsRegisteredTEICorpus")
    @ResponseWrapper(localName="isRegisteredTEICorpusResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.IsRegisteredTEICorpusResponse")
    public boolean isRegisteredTEICorpus(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="unregisterCorpus", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnregisterCorpus")
    @ResponseWrapper(localName="unregisterCorpusResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnregisterCorpusResponse")
    public void unregisterCorpus(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2) throws CMTServerException_Exception;

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="lockTaxonomyTree", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.LockTaxonomyTree")
    @ResponseWrapper(localName="lockTaxonomyTreeResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.LockTaxonomyTreeResponse")
    public String lockTaxonomyTree(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="unlockUpdatingTaxonomyTree", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnlockUpdatingTaxonomyTree")
    @ResponseWrapper(localName="unlockUpdatingTaxonomyTreeResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnlockUpdatingTaxonomyTreeResponse")
    public void unlockUpdatingTaxonomyTree(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="unlockUpdatingTaxonomyTreeSecure", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnlockUpdatingTaxonomyTreeSecure")
    @ResponseWrapper(localName="unlockUpdatingTaxonomyTreeSecureResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnlockUpdatingTaxonomyTreeSecureResponse")
    public void unlockUpdatingTaxonomyTreeSecure(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3, @WebParam(name="arg3", targetNamespace="") String var4) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="unlockDiscartingTaxonomyTree", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnlockDiscartingTaxonomyTree")
    @ResponseWrapper(localName="unlockDiscartingTaxonomyTreeResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnlockDiscartingTaxonomyTreeResponse")
    public void unlockDiscartingTaxonomyTree(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2) throws CMTServerException_Exception;

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="getTaxonomyTree", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetTaxonomyTree")
    @ResponseWrapper(localName="getTaxonomyTreeResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetTaxonomyTreeResponse")
    public String getTaxonomyTree(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2) throws CMTServerException_Exception;

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="lockDocument", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.LockDocument")
    @ResponseWrapper(localName="lockDocumentResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.LockDocumentResponse")
    public String lockDocument(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="unlockUpdatingDocument", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnlockUpdatingDocument")
    @ResponseWrapper(localName="unlockUpdatingDocumentResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnlockUpdatingDocumentResponse")
    public void unlockUpdatingDocument(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3, @WebParam(name="arg3", targetNamespace="") String var4) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="unlockUpdatingDocumentSecure", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnlockUpdatingDocumentSecure")
    @ResponseWrapper(localName="unlockUpdatingDocumentSecureResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnlockUpdatingDocumentSecureResponse")
    public void unlockUpdatingDocumentSecure(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3, @WebParam(name="arg3", targetNamespace="") String var4, @WebParam(name="arg4", targetNamespace="") String var5) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="unlockDiscartingDocument", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnlockDiscartingDocument")
    @ResponseWrapper(localName="unlockDiscartingDocumentResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.UnlockDiscartingDocumentResponse")
    public void unlockDiscartingDocument(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="addDocument", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.AddDocument")
    @ResponseWrapper(localName="addDocumentResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.AddDocumentResponse")
    public void addDocument(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3, @WebParam(name="arg3", targetNamespace="") String var4) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="addDocumentSecure", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.AddDocumentSecure")
    @ResponseWrapper(localName="addDocumentSecureResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.AddDocumentSecureResponse")
    public void addDocumentSecure(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3, @WebParam(name="arg3", targetNamespace="") String var4, @WebParam(name="arg4", targetNamespace="") String var5) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="removeDocument", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.RemoveDocument")
    @ResponseWrapper(localName="removeDocumentResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.RemoveDocumentResponse")
    public void removeDocument(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3) throws CMTServerException_Exception;

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="getAllRegisteredCorpus", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetAllRegisteredCorpus")
    @ResponseWrapper(localName="getAllRegisteredCorpusResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetAllRegisteredCorpusResponse")
    public List<String> getAllRegisteredCorpus();

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="getAllPublicCorpus", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetAllPublicCorpus")
    @ResponseWrapper(localName="getAllPublicCorpusResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetAllPublicCorpusResponse")
    public List<String> getAllPublicCorpus();

    @WebMethod
    @RequestWrapper(localName="setCorpusVisibility", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.SetCorpusVisibility")
    @ResponseWrapper(localName="setCorpusVisibilityResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.SetCorpusVisibilityResponse")
    public void setCorpusVisibility(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") Boolean var3) throws CMTServerException_Exception;

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="isCorpusPublic", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.IsCorpusPublic")
    @ResponseWrapper(localName="isCorpusPublicResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.IsCorpusPublicResponse")
    public Boolean isCorpusPublic(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2) throws CMTServerException_Exception;

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="getAllAvailableDocument", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetAllAvailableDocument")
    @ResponseWrapper(localName="getAllAvailableDocumentResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetAllAvailableDocumentResponse")
    public List<String> getAllAvailableDocument(@WebParam(name="arg0", targetNamespace="") String var1) throws CMTServerException_Exception;

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="getCorpusSignature", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetCorpusSignature")
    @ResponseWrapper(localName="getCorpusSignatureResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetCorpusSignatureResponse")
    public String getCorpusSignature(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2) throws CMTServerException_Exception;

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="getTaxonomySignature", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetTaxonomySignature")
    @ResponseWrapper(localName="getTaxonomySignatureResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetTaxonomySignatureResponse")
    public String getTaxonomySignature(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2) throws CMTServerException_Exception;

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="getContentSignature", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetContentSignature")
    @ResponseWrapper(localName="getContentSignatureResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetContentSignatureResponse")
    public String getContentSignature(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2) throws CMTServerException_Exception;

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="getDocumentSignature", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetDocumentSignature")
    @ResponseWrapper(localName="getDocumentSignatureResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetDocumentSignatureResponse")
    public String getDocumentSignature(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3) throws CMTServerException_Exception;

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="getFullTEICorpusFile", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetFullTEICorpusFile")
    @ResponseWrapper(localName="getFullTEICorpusFileResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetFullTEICorpusFileResponse")
    public String getFullTEICorpusFile(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2) throws CMTServerException_Exception;

    @WebMethod
    @RequestWrapper(localName="deleteCategory", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.DeleteCategory")
    @ResponseWrapper(localName="deleteCategoryResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.DeleteCategoryResponse")
    public void deleteCategory(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3) throws CMTServerException_Exception;

    @WebMethod
    @WebResult(targetNamespace="")
    @RequestWrapper(localName="getDocument", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetDocument")
    @ResponseWrapper(localName="getDocumentResponse", targetNamespace="http://www.um.es/backbone/services", className="um.backbone.cmtclient.jaxws.GetDocumentResponse")
    public String getDocument(@WebParam(name="arg0", targetNamespace="") String var1, @WebParam(name="arg1", targetNamespace="") String var2, @WebParam(name="arg2", targetNamespace="") String var3) throws CMTServerException_Exception;
}

