/*
 * Decompiled with CFR 0_118.
 */
package um.backbone.cmtclient.jaxws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(value=XmlAccessType.FIELD)
@XmlType(name="getAllPublicCorpusResponse", propOrder={"_return"})
public class GetAllPublicCorpusResponse {
    @XmlElement(name="return")
    protected List<String> _return;

    public List<String> getReturn() {
        if (this._return == null) {
            this._return = new ArrayList<String>();
        }
        return this._return;
    }
}

