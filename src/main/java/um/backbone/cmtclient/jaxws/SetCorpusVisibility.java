/*
 * Decompiled with CFR 0_118.
 */
package um.backbone.cmtclient.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(value=XmlAccessType.FIELD)
@XmlType(name="setCorpusVisibility", propOrder={"arg0", "arg1", "arg2"})
public class SetCorpusVisibility {
    protected String arg0;
    protected String arg1;
    protected Boolean arg2;

    public String getArg0() {
        return this.arg0;
    }

    public void setArg0(String value) {
        this.arg0 = value;
    }

    public String getArg1() {
        return this.arg1;
    }

    public void setArg1(String value) {
        this.arg1 = value;
    }

    public Boolean isArg2() {
        return this.arg2;
    }

    public void setArg2(Boolean value) {
        this.arg2 = value;
    }
}

