/*
 * Decompiled with CFR 0_118.
 */
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class JDOMAbout {
    public static void main(String[] args) throws Exception {
        Info info = new JDOMAbout().new Info();
        String title = info.title;
        System.out.println(String.valueOf(title) + " version " + info.version);
        System.out.println("Copyright " + info.copyright);
        System.out.println();
        System.out.println(info.description);
        System.out.println();
        System.out.println("Authors:");
        Iterator it = info.authors.iterator();
        while (it.hasNext()) {
            Author author = (Author)it.next();
            System.out.print("  " + author.name);
            if (author.email == null) {
                System.out.println();
                continue;
            }
            System.out.println(" <" + author.email + ">");
        }
        System.out.println();
        System.out.println(String.valueOf(title) + " license:");
        System.out.println(info.license);
        System.out.println();
        System.out.println(String.valueOf(title) + " support:");
        System.out.println(info.support);
        System.out.println();
        System.out.println(String.valueOf(title) + " web site: " + info.website);
        System.out.println();
    }

    private class Info {
        String title;
        String version;
        String copyright;
        String description;
        List authors;
        String license;
        String support;
        String website;

        Info() throws Exception {
            String INFO_FILENAME = "META-INF/info.xml";
            SAXBuilder builder = new SAXBuilder();
            JarFile jarFile = null;
            ZipEntry zipEntry = null;
            String classpath = System.getProperty("java.class.path");
            StringTokenizer tokenizer = new StringTokenizer(classpath, ";:");
            while (tokenizer.hasMoreTokens() && zipEntry == null) {
                String token = tokenizer.nextToken();
                try {
                    jarFile = new JarFile(token);
                    zipEntry = jarFile.getEntry("META-INF/info.xml");
                    continue;
                }
                catch (Exception v0) {}
            }
            if (zipEntry == null) {
                throw new FileNotFoundException("META-INF/info.xml not found; it should be within the JDOM JAR but isn't");
            }
            InputStream in = jarFile.getInputStream(zipEntry);
            Document doc = builder.build(in);
            Element root = doc.getRootElement();
            this.title = root.getChildTextTrim("title");
            this.version = root.getChildTextTrim("version");
            this.copyright = root.getChildTextTrim("copyright");
            this.description = root.getChildTextTrim("description");
            this.license = root.getChildTextTrim("license");
            this.support = root.getChildTextTrim("support");
            this.website = root.getChildTextTrim("web-site");
            List authorElements = root.getChildren("author");
            this.authors = new LinkedList();
            Iterator it = authorElements.iterator();
            while (it.hasNext()) {
                Element element = (Element)it.next();
                Author author = new Author();
                author.name = element.getChildTextTrim("name");
                author.email = element.getChildTextTrim("e-mail");
                this.authors.add(author);
            }
        }
    }

    private class Author {
        String name;
        String email;

        Author() {
        }
    }

}

