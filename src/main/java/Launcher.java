/*
 * Decompiled with CFR 0_118.
 */
import com.stefankrause.xplookandfeel.XPLookAndFeel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import javax.swing.UIManager;

public class Launcher {
    static /* synthetic */ Class array$Ljava$lang$String;

    public static void main(String[] arrstring) throws Exception {
        System.setProperty("sun.java2d.ddscale", "true");
        UIManager.setLookAndFeel("com.stefankrause.xplookandfeel.XPLookAndFeel");
        UIManager.addPropertyChangeListener(new PropertyChangeListener(){

            /*
             * Enabled force condition propagation
             * Lifted jumps to return sites
             */
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                Object object = propertyChangeEvent.getOldValue();
                Object object2 = propertyChangeEvent.getNewValue();
                if (object2 instanceof XPLookAndFeel) return;
                try {
                    UIManager.setLookAndFeel(new XPLookAndFeel());
                    return;
                }
                catch (Exception var4_4) {
                    var4_4.printStackTrace();
                }
            }
        });
        Class class_ = Class.forName(arrstring[0]);
        Class[] arrclass = new Class[1];
        Class class_2 = array$Ljava$lang$String == null ? (Launcher.array$Ljava$lang$String = Launcher.class$("[Ljava.lang.String;")) : array$Ljava$lang$String;
        arrclass[0] = class_2;
        Method method = class_.getMethod("main", arrclass);
        String[] arrstring2 = new String[arrstring.length - 1];
        int n = 1;
        while (n < arrstring.length) {
            arrstring2[n - 1] = arrstring[n];
            ++n;
        }
        method.invoke(class_, arrstring2);
    }

    static /* synthetic */ Class class$(String string) {
        try {
            return Class.forName(string);
        }
        catch (ClassNotFoundException var1_1) {
            throw new NoClassDefFoundError(var1_1.getMessage());
        }
    }

}

