/*
 * Decompiled with CFR 0_118.
 */
package util.statistical;

import annotador.utils.DocumentSingleton;
import annotador.utils.TEIConstants;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.io.PrintStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class TextsOfACategory {
    Document doc = DocumentSingleton.getInstance();
    XPath xpath = XPathFactory.newInstance().newXPath();

    public TextsOfACategory() {
        this.xpath.setNamespaceContext(new TEINamespaceContext());
    }

    public int countAllDocuments() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI", this.doc, XPathConstants.NODESET);
        return list.getLength();
    }

    public HashMap<String, ArrayList<String>> getAllKeywordGroupByCategoriesr(int documentIndex) throws XPathExpressionException {
        NodeList anchorList = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:anchor", this.doc, XPathConstants.NODESET);
        return this.getAllKeywordGroupByCategoriesInNodeList(anchorList);
    }

    public HashMap<String, ArrayList<String>> getAllKeywordGroupByCategoriesInNodeList(NodeList anchorList) {
        HashMap<String, ArrayList<String>> keywordsByCategory = new HashMap<String, ArrayList<String>>();
        for (int i = 0; i < anchorList.getLength(); ++i) {
            ArrayList keywords;
            Element anchor = (Element)anchorList.item(i);
            String type = anchor.getAttribute("type");
            if (anchor.getAttribute("next").equals("")) continue;
            if (keywordsByCategory.containsKey(type)) {
                keywords = keywordsByCategory.get(type);
            } else {
                keywords = new ArrayList();
                keywordsByCategory.put(type, keywords);
            }
            ArrayList<Node> allNodes = new ArrayList();
            allNodes = Utils.getInOrderRecursivo(0, anchor.getParentNode(), allNodes);
            String located = null;
            String text = "";
            for (Node actualNode : allNodes) {
                Element e;
                if (actualNode == anchor) {
                    located = anchor.getAttribute("next");
                }
                if (located == null) continue;
                if (actualNode instanceof Text) {
                    text = text + actualNode.getTextContent();
                }
                if (!(actualNode instanceof Element) || !(e = (Element)actualNode).getTagName().equalsIgnoreCase(TEIConstants.TEI_TAG_ANCHOR) || !e.getAttribute("prev").equalsIgnoreCase(located)) continue;
                break;
            }
            keywords.add(text);
        }
        return keywordsByCategory;
    }

    public String getDescription(String id) throws XPathExpressionException {
        return (String)this.xpath.evaluate("//tei:category[@xml:id='" + id + "']/tei:catDesc/text()", this.doc, XPathConstants.STRING);
    }

    public static void main(String[] args) throws XPathExpressionException {
        DocumentSingleton.loadFromFile(args[0]);
        TextsOfACategory textsOfACategory = new TextsOfACategory();
        StringWriter w = new StringWriter();
        int documents = textsOfACategory.countAllDocuments();
        HashMap<String, String> categories = new HashMap<String, String>();
        for (int i = 0; i < documents; ++i) {
            System.out.println("Processing Document:" + (i + 1));
            HashMap<String, ArrayList<String>> texts = textsOfACategory.getAllKeywordGroupByCategoriesr(i + 1);
            for (String category : texts.keySet()) {
                categories.put(category, category);
                for (String text : texts.get(category)) {
                    w.write("" + (i + 1) + ";" + textsOfACategory.getDescription(category) + ";" + text + "\n");
                }
            }
        }
        System.out.println(w.toString());
    }
}

