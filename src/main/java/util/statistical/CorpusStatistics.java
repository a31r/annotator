/*
 * Decompiled with CFR 0_118.
 */
package util.statistical;

import annotador.utils.DocumentSingleton;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class CorpusStatistics {
    Document doc = DocumentSingleton.getInstance();
    XPath xpath = XPathFactory.newInstance().newXPath();

    public CorpusStatistics() {
        this.xpath.setNamespaceContext(new TEINamespaceContext());
    }

    public int countAllCorpusAnchor() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:anchor", this.doc, XPathConstants.NODESET);
        return list.getLength() / 2;
    }

    public int countAllDocumentAnchor(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:anchor", this.doc, XPathConstants.NODESET);
        return list.getLength() / 2;
    }

    public int countAllSectionAnchor(int documentIndex, int sectionIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]//tei:anchor", this.doc, XPathConstants.NODESET);
        return list.getLength() / 2;
    }

    public int countAllWords() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:u", this.doc, XPathConstants.NODESET);
        int words = this.countWordinNodeList(list);
        return words;
    }

    public int countAllDocumentWords(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:u", this.doc, XPathConstants.NODESET);
        int words = this.countWordinNodeList(list);
        return words;
    }

    public int countAllSectionWords(int documentIndex, int sectionIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]//tei:u", this.doc, XPathConstants.NODESET);
        int words = this.countWordinNodeList(list);
        return words;
    }

    public int countAllAppliedCategories() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:div/@decls", this.doc, XPathConstants.NODESET);
        int appliedCategories = this.countAllAppliedCategoriesInNodeList(list);
        return appliedCategories;
    }

    public int countAllDocumentAppliedCategories(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div/@decls", this.doc, XPathConstants.NODESET);
        int appliedCategories = this.countAllAppliedCategoriesInNodeList(list);
        return appliedCategories;
    }

    public int countAllSectionAppliedCategories(int documentIndex, int sectionIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]/@decls", this.doc, XPathConstants.NODESET);
        int appliedCategories = this.countAllAppliedCategoriesInNodeList(list);
        return appliedCategories;
    }

    private int countAllAppliedCategoriesInNodeList(NodeList list) {
        int categories = 0;
        for (int i = 0; i < list.getLength(); ++i) {
            Node node = list.item(i);
            String text = "";
            if (node instanceof Text) {
                text = node.getTextContent();
            }
            if (node instanceof Attr) {
                Attr attr = (Attr)node;
                text = attr.getValue();
            }
            for (int j = 0; j < text.length(); ++j) {
                if (text.charAt(j) != '#') continue;
                ++categories;
            }
        }
        return categories;
    }

    public int countAllDifferentAppliedCategories() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:div/@decls", this.doc, XPathConstants.NODESET);
        int appliedCategories = this.countAllDifferentAppliedCategoriesInNodeList(list);
        return appliedCategories;
    }

    public int countAllDifferentDocumentAppliedCategories(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div/@decls", this.doc, XPathConstants.NODESET);
        int appliedCategories = this.countAllDifferentAppliedCategoriesInNodeList(list);
        return appliedCategories;
    }

    private int countAllDifferentAppliedCategoriesInNodeList(NodeList list) {
        HashMap<String, String> map = new HashMap<String, String>();
        Pattern pattern = Pattern.compile("#\\p{Graph}+", 64);
        int categories = 0;
        for (int i = 0; i < list.getLength(); ++i) {
            Node node = list.item(i);
            String text = "";
            if (node instanceof Text) {
                text = node.getTextContent();
            }
            if (node instanceof Attr) {
                Attr attr = (Attr)node;
                text = attr.getValue();
            }
            Matcher matcher = pattern.matcher(text);
            while (matcher.find()) {
                if (map.containsKey(text.substring(matcher.start(), matcher.end()))) continue;
                map.put(text.substring(matcher.start(), matcher.end()), ";-)<=>");
                ++categories;
            }
        }
        return categories;
    }

    private int countWordinNodeList(NodeList list) {
        int wordCount = 0;
        for (int i = 0; i < list.getLength(); ++i) {
            Element node = (Element)list.item(i);
            wordCount += this.countWordinNode(node);
        }
        return wordCount;
    }

    private int countWordinNode(Node node) {
        ArrayList<Node> childs = new ArrayList();
        childs = Utils.getInOrderRecursivo(0, node, childs);
        int wordCount = 0;
        for (Node n : childs) {
            if (!(n instanceof Text)) continue;
            String text = n.getTextContent();
            wordCount = (int)((long)wordCount + CorpusStatistics.countWords(text));
        }
        return wordCount;
    }

    public static long countWords(String line) {
        long numWords = 0;
        int index = 0;
        boolean prevWhitespace = true;
        while (index < line.length()) {
            char c = line.charAt(index++);
            boolean currWhitespace = Character.isWhitespace(c);
            if (prevWhitespace && !currWhitespace) {
                ++numWords;
            }
            prevWhitespace = currWhitespace;
        }
        return numWords;
    }

    public int countAllDocuments() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI", this.doc, XPathConstants.NODESET);
        return list.getLength();
    }

    public int countAllSections() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:div", this.doc, XPathConstants.NODESET);
        return list.getLength();
    }

    public int countAllUtterances() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:u", this.doc, XPathConstants.NODESET);
        return list.getLength();
    }

    public int countAllDocumentSection(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div", this.doc, XPathConstants.NODESET);
        return list.getLength();
    }

    public int countAllDocumentUtterance(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:u", this.doc, XPathConstants.NODESET);
        return list.getLength();
    }

    public int countAllSectionUterance(int documentIndex, int sectionIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]//tei:u", this.doc, XPathConstants.NODESET);
        return list.getLength();
    }

    public HashMap<String, Integer> countAppliedCategoriesGrupByCategories() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:div/@decls", this.doc, XPathConstants.NODESET);
        return this.countAllAppliedCategoriesGrupByCategoriesInNodeList(list);
    }

    public HashMap<String, Integer> countAppliedCategoriesGrupByCategories(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div/@decls", this.doc, XPathConstants.NODESET);
        return this.countAllAppliedCategoriesGrupByCategoriesInNodeList(list);
    }

    private HashMap<String, Integer> countAllAppliedCategoriesGrupByCategoriesInNodeList(NodeList list) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        for (int i = 0; i < list.getLength(); ++i) {
            Node node = list.item(i);
            String text = "";
            if (node instanceof Text) {
                text = node.getTextContent();
            }
            if (node instanceof Attr) {
                Attr attr = (Attr)node;
                text = attr.getValue();
            }
            Pattern pattern = Pattern.compile("#\\p{Graph}+", 64);
            Matcher matcher = pattern.matcher(text);
            while (matcher.find()) {
                int v = 0;
                if (map.containsKey(text.substring(matcher.start(), matcher.end()))) {
                    v = map.get(text.substring(matcher.start(), matcher.end()));
                }
                map.put(text.substring(matcher.start(), matcher.end()), ++v);
            }
        }
        return map;
    }

    public HashMap<String, Integer> countAllKeywordGroupByCategories() throws XPathExpressionException {
        NodeList anchorList = (NodeList)this.xpath.evaluate("//tei:anchor", this.doc, XPathConstants.NODESET);
        NodeList declsList = (NodeList)this.xpath.evaluate("//tei:div/@decls", this.doc, XPathConstants.NODESET);
        return this.countAllKeywordGroupByCategoriesInNodeList(anchorList, declsList);
    }

    public HashMap<String, Integer> countAllKeywordGroupByCategoriesr(int documentIndex) throws XPathExpressionException {
        NodeList anchorList = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:anchor", this.doc, XPathConstants.NODESET);
        NodeList declsList = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div/@decls", this.doc, XPathConstants.NODESET);
        return this.countAllKeywordGroupByCategoriesInNodeList(anchorList, declsList);
    }

    public HashMap<String, Integer> countAllKeywordGroupByCategories(int documentIndex, int sectionIndex) throws XPathExpressionException {
        NodeList anchorList = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]//tei:anchor", this.doc, XPathConstants.NODESET);
        NodeList declsList = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]/@decls", this.doc, XPathConstants.NODESET);
        return this.countAllKeywordGroupByCategoriesInNodeList(anchorList, declsList);
    }

    public HashMap<String, Integer> countAllWordInKeywordGroupByCategories() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:anchor", this.doc, XPathConstants.NODESET);
        return this.countAllWordInKeywordGroupByCategoriesInNodeList(list);
    }

    public HashMap<String, Integer> countAllWordInKeywordGroupByCategories(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:anchor", this.doc, XPathConstants.NODESET);
        return this.countAllWordInKeywordGroupByCategoriesInNodeList(list);
    }

    public HashMap<String, Integer> countAllWordInKeywordGroupByCategories(int documentIndex, int sectionIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]//tei:anchor", this.doc, XPathConstants.NODESET);
        return this.countAllWordInKeywordGroupByCategoriesInNodeList(list);
    }

    public HashMap<String, Integer> countAllWordInKeywordGroupByCategoriesInNodeList(NodeList list) {
        HashMap<String, Integer> frecuency = new HashMap<String, Integer>();
        for (int i = 0; i < list.getLength(); ++i) {
            Element anchor = (Element)list.item(i);
            String type = "#" + anchor.getAttribute("type");
            String id = anchor.getAttribute("xml:id");
            Integer frec = 0;
            if (frecuency.containsKey(type)) {
                frec = frecuency.get(type);
            }
            if (!anchor.getAttribute("next").equals("")) {
                ArrayList<Node> childs = new ArrayList();
                Utils.getInOrderRecursivo(0, anchor.getParentNode(), childs);
                boolean start = false;
                for (Node n : childs) {
                    if (n == anchor) {
                        start = true;
                    }
                    if (!start) continue;
                    if (n instanceof Element && n.getNodeName().equals("anchor") && ((Element)n).getAttribute("prev").equals(id)) {
                        start = false;
                    }
                    if (!(n instanceof Text)) continue;
                    String s = n.getTextContent();
                    int word = (int)CorpusStatistics.countWords(s);
                    frec = frec + word;
                }
            }
            frecuency.put(type, frec);
        }
        return frecuency;
    }

    public HashMap<String, Integer> countAllKeywordGroupByCategoriesInNodeList(NodeList anchorList, NodeList declsList) {
        int i;
        HashMap<String, Integer> frecuency = new HashMap<String, Integer>();
        for (i = 0; i < declsList.getLength(); ++i) {
            Node node = declsList.item(i);
            String text = "";
            if (node instanceof Text) {
                text = node.getTextContent();
            }
            if (node instanceof Attr) {
                Attr attr = (Attr)node;
                text = attr.getValue();
            }
            Pattern p = Pattern.compile("#\\p{Alnum}+", 64);
            Matcher m = p.matcher(text);
            while (m.find()) {
                String s = text.substring(m.start(), m.end());
                frecuency.put(s, 0);
            }
        }
        for (i = 0; i < anchorList.getLength(); ++i) {
            Element anchor = (Element)anchorList.item(i);
            String type = "#" + anchor.getAttribute("type");
            if (anchor.getAttribute("next").equals("")) continue;
            Integer frec = 0;
            if (frecuency.containsKey(type)) {
                frec = frecuency.get(type);
            }
            frec = frec + 1;
            frecuency.put(type, frec);
        }
        return frecuency;
    }

    public int countValues(HashMap<String, Integer> values) {
        int i = 0;
        for (Integer value : values.values()) {
            i += value.intValue();
        }
        return i;
    }

    public HashMap<String, Double> getMean(HashMap<String, Integer> numberOfItems, HashMap<String, Integer> valuesOfItems) {
        HashMap<String, Double> result = new HashMap<String, Double>();
        for (String key : numberOfItems.keySet()) {
            double value = valuesOfItems.containsKey(key) ? (double)valuesOfItems.get(key).intValue() / (double)numberOfItems.get(key).intValue() : 0.0;
            result.put(key, value);
        }
        return result;
    }

    public void printKeywordHashMap(HashMap<String, Integer> map, String prefix) {
        for (String key : map.keySet()) {
            Integer number = map.get(key);
            System.out.println(prefix + "Category: " + key + "\t\t N\u00ba Of Keywords: " + number);
        }
    }

    public void printWordInKeywordHashMap(HashMap<String, Integer> map, String prefix) {
        for (String key : map.keySet()) {
            Integer number = map.get(key);
            System.out.println(prefix + "Category: " + key + "\t\t N\u00ba Of Word In Keywords: " + number);
        }
    }

    public void printAverageWordInKeywordHashMap(HashMap<String, Double> map, String prefix) {
        for (String key : map.keySet()) {
            Double number = map.get(key);
            System.out.println(prefix + "Category: " + key + "\t\t Average Word In Keywords: " + number);
        }
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Syntax error. Use java CorpusStatistics corpusFile.xml");
            System.exit(1);
        }
        DocumentSingleton.loadFromFile(args[0]);
        CorpusStatistics talc08 = new CorpusStatistics();
        try {
            System.out.println("Processing Corpus: " + args[0]);
            System.out.println();
            System.out.println("Corpus Level");
            System.out.println("------------");
            int corpusAnchor = talc08.countAllCorpusAnchor();
            System.out.println("N\u00ba Of Anchor Tag in whole corpus is: " + corpusAnchor);
            int documents = talc08.countAllDocuments();
            System.out.println("N\u00ba Documents in the corpus is: " + documents);
            int sections = talc08.countAllSections();
            System.out.println("N\u00ba Of Sections in the corpus is: " + sections);
            int utterances = talc08.countAllUtterances();
            System.out.println("N\u00ba Of Utterances in the corpus is: " + utterances);
            int words = talc08.countAllWords();
            System.out.println("N\u00ba of Words in the corpus is: " + words);
            int appliedCategories = talc08.countAllAppliedCategories();
            System.out.println("N\u00ba of Applied Categories in the corpus is: " + appliedCategories);
            int differentAppliedCategories = talc08.countAllDifferentAppliedCategories();
            System.out.println("N\u00ba of Different Appliued Categories in the corpus is: " + differentAppliedCategories);
            HashMap<String, Integer> keywordGroupByCategory = talc08.countAllKeywordGroupByCategories();
            System.out.println();
            System.out.println("This is the list of the N\u00ba Of association category-keyword in the whole corpus");
            System.out.println("------------------------------------------------------------------------------");
            talc08.printKeywordHashMap(keywordGroupByCategory, "");
            HashMap<String, Integer> categoriesGroupByCategory = talc08.countAppliedCategoriesGrupByCategories();
            System.out.println();
            System.out.println("This is the list of the N\u00ba Of categories group by category in the whole corpus");
            System.out.println("------------------------------------------------------------------------------");
            talc08.printKeywordHashMap(categoriesGroupByCategory, "");
            HashMap<String, Integer> wordInwordGroupByCategory = talc08.countAllWordInKeywordGroupByCategories();
            System.out.println();
            System.out.println("This is the list of the N\u00ba Of Word in Keyword associated to categorys in the whole corpus");
            System.out.println("------------------------------------------------------------------------------");
            talc08.printWordInKeywordHashMap(wordInwordGroupByCategory, "");
            HashMap<String, Double> meanOfWordInKeywordGroupByCategory = talc08.getMean(keywordGroupByCategory, wordInwordGroupByCategory);
            System.out.println();
            System.out.println("This is the Average N\u00ba Of Words in Keyword associated to categorys in the whole corpus");
            System.out.println("------------------------------------------------------------------------------");
            talc08.printAverageWordInKeywordHashMap(meanOfWordInKeywordGroupByCategory, "");
            System.out.println("");
            int numberOfKeyWords = talc08.countValues(keywordGroupByCategory);
            System.out.println("N\u00ba of KeyWords in whole corpus: " + numberOfKeyWords);
            int numberOfWordsinKeyWords = talc08.countValues(wordInwordGroupByCategory);
            System.out.println("N\u00ba of Word in Keyword in whole corpus: " + numberOfWordsinKeyWords);
            double averageNumberOfWordInKeywords = numberOfWordsinKeyWords / numberOfKeyWords;
            System.out.println("Average N\u00ba of Word in Keyword in whole corpus: " + averageNumberOfWordInKeywords);
            for (int i = 0; i < documents; ++i) {
                System.out.println();
                System.out.println("Document Level");
                System.out.println("------------");
                System.out.println();
                System.out.println("\tProcesing document N\u00ba: " + (i + 1));
                int documentSections = talc08.countAllDocumentSection(i + 1);
                System.out.println("\tN\u00ba Of Sections in this document: " + documentSections);
                int documentAnchor = talc08.countAllDocumentAnchor(i + 1);
                System.out.println("\tN\u00ba Of Anchor in this document: " + documentAnchor);
                int utterancesDocument = talc08.countAllDocumentUtterance(i + 1);
                System.out.println("\tN\u00ba Of Utterances in this document: " + utterancesDocument);
                int documentWords = talc08.countAllDocumentWords(i + 1);
                System.out.println("\tN\u00ba Of Word in this docuemnt: " + documentWords);
                int documentAppliedCategories = talc08.countAllDocumentAppliedCategories(i + 1);
                System.out.println("\tN\u00ba Of Applied Categories in this document: " + documentAppliedCategories);
                int documentDifferentAppliedCategories = talc08.countAllDifferentDocumentAppliedCategories(i + 1);
                System.out.println("\tN\u00ba Of Different Applied Categories in this document: " + documentDifferentAppliedCategories);
                HashMap<String, Integer> documentKeywordGroupByCategory = talc08.countAllKeywordGroupByCategoriesr(i + 1);
                System.out.println();
                System.out.println("\tThis is the list of the N\u00ba Of association category-keyword in this document");
                System.out.println("\t------------------------------------------------------------------------------");
                talc08.printKeywordHashMap(documentKeywordGroupByCategory, "\t");
                HashMap<String, Integer> documentAppliedCategoriesGroupByCategory = talc08.countAppliedCategoriesGrupByCategories(i + 1);
                System.out.println();
                System.out.println("\tThis is the list of the applied categories grouped by categories in this document");
                System.out.println("\t------------------------------------------------------------------------------");
                talc08.printKeywordHashMap(documentAppliedCategoriesGroupByCategory, "\t");
                HashMap<String, Integer> documentWordInwordGroupByCategory = talc08.countAllWordInKeywordGroupByCategories(i + 1);
                System.out.println();
                System.out.println("\tThis is the list of the N\u00ba Of Word in Keyword associated to categorys in this document");
                System.out.println("\t------------------------------------------------------------------------------");
                talc08.printWordInKeywordHashMap(documentWordInwordGroupByCategory, "\t");
                HashMap<String, Double> documentMeanOfWordInKeywordGroupByCategory = talc08.getMean(documentKeywordGroupByCategory, documentWordInwordGroupByCategory);
                System.out.println();
                System.out.println("\tThis is the Average N\u00ba Of Words in Keyword associated to categorys in this document");
                System.out.println("\t------------------------------------------------------------------------------");
                talc08.printAverageWordInKeywordHashMap(documentMeanOfWordInKeywordGroupByCategory, "\t");
                System.out.println("");
                int documentNumberOfKeyWords = talc08.countValues(documentKeywordGroupByCategory);
                System.out.println("\tN\u00ba of KeyWords in this document: " + documentNumberOfKeyWords);
                int documentNumberOfWordsinKeyWords = talc08.countValues(documentWordInwordGroupByCategory);
                System.out.println("\tN\u00ba of Word in Keyword in this document: " + documentNumberOfWordsinKeyWords);
                double documentAverageNumberOfWordInKeywords = documentNumberOfWordsinKeyWords / documentNumberOfKeyWords;
                System.out.println("\tAverage N\u00ba of Word in Keyword in this document: " + documentAverageNumberOfWordInKeywords);
                for (int j = 0; j < sections; ++j) {
                    System.out.println();
                    System.out.println("\t\tSection Level");
                    System.out.println("\t\t------------");
                    System.out.println();
                    System.out.println("\t\tProcesing section N\u00ba: " + (j + 1) + " Of document N\u00ba: " + (i + 1));
                    int sectionUtterances = talc08.countAllSectionUterance(i + 1, j + 1);
                    System.out.println("\t\tN\u00ba of Utterances in this section: " + sectionUtterances);
                    int sectionAnchor = talc08.countAllSectionAnchor(i + 1, j + 1);
                    System.out.println("\t\tN\u00ba of Anchor Tag in this section: " + sectionAnchor);
                    int sectionWords = talc08.countAllSectionWords(i + 1, j + 1);
                    System.out.println("\t\tN\u00ba of Words in this section: " + sectionWords);
                    int sectionAppliedCategories = talc08.countAllSectionAppliedCategories(i + 1, j + 1);
                    System.out.println("\t\tN\u00ba of Applied Categories in this section: " + sectionAppliedCategories);
                    HashMap<String, Integer> sectionKeywordGroupByCategory = talc08.countAllKeywordGroupByCategories(i + 1, j + 1);
                    System.out.println();
                    System.out.println("\t\tThis is the list of the N\u00ba Of association category-keyword in this section");
                    System.out.println("\t\t------------------------------------------------------------------------------");
                    talc08.printKeywordHashMap(sectionKeywordGroupByCategory, "\t\t");
                    HashMap<String, Integer> sectionWordInwordGroupByCategory = talc08.countAllWordInKeywordGroupByCategories(i + 1, j + 1);
                    System.out.println();
                    System.out.println("\t\tThis is the list of the N\u00ba Of Word in Keyword associated to categorys in this section");
                    System.out.println("\t\t------------------------------------------------------------------------------");
                    talc08.printWordInKeywordHashMap(sectionWordInwordGroupByCategory, "\t\t");
                    System.out.println();
                    System.out.println("\t\tThis is the Average N\u00ba Of Words in Keyword associated to categorys in this section");
                    System.out.println("\t\t------------------------------------------------------------------------------");
                    HashMap<String, Double> sectionMeanOfWordInKeywordGroupByCategory = talc08.getMean(sectionKeywordGroupByCategory, sectionWordInwordGroupByCategory);
                    talc08.printAverageWordInKeywordHashMap(sectionMeanOfWordInKeywordGroupByCategory, "\t\t");
                    System.out.println("");
                    int sectionNumberOfKeyWords = talc08.countValues(sectionKeywordGroupByCategory);
                    System.out.println("\t\tN\u00ba of KeyWords in this section: " + sectionNumberOfKeyWords);
                    int sectionNumberOfWordsinKeyWords = talc08.countValues(sectionWordInwordGroupByCategory);
                    System.out.println("\t\tN\u00ba of Word in Keyword in this section: " + sectionNumberOfWordsinKeyWords);
                    double sectionAverageNumberOfWordInKeywords = sectionNumberOfWordsinKeyWords / sectionNumberOfKeyWords;
                    System.out.println("\t\tAverage N\u00ba of Word in Keyword in this section: " + sectionAverageNumberOfWordInKeywords);
                }
            }
        }
        catch (XPathExpressionException e) {
            e.printStackTrace();
        }
    }
}

