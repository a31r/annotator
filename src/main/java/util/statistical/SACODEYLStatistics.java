/*
 * Decompiled with CFR 0_118.
 */
package util.statistical;

import annotador.utils.DocumentSingleton;
import annotador.utils.TEINamespaceContext;
import annotador.utils.Utils;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class SACODEYLStatistics {
    Document doc = DocumentSingleton.getInstance();
    XPath xpath = XPathFactory.newInstance().newXPath();

    public SACODEYLStatistics() {
        this.xpath.setNamespaceContext(new TEINamespaceContext());
    }

    public int countAllCorpusAnchor() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:anchor", this.doc, XPathConstants.NODESET);
        return list.getLength() / 2;
    }

    public int countAllDocumentAnchor(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:anchor", this.doc, XPathConstants.NODESET);
        return list.getLength() / 2;
    }

    public int countAllSectionAnchor(int documentIndex, int sectionIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]//tei:anchor", this.doc, XPathConstants.NODESET);
        return list.getLength() / 2;
    }

    public int countAllWords() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:u", this.doc, XPathConstants.NODESET);
        int words = this.countWordinNodeList(list);
        return words;
    }

    public int countAllDocumentWords(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:u", this.doc, XPathConstants.NODESET);
        int words = this.countWordinNodeList(list);
        return words;
    }

    public int countAllSectionWords(int documentIndex, int sectionIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]//tei:u", this.doc, XPathConstants.NODESET);
        int words = this.countWordinNodeList(list);
        return words;
    }

    public int countAllAppliedCategories() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:div/@decls", this.doc, XPathConstants.NODESET);
        int appliedCategories = this.countAllAppliedCategoriesInNodeList(list);
        return appliedCategories;
    }

    public int countAllDocumentAppliedCategories(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div/@decls", this.doc, XPathConstants.NODESET);
        int appliedCategories = this.countAllAppliedCategoriesInNodeList(list);
        return appliedCategories;
    }

    public int countAllSectionAppliedCategories(int documentIndex, int sectionIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]/@decls", this.doc, XPathConstants.NODESET);
        int appliedCategories = this.countAllAppliedCategoriesInNodeList(list);
        return appliedCategories;
    }

    private int countAllAppliedCategoriesInNodeList(NodeList list) {
        int categories = 0;
        for (int i = 0; i < list.getLength(); ++i) {
            Node node = list.item(i);
            String text = "";
            if (node instanceof Text) {
                text = node.getTextContent();
            }
            if (node instanceof Attr) {
                Attr attr = (Attr)node;
                text = attr.getValue();
            }
            for (int j = 0; j < text.length(); ++j) {
                if (text.charAt(j) != '#') continue;
                ++categories;
            }
        }
        return categories;
    }

    public int countAllDifferentAppliedCategories() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:div/@decls", this.doc, XPathConstants.NODESET);
        int appliedCategories = this.countAllDifferentAppliedCategoriesInNodeList(list);
        return appliedCategories;
    }

    public int countAllDifferentDocumentAppliedCategories(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div/@decls", this.doc, XPathConstants.NODESET);
        int appliedCategories = this.countAllDifferentAppliedCategoriesInNodeList(list);
        return appliedCategories;
    }

    private int countAllDifferentAppliedCategoriesInNodeList(NodeList list) {
        HashMap<String, String> map = new HashMap<String, String>();
        Pattern pattern = Pattern.compile("#\\p{Graph}+", 64);
        int categories = 0;
        for (int i = 0; i < list.getLength(); ++i) {
            Node node = list.item(i);
            String text = "";
            if (node instanceof Text) {
                text = node.getTextContent();
            }
            if (node instanceof Attr) {
                Attr attr = (Attr)node;
                text = attr.getValue();
            }
            Matcher matcher = pattern.matcher(text);
            while (matcher.find()) {
                if (map.containsKey(text.substring(matcher.start(), matcher.end()))) continue;
                map.put(text.substring(matcher.start(), matcher.end()), ";-)<=>");
                ++categories;
            }
        }
        return categories;
    }

    private int countWordinNodeList(NodeList list) {
        int wordCount = 0;
        for (int i = 0; i < list.getLength(); ++i) {
            Element node = (Element)list.item(i);
            wordCount += this.countWordinNode(node);
        }
        return wordCount;
    }

    private int countWordinNode(Node node) {
        ArrayList<Node> childs = new ArrayList();
        childs = Utils.getInOrderRecursivo(0, node, childs);
        int wordCount = 0;
        for (Node n : childs) {
            if (!(n instanceof Text)) continue;
            String text = n.getTextContent();
            wordCount = (int)((long)wordCount + SACODEYLStatistics.countWords(text));
        }
        return wordCount;
    }

    public static long countWords(String line) {
        long numWords = 0;
        int index = 0;
        boolean prevWhitespace = true;
        while (index < line.length()) {
            char c = line.charAt(index++);
            boolean currWhitespace = Character.isWhitespace(c);
            if (prevWhitespace && !currWhitespace) {
                ++numWords;
            }
            prevWhitespace = currWhitespace;
        }
        return numWords;
    }

    public int countAllDocuments() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI", this.doc, XPathConstants.NODESET);
        return list.getLength();
    }

    public int countAllSections() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:div", this.doc, XPathConstants.NODESET);
        return list.getLength();
    }

    public int countAllUtterances() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:u", this.doc, XPathConstants.NODESET);
        return list.getLength();
    }

    public int countAllDocumentSection(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div", this.doc, XPathConstants.NODESET);
        return list.getLength();
    }

    public int countAllDocumentUtterance(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:u", this.doc, XPathConstants.NODESET);
        return list.getLength();
    }

    public int countAllSectionUterance(int documentIndex, int sectionIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]//tei:u", this.doc, XPathConstants.NODESET);
        return list.getLength();
    }

    public HashMap<String, Integer> countAppliedCategoriesGrupByCategories() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:div/@decls", this.doc, XPathConstants.NODESET);
        return this.countAllAppliedCategoriesGrupByCategoriesInNodeList(list);
    }

    public HashMap<String, Integer> countAppliedCategoriesGrupByCategories(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div/@decls", this.doc, XPathConstants.NODESET);
        return this.countAllAppliedCategoriesGrupByCategoriesInNodeList(list);
    }

    private HashMap<String, Integer> countAllAppliedCategoriesGrupByCategoriesInNodeList(NodeList list) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        for (int i = 0; i < list.getLength(); ++i) {
            Node node = list.item(i);
            String text = "";
            if (node instanceof Text) {
                text = node.getTextContent();
            }
            if (node instanceof Attr) {
                Attr attr = (Attr)node;
                text = attr.getValue();
            }
            Pattern pattern = Pattern.compile("#\\p{Graph}+", 64);
            Matcher matcher = pattern.matcher(text);
            while (matcher.find()) {
                int v = 0;
                if (map.containsKey(text.substring(matcher.start(), matcher.end()))) {
                    v = map.get(text.substring(matcher.start(), matcher.end()));
                }
                map.put(text.substring(matcher.start(), matcher.end()), ++v);
            }
        }
        return map;
    }

    public HashMap<String, Integer> countAllKeywordGroupByCategories() throws XPathExpressionException {
        NodeList anchorList = (NodeList)this.xpath.evaluate("//tei:anchor", this.doc, XPathConstants.NODESET);
        NodeList declsList = (NodeList)this.xpath.evaluate("//tei:div/@decls", this.doc, XPathConstants.NODESET);
        return this.countAllKeywordGroupByCategoriesInNodeList(anchorList, declsList);
    }

    public HashMap<String, Integer> countAllKeywordGroupByCategoriesr(int documentIndex) throws XPathExpressionException {
        NodeList anchorList = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:anchor", this.doc, XPathConstants.NODESET);
        NodeList declsList = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div/@decls", this.doc, XPathConstants.NODESET);
        return this.countAllKeywordGroupByCategoriesInNodeList(anchorList, declsList);
    }

    public HashMap<String, Integer> countAllKeywordGroupByCategories(int documentIndex, int sectionIndex) throws XPathExpressionException {
        NodeList anchorList = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]//tei:anchor", this.doc, XPathConstants.NODESET);
        NodeList declsList = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]/@decls", this.doc, XPathConstants.NODESET);
        return this.countAllKeywordGroupByCategoriesInNodeList(anchorList, declsList);
    }

    public HashMap<String, Integer> countAllWordInKeywordGroupByCategories() throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:anchor", this.doc, XPathConstants.NODESET);
        return this.countAllWordInKeywordGroupByCategoriesInNodeList(list);
    }

    public HashMap<String, Integer> countAllWordInKeywordGroupByCategories(int documentIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:anchor", this.doc, XPathConstants.NODESET);
        return this.countAllWordInKeywordGroupByCategoriesInNodeList(list);
    }

    public HashMap<String, Integer> countAllWordInKeywordGroupByCategories(int documentIndex, int sectionIndex) throws XPathExpressionException {
        NodeList list = (NodeList)this.xpath.evaluate("//tei:TEI[" + documentIndex + "]//tei:div[" + sectionIndex + "]//tei:anchor", this.doc, XPathConstants.NODESET);
        return this.countAllWordInKeywordGroupByCategoriesInNodeList(list);
    }

    public HashMap<String, Integer> countAllWordInKeywordGroupByCategoriesInNodeList(NodeList list) {
        HashMap<String, Integer> frecuency = new HashMap<String, Integer>();
        for (int i = 0; i < list.getLength(); ++i) {
            Element anchor = (Element)list.item(i);
            String type = "#" + anchor.getAttribute("type");
            String id = anchor.getAttribute("xml:id");
            Integer frec = 0;
            if (frecuency.containsKey(type)) {
                frec = frecuency.get(type);
            }
            if (!anchor.getAttribute("next").equals("")) {
                ArrayList<Node> childs = new ArrayList();
                Utils.getInOrderRecursivo(0, anchor.getParentNode(), childs);
                boolean start = false;
                for (Node n : childs) {
                    if (n == anchor) {
                        start = true;
                    }
                    if (!start) continue;
                    if (n instanceof Element && n.getNodeName().equals("anchor") && ((Element)n).getAttribute("prev").equals(id)) {
                        start = false;
                    }
                    if (!(n instanceof Text)) continue;
                    String s = n.getTextContent();
                    int word = (int)SACODEYLStatistics.countWords(s);
                    frec = frec + word;
                }
            }
            frecuency.put(type, frec);
        }
        return frecuency;
    }

    public HashMap<String, Integer> countAllKeywordGroupByCategoriesInNodeList(NodeList anchorList, NodeList declsList) {
        int i;
        HashMap<String, Integer> frecuency = new HashMap<String, Integer>();
        for (i = 0; i < declsList.getLength(); ++i) {
            Node node = declsList.item(i);
            String text = "";
            if (node instanceof Text) {
                text = node.getTextContent();
            }
            if (node instanceof Attr) {
                Attr attr = (Attr)node;
                text = attr.getValue();
            }
            Pattern p = Pattern.compile("#\\p{Alnum}+", 64);
            Matcher m = p.matcher(text);
            while (m.find()) {
                String s = text.substring(m.start(), m.end());
                frecuency.put(s, 0);
            }
        }
        for (i = 0; i < anchorList.getLength(); ++i) {
            Element anchor = (Element)anchorList.item(i);
            String type = "#" + anchor.getAttribute("type");
            if (anchor.getAttribute("next").equals("")) continue;
            Integer frec = 0;
            if (frecuency.containsKey(type)) {
                frec = frecuency.get(type);
            }
            frec = frec + 1;
            frecuency.put(type, frec);
        }
        return frecuency;
    }

    public int countValues(HashMap<String, Integer> values) {
        int i = 0;
        for (Integer value : values.values()) {
            i += value.intValue();
        }
        return i;
    }

    public HashMap<String, Double> getMean(HashMap<String, Integer> numberOfItems, HashMap<String, Integer> valuesOfItems) {
        HashMap<String, Double> result = new HashMap<String, Double>();
        for (String key : numberOfItems.keySet()) {
            double value = valuesOfItems.containsKey(key) ? (double)valuesOfItems.get(key).intValue() / (double)numberOfItems.get(key).intValue() : 0.0;
            result.put(key, value);
        }
        return result;
    }

    public void printKeywordHashMap(HashMap<String, Integer> map, String prefix, Writer fout) throws IOException {
        for (String key : map.keySet()) {
            Integer number = map.get(key);
            fout.write(prefix + "Category: " + key + "\t\t N\u00ba Of Keywords: " + number + "\n");
        }
    }

    public void printWordInKeywordHashMap(HashMap<String, Integer> map, String prefix, Writer fout) throws IOException {
        for (String key : map.keySet()) {
            Integer number = map.get(key);
            fout.write(prefix + "Category: " + key + "\t\t N\u00ba Of Word In Keywords: " + number + "\n");
        }
    }

    public void printAverageWordInKeywordHashMap(HashMap<String, Double> map, String prefix, Writer fout) throws IOException {
        for (String key : map.keySet()) {
            Double number = map.get(key);
            fout.write(prefix + "Category: " + key + "\t\t Average Word In Keywords: " + number + "\n");
        }
    }

    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.out.println("Syntax error. Use java SACODEYLStatistics corpusDirectoryPath");
            System.exit(1);
        }
        File files = new File(args[0]);
        for (String file : files.list()) {
            DocumentSingleton.loadFromFile(file);
            SACODEYLStatistics talc08 = new SACODEYLStatistics();
            FileWriter fout = new FileWriter(file + ".out");
            try {
                System.out.println("Processing Corpus: " + file);
                fout.write("Processing Corpus: " + file + "\n");
                fout.write("\n");
                fout.write("Corpus Level\n");
                fout.write("------------\n");
                int corpusAnchor = talc08.countAllCorpusAnchor();
                fout.write("N\u00ba Of Keywords in whole corpus is: " + corpusAnchor + "\n");
                int documents = talc08.countAllDocuments();
                fout.write("N\u00ba Documents in the corpus is: " + documents + "\n");
                int sections = talc08.countAllSections();
                fout.write("N\u00ba Of Sections in the corpus is: " + sections + "\n");
                int utterances = talc08.countAllUtterances();
                fout.write("N\u00ba Of Utterances in the corpus is: " + utterances + "\n");
                int words = talc08.countAllWords();
                fout.write("N\u00ba of Words in the corpus is: " + words + "\n");
                int appliedCategories = talc08.countAllAppliedCategories();
                fout.write("N\u00ba of Applied Categories in the corpus is: " + appliedCategories + "\n");
                int differentAppliedCategories = talc08.countAllDifferentAppliedCategories();
                fout.write("N\u00ba of Different Appliued Categories in the corpus is: " + differentAppliedCategories + "\n");
                HashMap<String, Integer> keywordGroupByCategory = talc08.countAllKeywordGroupByCategories();
                fout.write("\n");
                fout.write("This is the list of the N\u00ba Of association category-keyword in the whole corpus\n");
                fout.write("------------------------------------------------------------------------------\n");
                talc08.printKeywordHashMap(keywordGroupByCategory, "", fout);
                HashMap<String, Integer> categoriesGroupByCategory = talc08.countAppliedCategoriesGrupByCategories();
                fout.write("\n");
                fout.write("This is the list of the N\u00ba Of categories group by category in the whole corpus\n");
                fout.write("------------------------------------------------------------------------------\n");
                talc08.printKeywordHashMap(categoriesGroupByCategory, "", fout);
                HashMap<String, Integer> wordInwordGroupByCategory = talc08.countAllWordInKeywordGroupByCategories();
                fout.write("\n");
                fout.write("This is the list of the N\u00ba Of Word in Keyword associated to categorys in the whole corpus\n");
                fout.write("------------------------------------------------------------------------------\n");
                talc08.printWordInKeywordHashMap(wordInwordGroupByCategory, "", fout);
                HashMap<String, Double> meanOfWordInKeywordGroupByCategory = talc08.getMean(keywordGroupByCategory, wordInwordGroupByCategory);
                fout.write("\n");
                fout.write("This is the Average N\u00ba Of Words in Keyword associated to categorys in the whole corpus\n");
                fout.write("------------------------------------------------------------------------------\n");
                talc08.printAverageWordInKeywordHashMap(meanOfWordInKeywordGroupByCategory, "", fout);
                fout.write("\n");
                int numberOfKeyWords = talc08.countValues(keywordGroupByCategory);
                fout.write("N\u00ba of KeyWords in whole corpus: " + numberOfKeyWords + "\n");
                int numberOfWordsinKeyWords = talc08.countValues(wordInwordGroupByCategory);
                fout.write("N\u00ba of Word in Keyword in whole corpus: " + numberOfWordsinKeyWords + "\n");
                double averageNumberOfWordInKeywords = numberOfWordsinKeyWords / numberOfKeyWords;
                fout.write("Average N\u00ba of Word in Keyword in whole corpus: " + averageNumberOfWordInKeywords + "\n");
                for (int i = 0; i < documents; ++i) {
                    double documentAverageNumberOfWordInKeywords;
                    fout.write("\n");
                    fout.write("Document Level\n");
                    fout.write("------------\n");
                    fout.write("\n");
                    fout.write("\tProcesing document N\u00ba: " + (i + 1) + "\n");
                    System.out.println("\tProcesing document N\u00ba: " + (i + 1));
                    int documentSections = talc08.countAllDocumentSection(i + 1);
                    fout.write("\tN\u00ba Of Sections in this document: " + documentSections + "\n");
                    int documentAnchor = talc08.countAllDocumentAnchor(i + 1);
                    fout.write("\tN\u00ba Of Keywords in this document: " + documentAnchor + "\n");
                    int utterancesDocument = talc08.countAllDocumentUtterance(i + 1);
                    fout.write("\tN\u00ba Of Utterances in this document: " + utterancesDocument + "\n");
                    int documentWords = talc08.countAllDocumentWords(i + 1);
                    fout.write("\tN\u00ba Of Word in this docuemnt: " + documentWords + "\n");
                    int documentAppliedCategories = talc08.countAllDocumentAppliedCategories(i + 1);
                    fout.write("\tN\u00ba Of Applied Categories in this document: " + documentAppliedCategories + "\n");
                    int documentDifferentAppliedCategories = talc08.countAllDifferentDocumentAppliedCategories(i + 1);
                    fout.write("\tN\u00ba Of Different Applied Categories in this document: " + documentDifferentAppliedCategories + "\n");
                    HashMap<String, Integer> documentKeywordGroupByCategory = talc08.countAllKeywordGroupByCategoriesr(i + 1);
                    fout.write("+\n");
                    fout.write("\tThis is the list of the N\u00ba Of association category-keyword in this document\n");
                    fout.write("\t------------------------------------------------------------------------------\n");
                    talc08.printKeywordHashMap(documentKeywordGroupByCategory, "\t", fout);
                    HashMap<String, Integer> documentAppliedCategoriesGroupByCategory = talc08.countAppliedCategoriesGrupByCategories(i + 1);
                    fout.write("\n");
                    fout.write("\tThis is the list of the applied categories grouped by categories in this document\n");
                    fout.write("\t------------------------------------------------------------------------------\n");
                    talc08.printKeywordHashMap(documentAppliedCategoriesGroupByCategory, "\t", fout);
                    HashMap<String, Integer> documentWordInwordGroupByCategory = talc08.countAllWordInKeywordGroupByCategories(i + 1);
                    fout.write("\n");
                    fout.write("\tThis is the list of the N\u00ba Of Word in Keyword associated to categorys in this document\n");
                    fout.write("\t------------------------------------------------------------------------------\n");
                    talc08.printWordInKeywordHashMap(documentWordInwordGroupByCategory, "\t", fout);
                    HashMap<String, Double> documentMeanOfWordInKeywordGroupByCategory = talc08.getMean(documentKeywordGroupByCategory, documentWordInwordGroupByCategory);
                    fout.write("\n");
                    fout.write("\tThis is the Average N\u00ba Of Words in Keyword associated to categorys in this document\n");
                    fout.write("\t------------------------------------------------------------------------------\n");
                    talc08.printAverageWordInKeywordHashMap(documentMeanOfWordInKeywordGroupByCategory, "\t", fout);
                    fout.write("\n");
                    int documentNumberOfKeyWords = talc08.countValues(documentKeywordGroupByCategory);
                    fout.write("\tN\u00ba of KeyWords in this document: " + documentNumberOfKeyWords + "\n");
                    int documentNumberOfWordsinKeyWords = talc08.countValues(documentWordInwordGroupByCategory);
                    fout.write("\tN\u00ba of Word in Keyword in this document: " + documentNumberOfWordsinKeyWords + "\n");
                    try {
                        documentAverageNumberOfWordInKeywords = documentNumberOfWordsinKeyWords / documentNumberOfKeyWords;
                    }
                    catch (ArithmeticException ae) {
                        System.out.println("BE CAREFUL -> Document: " + i + " not has keywords!!!!!");
                        documentAverageNumberOfWordInKeywords = 0.0;
                    }
                    fout.write("\tAverage N\u00ba of Word in Keyword in this document: " + documentAverageNumberOfWordInKeywords + "\n");
                    for (int j = 0; j < documentSections; ++j) {
                        double sectionAverageNumberOfWordInKeywords;
                        fout.write("\n");
                        fout.write("\t\tSection Level\n");
                        fout.write("\t\t------------\n");
                        fout.write("\n");
                        fout.write("\t\tProcesing section N\u00ba: " + (j + 1) + " Of document N\u00ba: " + (i + 1) + "\n");
                        System.out.println("\t\tProcesing section N\u00ba: " + (j + 1) + " Of document N\u00ba: " + (i + 1) + "\n");
                        int sectionUtterances = talc08.countAllSectionUterance(i + 1, j + 1);
                        fout.write("\t\tN\u00ba of Utterances in this section: " + sectionUtterances + "\n");
                        int sectionAnchor = talc08.countAllSectionAnchor(i + 1, j + 1);
                        fout.write("\t\tN\u00ba of Keywords in this section: " + sectionAnchor + "\n");
                        int sectionWords = talc08.countAllSectionWords(i + 1, j + 1);
                        fout.write("\t\tN\u00ba of Words in this section: " + sectionWords + "\n");
                        int sectionAppliedCategories = talc08.countAllSectionAppliedCategories(i + 1, j + 1);
                        fout.write("\t\tN\u00ba of Applied Categories in this section: " + sectionAppliedCategories + "\n");
                        HashMap<String, Integer> sectionKeywordGroupByCategory = talc08.countAllKeywordGroupByCategories(i + 1, j + 1);
                        fout.write("\n");
                        fout.write("\t\tThis is the list of the N\u00ba Of association category-keyword in this section\n");
                        fout.write("\t\t------------------------------------------------------------------------------\n");
                        talc08.printKeywordHashMap(sectionKeywordGroupByCategory, "\t\t", fout);
                        HashMap<String, Integer> sectionWordInwordGroupByCategory = talc08.countAllWordInKeywordGroupByCategories(i + 1, j + 1);
                        fout.write("\n");
                        fout.write("\t\tThis is the list of the N\u00ba Of Word in Keyword associated to categorys in this section\n");
                        fout.write("\t\t------------------------------------------------------------------------------\n");
                        talc08.printWordInKeywordHashMap(sectionWordInwordGroupByCategory, "\t\t", fout);
                        fout.write("\n");
                        fout.write("\t\tThis is the Average N\u00ba Of Words in Keyword associated to categorys in this section\n");
                        fout.write("\t\t------------------------------------------------------------------------------\n");
                        HashMap<String, Double> sectionMeanOfWordInKeywordGroupByCategory = talc08.getMean(sectionKeywordGroupByCategory, sectionWordInwordGroupByCategory);
                        talc08.printAverageWordInKeywordHashMap(sectionMeanOfWordInKeywordGroupByCategory, "\t\t", fout);
                        fout.write("\n");
                        int sectionNumberOfKeyWords = talc08.countValues(sectionKeywordGroupByCategory);
                        fout.write("\t\tN\u00ba of KeyWords in this section: " + sectionNumberOfKeyWords + "\n");
                        int sectionNumberOfWordsinKeyWords = talc08.countValues(sectionWordInwordGroupByCategory);
                        fout.write("\t\tN\u00ba of Word in Keyword in this section: " + sectionNumberOfWordsinKeyWords + "\n");
                        try {
                            sectionAverageNumberOfWordInKeywords = sectionNumberOfWordsinKeyWords / sectionNumberOfKeyWords;
                        }
                        catch (ArithmeticException ae) {
                            System.out.println("BE CAREFUL -> Section: " + j + " in the Document: " + i + " not has keywords!!!!!");
                            sectionAverageNumberOfWordInKeywords = 0.0;
                        }
                        fout.write("\t\tAverage N\u00ba of Word in Keyword in this section: " + sectionAverageNumberOfWordInKeywords + "\n");
                    }
                }
            }
            catch (XPathExpressionException e) {
                e.printStackTrace();
            }
            fout.flush();
            fout.close();
        }
    }
}

