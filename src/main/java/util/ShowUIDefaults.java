/*
 * Decompiled with CFR 0_118.
 */
package util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.IconUIResource;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

public class ShowUIDefaults
extends JFrame
implements ActionListener {
    JFrame frame;
    JTabbedPane tabbedPane;
    JButton metal;
    JButton windows;
    JButton motif;
    SampleRenderer sampleRenderer;

    public ShowUIDefaults(String title) {
        super(title);
        this.frame = this;
        this.getContentPane().setLayout(new BorderLayout());
        this.tabbedPane = this.getTabbedPane();
        this.getContentPane().add(this.tabbedPane);
        JPanel buttons = new JPanel();
        buttons.setLayout(new GridLayout(1, 3));
        this.getContentPane().add((Component)buttons, "South");
        this.metal = new JButton("Metal");
        this.metal.addActionListener(this);
        buttons.add(this.metal);
        this.windows = new JButton("Windows");
        this.windows.addActionListener(this);
        buttons.add(this.windows);
        this.motif = new JButton("Motif");
        this.motif.addActionListener(this);
        buttons.add(this.motif);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String laf = "";
        Object o = e.getSource();
        if (o == this.metal) {
            laf = "javax.swing.plaf.metal.MetalLookAndFeel";
        } else if (o == this.windows) {
            laf = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
        } else if (o == this.motif) {
            laf = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
        }
        try {
            UIManager.setLookAndFeel(laf);
        }
        catch (Exception e2) {
            System.out.println(e2);
        }
        this.getContentPane().remove(this.tabbedPane);
        this.tabbedPane = this.getTabbedPane();
        this.getContentPane().add(this.tabbedPane);
        SwingUtilities.updateComponentTreeUI(this.frame);
        this.frame.pack();
    }

    private JTabbedPane getTabbedPane() {
        TreeMap components = new TreeMap();
        UIDefaults defaults = UIManager.getDefaults();
        Enumeration en = defaults.keys();
        while (en.hasMoreElements()) {
            Object key = en.nextElement();
            Object value = defaults.get(key);
            Map componentMap = this.getComponentMap(components, key.toString());
            if (componentMap == null) continue;
            componentMap.put(key, value);
        }
        JTabbedPane pane = new JTabbedPane(3);
        pane.setPreferredSize(new Dimension(800, 400));
        this.addComponentTabs(pane, components);
        return pane;
    }

    private Map getComponentMap(Map components, String key) {
        if (key.startsWith("class") | key.startsWith("javax")) {
            return null;
        }
        int pos = key.indexOf(".");
        String componentName = pos == -1 ? (key.endsWith("UI") ? key.substring(0, key.length() - 2) : "System Colors") : key.substring(0, pos);
        Object componentMap = components.get(componentName);
        if (componentMap == null) {
            componentMap = new TreeMap();
            components.put(componentName, componentMap);
        }
        return (Map)componentMap;
    }

    private void addComponentTabs(JTabbedPane pane, Map components) {
        this.sampleRenderer = new SampleRenderer();
        String[] colName = new String[]{"Key", "Value", "Sample"};
        Set<String> c = components.keySet();
        for (String component : c) {
            Map attributes = (Map)components.get(component);
            Object[][] rowData = new Object[attributes.size()][3];
            int n = 0;
            Set<String> a = attributes.keySet();
            for (String attribute : a) {
                rowData[n][0] = attribute;
                Object o = attributes.get(attribute);
                if (o != null) {
                    rowData[n][1] = o.toString();
                    rowData[n][2] = "";
                    if (o instanceof Font) {
                        rowData[n][2] = (Font)o;
                    }
                    if (o instanceof Color) {
                        rowData[n][2] = (Color)o;
                    }
                    if (o instanceof IconUIResource) {
                        rowData[n][2] = (Icon)o;
                    }
                } else {
                    rowData[n][1] = "";
                    rowData[n][2] = "";
                }
                ++n;
            }
            MyTableModel myModel = new MyTableModel(rowData, colName);
            JTable table = new JTable(myModel);
            table.setDefaultRenderer(this.sampleRenderer.getClass(), this.sampleRenderer);
            table.getColumnModel().getColumn(0).setPreferredWidth(250);
            table.getColumnModel().getColumn(1).setPreferredWidth(500);
            table.getColumnModel().getColumn(2).setPreferredWidth(50);
            pane.addTab(component, new JScrollPane(table));
        }
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e) {
            // empty catch block
        }
        ShowUIDefaults f = new ShowUIDefaults("UI Defaults");
        f.setDefaultCloseOperation(3);
        f.pack();
        f.setVisible(true);
    }

    class SampleRenderer
    extends JLabel
    implements TableCellRenderer {
        public SampleRenderer() {
            this.setHorizontalAlignment(0);
            this.setOpaque(true);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object sample, boolean isSelected, boolean hasFocus, int row, int column) {
            this.setBackground(null);
            this.setIcon(null);
            this.setText("");
            if (sample instanceof Color) {
                this.setBackground((Color)sample);
            }
            if (sample instanceof Font) {
                this.setText("Sample");
                this.setFont((Font)sample);
            }
            if (sample instanceof Icon) {
                this.setIcon((Icon)sample);
            }
            return this;
        }
    }

    class MyTableModel
    extends AbstractTableModel {
        private String[] columnNames;
        private Object[][] rowData;

        public MyTableModel(Object[][] rowData, String[] columnNames) {
            this.rowData = rowData;
            this.columnNames = columnNames;
        }

        @Override
        public int getColumnCount() {
            return this.columnNames.length;
        }

        @Override
        public int getRowCount() {
            return this.rowData.length;
        }

        @Override
        public String getColumnName(int col) {
            return this.columnNames[col];
        }

        @Override
        public Object getValueAt(int row, int col) {
            return this.rowData[row][col];
        }

        public Class getColumnClass(int c) {
            SampleRenderer o = c == 2 ? ShowUIDefaults.this.sampleRenderer : (SampleRenderer) this.getValueAt(0, c);
            return o.getClass();
        }

        @Override
        public void setValueAt(Object value, int row, int col) {
            this.rowData[row][col] = value;
            this.fireTableCellUpdated(row, col);
        }
    }

}

