/*
 * Decompiled with CFR 0_118.
 */
package util.convert;

import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import annotador.utils.TEINamespaceContext;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import util.convert.ConvertData;

public class TimeStampingFix {
    HashMap<Integer, String> idFile;
    HashMap<Integer, String> ids;

    public TimeStampingFix(String corpusPath, String transcriptionDir) throws Exception, IOException {
        File[] trancriptionFiles = this.getTranscriptionFiles(transcriptionDir);
        DocumentSingleton.loadFromFile(corpusPath);
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new TEINamespaceContext());
        NodeList interviews = (NodeList)xpath.evaluate("//tei:TEI", DocumentSingleton.getInstance(), XPathConstants.NODESET);
        System.out.println("Corpus Interviews: " + interviews.getLength());
        System.out.println("Transcription Files: " + trancriptionFiles.length);
        this.idFile = new HashMap();
        this.ids = new HashMap();
        boolean error = false;
        for (int i = 0; i < interviews.getLength(); ++i) {
            Attr at = (Attr)xpath.evaluate("//tei:TEI[" + (i + 1) + "]/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:recordingStmt/tei:recording/@rend", DocumentSingleton.getInstance(), XPathConstants.NODE);
            if (at != null) {
                String id = at.getValue();
                String file = this.getTranscriptionFile(trancriptionFiles, id);
                if (file != null) {
                    this.idFile.put(i + 1, file);
                    this.ids.put(i + 1, id);
                    continue;
                }
                error = true;
                System.out.println("This ID:" + id + " has not associated to any transcription file");
                continue;
            }
            System.out.println("The are no MediaFile in the interview with TEI index = " + (i + 1));
        }
        if (error) {
            // empty if block
        }
        for (Integer index : this.idFile.keySet()) {
            String interview = this.idFile.get(index);
            ArrayList<String> us = this.separeUs(interview, this.ids.get(index));
            NodeList utterances = (NodeList)xpath.evaluate("//tei:TEI[" + index + "]/tei:text/tei:body//tei:u", DocumentSingleton.getInstance(), XPathConstants.NODESET);
            if (us.size() == utterances.getLength()) {
                for (int i2 = 0; i2 < us.size(); ++i2) {
                    ArrayList<ConvertData> times = this.parseTurn(us.get(i2));
                    NodeList timesTag = ((Element)utterances.item(i2)).getElementsByTagName("time");
                    if (timesTag.getLength() == times.size()) {
                        for (int j = 0; j < times.size(); ++j) {
                            ConvertData data = times.get(j);
                            Element time = (Element)timesTag.item(j);
                            if (time.getAttribute("from").equals(data.getOriginalStart()) && time.getAttribute("to").equals(data.getOriginalEnd())) {
                                int fromI;
                                int toI;
                                String from = data.getNewStart();
                                String to = data.getNewEnd();
                                if (!time.getAttribute("from").equals(from)) {
                                    time.setAttribute("from", from);
                                }
                                if (!time.getAttribute("to").equals(to)) {
                                    time.setAttribute("to", to);
                                }
                                if ((fromI = Integer.parseInt(from)) <= (toI = Integer.parseInt(to))) continue;
                                time.setAttribute("from", to);
                                time.setAttribute("to", from);
                                continue;
                            }
                            System.out.println("ERROR Alignement incorrect in timeStamping " + time.getAttribute("from") + " <> " + data.getOriginalStart() + " or " + time.getAttribute("to") + " <> " + data.getOriginalEnd());
                        }
                        continue;
                    }
                    if (timesTag.getLength() == 0 && times.size() == 1) {
                        Element u = (Element)utterances.item(i2);
                        Element time = u.getOwnerDocument().createElement("time");
                        ConvertData d = times.get(0);
                        time.setAttribute("from", d.getNewStart());
                        time.setAttribute("to", d.getNewEnd());
                        u.insertBefore(time, u.getFirstChild());
                        continue;
                    }
                    System.out.println("Error version missmatch. The Trasncription with ID: " + this.ids.get(index) + " and the corpus no has the same number of times");
                    System.out.println("SIZE T=" + times.size() + " in U:" + i2);
                    System.out.println("SIZE C=" + timesTag.getLength() + " in U:" + i2);
                }
                continue;
            }
            System.out.println("Error version missmatch. The Trasncription with ID: " + this.ids.get(index) + " and the corpus no has the same number of utterances");
            System.out.println("SIZE T=" + us.size());
            System.out.println("SIZE C=" + utterances.getLength());
        }
        this.saveCorpus(corpusPath);
    }

    public void saveCorpus(String corpusName) throws Exception {
        corpusName = corpusName.substring(0, corpusName.length() - 4);
        corpusName = corpusName + "-Modification.xml";
        FileOutputStream fw = new FileOutputStream(corpusName, false);
        OutputStreamWriter osw = new OutputStreamWriter((OutputStream)fw, Constants.ENCODING);
        BufferedWriter writer = new BufferedWriter(osw);
        String xml = DOMUtils.DOMToString(DocumentSingleton.getInstance(), Constants.ENCODING);
        writer.write(xml);
        writer.flush();
        osw.flush();
        fw.flush();
        writer.close();
        osw.close();
        fw.close();
    }

    public String changeTimeFormat(String time) {
        String aux = time;
        if (aux.contains(",") || aux.contains(".")) {
            String decimal = aux.substring(Math.max(aux.lastIndexOf(",") + 1, aux.lastIndexOf(".") + 1));
            if (decimal.length() < 2) {
                aux = aux + "0";
            }
        } else {
            aux = aux + ",00";
        }
        aux = aux.replaceAll(",", "").replaceAll("\\.", "");
        while (aux.length() < 4) {
            aux = "0" + aux;
        }
        if (aux.equals("0000")) {
            aux = "0001";
        }
        return aux;
    }

    public ArrayList<ConvertData> parseTurn(String turn) {
        ArrayList<ConvertData> aux = new ArrayList<ConvertData>();
        String first = turn.substring(0, turn.indexOf(58) + 1);
        String text = turn.substring(turn.indexOf(58) + 1);
        Pattern timeStamping = Pattern.compile("\\[[\\p{Digit}|,|\\.]*\\]");
        Matcher m = timeStamping.matcher(first);
        ArrayList<String> times = new ArrayList<String>();
        ArrayList<String> original = new ArrayList<String>();
        while (m.find()) {
            String time = first.substring(m.start(), m.end());
            time = time.substring(1, time.length() - 1);
            original.add(time);
            time = time.replaceAll(",", "");
            while (time.length() < 4) {
                time = "0" + time;
            }
            if (time.equals("0000")) {
                time = "0001";
            }
            times.add(time);
        }
        String endEnd = null;
        String originalEnd = null;
        if (times.size() >= 2) {
            endEnd = (String)times.remove(1);
            originalEnd = (String)original.remove(1);
        }
        m = Pattern.compile("\\[[\\p{Digit}|,|\\.]*\\]").matcher(text);
        while (m.find()) {
            String time = text.substring(m.start(), m.end());
            time = time.substring(1, time.length() - 1);
            original.add(time);
            time = time.replaceAll(",", "");
            while (time.length() < 4) {
                time = "0" + time;
            }
            if (time.equals("0000")) {
                time = "0001";
            }
            times.add(time);
        }
        if (endEnd != null) {
            times.add(endEnd);
            original.add(originalEnd);
        }
        if (times.size() > 2) {
            System.out.println("YES, >2 TIMES");
        }
        m = Pattern.compile("\\[[\\p{Digit}|,|\\.]*\\]").matcher(text);
        for (int index = 0; index < times.size() - 1; ++index) {
            String startTime = (String)times.get(index);
            String endTime = (String)times.get(index + 1);
            String originalStartTime = (String)original.get(index);
            String originalEndTime = (String)original.get(index + 1);
            ConvertData cd = new ConvertData();
            cd.setNewEnd(this.changeTimeFormat(originalEndTime));
            cd.setNewStart(this.changeTimeFormat(originalStartTime));
            cd.setOriginalEnd(endTime);
            cd.setOriginalStart(startTime);
            aux.add(cd);
        }
        return aux;
    }

    public ArrayList<String> separeUs(String interview, String id) {
        ArrayList<String> us = new ArrayList<String>();
        String body = null;
        Pattern bodyP = Pattern.compile("\\[BODY\\].*\\[/BODY]", 106);
        Matcher match = bodyP.matcher(interview);
        if (match.find()) {
            body = interview.substring(match.start(), match.end());
        } else {
            System.out.println("The interview with ID: " + id + " no has the correct format (NO BODY TAGS)");
            System.exit(1);
        }
        int start = -1;
        ArrayList<String> sections = new ArrayList<String>();
        for (int i = 0; i < body.length(); ++i) {
            if (body.charAt(i) != '#') continue;
            if (start != -1) {
                sections.add(body.substring(start + 1, i));
            }
            start = i;
        }
        for (String section : sections) {
            int i2;
            int numberOfTurn = 0;
            ArrayList<Integer> indexs = new ArrayList<Integer>();
//            for (i2 = 0; i2 < section.length(); ++i2) {
//                if (section.charAt(i2) != ':') continue;
//                ++numberOfTurn;
//                indexs.add(i2);
//            }
            Matcher matcher = Pattern.compile("\\[\\d*,..\\]\\[\\d*,..\\]").matcher(section);
            while(matcher.find()) {
                int index = matcher.start();
                indexs.add(index);
                ++numberOfTurn;
            }
            start = -1;
            for (i2 = 0; i2 < numberOfTurn; ++i2) {
                int aux = this.getIndexOfBeforeReturn(section, (Integer)indexs.get(i2));
                if (start != -1 && aux > start) {
                    us.add(section.substring(start + 1, aux));
                }
                start = aux;
            }
            us.add(section.substring(start + 1));
        }
        return us;
    }

    public int getIndexOfBeforeReturn(String section, int colon) {
        boolean found = false;
        for (int i = colon; i >= 0 && !found; --i) {
            if (section.charAt(i) != '\n') continue;
            return i;
        }
        return 0;
    }

    private String getTranscriptionFile(File[] transcriptionFiles, String media) throws IOException {
        for (File actual : transcriptionFiles) {
            FileInputStream fr = new FileInputStream(actual);
            InputStreamReader isr = new InputStreamReader((InputStream)fr, "UTF-16");
            StringBuilder sb = new StringBuilder();
            int aux = 0;
            while ((aux = isr.read()) != -1) {
                sb.append((char)aux);
            }
            String file = sb.toString();
            Matcher m = Pattern.compile("MediaFileName:.*").matcher(file);
            if (m.find()) {
                String mediaFileName = file.substring(m.start(), m.end());
                if (!(mediaFileName = mediaFileName.substring(14).trim()).toUpperCase().startsWith(media.toUpperCase())) continue;
                return file;
            }
            System.out.println("File :" + actual.getAbsolutePath() + " NOT HAS MediaFileName field:");
        }
        return null;
    }

    private File[] getTranscriptionFiles(String transcriptionDir) {
        File dir = new File(transcriptionDir);
        File[] files = dir.isDirectory() ? dir.listFiles(new FilenameFilter(){

            @Override
            public boolean accept(File dir, String name) {
                if (name.toUpperCase().endsWith(".TXT")) {
                    return true;
                }
                return false;
            }
        }) : new File[]{dir};
        return files;
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Syntax error. User java TimeStampingFix [corpus][transcription directory]");
            System.exit(1);
        }
        try {
            new TimeStampingFix(args[0], args[1]);
        }
        catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}

