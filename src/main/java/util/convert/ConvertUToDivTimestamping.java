/*
 * Decompiled with CFR 0_118.
 */
package util.convert;

import annotador.utils.Constants;
import annotador.utils.DOMUtils;
import annotador.utils.DocumentSingleton;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ConvertUToDivTimestamping {
    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            System.out.println("Error. The path of the corpus file must be passed as parameter");
            System.exit(1);
        }
        DocumentSingleton.loadFromFile(args[0]);
        Document doc = DocumentSingleton.getInstance();
        NodeList times = doc.getElementsByTagName("time");
        for (int i = 0; i < times.getLength(); ++i) {
            Element time = (Element)times.item(i);
            String from = time.getAttribute("from");
            String to = time.getAttribute("to");
            if (from.equals("") || to.equals("")) continue;
            Node div = time.getParentNode();
            div.removeChild(time);
            NodeList us = div.getChildNodes();
            Node fristU = null;
            Element lastU = null;
            int uNumber = 0;
            for (int j = 0; j < us.getLength(); ++j) {
                Node node = us.item(j);
                if (!(node instanceof Element) || !node.getNodeName().equals("u")) continue;
                if (++uNumber == 1) {
                    fristU = (Element)node;
                }
                lastU = (Element)node;
            }
            if (uNumber == 1 && fristU != null) {
                fristU.insertBefore(time, fristU.getFirstChild());
            }
            if (uNumber <= 1 || fristU == null || lastU == null) continue;
            Element newTime = doc.createElement("time");
            newTime.setAttribute("from", from);
            fristU.insertBefore(newTime, fristU.getFirstChild());
            Element newTime2 = doc.createElement("time");
            newTime2.setAttribute("to", to);
            lastU.insertBefore(newTime2, lastU.getFirstChild());
        }
        doc.normalizeDocument();
        FileOutputStream fw = new FileOutputStream("out.xml", false);
        OutputStreamWriter osw = new OutputStreamWriter((OutputStream)fw, Constants.ENCODING);
        BufferedWriter writer = new BufferedWriter(osw);
        String xml = DOMUtils.DOMToString(doc, Constants.ENCODING);
        System.out.println(xml);
        writer.write(xml);
        writer.flush();
        osw.flush();
        fw.flush();
        writer.close();
        osw.close();
        fw.close();
    }
}

