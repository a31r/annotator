/*
 * Decompiled with CFR 0_118.
 */
package util.convert;

public class ConvertData {
    String originalStart;
    String originalEnd;
    String newStart;
    String newEnd;

    public String getOriginalStart() {
        return this.originalStart;
    }

    public void setOriginalStart(String originalStart) {
        this.originalStart = originalStart;
    }

    public String getOriginalEnd() {
        return this.originalEnd;
    }

    public void setOriginalEnd(String originalEnd) {
        this.originalEnd = originalEnd;
    }

    public String getNewStart() {
        return this.newStart;
    }

    public void setNewStart(String newStart) {
        this.newStart = newStart;
    }

    public String getNewEnd() {
        return this.newEnd;
    }

    public void setNewEnd(String newEnd) {
        this.newEnd = newEnd;
    }
}

