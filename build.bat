@echo ============================================================================================
@echo Adding XPLookAndFeel.jar
@echo ============================================================================================
call %CD%/apache-maven/bin/mvn install:install-file -Dfile=%CD%/libs/xplookandfeel-jose.jar -DgroupId=com.a31r -DartifactId=xplookandfeel -Dversion=1.0 -Dpackaging=jar
@echo Done. XPLookAndFeel.jar added
@echo ============================================================================================
@echo Adding fobs4jmf.jar
@echo ============================================================================================
call %CD%/apache-maven/bin/mvn install:install-file -Dfile=%CD%/libs/fobs4jmf.jar -DgroupId=com.a31r -DartifactId=fobs4jmf -Dversion=1.0 -Dpackaging=jar
@echo Done. fobs4jmf.jar added
@echo ============================================================================================
@echo Adding jmf.jar
@echo ============================================================================================
call %CD%/apache-maven/bin/mvn install:install-file -Dfile=%CD%/libs/jmf.jar -DgroupId=com.a31r -DartifactId=jmf -Dversion=1.0 -Dpackaging=jar
@echo Done. jmf.jar added
@echo ============================================================================================
@echo Adding mp3plugin.jar
@echo ============================================================================================
call %CD%/apache-maven/bin/mvn install:install-file -Dfile=%CD%/libs/mp3plugin.jar -DgroupId=com.a31r -DartifactId=mp3plugin -Dversion=1.0 -Dpackaging=jar
@echo Done. mp3plugin.jar added
@echo ============================================================================================
@echo Clean project from old version
@echo ============================================================================================
call %CD%/apache-maven/bin/mvn clean
@echo ============================================================================================
@echo Build project
@echo ============================================================================================
call %CD%/apache-maven/bin/mvn package
if not exist "%CD%\.annotator\" mkdir %CD%\.annotator
copy "%CD%\target\annotator-1.0-SNAPSHOT-jar-with-dependencies.jar" "%CD%\.annotator\annotator.jar"
%SystemRoot%\explorer.exe "%CD%\.annotator\"